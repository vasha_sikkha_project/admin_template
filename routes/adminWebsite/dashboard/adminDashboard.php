<?php

Auth::routes();

Route::middleware(['admin'])->group(function () {

    /** show
     *
     */
    Route::get('/admin/dashboard', 'ADMIN_WEB\DashboardController@getDashboard')->name('show-all-dashboard-data');


    Route::get('/admin/chart', 'ADMIN_WEB\DashboardController@getChart')->name('show-all-chart-data');

    Route:: get('/admin/chart/userdata', 'ADMIN_WEB\UserController@getAdminNumber_created_at');

    Route:: get('/admin/chart/studentdata', 'ADMIN_WEB\UserController@getStudentNumber_created_at');

    Route:: get('/admin/chart/teacherdata', 'ADMIN_WEB\UserController@getTeacherNumber_created_at');

    Route:: get('/admin/chart/mcqperformancedata', 'ADMIN_WEB\UserController@getuserperformance');

    Route:: get('/admin/chart/mcqfailuredata', 'ADMIN_WEB\UserController@getuserfailure');

    Route:: get('/admin/chart/fillintheblanksperformancedata', 'ADMIN_WEB\UserController@getuserperformance');

    Route:: get('/admin/chart/fillintheblanksfailuredata', 'ADMIN_WEB\UserController@getuserfailure');

    Route:: get('/admin/chart/vocabularyperformancedata', 'ADMIN_WEB\UserController@getuserperformance');

    Route:: get('/admin/chart/vocabularyfailuredata', 'ADMIN_WEB\UserController@getuserfailure');

    Route:: get('/admin/chart/SynonymAntonymperformancedata', 'ADMIN_WEB\UserController@getuserperformance');

    Route:: get('/admin/chart/SynonymAntonymfailuredata', 'ADMIN_WEB\UserController@getuserfailure');

    Route:: get('/admin/chart/taskperformancedata', 'ADMIN_WEB\UserController@gettaskwiseuserperformance');

    Route:: get('/admin/chart/taskfailuredata', 'ADMIN_WEB\UserController@gettaskwiseuserfailure');




    // Route:: get('/admin/chart/userdata1')

    

    /**
     * update - no need. because this view cosists of other modular views.
     */

    /**
     * delete - no need. because this view cosists of other modular views.
     */

});

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */


    

    /**
     * update - no need. because this view cosists of other modular views.
     */

    /**
     * delete - no need. because this view cosists of other modular views.
     */

});
