<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/fill_in_the_blanks/{id}', 'ADMIN_WEB\FillInTheBlanksController@getLimitedFillInTheBlanksWithUI')->name('show-fill-blank-sentence');

    Route::get('/sentence/fill_in_the_blanksQuiz/{id}', 'ADMIN_WEB\FillInTheBlanksController@getLimitedFillInTheBlanksWithUIQuiz')->name('show-fill-blank-sentence-quiz');

    /**
     * update
     */
    Route::post('/sentence/fill_in_the_blanks/update/{id}', 'ADMIN_WEB\FillInTheBlanksController@update')->name('update-fill-blank-sentence');

    /**
     * delete
     */
    Route::delete('/sentence/fill_in_the_blanks/delete/{id}', 'ADMIN_WEB\FillInTheBlanksController@delete')->name('delete-fillblank-sentence-entry');

});
