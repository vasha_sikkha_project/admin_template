<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/translation/{id}', 'ADMIN_WEB\TranslationController@getLimitedTranslationWithUI')->name('show-translations');

    /**
     * update
     */

    Route::post('/sentence/translation/update/{id}', 'ADMIN_WEB\TranslationController@update')->name('update-sentence-translation');

    /**
     * delete
     */
    Route::delete('/sentence/translation/delete/{id}', 'ADMIN_WEB\TranslationController@deleteTranslationEntry')->name('delete-sentence-translation');

});
