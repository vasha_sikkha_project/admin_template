<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/sentence_matching/{id}', 'ADMIN_WEB\SentenceMatchingController@getLimitedSentenceMatchingWithUI')->name('show-sentence-matching');
    Route::get('/sentence/sentence_matchingQuiz/{id}', 'ADMIN_WEB\SentenceMatchingController@getLimitedSentenceMatchingWithUIQuiz')->name('show-sentence-matching-quiz');

    /**
     * update
     */

    Route::post('/sentence/sentence_matching/update/{id}', 'ADMIN_WEB\SentenceMatchingController@update')->name('update-sentence-matching');

    /**
     * delete
     */
    Route::delete('/sentence/sentence_matching/delete/{id}', 'ADMIN_WEB\SentenceMatchingController@delete')->name('delete-sentence-matching');

});
