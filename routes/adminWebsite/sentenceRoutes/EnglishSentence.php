<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/english-sentences/{id}', 'ADMIN_WEB\EnglishSentencesController@getLimitedEnglishSentencesWithUI')->name('show-english-sentence');

    /**
     * update
     */
    Route::post('/sentence/english_sentences/update/{id}', 'ADMIN_WEB\EnglishSentencesController@update')->name('update-english-sentences');

    /**
     * delete
     */
    Route::delete('/sentence/english_sentences/delete/{id}', 'ADMIN_WEB\EnglishSentencesController@deleteEnglishSentence')->name('delete-english-sentence');

});
