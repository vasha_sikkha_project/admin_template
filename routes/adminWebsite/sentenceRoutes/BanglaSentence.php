<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/bangla-sentences/{id}', 'ADMIN_WEB\BanglaSentencesController@getLimitedBanglaSentencesWithUI')->name('show-bangla-sentence');

    /**
     * update
     */
    Route::post('/sentence/bangla_sentences/update/{id}', 'ADMIN_WEB\BanglaSentencesController@update')->name('update-bangla-sentences');

    /**
     * delete
     */
    Route::delete('/sentence/bangla_sentences/delete/{id}', 'ADMIN_WEB\BanglaSentencesController@deleteBanglaSentence')->name('delete-bangla-sentence');

});
