<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence', 'ADMIN_WEB\SentenceTaskController@getData')->name('show-all-sentence');

    /**
     * update - no need. because this view cosists of other modular views.
     */

    /**
     * delete - no need. because this view cosists of other modular views.
     */

});
