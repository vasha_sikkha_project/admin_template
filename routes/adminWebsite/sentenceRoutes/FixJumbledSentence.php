<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/sentence/jumbled_sentence/{id}', 'ADMIN_WEB\FixJumbledSentencesController@getfixJumbledSentencesWithUI')->name('show-fixed-jumble');
    Route::get('/sentence/jumbled_sentenceQuiz/{id}', 'ADMIN_WEB\FixJumbledSentencesController@getfixJumbledSentencesWithUIQuiz')->name('show-fixed-jumble-quiz');

    /**
     * update
     */

    Route::post('/sentence/jumbled_sentence/update/{id}', 'ADMIN_WEB\FixJumbledSentencesController@update')->name('update-fixed-jumble');

    /**
     * delete
     */
    Route::delete('/sentence/jumbled_sentence/delete/{id}', 'ADMIN_WEB\FixJumbledSentencesController@delete')->name('delete-fixjumble-sentence');

});
