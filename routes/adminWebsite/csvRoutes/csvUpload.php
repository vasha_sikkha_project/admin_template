<?php

Auth::routes();


/**
 *
 *
 *          CSV FILE UPLOAD WORKS
 *
 */

Route::middleware(['teacher'])->group(function () {


    /**
     *
     *
     * Sample for Sunny:
     * URL sample: /sentence/fillintheblank/upload instead of /uploadfillintheblanksentence
     *
     */


    Route::get('/uploadfile', 'ADMIN_WEB\UploadFileController@index');
    Route::post('/uploadfile', 'ADMIN_WEB\UploadFileController@showUploadFile');

    // upload sample csv


    Route::get('/uploadsamplecsv', 'ADMIN_WEB\csvController\sampleCSVuploadercontroller@index')->name('upload-csv');
    Route::post('/uploadsamplecsv', 'ADMIN_WEB\csvController\sampleCSVuploadercontroller@showUploadFile')->name("modify-sample");

    // upload fill in the blank csv

    Route::get('/uploadfillintheblank', 'ADMIN_WEB\csvController\csvForFillInTheBlankController@index')->name("Instruction-for-fill-in-the-blank");
    Route::post('/uploadfillintheblank', 'ADMIN_WEB\csvController\csvForFillInTheBlankController@showUploadFile')->name("Uplaod-fill-in-the-blank");

    // upload MCQ

    Route::get('/uploadMCQ', 'ADMIN_WEB\csvController\csvForMCQController@index')->name("Instruction-for-MCQ");
    Route::post('/uploadMCQ', 'ADMIN_WEB\csvController\csvForMCQController@showUploadFile')->name("Upload-mcq");;

    // upload True False

    Route::get('/uploadTrueFalse', 'ADMIN_WEB\csvController\csvForTrueFalseController@index')->name("Instruction-for-true-false");;
    Route::post('/uploadTrueFalse', 'ADMIN_WEB\csvController\csvForTrueFalseController@showUploadFile')->name("Upload-True-false");

    // upload Jumbled sentence

    Route::get('/uploadJSentence', 'ADMIN_WEB\csvController\csvForJSentenceController@index')->name("Instruction-for-jumble-sentence");;
    Route::post('/uploadJSentence', 'ADMIN_WEB\csvController\csvForJSentenceController@showUploadFile')->name("Upload-JSentence");

    // upload  sentence Matching

    Route::get('/uploadSMatching', 'ADMIN_WEB\csvController\csvForSMatchingController@index')->name("Instruction-for-sentence-matching");;
    Route::post('/uploadSMatching', 'ADMIN_WEB\csvController\csvForSMatchingController@showUploadFile')->name("Upload-SMatching");

    // upload synonyms and antonyms

    Route::get('/uploadsynantonym', 'ADMIN_WEB\csvController\csvForSynAntonyms@index')->name("Instruction-for-Synonyms and antonyms");
    Route::post('/uploadsynantonym', 'ADMIN_WEB\csvController\csvForSynAntonyms@showUploadFile')->name("Upload-synantonyms");;

    // upload Image matching

    Route::get('/uploadimage', 'ADMIN_WEB\csvController\csvForImageController@index')->name("Instruction-for-Image-Matching");
    Route::post('/uploadimage', 'ADMIN_WEB\csvController\csvForImageController@showUploadFile')->name("Upload-Image-Matching");;

    // upload Word Picture

    Route::get('/uploadwordpicture', 'ADMIN_WEB\csvController\csvForWordPicture@index')->name("Instruction-for-word-picture");
    Route::post('/uploadwordpicture', 'ADMIN_WEB\csvController\csvForWordPicture@showUploadFile')->name("Upload-Image-wordp-icture");;

    //download sample files

    Route::get('/downloadsamplefilleintheblank', 'ADMIN_WEB\FileDownloadController@fillintheblank')->name("Download-Fill-in-the-blank-sample");;
    Route::get('/downloadsampleMCQ', 'ADMIN_WEB\FileDownloadController@MCQ')->name("Download-MCQ-sample");;
    Route::get('/downloadsampleTrueFalse', 'ADMIN_WEB\FileDownloadController@TrueFalse')->name("Download-True-false-sample");;
    Route::get('/downloadsampleJSentence', 'ADMIN_WEB\FileDownloadController@JSentence')->name("Download-JSentence-sample");;
    Route::get('/downloadsampleSMatching', 'ADMIN_WEB\FileDownloadController@SMatching')->name("Download-SMatching-sample");;
    Route::get('/downloadsampleSynAntonyms', 'ADMIN_WEB\FileDownloadController@SynAntonyms')->name("Download-SynAntonyms-sample");;
    Route::get('/downloadsampleImage', 'ADMIN_WEB\FileDownloadController@ImageMatching')->name("Download-ImageMatching-sample");;
    Route::get('/downloadsampleWordPicture', 'ADMIN_WEB\FileDownloadController@WordPicture')->name("Download-WordPicture-sample");;

});


