<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/profile', 'AdminProfileController@index')->name('show-profile');

    /**
     * update
     */
    Route::post('/user/profile/update/user/{id}', 'ADMIN_WEB\UserController@updateYourProfile')->name('update-your-profile');

    /**
     * delete
     */



});
