<?php

Auth::routes();

Route::middleware(['admin'])->group(function () {

    /** show
     *
     */
    Route::get('/user/{id}', 'ADMIN_WEB\UserController@getUserData')->name('show-all-user');
    Route::get('/user/admins/{id}', 'ADMIN_WEB\UserController@getAdminsDataWihUI')->name('show-all-admins');
    Route::get('/user/teachers/{id}', 'ADMIN_WEB\UserController@getTeacherDataWithUI')->name('show-all-teachers');
    Route::get('/user/students/{id}', 'ADMIN_WEB\UserController@getStudentDataWithUI')->name('show-all-students');
    Route::get('/user/new/{id}', 'ADMIN_WEB\UserController@getNewDataWithUI')->name('show-new-teachers');

    /**
     * update
     */
    Route::post('/user/allUser/update/{id}', 'ADMIN_WEB\UserController@update')->name('update-user-data');


    /**
     * delete
     */

    Route::delete('/user/allUser/delete/{id}', 'ADMIN_WEB\UserController@delete')->name('delete-user-data');


});
