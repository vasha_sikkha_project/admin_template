<?php

Auth::routes();

Route::middleware(['admin'])->group(function () {

    /** show
     *
     */
    //new topic addition
    Route::get('/addTopic', 'ADMIN_WEB\csvController\addTopic@index')->name("Add-topic-UI");
    Route::get('/allTopics', 'ADMIN_WEB\TopicController@getAllTopics')->name("show-topic");
    

    /**
     * update - no need. because this view cosists of other modular views.
     */

    Route::post('/addTopic', 'ADMIN_WEB\csvController\addTopic@addNewTopic')->name("Add-new-topic");
    /**
     * delete - no need. because this view cosists of other modular views.
     */

    Route::delete('/topics/delete/{id}', 'ADMIN_WEB\TopicController@deleteTopic')->name('delete-topic');

});
