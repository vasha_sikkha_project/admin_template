<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/FillInTheBlanksWordVer/{id}', 'ADMIN_WEB\FillInTheBlanksWordVerController@getLimitedFillInTheBlanksWordVerWithUI')->name('fill-blank-word-ver');
    Route::get('/word/FillInTheBlanksWordVerQuiz/{id}', 'ADMIN_WEB\FillInTheBlanksWordVerController@getLimitedFillInTheBlanksWordVerWithUIQuiz')->name('fill-blank-word-ver-quiz');

    /**
     * update
     */

    Route::post('/word/fill_in_the_blanks/update/{id}', 'ADMIN_WEB\FillInTheBlanksWordVerController@update')->name('update-fill-blank-word-ver');

    /**
     * delete
     */
    Route::delete('/word/fill_in_the_blanks/delete/{id}', 'ADMIN_WEB\FillInTheBlanksWordVerController@deleteWordGap')->name('delete-wordgap-entry');

});
