<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/vocabulary/{id}', 'ADMIN_WEB\VocabularyTaskController@getLimitedVocabularyWithUI')->name('show-vocabulary-all');
    Route::get('/word/vocabularyQuiz/{id}', 'ADMIN_WEB\VocabularyTaskController@getLimitedVocabularyWithUIQuiz')->name('show-vocabulary-all-quiz');

    /**
     * update
     */
    Route::post('/word/vocabulary/update/{dictionaryid}', 'ADMIN_WEB\VocabularyTaskController@update')->name('update-vocabulary');

    /**
     * delete
     */
    Route::delete('/word/vocabulary/delete/{id}', 'ADMIN_WEB\VocabularyTaskController@deleteDictionaryEntry')->name('delete-vocabulary-entry');

});
