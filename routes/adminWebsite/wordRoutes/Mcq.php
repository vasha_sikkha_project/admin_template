<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/Mcq/{id}', 'ADMIN_WEB\MCQController@getLimitedMCQWithUI')->name('show-mcq-all');
    Route::get('/word/McqQuiz/{id}', 'ADMIN_WEB\MCQController@getLimitedMCQWithUIQuiz')->name('show-mcq-all-quiz');

    /**
     * update
     */

    Route::post('/word/Mcq/update/{id}', 'ADMIN_WEB\MCQController@update')->name('update-mcq');

    /**
     * delete
     */
    Route::delete('/word/Mcq/delete/{id}', 'ADMIN_WEB\MCQController@deleteMCQ')->name('delete-mcq-entry');

});