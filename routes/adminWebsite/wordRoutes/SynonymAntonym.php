<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/syn_ant/{id}', 'ADMIN_WEB\SynonymAntonymController@getLimitedSynonymAntonymWithUI')->name('show-synonymAntonym-all');
    Route::get('/word/syn_antQuiz/{id}', 'ADMIN_WEB\SynonymAntonymController@getLimitedSynonymAntonymWithUIQuiz')->name('show-synonymAntonym-all-quiz');


    /**
     * update
     */
    Route::post('/word/syn_ant/update/{id}', 'ADMIN_WEB\SynonymAntonymController@update')->name('update-syn-ant');

    /**
     * delete
     */

});
