<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/dictionary/{id}', 'ADMIN_WEB\DictionaryController@getLimitedDictionaryWithUI')->name('show-dictionary-all');

    /**
     * update
     */
    Route::post('/word/dictionary/update/{dictionaryid}', 'ADMIN_WEB\DictionaryController@update')->name('update-dictionary');

    /**
     * delete
     */
    Route::delete('/word/dictionary/delete/{id}', 'ADMIN_WEB\DictionaryController@deleteDictionaryEntry')->name('delete-dictionary-entry');

});
