<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word', 'ADMIN_WEB\WordTaskController@getData')->name('show-word-all');

    /**
     * update - no need. because this view cosists of other modular views.
     */

    /**
     * delete - no need. because this view cosists of other modular views.
     */

});
