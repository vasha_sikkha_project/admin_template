<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/banglaresources/{id}', 'ADMIN_WEB\BanglaResourcesController@getLimitedBanglaWordsWithUI')->name('show-bangla-words');

    /**
     * update
     */
    Route::post('/word/banglaresources/update/{id}', 'ADMIN_WEB\BanglaResourcesController@update')->name('update-bangla-resources');




    Route::post('/word/banglaresources/insert/{id}', 'ADMIN_WEB\BanglaResourcesController@inserted')->name('insert-bangla');

    /**
     * delete
     */
    Route::delete('/word/banglaresources/delete/{id}', 'ADMIN_WEB\BanglaResourcesController@deleteBanglaWord')->name('delete-bangla-resources');

});
