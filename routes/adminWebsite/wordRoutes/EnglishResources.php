<?php

Auth::routes();

Route::middleware(['teacher'])->group(function () {

    /** show
     *
     */
    Route::get('/word/englishresources/{id}', 'ADMIN_WEB\EnglishResourcesController@getLimitedEnglishWordsWithUI')->name('show-english-words');

    /**
     * update
     */
    Route::post('/word/englishresources/update/{id}', 'ADMIN_WEB\EnglishResourcesController@update')->name('update-english-resources');
    

    /**
     * delete
     */
    Route::delete('/word/englishresources/delete/{id}', 'ADMIN_WEB\EnglishResourcesController@deleteEnglishWord')->name('delete-english-resources');

});
