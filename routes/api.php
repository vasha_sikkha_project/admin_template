<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('register', 'API\AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'API\AuthController@logout');

        Route::get('details', 'API\AuthController@user');

        Route::get('getTask/{topic_id}', 'API\task\allTaskController@getTask');

        Route::get('topics', 'API\TopicController@showTopics');

        Route::get('getmcq/{topic_id}', 'API\MCQController@getMCQ');

        Route::get('getfillintheblanks/{topic_id}', 'API\FillInTheBlanksController@getFillInTheBlanks');

        Route::get('getsentencematching/{topic_id}', 'API\SentenceMatchingController@getSentenceMatching');

        Route::get('gettruefalse', 'API\TrueFalseController@getTrueFalseTask');

        Route::get('get-eng-to-eng-sentence-matching', 'API\SentenceMatchingEngToEngController@getSentenceMatchingTask');


    });
});

