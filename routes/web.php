<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();

Route::get('/', 'ADMIN_WEB\HomeController@index');

Route::get('/home', 'ADMIN_WEB\HomeController@index')->name('home-page');


// show admin profile
Route::get('/developers', 'ADMIN_WEB\DevelopersController@getAllDevelopers')->name('project-developers');

