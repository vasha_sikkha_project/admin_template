<?php

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('mobile-login', 'MobileAPI\AuthController@login');
    Route::post('mobile-register', 'MobileAPI\AuthController@signup');

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('mobile-logout', 'MobileAPI\AuthController@logout');

        Route::get('mobile-details', 'MobileAPI\AuthController@user');

        Route::get('mobile-topics', 'MobileAPI\TopicController@showTopics');

        Route::get('mobile-getmcq', 'MobileAPI\MCQController@getMCQ');

        Route::get('mobile-getfillintheblanks', 'MobileAPI\FillInTheBlanksController@getFillInTheBlanks');

        Route::get('mobile-getSentenceMatching', 'MobileAPI\SentenceMatchingController@getSentenceMatchingTask');

        Route::get('mobile-gettruefalse', 'MobileAPI\TrueFalseController@getTrueFalseTask');

        Route::get('mobile-get-eng-to-eng-sentence-matching', 'MobileAPI\SentenceMatchingEngToEngController@getSentenceMatchingTask');

        Route::get('mobile-getalltask', 'MobileAPI\allTaskController@getAllTask');

        Route::get('mobile-get-memory-game', 'MobileAPI\MemoryGameController@getMemoryGameTask');

        Route::get('mobile-get-picture-to-word', 'MobileAPI\PictureToWordController@getPictureToWordTask');

        Route::get('mobile-get-word-to-picture', 'MobileAPI\WordToPictureController@getWordToPictureTask');

        Route::get('mobile-get-fixed-jumbled-sentence', 'MobileAPI\FixJumbledSentencesController@getFixedJumbledSentenceTask');

    });
});

