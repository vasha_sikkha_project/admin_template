<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserHistoryController
 * @package App\Models
 * @version December 26, 2018, 6:22 pm UTC
 *
 * @property integer user_id
 * @property integer topic_id
 * @property integer task_id
 * @property integer level_id
 * @property string remember_token
 */
class UserHistory extends Model
{
    use SoftDeletes;

    public $table = 'user_history';
    protected $primaryKey = 'userhistory_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'topic_id',
        'level_id',
        'user_level',
        'upload_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'userhistory_id' => 'integer',
        'user_id' => 'integer',
        'topic_id' => 'integer',
        'level_id' => 'integer',
        'user_level' => 'integer',
        'upload_time' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
