<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrosswordPuzzleInfo extends Model
{
    use SoftDeletes;

    public $table = 'cross_word_puzzle_info';
    protected $primaryKey = 'info_id';
    public $timestamps = true;
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'corss_id',	
        'dictionary_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'info_id' => 'integer',
        'corss_id' => 'integer',
        'dictionary_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
