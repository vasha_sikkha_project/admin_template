<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LevelController
 * @package App\Models
 * @version December 26, 2018, 6:21 pm UTC
 *
 * @property integer difficulty
 * @property integer topic_id
 * @property integer task_id
 * @property integer score_required
 */
class Level extends Model
{
    use SoftDeletes;

    public $table = 'level';
    protected $primaryKey = 'level_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'difficulty',
        'score_required'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'level_id' => 'integer',
        'difficulty' => 'integer',
        'score_required' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
