<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPerformance extends Model
{
    use SoftDeletes;

    public $table = 'user_performance';
    protected $primaryKey = 'user_performance_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'task_id',
        'topic_id',
        'level_id',
        'solved',
        'specific_task_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_performance_id' => 'integer',
        'user_id' => 'integer',
        'task_id' => 'integer',
        'topic_id' => 'integer',
        'level_id' => 'integer',
        'solved' => 'integer',
        'specific_task_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
