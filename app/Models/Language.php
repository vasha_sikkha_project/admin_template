<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LevelController
 * @package App\Models
 * @version December 26, 2018, 6:21 pm UTC
 *
 * @property integer difficulty
 * @property integer topic_id
 * @property integer task_id
 * @property integer score_required
 */
class Language extends Model
{
    use SoftDeletes;

    public $table = 'language';
    protected $primaryKey = 'language_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'language_id' => 'integer',
        'language_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
