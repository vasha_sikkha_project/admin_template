<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TopicController
 * @package App\Models
 * @version December 26, 2018, 6:21 pm UTC
 *
 * @property string topic_name
 */
class Topic extends Model
{
    use SoftDeletes;

    public $table = 'topic';
    protected $primaryKey = 'topic_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'topic_name',
        'topic_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'topic_id' => 'integer',
        'topic_name' => 'string',
        'topic_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function allTopics()
    {
        $data = TopicController::all();
        return response()->json($data);
    }


    
}
