<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TaskController
 * @package App\Models
 * @version December 26, 2018, 6:21 pm UTC
 *
 * @property string task_name
 */
class Task extends Model
{
    use SoftDeletes;

    public $table = 'task';
    protected $primaryKey = 'task_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'task_name',
        'wordOrsentence'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'task_id' => 'integer',
        'task_name' => 'string',
        'wordOrsentence' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
