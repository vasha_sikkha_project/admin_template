<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BanglaResourcesController
 * @package App\Models
 * @version February 11, 2019, 11:17 am UTC
 *
 * @property string word
 */
class BanglaResources extends Model
{
    use SoftDeletes;

    public $table = 'quiz';
    protected $primaryKey = 'quiz_id';
    
    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    // protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'quiz_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ]; 

    
}
