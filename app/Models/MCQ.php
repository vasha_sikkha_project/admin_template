<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MCQ extends Model
{
    use SoftDeletes;

    public $table = 'mcq';
    protected $primaryKey = 'mcq_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'word_task_id',
        'explanation',
        'user_id',
        'question',
        'answer',
        'quiz_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'mcq_id' => 'integer',
        'word_task_id' => 'integer',
        'explanation' => 'string',
        'user_id' => 'integer',
        'question' => 'string',
        'answer' => 'integer',
        'quiz_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

}
