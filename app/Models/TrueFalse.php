<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrueFalse extends Model
{
    use SoftDeletes;

    public $table = 'true_false';
    protected $primaryKey = 'true_false_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public $fillable = [
        'sentence_task_id',
        'answer',
        'explanation',
        'user_id',
        'translation_id',
        'question',
        'quiz_id' 
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'true_false_id' => 'integer',
        'sentence_task_id' => 'integer',
        'answer' => 'string',
        'explanation' => 'string',
        'user_id' => 'integer',
        'translation_id' => 'integer',
        'question' => 'string',
        'quiz_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
