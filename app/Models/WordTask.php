<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WordTaskController
 * @package App\Models
 * @version December 26, 2018, 6:22 pm UTC
 *
 * @property integer task_id
 * @property integer topic_id
 * @property integer level_id
 */
class WordTask extends Model
{
    use SoftDeletes;

    public $table = 'word_task';
    protected $primaryKey = 'word_task_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'task_id',
        'topic_id',
        'level_id',
        'experience_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'word_task_id' => 'integer',
        'task_id' => 'integer',
        'topic_id' => 'integer',
        'level_id' => 'integer',
        'experience_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
