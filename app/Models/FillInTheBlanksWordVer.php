<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FillInTheBlanksWordVer extends Model
{
    use SoftDeletes;

    public $table = 'fill_in_the_blanks_word_ver';
    protected $primaryKey = 'fill_blanks_word_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'word_task_id',
        'dictionary_id',
        'incomplete_word',
        'user_id',
        'quiz_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fill_blanks_word_id' => 'integer',
        'word_task_id' => 'integer',
        'dictionary_id' => 'integer',
        'incomplete_word' => 'string',
        'user_id' => 'integer',
        'quiz_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
