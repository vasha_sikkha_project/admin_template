<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FbOptions extends Model
{
    use SoftDeletes;

    public $table = 'fb_options';
    protected $primaryKey = 'options_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public $fillable = [
        'fill_blanks_id',
        'options'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'options_id',
        'fill_blanks_id',
        'options'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
