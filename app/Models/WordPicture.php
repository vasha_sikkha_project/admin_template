<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WordPicture extends Model
{
    use SoftDeletes;

    public $table = 'word_picture';
    protected $primaryKey = 'word_picture_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'word_task_id',
        'dictionary_id',
        'user_id',
        'quiz_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'word_picture_id' => 'integer',
        'word_task_id' => 'integer',
        'dictionary_id' => 'integer',
        'user_id' => 'integer',
        'quiz_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
