<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TranslationController
 * @package App\Models
 * @version December 26, 2018, 6:21 pm UTC
 *
 * @property integer english_sentence_id
 * @property integer bangla_sentence_id
 */
class Translation extends Model
{
    use SoftDeletes;

    public $table = 'translation';
    protected $primaryKey = 'translation_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public $fillable = [
        'another_sentence_id',
        'bangla_sent_id',
        'user_id',
        'language_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'translation_id' => 'integer',
        'another_sentence_id' => 'integer',
        'bangla_sentence_id' => 'integer',
        'user_id' => 'integer',
        'language_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
