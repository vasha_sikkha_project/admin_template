<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserController
 * @package App\Models
 * @version December 26, 2018, 6:20 pm UTC
 *
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    protected $primaryKey = 'user_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'email',
        'age',
        'password',
        'latest_image',
        'privilege',
        'experience_id',
        'mobile_number',
        'latest_image',
        'institution',
        'present_address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'age' => 'integer',
        'password' => 'string',
        'latest_image' => 'string',
        'privilege' => 'integer',
        'experience_id' => 'integer',
        'mobile_number' => 'string',
        'latest_image' => 'string',
        'institution' => 'string',
        'present_address' =>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
