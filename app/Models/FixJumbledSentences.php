<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixJumbledSentences extends Model
{
    use SoftDeletes;

    public $table = 'fix_jumbled_sentence';
    protected $primaryKey = 'fix_jum_sen_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sentence_task_id',
        'translation_id',
        'question',
        'user_id',
        'quiz_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fix_jum_sen_id' => 'integer',
        'sentence_task_id' => 'integer',
        'translation_id' => 'integer',
        'question' => 'string',
        'user_id' =>'integer',
        'quiz_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
