<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LovedTopic extends Model
{
    use SoftDeletes;

    public $table = 'loved_topic';
    protected $primaryKey = 'favourite_topic_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = true;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'topic_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'favourite_topic_id' => 'integer',
        'topic_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
