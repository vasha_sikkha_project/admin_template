<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use App\Models\FillInTheBlanks;

class FillInTheBlanksController extends Controller
{
    public function getFillInTheBlanks(Request $request, $topic_id)
    {
        $user = $request->user();

        $task = DB::table('task')
                   ->where('task_name', 'fillintheblanks')
                   ->first();

        $user_history = DB::table('user_history')
                        ->orderBy('userhistory_id', 'DESC')
                        ->where('user_id', $user->user_id)
                        ->where('topic_id', $topic_id)
                        ->where('task_id', $task->task_id)
                        ->first();

        $level_id = 1;

        if ($user_history != null) {
            $level_id = $user_history->level_id;
        }

        //a user in a level and in a topic has a single sentence task id

        $sentence_task = DB::table('sentence_task')
                    ->where('level_id', $level_id)
                    ->where('topic_id', $topic_id)
                    ->where('task_id', $task->task_id)
                    ->where('experience_id', $user->experience_id)
                    ->first();

        $blanks = DB::table('fill_in_the_blanks')
                  ->where('sentence_task_id', $sentence_task->sentence_task_id)
                  ->get();

        $i = 0;
        $arr = array();

        foreach ($blanks as $data)
        {
            $obj = array("Incomplete_Sentence" => "",
                         "Options" => array(),
                         "Answers" => array(),
                         "Explanation" => "");

            $obj['Incomplete_Sentence'] = $data->incomplete_sentence;

            $options = DB::table('fb_options')
                       ->where('fill_blanks_id', $data->fill_blanks_id)
                       ->join('dictionary', 'fb_options.options', '=', 'dictionary.dictionary_id')
                       ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                       ->select('english_resource.word')
                       ->get();

            $j = 0;

            foreach ($options as $option) {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $data->explanation;

            $answers = DB::table('fb_answer')
                       ->where('fill_blanks_id', $data->fill_blanks_id)
                       ->join('dictionary', 'fb_answer.answer', '=', 'dictionary.dictionary_id')
                       ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                       ->select('english_resource.word')
                       ->get();

            $k = 0;

            foreach ($answers as $answer) {
                $obj['Answers'][$k++] = $answer->word;
            }

            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }
}
