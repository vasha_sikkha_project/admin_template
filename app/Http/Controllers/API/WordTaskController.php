<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\BanglaResources;
use App\Models\EnglishResources;
use App\Models\Dictionary;
use App\Models\SynonymAntonym;
use App\Models\Translation;
use App\Models\VocabularyTask;
use View;

class WordTaskController extends Controller
{

    public function getData()
    {

        return View::make('word.wordTable')
        ->with('dataBangla', app('App\Http\Controllers\ADMIN_WEB\BanglaResourcesController')->getLimitedBanglaWords(0))
        ->with('dataEnglish', app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->getLimitedEnglishWords(0))
        ->with('dictionary',app('App\Http\Controllers\ADMIN_WEB\DictionaryController')->getLimitedDictionary(0))
        ->with('synonymAntonym', SynonymAntonym::all())
        ->with('translation',Translation::all()
    );
    }
}
