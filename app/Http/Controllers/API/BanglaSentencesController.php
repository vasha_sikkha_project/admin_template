<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;

use App\Models\BanglaSentences;
use App\Models\EnglishSentence;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use View;

class BanglaSentencesController extends Controller
{

    private $dataBangla;
    private $dataEnglish;

    public function getData()
    {

        $dataBangla = BanglaSentences::all();
        $dataEnglish = EnglishSentence::all();
        return View::make('sentence.sentencetable')
        ->with('dataBangla',$dataBangla)
        ->with('dataEnglish', $dataEnglish);
    }
}
