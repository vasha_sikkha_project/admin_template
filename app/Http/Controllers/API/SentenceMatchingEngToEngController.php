<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SentenceMatchingEngToEngController extends Controller
{
    public function getsentenceMatching()
    {
        $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as explanation',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->get()->toArray();

        return $data;
    }

    public function getLimitedSentenceMatching($id)
    {
        $SentenceMatching = \DB::table('sentence_matching')
            ->where('sentence_matching.sen_match_id', '>=', $id)
            ->limit(100)->get();

        return $SentenceMatching;
    }

    /**
     * Sentence matching is a task in the "Task" table. In this table, sentenceMatching is task_name, task_id for this is 1.
     * wordOrSentence = 1 means it's sentence related task.
     *
     * json output form -
     *
     *
     *[
     *    {
     *        "broken_sentence": "sdfd#sdf",
     *       "bangla_sentence": "eI0gQ804Yu",
     *      "english_sentence": "XLXuNyLsFL"
     *    },
     *    {
     *       "broken_sentence": "sf#dtgdf",
     *       "bangla_sentence": "q1ORLcfrti",
     *       "english_sentence": "b6J6pCn0fE"
     *    }
     *]
     *
     */

    public function getSentenceMatchingTask(Request $request)
    {
        // $input_topic_name = $request->header('topic_name'); // given as input in request header

        $topic_id = $request->header('topic_id');

        $user_id = $request->user()->user_id; // retrieve user id (or other data) from access token

        // $topic_id = Topic::where('topic_name', $input_topic_name)->first()->topic_id;

        $task_name = 'sentenceMatchingEngToEng'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user_id)
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('user_history.deleted_at', '=', null)
            ->first();

        $user_data = DB::table('users')
            ->where('users.user_id', '=', $user_id)
            ->where('users.deleted_at', '=', null)
            ->first();

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $user_experience_id = $user_data->experience_id;

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user_experience_id)
            ->where('deleted_at', '=', null)
            ->first();

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $sentence_match_data = DB::table('sentence_matching')
            ->join('translation', 'translation.translation_id', '=', 'sentence_matching.translation_id')
            ->join('bangla_sentence', 'translation.bangla_sent_id', '=', 'bangla_sentence.bangla_sentence_id')
            ->join('english_sentence', 'translation.another_sentence_id', '=', 'english_sentence.english_sentence_id')
            ->where('sentence_matching.sentence_task_id', $sentence_task_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->where('english_sentence.deleted_at', '=', null)
            ->select('sentence_matching.broken_sentence as broken_sentence', 'bangla_sentence.bangla_sentence as bangla_sentence',
                'english_sentence.english_sentence as english_sentence')
            ->get();

        $i = 0;
        $arr = array();

        foreach ($sentence_match_data as $data) {
            $obj = array(
                "first_segment" => "",
                "last_segment" => "",
                "broken_sentence" => "",
                "bangla_sentence" => "",
                "english_sentence" => "",
            );

            $broken_sent = explode("#", $data->broken_sentence);

            $obj['first_segment'] = array_shift($broken_sent); // string parsing and tokenizing where delimeter is '#'
            $obj['last_segment'] = implode("#", $broken_sent);

            $obj['broken_sentence'] = $data->broken_sentence;
            $obj['bangla_sentence'] = $data->bangla_sentence;
            $obj['english_sentence'] = $data->english_sentence;

            $arr[$i++] = $obj;
        }

        return response()->json($arr);

    }
}
