<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\EnglishResources;

class EnglishResourcesController extends Controller
{
    public function insertAndReturnID( $word )
    {
       
            $data = EnglishResources::create(
                [
                    'word' => $word,
                    'image_link' => 'ni IMG'
                ]
            );

            return $data->english_word_id;
    }


    public function getLimitedEnglishWords( $id )
    {
        $english = \DB::table('english_resource')
                    ->where('english_resource.english_word_id', '>=' , $id)
                    ->limit(100)->get();

        return $english;
    }

}
