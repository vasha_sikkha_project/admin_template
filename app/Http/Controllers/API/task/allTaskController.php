<?php

namespace App\Http\Controllers\API\task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
// use App\Models\FillInTheBlanks;
use App\Http\Controllers\API;

class allTaskController extends Controller
{
    public function getTask(Request $request, $topic_id)
    {
        $task = array();

        $mcq_ob = new MCQController;
        $mcq_arr = array();
        $mcq_arr = $mcq_ob->getmcq($request, $topic_id);
        $task = $mcq_arr;
        $i = sizeof($task) - 1;

        $blanks_ob = new FillInTheBlanksController;
        $blanks_arr = array();
        $blanks_arr = $blanks_ob->getFillInTheBlanks($request, $topic_id);

        foreach ($blanks_arr as $blank)
        {
            $task[$i] = $blank;
            ++$i;
        }

        $i = sizeof($task) - 1;

        // $rand_gen = array();

        // for ($idx = 0; $idx <= $i; idx += 1)
        //     $rand_gen[$idx] = $idx;

        // $rand_gen.shuffle();

        shuffle($task);

        return response()->json($task);
    }
}
