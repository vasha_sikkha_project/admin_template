<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class TrueFalseController extends Controller
{
    public function getTrueFalseTask(Request $request)
    {
        $user_id = $request->user()->user_id; // retrieve user id (or other data) from access token

        $topic_id = $request->header('topic_id');

        $task_name = 'truefalse'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // true false

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user_id)
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('user_history.deleted_at', '=', null)
            ->first();

        $user_data = DB::table('users')
            ->where('users.user_id', '=', $user_id)
            ->where('users.deleted_at', '=', null)
            ->first();

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $user_experience_id = $user_data->experience_id;

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user_experience_id)
            ->where('deleted_at', '=', null)
            ->first();

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $trueFalseData = DB::table('true_false')
            ->where('sentence_task_id', $sentence_task_id)
            ->where('deleted_at', '=', null)
            ->get();

        $i = 0;
        $arr = array();

        foreach ($trueFalseData as $data) {
            $obj = array(
                "question" => "",
                "answer" => "",
                "explanation" => "",
            );

            $obj['question'] = $data->question;
            $obj['answer'] = $data->answer;
            $obj['explanation'] = $data->explanation;

            $arr[$i++] = $obj;
        }

        return response()->json($arr);

    }
}
