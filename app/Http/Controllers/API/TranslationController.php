<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TranslationController extends Controller
{
    public function getData()
    {
        $data = \DB::table('translation')
                ->join('bangla_sentence','bangla_sentence.bangla_sentence_id','=','translation.bangla_sent_id')
                ->join('english_sentence','english_sentence.english_sentence_id','=','translation.another_sentence_id')
                ->select('translation.translation_id as id','bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence','translation.created_at as created_at')
                ->get()->toArray(); 
        
        return $data;
    }
}
