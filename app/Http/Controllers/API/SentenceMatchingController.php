<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SentenceMatchingController extends Controller
{
    public function getSentenceMatchingTask(Request $request, $topic_id)
    {
        $user = $request->user();

        $task = DB::table('task')
                   ->where('task_name', 'sentenceMatching')
                   ->first();

        $user_history = DB::table('user_history')
                        ->orderBy('userhistory_id', 'DESC')
                        ->where('user_id', $user->user_id)
                        ->where('topic_id', $topic_id)
                        ->where('task_id', $task->task_id)
                        ->first();

        $level_id = 1;

        if ($user_history != null) {
            $level_id = $user_history->level_id;
        }

        //a user in a level and in a topic has a single word task id

        $sentence_task = DB::table('sentence_task')
                    ->where('level_id', $level_id)
                    ->where('topic_id', $topic_id)
                    ->where('task_id', $task->task_id)
                    ->where('experience_id', $user->experience_id)
                    ->first();

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $sentence_match_data = DB::table('sentence_matching')
            ->join('translation', 'translation.translation_id', '=', 'sentence_matching.translation_id')
            ->join('bangla_sentence', 'translation.bangla_sent_id', '=', 'bangla_sentence.bangla_sentence_id')
            ->join('english_sentence', 'translation.another_sentence_id', '=', 'english_sentence.english_sentence_id')
            ->where('sentence_matching.sentence_task_id', $sentence_task_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->where('english_sentence.deleted_at', '=', null)
            ->select('sentence_matching.broken_sentence as broken_sentence', 'bangla_sentence.bangla_sentence as bangla_sentence',
                'english_sentence.english_sentence as english_sentence')
            ->get();

        // print_r($sentence_match_data);

        $i = 0;
        $arr = array();

        foreach ($sentence_match_data as $data) {
            $obj = array(
                "broken_sentence" => "",
                "bangla_sentence" => "",
                "english_sentence" => "",
            );

            $obj['broken_sentence'] = $data->broken_sentence;
            $obj['bangla_sentence'] = $data->bangla_sentence;
            $obj['english_sentence'] = $data->english_sentence;

            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }
}
