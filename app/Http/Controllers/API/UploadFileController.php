<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use League\Csv\Reader;

class UploadFileController extends Controller {
   public function index() {
      return view('uploadfile');
   }
   public function showUploadFile(Request $request) {
      $file = $request->file('csv');

      //Display File Name
      echo 'File Name: '.$file->getClientOriginalName();
      echo '<br>';

      //Display File Extension
      echo 'File Extension: '.$file->getClientOriginalExtension();
      echo '<br>';

      //Display File Real Path
      echo 'File Real Path: '.$file->getRealPath();
      echo '<br>';

      //Display File Size
      echo 'File Size: '.$file->getSize();
      echo '<br>';

      //Display File Mime Type
      echo 'File Mime Type: '.$file->getMimeType();
      echo '<br>';
       // Get uploaded CSV file
     $file1 = $request->file('csv');


    // Create a CSV reader instance
     $reader = Reader::createFromFileObject($file1->openFile());
    // Create a customer from each row in the CSV file

    $count = 0;

   foreach ($reader as $index => $row)
   {
      $idOfBanglaWord;
      $idOfEnglishWord;
      $count = 0;

      foreach( $row as $line )
      {
         if( $count == 0 )
         {
            $idOfEnglishWord =  app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->insertAndReturnID($line);
         }else if( $count == 1 )
         {
            $idOfBanglaWord =  app('App\Http\Controllers\ADMIN_WEB\BanglaResourcesController')->insertAndReturnID($line);
         }

         $count++;
         echo $line;
      }

      app('App\Http\Controllers\ADMIN_WEB\DictionaryController')->insertAndReturnID($idOfBanglaWord , $idOfEnglishWord);

   }

      //Move Uploaded File
      $destinationPath = 'uploads';
      $file->move($destinationPath,$file->getClientOriginalName());

   }
}
