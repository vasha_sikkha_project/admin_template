<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;


use App\Http\Requests\CreateFixJumbledSentencesControllerRequest;
use App\Http\Requests\UpdateFixJumbledSentencesControllerRequest;
use App\Repositories\FixJumbledSentencesControllerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FixJumbledSentencesController extends Controller
{
    public function getfixJumbledSentences()
    {
        $data = \DB::table('translation')
                ->join('fix_jumbled_sentence','fix_jumbled_sentence.english_sentence_id','=','translation.another_sentence_id')
                ->join('bangla_sentence','bangla_sentence.bangla_sentence_id','=','translation.bangla_sent_id')
                ->join('english_sentence','english_sentence.english_sentence_id','=','translation.another_sentence_id')
                ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.explanation as explanation',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence','fix_jumbled_sentence.created_at as created_at')
                ->get()->toArray(); 
        
        return $data;
    }



    public function getLimitedFixJumbledSentences( $id )
    {
        $FixJumbledSentences = \DB::table('fix_jumbled_sentence')
         ->where('fix_jumbled_sentence.fix_jum_sen_id', '>=' , $id)
         ->limit(100)->get();

         return $FixJumbledSentences;
    }
}
