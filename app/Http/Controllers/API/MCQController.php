<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use App\Models\MCQ;

class MCQController extends Controller
{
    public function getMCQ(Request $request, $topic_id)
    {
        $user = $request->user();

        $task = DB::table('task')
                   ->where('task_name', 'mcq')
                   ->first();

        $user_history = DB::table('user_history')
                        ->orderBy('userhistory_id', 'DESC')
                        ->where('user_id', $user->user_id)
                        ->where('topic_id', $topic_id)
                        ->where('task_id', $task->task_id)
                        ->first();

        $level_id = 1;

        if ($user_history != null) {
            $level_id = $user_history->level_id;
        }

        //a user in a level and in a topic has a single word task id

        $word_task = DB::table('word_task')
                    ->where('level_id', $level_id)
                    ->where('topic_id', $topic_id)
                    ->where('task_id', $task->task_id)
                    ->where('experience_id', $user->experience_id)
                    ->first();

        $mcq = DB::table('mcq')
               ->where('word_task_id', $word_task->word_task_id)
               ->get();

        $i = 0;
        $arr = array();

        foreach($mcq as $data)
        {
            $obj = array("Question" => "",
                         "Options" => array(),
                         "Answer" => "",
                         "Explanation" => "");

            $obj['Question'] = $mcq[$i]->question;

            $options = DB::table('mcq_options')
                       ->where('mcq_id', $mcq[$i]->mcq_id)
                       ->join('dictionary', 'mcq_options.dictionary_id', '=', 'dictionary.dictionary_id')
                       ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                       ->select('english_resource.word')
                       ->get();

            $j = 0;

            foreach($options as $option)
            {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $mcq[$i]->explanation;

            $answer = DB::table('dictionary')
                  ->where('dictionary_id', $mcq[$i]->answer)
                  ->join('english_resource', 'another_word_id', '=', 'english_resource.english_word_id')
                  ->first();

            $ans_idx = 0;

            foreach ($options as $option)
            {
                if ($option->word == $answer->word)
                    break;
                
                ++$ans_idx;
            }

            $obj['Answer'] = $ans_idx;
            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }
}
