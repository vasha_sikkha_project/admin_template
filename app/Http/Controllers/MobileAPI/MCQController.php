<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MCQController extends Controller
{
    public function getMCQ(Request $request)
    {
        $task_name = 'mcq';
        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single word task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $word_task = DB::table('word_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        $mcq = DB::table('mcq')
            ->where('word_task_id', $word_task->word_task_id)
            ->get();

        $i = 0;
        $arr = array();

        foreach ($mcq as $data) {
            $obj = array("Question" => "",
                "Options" => array(),
                "Answer" => "",
                "Explanation" => "");

            $obj['Question'] = $mcq[$i]->question;

            $options = DB::table('mcq_options')
                ->where('mcq_id', $mcq[$i]->mcq_id)
                ->join('dictionary', 'mcq_options.dictionary_id', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $j = 0;

            foreach ($options as $option) {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $mcq[$i]->explanation;

            $answer = DB::table('dictionary')
                ->where('dictionary_id', $mcq[$i]->answer)
                ->join('english_resource', 'another_word_id', '=', 'english_resource.english_word_id')
                ->first();

            // $ans_idx = 0;

            // foreach ($options as $option) {

            //     if ($option->word == $answer->word) {
            //         break;
            //     }

            //     ++$ans_idx;
            // }

            // $obj['Answer'] = $ans_idx;

            $obj['Answer'] = $answer->word;

            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }

    /**
     * getLimited_{TASK_NAME}_ForMixAPI - this function will only be used for MixAPI purposes.
     * This function is needed because the returned JSON file structure is different than
     * other JSON files.
     *
     * We need task_name and target_index fields in JSON files here which weren't needed before.getLimitedMCQ
     */

    public function getLimitedMCQForMixAPI(Request $request, $limit)
    {
        $task_name = 'mcq';
        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single word task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $word_task = DB::table('word_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        if ($word_task == null) {
            return array();
        }

        $mcq = DB::table('mcq')
            ->where('word_task_id', $word_task->word_task_id)
            ->limit($limit)->get();

        $i = 0;
        $arr = array();

        foreach ($mcq as $data) {
            $obj = array(
                "Task_Name" => $task_name,
                "Target_Index" => "",
                "Question" => "",
                "Options" => array(),
                "Answer" => "",
                "Explanation" => "",
            );

            $obj['Question'] = $mcq[$i]->question;

            $options = DB::table('mcq_options')
                ->where('mcq_id', $mcq[$i]->mcq_id)
                ->join('dictionary', 'mcq_options.dictionary_id', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $j = 0;

            foreach ($options as $option) {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $mcq[$i]->explanation;

            $answer = DB::table('dictionary')
                ->where('dictionary_id', $mcq[$i]->answer)
                ->join('english_resource', 'another_word_id', '=', 'english_resource.english_word_id')
                ->first();

            $obj['Answer'] = $answer->word;

            $arr[$i++] = $obj;
        }

        return $arr;
    }
}
