<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FillInTheBlanksController extends Controller
{
    public function getFillInTheBlanks(Request $request)
    {
        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $task_name = 'fillintheblanks'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single sentence task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        if ($sentence_task == null) {
            return array();
        }

        $blanks = DB::table('fill_in_the_blanks')
            ->where('sentence_task_id', $sentence_task->sentence_task_id)
            ->get();

        $i = 0;
        $arr = array();

        foreach ($blanks as $data) {
            $obj = array("Incomplete_Sentence" => "",
                "Options" => array(),
                "Answers" => array(),
                "Explanation" => "");

            $obj['Incomplete_Sentence'] = $data->incomplete_sentence;

            $options = DB::table('fb_options')
                ->where('fill_blanks_id', $data->fill_blanks_id)
                ->join('dictionary', 'fb_options.options', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $j = 0;

            foreach ($options as $option) {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $data->explanation;

            $answers = DB::table('fb_answer')
                ->where('fill_blanks_id', $data->fill_blanks_id)
                ->join('dictionary', 'fb_answer.answer', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $k = 0;

            foreach ($answers as $answer) {
                $obj['Answers'][$k++] = $answer->word;
            }

            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }

    /**
     * getLimited_{TASK_NAME}_ForMixAPI - this function will only be used for MixAPI purposes.
     * This function is needed because the returned JSON file structure is different than
     * other JSON files.
     *
     * We need task_name and target_index fields in JSON files here which weren't needed before.getLimitedMCQ
     */

    public function getLimitedFillInTheBlanksForMixAPI(Request $request, $limit)
    {
        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $task_name = 'fillintheblanks'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single sentence task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        if ($sentence_task == null) {
            return array();
        }

        $blanks = DB::table('fill_in_the_blanks')
            ->where('sentence_task_id', $sentence_task->sentence_task_id)
            ->limit($limit)->get();

        $i = 0;
        $arr = array();

        foreach ($blanks as $data) {
            $obj = array(
                "Task_Name" => $task_name,
                "Target_Index" => "",
                "Incomplete_Sentence" => "",
                "Options" => array(),
                "Answers" => array(),
                "Explanation" => "",
            );

            $obj['Incomplete_Sentence'] = $data->incomplete_sentence;

            $options = DB::table('fb_options')
                ->where('fill_blanks_id', $data->fill_blanks_id)
                ->join('dictionary', 'fb_options.options', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $j = 0;

            foreach ($options as $option) {
                $obj['Options'][$j++] = $option->word;
            }

            $obj['Explanation'] = $data->explanation;

            $answers = DB::table('fb_answer')
                ->where('fill_blanks_id', $data->fill_blanks_id)
                ->join('dictionary', 'fb_answer.answer', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word')
                ->get();

            $k = 0;

            foreach ($answers as $answer) {
                $obj['Answers'][$k++] = $answer->word;
            }

            $arr[$i++] = $obj;
        }

        return $arr;

    }
}
