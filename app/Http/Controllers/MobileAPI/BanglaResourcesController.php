<?php

namespace App\Http\Controllers\MobileAPI;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests;
use App\Models\BanglaResources;


class BanglaResourcesController extends Controller
{


    public function insertAndReturnID( $word )
    {

            $data = BanglaResources::create(
                [
                    'word'=> $word
                ]
            );

            return $data->bangla_word_id;
    }



    public function getLimitedBanglaWords( $id )
    {
        $bangla = \DB::table('bangla_resource')
                   ->where('bangla_resource.bangla_word_id', '>=' , $id)
                   ->limit(100)->get();

         return $bangla;
    }

}
