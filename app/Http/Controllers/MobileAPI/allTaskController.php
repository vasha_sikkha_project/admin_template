<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// use App\Models\FillInTheBlanks;

class allTaskController extends Controller
{
    /**
     * Every time you want to add a new task in this mixed api function, add 3 lines manually.
     * Tweak the value of $number_per_task to change the number of tasks_to_do per task type.
     */

    public function getAllTask(Request $request)
    {
        $mixed_task_arr = array();

        $number_per_task = 5;

        $sent_match_ob = new SentenceMatchingController;
        $sent_match_arr = array();
        $sent_match_arr = $sent_match_ob->getLimitedSentenceMatchingTaskForMixAPI($request, 3 * $number_per_task);

        $sent_match_EngToEng_ob = new SentenceMatchingEngToEngController;
        $sent_match_EngToEng_arr = array();
        $sent_match_EngToEng_arr = $sent_match_EngToEng_ob->getLimitedSentenceMatchingTaskForMixAPI($request, 3 * $number_per_task);

        $mcq_ob = new MCQController;
        $mcq_arr = array();
        $mcq_arr = $mcq_ob->getLimitedMCQForMixAPI($request, $number_per_task);

        $blanks_ob = new FillInTheBlanksController;
        $blanks_arr = array();
        $blanks_arr = $blanks_ob->getLimitedFillInTheBlanksForMixAPI($request, $number_per_task);

        $true_false_ob = new TrueFalseController;
        $true_false_arr = array();
        $true_false_arr = $true_false_ob->getLimitedTrueFalseTaskForMixAPI($request, $number_per_task);

        //array_merge_recursive keeps the repeating values too.

        $mixed_task_arr = array_merge_recursive($mixed_task_arr, $mcq_arr, $blanks_arr, $true_false_arr, $sent_match_arr, $sent_match_EngToEng_arr);
        
        // $mixed_task_arr = array_merge_recursive($mixed_task_arr, $mcq_arr, $blanks_arr);

        shuffle($mixed_task_arr);

        return response()->json($mixed_task_arr);
    }
}
