<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WordToPictureController extends Controller
{
    public function getWordToPictureTask(Request $request)
    {
        $task_name = 'word_to_picture';
        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // picture_to_word

        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single word task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $word_task = DB::table('word_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        $word_to_pic_data = DB::table('word_picture')
            ->where('word_picture.word_task_id', '=', $word_task->word_task_id)
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'word_picture.dictionary_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->select('word_picture.word_picture_id as id', 'english_resource.image_link as image_link', 'english_resource.word as word', 'bangla_resource.word as bangla_meaning')
            ->get();

        $arr = array();

        foreach ($word_to_pic_data as $data) {
            $obj = array(
                "Word" => "",
                "Meaning" => "",
                "Option_links" => array(),
                "Correct_image_link" => "",
            );

            $obj['Word'] = $data->word;
            $obj['Meaning'] = $data->bangla_meaning;
            $obj['Correct_image_link'] = $data->image_link;

            $options = DB::table('word_picture_options')
                ->where('word_picture_options.word_picture_id', '=', $data->id)
                ->join('dictionary', 'word_picture_options.options', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word as word', 'english_resource.image_link as option_link')
                ->get();

            foreach ($options as $option) {
                array_push($obj['Option_links'], $option->option_link);
            }

            array_push($arr, $obj);
        }

        return response()->json($arr);
    }

    /**
     * getLimited_{TASK_NAME}_ForMixAPI - this function will only be used for MixAPI purposes.
     * This function is needed because the returned JSON file structure is different than
     * other JSON files.
     *
     * We need task_name and target_index fields in JSON files here which weren't needed before.getLimitedMCQ
     */

    public function getLimitedPictureToWordForMixAPI(Request $request, $limit)
    {
        /**
         * Will complete later if needed.
         *
         */
    }
}
