<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FixJumbledSentencesController extends Controller
{
    public function getfixJumbledSentences()
    {
        $data = \DB::table('translation')
            ->join('fix_jumbled_sentence', 'fix_jumbled_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.explanation as explanation',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fix_jumbled_sentence.created_at as created_at')
            ->get()->toArray();

        return $data;
    }

    public function getLimitedFixJumbledSentences($id)
    {
        $FixJumbledSentences = \DB::table('fix_jumbled_sentence')
            ->where('fix_jumbled_sentence.fix_jum_sen_id', '>=', $id)
            ->limit(100)->get();

        return $FixJumbledSentences;
    }

    //fixed_jumbled_sentence

    public function getFixedJumbledSentenceTask(Request $request)
    {
        // $input_topic_name = $request->header('topic_name'); // given as input in request header

        $topic_id = $request->header('topic_id');

        $user_id = $request->user()->user_id; // retrieve user id (or other data) from access token

        // $topic_id = Topic::where('topic_name', $input_topic_name)->first()->topic_id;

        $task_name = 'fixed_jumbled_sentence'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user_id)
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('user_history.deleted_at', '=', null)
            ->first();

        $user_data = DB::table('users')
            ->where('users.user_id', '=', $user_id)
            ->where('users.deleted_at', '=', null)
            ->first();

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $user_experience_id = $user_data->experience_id;

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user_experience_id)
            ->where('deleted_at', '=', null)
            ->first();

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $jumbled_sentence_data = DB::table('fix_jumbled_sentence')
            ->join('translation', 'translation.translation_id', '=', 'fix_jumbled_sentence.translation_id')
            ->join('bangla_sentence', 'translation.bangla_sent_id', '=', 'bangla_sentence.bangla_sentence_id')
            ->join('english_sentence', 'translation.another_sentence_id', '=', 'english_sentence.english_sentence_id')
            ->where('fix_jumbled_sentence.sentence_task_id', $sentence_task_id)
            ->where('fix_jumbled_sentence.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->where('english_sentence.deleted_at', '=', null)
            ->select('english_sentence.english_sentence as english_sentence', 'bangla_sentence.bangla_sentence as bangla_sentence')
            ->get();

        $arr = array();

        foreach ($jumbled_sentence_data as $data) {
            $obj = array(
                "segments" => array(),
                "english_sentence" => "",
                "bangla_meaning" => "",
            );

            $broken_sent = explode(" ", $data->english_sentence);

            $obj['segments'] = array_merge_recursive($obj['segments'], $broken_sent);
            shuffle($obj['segments']);

            $obj['bangla_meaning'] = $data->bangla_sentence;
            $obj['english_sentence'] = $data->english_sentence;

            array_push($arr, $obj);
        }

        shuffle($arr);
        return response()->json($arr);
    }
}
