<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemoryGameController extends Controller
{

    /**
     * @author Rumman <shamiulhasan93@gmail.com>
     *
     * @todo don't touch without asking me. It's written as immune to various corner cases. Be absolutely sure before touching it.
     *
     * @clarification:
     *
     * This is how the JSON file will look.
     *
     * In this case,
     *
     * $no_of_objects = 2
     * $no_of_task_per_object = 4;
     *
     *
     */

    // [
    //     [
    //         {
    //             "english_word": "match",
    //             "bangla_word": "ANRYpRucMC",
    //             "image_link": "XPY0i7nABA"
    //         },
    //         {
    //             "english_word": "capable",
    //             "bangla_word": "Fjl5evn17S",
    //             "image_link": "v1jo0ne4ar"
    //         },
    //         {
    //             "english_word": "criticize",
    //             "bangla_word": "ANRYpRucMC",
    //             "image_link": "8FfaoAv9gC"
    //         },
    //         {
    //             "english_word": "usually",
    //             "bangla_word": "Sjpy4XSSKa",
    //             "image_link": "GpXUe4cCMh"
    //         }
    //     ],
    //     [
    //         {
    //             "english_word": "man",
    //             "bangla_word": "lioaUZdGZi",
    //             "image_link": "11YtOyBP4T"
    //         },
    //         {
    //             "english_word": "lose",
    //             "bangla_word": "VBiCjyHgLx",
    //             "image_link": "e46Kc44CTy"
    //         },
    //         {
    //             "english_word": "sun",
    //             "bangla_word": "ULJPSg3maQ",
    //             "image_link": "kdYr0MNxnW"
    //         },
    //         {
    //             "english_word": "usually",
    //             "bangla_word": "Sjpy4XSSKa",
    //             "image_link": "GpXUe4cCMh"
    //         }
    //     ]
    // ]

    public function getMemoryGameTask(Request $request)
    {
        $task_name = 'memorygame';
        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single word task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $word_task = DB::table('word_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        /**
         * In this upcoming query, I am taking Bangla words too just because of probable future necessity.
         */

        $memory_game_data = DB::table('image_match')
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'image_match.dictionary_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->where('image_match.word_task_id', '=', $word_task->word_task_id)
            ->where('image_match.deleted_at', '=', null)
            ->where('dictionary.deleted_at', '=', null)
            ->where('english_resource.deleted_at', '=', null)
            ->select('english_resource.word as english_word', 'bangla_resource.word as bangla_word', 'english_resource.image_link as image_link')
            ->limit(20)->get();

        $no_of_objects = 5;
        $no_of_task_per_object = 4;
        $total_no_of_task = $no_of_objects * $no_of_task_per_object; // = 20

        $current_idx_in_queried_data = 0;
        $size_of_queried_data = sizeof($memory_game_data);

        // it's an array of arrays of arrays. (3D)
        $total_arr = array();

        //DON'T TOUCH. JUST DON'T. OTHERWISE, IT WILL BE A MASSACRE. I GUARANTEE YOU.

        for ($i = 0; $i < $no_of_objects; $i++) {

            // it's an array of arrays. (2D)
            $array_of_per_object_tasks = array();

            for ($j = 0; $j < $no_of_task_per_object; $j++) {

                // (1D) array.
                $obj = array(
                    "english_word" => "",
                    "bangla_word" => "",
                    "image_link" => "",
                );

                // it indicates the current index in the fetched data by the SQL query.
                $current_idx_in_queried_data = $i * $no_of_task_per_object + $j;
                $data = $memory_game_data[$current_idx_in_queried_data];

                $obj['english_word'] = $data->english_word;
                $obj['bangla_word'] = $data->bangla_word;
                $obj['image_link'] = $data->image_link;

                // array_push($array_of_per_object_tasks['task'], $obj);
                array_push($array_of_per_object_tasks, $obj);

                // This if condition is used when there is less data than 20 or its' multiple in one run.
                if ($current_idx_in_queried_data >= $size_of_queried_data - 1) {
                    break;
                }
            }

            // array of arrays.
            array_push($total_arr, $array_of_per_object_tasks);

            // This if condition is used when there is less data than 20 or its' multiple in one run.
            if ($current_idx_in_queried_data >= $size_of_queried_data - 1) {
                break;
            }
        }

        return response()->json($total_arr);
    }

}
