<?php

namespace App\Http\Controllers\MobileAPI;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\BanglaSentences;
use App\Models\EnglishSentence;
use App\Models\Translation;
// use App\Http\Controllers\TranslationController;
use View;

class SentenceTaskController extends Controller
{
   public $dataBangla;
   public $dataEnglish;
   public $dataTranslartion;
   public $fillInTheBlanks;
   public $sentenceMatching;

    public function getData()
    {

        $dataBangla =  BanglaSentences::all();
        $dataEnglish = EnglishSentence::all();
        $dataTranslartion = app('App\Http\Controllers\ADMIN_WEB\TranslationController')->getData();
        $fillInTheBlanks=app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksController')->getfillInTheBlanks();
        $fixJumbledSentences=app('App\Http\Controllers\ADMIN_WEB\FixJumbledSentencesController')->getfixJumbledSentences();
        $sentenceMatching=app('App\Http\Controllers\ADMIN_WEB\SentenceMatchingController')->getsentenceMatching();

        return View::make('sentence.sentenceTable')
        ->with('dataBangla', $dataBangla )
        ->with('dataEnglish', $dataEnglish )
        ->with('dataTranslartion', $dataTranslartion )
        ->with('fillInTheBlanks',$fillInTheBlanks )
        ->with('fixJumbledSentences',$fixJumbledSentences )
        ->with('sentenceMatching',$sentenceMatching );
    }


}
