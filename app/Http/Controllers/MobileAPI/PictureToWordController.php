<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PictureToWordController extends Controller
{
    public function getPictureToWordTask(Request $request)
    {
        $task_name = 'picture_to_word';
        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // picture_to_word

        $user = $request->user();

        $topic_id = $request->header('topic_id');

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user->user_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->first();

        //a user in a level and in a topic has a single word task id

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $word_task = DB::table('word_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user->experience_id)
            ->first();

        $pic_to_word_data = DB::table('image_match')
            ->where('image_match.word_task_id', $word_task->word_task_id)
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'image_match.dictionary_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->select('image_match.matchword_id as id','english_resource.image_link as image_link', 'english_resource.word as answer', 'bangla_resource.word as bangla_meaning')
            ->get();

        $arr = array();

        foreach ($pic_to_word_data as $data) {
            $obj = array(
                "Image_link" => "",
                "Options" => array(),
                "Answer" => "",
                "Meaning" => "",
            );

            $obj['Image_link'] = $data->image_link;
            $obj['Answer'] = $data->answer;
            $obj['Meaning'] = $data->bangla_meaning;

            $options = DB::table('image_match_option')
                ->where('image_match_option.matchword_id', '=', $data->id)
                ->join('dictionary', 'image_match_option.option', '=', 'dictionary.dictionary_id')
                ->join('english_resource', 'dictionary.another_word_id', '=', 'english_resource.english_word_id')
                ->select('english_resource.word as word')
                ->get();

            foreach ($options as $option) {
                array_push($obj['Options'], $option->word);
            }

            array_push($arr, $obj);
        }

        return response()->json($arr);
    }

    /**
     * getLimited_{TASK_NAME}_ForMixAPI - this function will only be used for MixAPI purposes.
     * This function is needed because the returned JSON file structure is different than
     * other JSON files.
     *
     * We need task_name and target_index fields in JSON files here which weren't needed before.getLimitedMCQ
     */

    public function getLimitedPictureToWordForMixAPI(Request $request, $limit)
    {
        /**
         * Will complete later if needed.
         *
         */
    }
}
