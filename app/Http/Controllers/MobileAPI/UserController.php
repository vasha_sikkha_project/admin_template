<?php

namespace App\Http\Controllers\MobileAPI;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\User;
use Response;
use View;


class UserController extends Controller
{
    public function getUserData()
    {
        return View::make('user.userTable')
        ->with('allUser', User::all());
    }



}
