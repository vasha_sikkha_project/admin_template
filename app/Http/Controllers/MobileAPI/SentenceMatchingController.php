<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SentenceMatchingController extends Controller
{
    public function getsentenceMatching()
    {
        $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as explanation',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->get()->toArray();

        return $data;
    }

    public function getLimitedSentenceMatching($id)
    {
        $SentenceMatching = \DB::table('sentence_matching')
            ->where('sentence_matching.sen_match_id', '>=', $id)
            ->limit(100)->get();

        return $SentenceMatching;
    }

    /**
     * Sentence matching is a task in the "Task" table. In this table, sentenceMatching is task_name, task_id for this is 1.
     * wordOrSentence = 1 means it's sentence related task.
     *
     * json output form -
     *
     *
     *[
     *    {
     *        "broken_sentence": "sdfd#sdf",
     *       "bangla_sentence": "eI0gQ804Yu",
     *      "english_sentence": "XLXuNyLsFL"
     *    },
     *    {
     *       "broken_sentence": "sf#dtgdf",
     *       "bangla_sentence": "q1ORLcfrti",
     *       "english_sentence": "b6J6pCn0fE"
     *    }
     *]


     *
     */

    public function getSentenceMatchingTask(Request $request)
    {
        // $input_topic_name = $request->header('topic_name'); // given as input in request header
        $user_id = $request->user()->user_id; // retrieve user id (or other data) from access token
        // $topic_id = Topic::where('topic_name', $input_topic_name)->first()->topic_id;

        $topic_id = $request->header('topic_id');

        $task_name = 'sentenceMatching'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user_id)
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('user_history.deleted_at', '=', null)
            ->first();

        $user_data = DB::table('users')
            ->where('users.user_id', '=', $user_id)
            ->where('users.deleted_at', '=', null)
            ->first();

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $user_experience_id = $user_data->experience_id;

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user_experience_id)
            ->where('deleted_at', '=', null)
            ->first();

        // print_r($sentence_task);

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $sentence_match_data = DB::table('sentence_matching')
            ->join('translation', 'translation.translation_id', '=', 'sentence_matching.translation_id')
            ->join('bangla_sentence', 'translation.bangla_sent_id', '=', 'bangla_sentence.bangla_sentence_id')
            ->join('english_sentence', 'translation.another_sentence_id', '=', 'english_sentence.english_sentence_id')
            ->where('sentence_matching.sentence_task_id', $sentence_task_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->where('english_sentence.deleted_at', '=', null)
            ->select('sentence_matching.broken_sentence as broken_sentence', 'bangla_sentence.bangla_sentence as bangla_sentence',
                'english_sentence.english_sentence as english_sentence')
            ->get();

        // print_r($sentence_match_data);

        $i = 0;
        $arr = array();

        foreach ($sentence_match_data as $data) {
            $obj = array(
                "bangla_sentence" => "",
                "english_sentence" => "",
            );

            $obj['bangla_sentence'] = $data->bangla_sentence;
            $obj['english_sentence'] = $data->english_sentence;

            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }

    /**
     * getLimited_{TASK_NAME}_ForMixAPI - this function will only be used for MixAPI purposes.
     * This function is needed because the returned JSON file structure is different than
     * other JSON files.
     *
     * We need task_name and target_index fields in JSON files here which weren't needed before.getLimitedMCQ
     */

    public function getLimitedSentenceMatchingTaskForMixAPI(Request $request, $limit)
    {
        // $input_topic_name = $request->header('topic_name'); // given as input in request header
        $user_id = $request->user()->user_id; // retrieve user id (or other data) from access token
        // $topic_id = Topic::where('topic_name', $input_topic_name)->first()->topic_id;

        $topic_id = $request->header('topic_id');

        $task_name = 'sentenceMatching'; //change task_name for every task API you write.
        //then search by this name in the 'task' table and find
        // the id to use task_id.

        $task_data = DB::table('task')
            ->where('task_name', '=', $task_name)
            ->first();

        $task_id = $task_data->task_id; // sentence matching

        $user_history = DB::table('user_history')
            ->orderBy('userhistory_id', 'DESC')
            ->where('user_id', $user_id)
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('user_history.deleted_at', '=', null)
            ->first();

        $user_data = DB::table('users')
            ->where('users.user_id', '=', $user_id)
            ->where('users.deleted_at', '=', null)
            ->first();

        $level_id = 0;
        if ($user_history == null) {
            $level_id = 1;
        } else {
            $level_id = $user_history->level_id;
        }

        $user_experience_id = $user_data->experience_id;

        $sentence_task = DB::table('sentence_task')
            ->where('level_id', $level_id)
            ->where('topic_id', $topic_id)
            ->where('task_id', $task_id)
            ->where('experience_id', $user_experience_id)
            ->where('deleted_at', '=', null)
            ->first();

        // print_r($sentence_task);

        $sentence_task_id = 0;
        if ($sentence_task == null) {
            $sentence_task_id = 1;
        } else {
            $sentence_task_id = $sentence_task->sentence_task_id;
        }

        $sentence_match_data = DB::table('sentence_matching')
            ->join('translation', 'translation.translation_id', '=', 'sentence_matching.translation_id')
            ->join('bangla_sentence', 'translation.bangla_sent_id', '=', 'bangla_sentence.bangla_sentence_id')
            ->join('english_sentence', 'translation.another_sentence_id', '=', 'english_sentence.english_sentence_id')
            ->where('sentence_matching.sentence_task_id', $sentence_task_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->where('english_sentence.deleted_at', '=', null)
            ->select('sentence_matching.broken_sentence as broken_sentence', 'bangla_sentence.bangla_sentence as bangla_sentence',
                'english_sentence.english_sentence as english_sentence')
            ->limit($limit)->get();


        $size_of_queried_data = sizeof($sentence_match_data);

        $sen_match_per_obj = 3;
        $total_array_size = $limit / $sen_match_per_obj;

        $total_arr = array();

        for ($i = 0; $i < $total_array_size; $i++) {
            $cluster_of_3_task = array(
                "Task_Name" => $task_name,
                "Target_Index" => "",
                "Task" => array(),
            );

            for ($j = 0; $j < $sen_match_per_obj; $j++) {
                $obj = array(
                    "bangla_sentence" => "",
                    "english_sentence" => "",
                );

                $current_idx_in_queried_data = $i * $sen_match_per_obj + $j;

                $obj['bangla_sentence'] = $sentence_match_data[$current_idx_in_queried_data]->bangla_sentence;
                $obj['english_sentence'] = $sentence_match_data[$current_idx_in_queried_data]->english_sentence;

                array_push($cluster_of_3_task['Task'], $obj);

                if ($current_idx_in_queried_data >= $size_of_queried_data - 1) {
                    break;
                }
            }
            array_push($total_arr, $cluster_of_3_task);

            if ($current_idx_in_queried_data >= $size_of_queried_data - 1) {
                break;
            }
        }

        return $total_arr;
    }
}
