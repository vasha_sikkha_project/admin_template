<?php

namespace App\Http\Controllers\MobileAPI;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use App\Models\Topic;


class TopicController extends Controller
{
    public function showTopics(Request $request)
    {
        $user = $request->user();

        $topics = DB::table('topic')
                  ->select('topic_id', 'topic_name', 'topic_image')
                  ->get();

        $loved_topics = DB::table('loved_topic')
                       ->select('topic_id')
                       ->where('loved_topic.user_id', $user->user_id)
                       ->get();

        $i = 0;
        $arr = array();

        foreach($topics as $data)
        {
            $obj = array("topic_id" => "",
                         "topic_name" => "",
                         "topic_image" => "",
                         "progress" => "",
                         "is_loved" => "");

            $obj['topic_id'] = $data->topic_id;
            $obj['topic_name'] = $data->topic_name;
            $obj['topic_image'] = $data->topic_image;
            $obj['progress'] = '50%';

            $is_found = 0;

            foreach ($loved_topics as $temp_topic)
            {
                if ($temp_topic->topic_id == $data->topic_id)
                    $is_found = 1;
            }

            $obj['is_loved'] = $is_found;
            $arr[$i++] = $obj;
        }

        return response()->json($arr);
    }
}
