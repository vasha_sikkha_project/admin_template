<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\FixJumbledSentences;
use Illuminate\Http\Request;
use Auth;

class FixJumbledSentencesController extends Controller
{
    public function getLimitedFixJumbledSentences($id)
    {
        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('translation')
            ->join('fix_jumbled_sentence', 'fix_jumbled_sentence.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fix_jumbled_sentence.fix_jum_sen_id', '>=', $id)
            ->where('fix_jumbled_sentence.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.quiz_id', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.question as jumbledSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fix_jumbled_sentence.created_at as created_at')
            ->limit(100)->get()->toArray();
        }else
        {
            $data = \DB::table('translation')
            ->join('fix_jumbled_sentence', 'fix_jumbled_sentence.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fix_jumbled_sentence.fix_jum_sen_id', '>=', $id)
            ->where('fix_jumbled_sentence.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.quiz_id', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.user_id', '=', Auth::user()->user_id)
            ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.question as jumbledSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fix_jumbled_sentence.created_at as created_at')
            ->limit(100)->get()->toArray();
        }
        

        return $data;
    }

    public function getfixJumbledSentencesWithUI($id)
    {
        $fixJumble = app('App\Http\Controllers\ADMIN_WEB\FixJumbledSentencesController')->getLimitedFixJumbledSentences($id);
        return view('sentence.jumbledSentence', compact('fixJumble'));
    }

    public function getLimitedFixJumbledSentencesQuiz($id)
    {
        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('translation')
            ->join('fix_jumbled_sentence', 'fix_jumbled_sentence.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fix_jumbled_sentence.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.quiz_id', '>=', $id)
            ->where('translation.deleted_at', '=', null)
            ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.question as jumbledSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fix_jumbled_sentence.created_at as created_at', 'fix_jumbled_sentence.quiz_id as quiz_id')
            ->limit(100)->get()->toArray();
        }else
        {
            $data = \DB::table('translation')
            ->join('fix_jumbled_sentence', 'fix_jumbled_sentence.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fix_jumbled_sentence.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.quiz_id', '>=', $id)
            ->where('translation.deleted_at', '=', null)
            ->where('fix_jumbled_sentence.user_id', '=', Auth::user()->user_id)
            ->select('fix_jumbled_sentence.fix_jum_sen_id as id', 'fix_jumbled_sentence.question as jumbledSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fix_jumbled_sentence.created_at as created_at', 'fix_jumbled_sentence.quiz_id as quiz_id')
            ->limit(100)->get()->toArray();
        }
        

        return $data;
    }

    public function getfixJumbledSentencesWithUIQuiz($id)
    {
        $fixJumble = app('App\Http\Controllers\ADMIN_WEB\FixJumbledSentencesController')->getLimitedFixJumbledSentencesQuiz($id);
        return view('sentence.jumbledSentence', compact('fixJumble'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedJumbledSentence' => 'required',
        ]);

        $fixJumbleData = FixJumbledSentences::find($id);

        $inputJumble = $request->updatedJumbledSentence; //jumble

        $fixJumbleData->question = $inputJumble;

        $fixJumbleData->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $image = FixJumbledSentences::where('fix_jum_sen_id', $id)->first()->delete();
        return redirect()->back();
    }

}
