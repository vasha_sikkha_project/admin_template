<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileDownloadController extends Controller
{
    public function fillintheblank() {
        $file = storage_path('app/uploads/samplefillintheblank.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function MCQ() {
        $file = storage_path('app/uploads/sampleMCQ.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function TrueFalse() {
        $file = storage_path('app/uploads/sampleTrueFalse.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function JSentence() {
        $file = storage_path('app/uploads/sampleJSentence.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function SMatching() {
        $file = storage_path('app/uploads/SMatching.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function SynAntonyms() {
        $file = storage_path('app/uploads/sampleSynAntonyms.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function ImageMatching() {
        $file = storage_path('app/uploads/sampleImage.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
    public function WordPicture() {
        $file = storage_path('app/uploads/sampleword.csv');
        //$name = basename($file);
        return  response()->download($file);
    }
}
