<?php

namespace App\Http\Controllers\ADMIN_WEB;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\FillInTheBlanks;
use Illuminate\Http\Request;

class FillInTheBlanksController extends Controller
{
    public function getLimitedFillInTheBlanks($id)
    {
        if(Auth::user()->privilege == 10)
        {
            $data = \DB::table('translation')
            ->join('fill_in_the_blanks', 'fill_in_the_blanks.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fill_in_the_blanks.quiz_id', '=', null)
            ->where('fill_in_the_blanks.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('fill_in_the_blanks.fill_blanks_id', '>=', $id)
            ->select('fill_in_the_blanks.fill_blanks_id as id', 'fill_in_the_blanks.incomplete_sentence as incomplete_sentence',
                'fill_in_the_blanks.explanation as explanation', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fill_in_the_blanks.created_at as created_at' )
            ->limit(100)->get();
        }else
        {
            $data = \DB::table('translation')
            ->join('fill_in_the_blanks', 'fill_in_the_blanks.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fill_in_the_blanks.quiz_id', '=', null)
            ->where('fill_in_the_blanks.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('fill_in_the_blanks.user_id', '=', Auth::user()->user_id)
            ->where('fill_in_the_blanks.fill_blanks_id', '>=', $id)
            ->select('fill_in_the_blanks.fill_blanks_id as id', 'fill_in_the_blanks.incomplete_sentence as incomplete_sentence',
                'fill_in_the_blanks.explanation as explanation', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fill_in_the_blanks.created_at as created_at' )
            ->limit(100)->get();
        }
       

        return $data;
    }

    public function getLimitedFillInTheBlanksWithUI($id)
    {
        $fillInTheBlanksSentence = app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksController')->getLimitedFillInTheBlanks($id);
        return view('sentence.fillinthegap', compact('fillInTheBlanksSentence'));
    }


    public function getLimitedFillInTheBlanksWithUIQuiz($id)
    {
        $fillInTheBlanksSentence = app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksController')->getLimitedFillInTheBlanksQuiz($id);
        return view('sentence.fillinthegap', compact('fillInTheBlanksSentence'));
    }

    public function getLimitedFillInTheBlanksQuiz($id)
    {
        if(Auth::user()->privilege == 10)
        {
            $fillInTheBlanksSentence = \DB::table('translation')
            ->join('fill_in_the_blanks', 'fill_in_the_blanks.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('fill_in_the_blanks.quiz_id', '>=', $id)
            ->where('fill_in_the_blanks.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('fill_in_the_blanks.fill_blanks_id as id', 'fill_in_the_blanks.incomplete_sentence as incomplete_sentence',
                'fill_in_the_blanks.explanation as explanation', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fill_in_the_blanks.created_at as created_at','fill_in_the_blanks.quiz_id as quiz_id')
            ->limit(200)->get();
        }else
        {
            $fillInTheBlanksSentence = \DB::table('translation')
            ->join('fill_in_the_blanks', 'fill_in_the_blanks.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('sentence_task.quiz_id', '>=', $id)
            ->where('fill_in_the_blanks.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->where('fill_in_the_blanks.user_id', '=', Auth::user()->user_id)
            ->select('fill_in_the_blanks.fill_blanks_id as id', 'fill_in_the_blanks.incomplete_sentence as incomplete_sentence',
                'fill_in_the_blanks.explanation as explanation', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'fill_in_the_blanks.created_at as created_at','fill_in_the_blanks.quiz_id as quiz_id')
            ->limit(200)->get();
        }

        return $fillInTheBlanksSentence;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedIncompletedSentence' => 'required',
            'updatedExplanation' => 'required',
        ]);

        $fillBlankSentenceData = FillInTheBlanks::find($id);

        $inputTaskSentence = $request->updatedIncompletedSentence; //gapped word
        $inputExp = $request->updatedExplanation; // answer

        // $dict_id = app('App\Http\Controllers\ADMIN_WEB\DictionaryController')->insertNewCoupleInDictionary($inputMeaning, $inputCompleteSentence, $fillBlankSentenceData->dictionary_id);


        $fillBlankSentenceData->incomplete_sentence = $inputTaskSentence;
        $fillBlankSentenceData->explanation = $inputExp;

        $fillBlankSentenceData->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $image = FillInTheBlanks::where('fill_blanks_id', $id)->first()->delete();
        return redirect()->back();
    }
}
