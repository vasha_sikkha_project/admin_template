<?php

namespace App\Http\Controllers\ADMIN_WEB;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\SentenceMatching;
use Illuminate\Http\Request;

class SentenceMatchingController extends Controller
{
    public function getLimitedSentenceMatching($id)
    {
        if(Auth::user()->privilege == 10)
        {
            $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('sentence_matching.sen_match_id', '>=', $id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as brokenSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->limit(100)->get()->toArray();
        }else
        {
            $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('sentence_matching.sen_match_id', '>=', $id)
            ->where('sentence_matching.user_id', '=', Auth::user()->user_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as brokenSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->limit(100)->get()->toArray();
        }
        

        return $data;
    }
    // ->where('sentence_matching.sen_match_id', '>=', $id)
//

    public function getLimitedSentenceMatchingWithUI($id)
    {
        $senMatch = app('App\Http\Controllers\ADMIN_WEB\SentenceMatchingController')->getLimitedSentenceMatching($id);
        return view('sentence.sentenceMatching', compact('senMatch'));
    }


    public function getLimitedSentenceMatchingQuiz($id)
    {
        if(Auth::user()->privilege == 10)
        {
            $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('sentence_matching.quiz_id', '>=', $id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as brokenSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->limit(100)->get()->toArray();
        }else
        {
            $data = \DB::table('translation')
            ->join('sentence_matching', 'sentence_matching.translation_id', '=', 'translation.translation_id')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            ->where('sentence_matching.quiz_id', '>=', $id)
            ->where('sentence_matching.user_id', '=', Auth::user()->user_id)
            ->where('sentence_matching.deleted_at', '=', null)
            ->where('translation.deleted_at', '=', null)
            ->select('sentence_matching.sen_match_id as id', 'sentence_matching.broken_sentence as brokenSentence',
                'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'sentence_matching.created_at as created_at')
            ->limit(100)->get()->toArray();
        }
        

        return $data;
    }
    // ->where('sentence_matching.sen_match_id', '>=', $id)
//

    public function getLimitedSentenceMatchingWithUIQuiz($id)
    {
        $senMatch = app('App\Http\Controllers\ADMIN_WEB\SentenceMatchingController')->getLimitedSentenceMatchingQuiz($id);
        return view('sentence.sentenceMatching', compact('senMatch'));
    }

    

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedBrokenSentence' => 'required',
        ]);

        $senMatchData = SentenceMatching::find($id);

        $inputSenMatchData = $request->updatedBrokenSentence; //jumble

        $senMatchData->broken_sentence = $inputSenMatchData;

        $senMatchData->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $image = SentenceMatching::where('sen_match_id', $id)->first()->delete();
        return redirect()->back();
    }

}
