<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\Translation;
use Illuminate\Http\Request;
use Auth;
class TranslationController extends Controller
{
    public function getData($id)
    {
        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('translation')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            // ->where('translation.translation_id', '=', $id)
            ->where('translation.deleted_at', '=', null)
            ->select('translation.translation_id as id', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'translation.created_at as created_at')
            ->limit(100)->get();
        }else
        {
            $data = \DB::table('translation')
            ->join('bangla_sentence', 'bangla_sentence.bangla_sentence_id', '=', 'translation.bangla_sent_id')
            ->join('english_sentence', 'english_sentence.english_sentence_id', '=', 'translation.another_sentence_id')
            // ->where('translation.translation_id', '=', $id)
            ->where('translation.user_id', '=' , Auth::user()->user_id)
            ->where('translation.deleted_at', '=', null)
            ->select('translation.translation_id as id', 'bangla_sentence.bangla_sentence as banglaSentence',
                'english_sentence.english_sentence as englishSentence', 'translation.created_at as created_at')
            ->limit(100)->get();
        }
        

        return $data;
    }

    public function getLimitedTranslationWithUI($id)
    {
        $dataTranslartion = app('App\Http\Controllers\ADMIN_WEB\TranslationController')->getData($id);

        return view('sentence.translation', compact('dataTranslartion'));
    }


    public function update_A_Particular_Entry($id, $englishSentence, $banglaSentence)
    {
        $translationData = Translation::find($id);

        $inputBanglaSentence = $banglaSentence;
        $inputAnotherSentence = $englishSentence;


        $oldBanglaSentenceID = $translationData->bangla_sent_id;
        $oldAnotherSentenceID = $translationData->another_sentence_id;

        $newBanglaSentenceId = app('App\Http\Controllers\ADMIN_WEB\BanglaSentencesController')->updateAnEntry($oldBanglaSentenceID, $inputBanglaSentence);
        $newAnotherSentenceId = app('App\Http\Controllers\ADMIN_WEB\EnglishSentencesController')->updateAnEntry($oldAnotherSentenceID, $inputAnotherSentence);

        return $id;
    }



    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedBanglaSentence' => 'required',
            'updatedEnglishSentence' => 'required',
        ]);

        $translationData = Translation::find($id);

        $inputBanglaSentence = $request->updatedBanglaSentence;
        $inputAnotherSentence = $request->updatedEnglishSentence;


        $oldBanglaSentenceID = $translationData->bangla_sent_id;
        $oldAnotherSentenceID = $translationData->another_sentence_id;


        $newBanglaSentenceId = app('App\Http\Controllers\ADMIN_WEB\BanglaSentencesController')->updateAnEntry($oldBanglaSentenceID, $inputBanglaSentence);
        $newAnotherSentenceId = app('App\Http\Controllers\ADMIN_WEB\EnglishSentencesController')->updateAnEntry($oldAnotherSentenceID, $inputAnotherSentence);

        $translationData->save();

        return redirect()->back();
    }

    public function deleteTranslationEntry($id)
    {
        $image = Translation::where('translation_id', $id)->first()->delete();
        return redirect()->back();
    }
}
