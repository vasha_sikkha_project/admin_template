<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\SynonymAntonym;
use View;

class SynonymAntonymController extends Controller
{
    public function getLimitedSynonymAntonym( $id )
    {

        if(Auth::user()->privilege >= 10)
        {
                $data = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.base_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->select('synonyms_antonyms.synonym_antonym_id as id',
                'english_resource.word as baseWord' )
                ->limit(25)->get();

        $data1 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.synonym_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->select('english_resource.word as synonymWord')
                ->limit(25)->get();


        $data2 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.antonym_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->select('english_resource.word as antonymWord', 'synonyms_antonyms.created_at as created_at')
                ->limit(25)->get();

        $finalArray = array();

        for( $i = 0 ; $i < sizeof($data) ; $i++ )
        {
              $finalArray[$i] = array();
        }

        $i = 0;

        foreach ($data as $d) {
                $finalArray[$i]['id'] = $d->id;
                $finalArray[$i]['baseWord'] = $d->baseWord;
                $i++;
        }
        $i = 0;
        foreach ($data1 as $d) {
                $finalArray[$i]['synonymWord'] = $d->synonymWord;
                $i++;
        }
        $i = 0;
        foreach ($data2 as $d) {
                $finalArray[$i]['antonymWord'] = $d->antonymWord;
                $finalArray[$i]['created_at'] = $d->created_at;
                $i++;
        }
        }
        
        
        else
        {
                $data = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.base_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('synonyms_antonyms.synonym_antonym_id as id',
                'english_resource.word as baseWord' )
                ->limit(25)->get();

        $data1 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.synonym_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('english_resource.word as synonymWord')
                ->limit(25)->get();


        $data2 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.antonym_word_id')
                ->where('synonyms_antonyms.synonym_antonym_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('english_resource.word as antonymWord', 'synonyms_antonyms.created_at as created_at')
                ->limit(25)->get();

        $finalArray = array();

        for( $i = 0 ; $i < sizeof($data) ; $i++ )
        {
              $finalArray[$i] = array();
        }

        $i = 0;

        foreach ($data as $d) {
                $finalArray[$i]['id'] = $d->id;
                $finalArray[$i]['baseWord'] = $d->baseWord;
                $i++;
        }
        $i = 0;
        foreach ($data1 as $d) {
                $finalArray[$i]['synonymWord'] = $d->synonymWord;
                $i++;
        }
        $i = 0;
        foreach ($data2 as $d) {
                $finalArray[$i]['antonymWord'] = $d->antonymWord;
                $finalArray[$i]['created_at'] = $d->created_at;
                $i++;
        }
        }

        

        // dd($finalArray);
;
        return $finalArray;
    }


    public function getLimitedSynonymAntonymWithUI( $id )
    {

        
        $finalArray =  app('App\Http\Controllers\ADMIN_WEB\SynonymAntonymController')->getLimitedSynonymAntonym($id);

        return View::make('word.synonymantonym')->with('synonymantonym', $finalArray);
    }


    public function getLimitedSynonymAntonymQuiz( $id )
    {

        if(Auth::user()->privilege >= 10)
        {
                $data = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.base_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->select('synonyms_antonyms.synonym_antonym_id as id',
                'english_resource.word as baseWord' )
                ->limit(25)->get();

        $data1 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.synonym_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->select('english_resource.word as synonymWord')
                ->limit(25)->get();


        $data2 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.antonym_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->select('english_resource.word as antonymWord', 'synonyms_antonyms.created_at as created_at')
                ->limit(25)->get();

        $finalArray = array();

        for( $i = 0 ; $i < sizeof($data) ; $i++ )
        {
              $finalArray[$i] = array();
        }

        $i = 0;

        foreach ($data as $d) {
                $finalArray[$i]['id'] = $d->id;
                $finalArray[$i]['baseWord'] = $d->baseWord;
                $i++;
        }
        $i = 0;
        foreach ($data1 as $d) {
                $finalArray[$i]['synonymWord'] = $d->synonymWord;
                $i++;
        }
        $i = 0;
        foreach ($data2 as $d) {
                $finalArray[$i]['antonymWord'] = $d->antonymWord;
                $finalArray[$i]['created_at'] = $d->created_at;
                $i++;
        }
        }
        
        
        else
        {
                $data = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.base_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('synonyms_antonyms.synonym_antonym_id as id',
                'english_resource.word as baseWord' )
                ->limit(25)->get();

        $data1 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.synonym_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('english_resource.word as synonymWord')
                ->limit(25)->get();


        $data2 = \DB::table('synonyms_antonyms')

                ->join('english_resource','english_resource.english_word_id','=','synonyms_antonyms.antonym_word_id')
                ->where('synonyms_antonyms.quiz_id', '>=' , $id)
                ->where('synonyms_antonyms.user_id', '=' , Auth::user()->user_id)
                ->select('english_resource.word as antonymWord', 'synonyms_antonyms.created_at as created_at')
                ->limit(25)->get();

        $finalArray = array();

        for( $i = 0 ; $i < sizeof($data) ; $i++ )
        {
              $finalArray[$i] = array();
        }

        $i = 0;

        foreach ($data as $d) {
                $finalArray[$i]['id'] = $d->id;
                $finalArray[$i]['baseWord'] = $d->baseWord;
                $i++;
        }
        $i = 0;
        foreach ($data1 as $d) {
                $finalArray[$i]['synonymWord'] = $d->synonymWord;
                $i++;
        }
        $i = 0;
        foreach ($data2 as $d) {
                $finalArray[$i]['antonymWord'] = $d->antonymWord;
                $finalArray[$i]['created_at'] = $d->created_at;
                $i++;
        }
        }

        

        // dd($finalArray);
;
        return $finalArray;
    }


    public function getLimitedSynonymAntonymWithUIQuiz( $id )
    {

        
        $finalArray =  app('App\Http\Controllers\ADMIN_WEB\SynonymAntonymController')->getLimitedSynonymAntonymQuiz($id);

        return View::make('word.synonymantonym')->with('synonymantonym', $finalArray);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedBaseWord' => 'required',
            'updatedSynWord' => 'required',
            'updatedAntWord' => 'required'
        ]);

        $synAntData = SynonymAntonym::find($id);


        $inputBaseWord = $request->updatedBaseWord;
        $inputSynWord = $request->updatedSynWord;
        $inputAntWord = $request->updatedAntWord;

        $newBaseId = app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->insertAndReturnID($inputBaseWord);
        "<?php echo $newBaseId; ?>";
        $newSynId = app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->insertAndReturnID($inputSynWord);

        $newAntId = app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->insertAndReturnID($inputAntWord);


        $synAntData->base_word_id = $newBaseId;
        $synAntData->synonym_word_id = $newSynId;
        $synAntData->antonym_word_id = $newAntId;
        $synAntData->save();

        return redirect('word');
    }

}
