<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests;
use App\Models\Topic;
use App\Http\Resources\TopicResource;
use View;

class TopicController extends Controller
{
    /** @var  TopicControllerRepository */
    private $topicControllerRepository;

    /**
     * Display a listing of the TopicController.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $topicResources = Topic::paginate(15);

        return TopicResource::collection($topicResources);
    }

    /**
     * Show the form for creating a new TopicController.
     *
     * @return Response
     */
    public function create()
    {
        return view('topic_controllers.create');
    }

    /**
     * Store a newly created TopicController in storage.
     *
     * @param CreateTopicControllerRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // $topic = $request->isMethod('put') ? Topic::findOrFail($request->id) : new Topic;
        // $topic->id = $request->input('id');
        // $topic->title = $request->input('topic_name');
        
        // if($topic->save()) {
        //     return new TopicResource($topic);
        // }




        // $user = new Topic([
        //     'id' => $request->id,
        //     'topic_name' => $request->topic_name
        // ]);
        // $user->save();
        

        \DB::table('topics')->insert(
            ['id' => $request->id, 
            'topic_name' => $request->topic_name]
        );
    }

    
    public function show($id)
    {
        $topic = Topic::findOrFail($id);

        return new TopicResource($topic);
    }

    /** 
     * Show the form for editing the specified TopicController.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified TopicController in storage.
     *
     * @param  int              $id
     * @param UpdateTopicControllerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTopicControllerRequest $request)
    {
        
    }

    /**
     * Remove the specified TopicController from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        
    }


    public function getAllTopics()
    {
        return View::make('topic.allTopics')->with('allTopics', Topic::all() );
    }

    public function deleteTopic($id)
    {
        $image = Topic::where('topic_id', $id)->first()->delete();
        return redirect()->back();
    }




}
