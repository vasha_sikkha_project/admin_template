<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\BanglaSentences;
use App\Models\EnglishSentence;
use Illuminate\Http\Request;
use View;

class BanglaSentencesController extends Controller
{

    private $dataBangla;
    private $dataEnglish;

    public function getData()
    {
        $dataBangla = BanglaSentences::all();
        $dataEnglish = EnglishSentence::all();
        return View::make('sentence.sentencetable')
            ->with('dataBangla', $dataBangla)
            ->with('dataEnglish', $dataEnglish);
    }

    public function getLimitedBanglaSentencesWithUI($id)
    {
        $dataBangla = \DB::table('bangla_sentence')
            ->where('bangla_sentence.bangla_sentence_id', '>=', $id)
            ->where('bangla_sentence.deleted_at', '=', null)
            ->limit(200)->get();

        return view('sentence.bangla_sentence', compact('dataBangla'));
    }

    public function insertAndReturnID($sentence)
    {
        $data = BanglaSentences::where('bangla_sentence', '=', $sentence)->first();

        if ($data == null) {
            $data = BanglaSentences::create(
                [
                    'bangla_sentence' => $sentence,
                ]
            );
        }

        return $data->bangla_sentence_id;
    }

    public function update(Request $request, $id)
    {
        $wrd = $request->input('updatedBanglaWord');
        $op = $id;

        \DB::update('update bangla_sentence set bangla_sentence = ? where bangla_sentence_id = ?', [$wrd, $op]);
        return redirect()->back();
    }

    public function deleteBanglaSentence(Request $request, $id)
    {
        $image = BanglaSentences::where('bangla_sentence_id', $id)->first()->delete();
        return redirect()->back();
    }

    /**
     *
     * updateAnEntry() - needed for dictionary
     * @update_A_Particular_English_Word() - this function is needed for dictionary words update.
     */

    public function update_A_Particular_Bangla_Sentence($sentence)
    {
        $data = BanglaSentences::where('bangla_sentence', '=', $sentence)->first();
        $data->bangla_sentence = $sentence;
        $data->save();
        return $data;
    }

    public function updateAnEntry($id, $sentence)
    {
        $data = BanglaSentences::find($id);
        $data->bangla_sentence = $sentence;
        $data->save();
        return $id;
    }

}
