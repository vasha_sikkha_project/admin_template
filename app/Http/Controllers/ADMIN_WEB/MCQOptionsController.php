<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Facade;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MCQOptionsController extends Controller
{
    public function getLimitedMCQOptions($id)
    {
        $mcqOptions = \DB::table('mcq_options')
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq_options.dictionary_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->where('mcq_options.options_id', '>=', $id)
            ->where('mcq.deleted_at', '=', null)
            ->select('english_resource.word as options','mcq_options.mcq_id as mcqID')
            ->limit(25)->get();
        
        return $mcqOptions;
    }

    public function getMCQOptions($id)
    {
        $mcqOptions = \DB::table('mcq_options')
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq_options.dictionary_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('mcq', 'mcq.mcq_id', '=', 'mcq_options.mcq_id')
            ->where('mcq.mcq_id', '=', $id)
            ->where('mcq_options.deleted_at', '=', null)
            ->select('english_resource.word as options_of_mcq')
            ->get();
        
        return $mcqOptions;
    }
}
