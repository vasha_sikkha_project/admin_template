<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\Dictionary;
use Illuminate\Http\Request;
use View;

class DictionaryController extends Controller
{
    public function getLimitedDictionary($id)
    {

        $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->where('dictionary.dictionary_id', '>=', $id)
            ->where('dictionary.deleted_at', '=', null)
            ->select('dictionary.dictionary_id as id', 'bangla_resource.word as banglaWord',
                'english_resource.word as englishWord', 'dictionary.created_at as created_at')
            ->limit(25)->get();

        return $data;
    }

    public function getLimitedDictionaryWithUI($id)
    {

        $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->where('dictionary.dictionary_id', '>=', $id)
            ->where('dictionary.deleted_at', '=', null)
            ->select('dictionary.dictionary_id as id', 'bangla_resource.word as banglaWord',
                'english_resource.word as englishWord', 'dictionary.created_at as created_at')
            ->limit(100)->get();

        return View::make('word.dictionary')->with('dictionary', $data);
    }

    public function insertAndReturnID($banglaID, $englishID)
    {

        $data = Dictionary::create(
            [
                'another_word_id' => $englishID,
                'bangla_word_id' => $banglaID,
                'language_id' => '1',
            ]
        );

        return $data->id;
    }

    public function update_A_Particular_Entry($id, $englishWord, $banglaWord)
    {
        $dictionaryData = Dictionary::find($id);

        $inputAnotherWord = $englishWord;
        $inputBanglaWord = $banglaWord;


        $oldBanglaWordID = $dictionaryData->bangla_word_id;
        $oldAnotherWordID = $dictionaryData->another_word_id;

        $newBanglaResId = app('App\Http\Controllers\ADMIN_WEB\BanglaResourcesController')->updateAnEntry($oldBanglaWordID, $inputBanglaWord);

        $newAnotherResId = app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->updateAnEntry($oldAnotherWordID, $inputAnotherWord);

        return $id;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedBanglaWord' => 'required',
            'updatedEnglishWord' => 'required',
        ]);

        $dictionaryData = Dictionary::find($id);

        $inputBanglaWord = $request->updatedBanglaWord;
        $inputAnotherWord = $request->updatedEnglishWord;

        $oldBanglaWordID = $dictionaryData->bangla_word_id;
        $oldAnotherWordID = $dictionaryData->another_word_id;

        $newBanglaResId = app('App\Http\Controllers\ADMIN_WEB\BanglaResourcesController')->updateAnEntry($oldBanglaWordID, $inputBanglaWord);

        $newAnotherResId = app('App\Http\Controllers\ADMIN_WEB\EnglishResourcesController')->updateAnEntry($oldAnotherWordID, $inputAnotherWord);

        return redirect()->back();
    }

    public function deleteDictionaryEntry($id)
    {
        $image = Dictionary::where('dictionary_id', $id)->first()->delete();
        return redirect()->back();
    }

}
