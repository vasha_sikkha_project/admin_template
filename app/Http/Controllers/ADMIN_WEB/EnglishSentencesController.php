<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\EnglishSentence;

class EnglishSentencesController extends Controller
{
    private $dataEnglish;

    public function getData()
    {
        $dataEnglish = EnglishSentence::all();
        return view('homePage.table',compact('dataEnglish'));
        // return view('welcome');
    }

    public function getLimitedEnglishSentencesWithUI( $id )
    {
        $dataEnglish = \DB::table('english_sentence')
                   ->where('english_sentence.english_sentence_id', '>=' , $id )
                   ->where('english_sentence.deleted_at', '=' , null)
                   ->limit(200)->get();

         return view('sentence.english_sentence',compact('dataEnglish'));
    }

    public function insertAndReturnID( $sentence )
    {
        $data = EnglishSentence::where('english_sentence', '=', $sentence)->first();

        if($data == null)
        {
            $data = EnglishSentence::create(
                [
                    'english_sentence'=> $sentence
                ]
            );
        }


        return $data->english_sentence_id;
    }

    public function update(Request $request, $id)
    {
        $wrd = $request->input('updatedEnglishWord'); //don't be confused seeing 'englishWORD' here. it's just a part of the form req that is coming.
        $op = $id;

        \DB::update('update english_sentence set english_sentence = ? where english_sentence_id = ?',[$wrd,$op]);
        return redirect()->back();
    }


    public function deleteEnglishSentence(Request $request, $id)
    {
        $image = EnglishSentence::where('english_sentence_id', $id)->first()->delete();
        return redirect()->back();
        // return view('test',compact('id'));

    }


    /**
     *
     * updateAnEntry() - needed for dictionary
     * @update_A_Particular_English_Word() - this function is needed for dictionary words update.
     */

    public function update_A_Particular_English_Sentence($sentence)
    {
        $data = EnglishSentence::where('english_sentence', '=', $sentence)->first();
        $data->english_sentence = $sentence;
        $data->save();
        return $data;
    }

    public function updateAnEntry($id, $sentence)
    {
        $data = EnglishSentence::find($id);
        $data->english_sentence = $sentence;
        $data->save();
        return $id;
    }

}
