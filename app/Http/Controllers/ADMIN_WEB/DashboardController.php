<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use View;
class DashboardController extends Controller
{
    public function getDashboard()
    {
        return view('adminDashboard/dashboard')
        ->with('admindata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getAdminNumber_created_at())
        ->with('studentdata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getStudentNumber_created_at())
        ->with('teacherdata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getTeacherNumber_created_at())
        ->with('taskperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->gettaskwiseuserperformance())
        ->with('taskfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->gettaskwiseuserfailure())
        ->with('mcqperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(2))
        ->with('mcqfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserfailure(2))
        ->with('fillintheblanksperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(3))
        ->with('fillintheblanksfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserfailure(3))
        ->with('vocabularyperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(1))
        ->with('vocabularyfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserfailure(1))
        ->with('SynonymAntonymperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(5))
        ->with('SynonymAntonymfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserfailure(5));
        
    }

    public function getChart()
    {
        return view('adminDashboard/chartsOfSystem')
        ->with('topicperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->gettaskwiseuserperformance())
        ->with('mcqperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(2))
        ->with('mcqfailuredata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserfailure(2))
        ->with('fillintheblanksperformancedata', app('App\Http\Controllers\ADMIN_WEB\UserController')->getuserperformance(3));
    }

   
}
