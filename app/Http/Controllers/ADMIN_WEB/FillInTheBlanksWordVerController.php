<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\FillInTheBlanksWordVer;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\EnglishResources;
use App\Models\BanglaResources;
use App\Models\Dictionary;
use View;

class FillInTheBlanksWordVerController extends Controller
{

    public function getLimitedFillInTheBlanksWordVer($id)
    {

        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('fill_in_the_blanks_word_ver', 'fill_in_the_blanks_word_ver.dictionary_id', '=', 'dictionary.dictionary_id')
            ->where('fill_in_the_blanks_word_ver.fill_blanks_word_id', '>=', $id)
            ->where('fill_in_the_blanks_word_ver.deleted_at', '=', null)
            ->where('fill_in_the_blanks_word_ver.quiz_id', '=', null)
            ->where('dictionary.deleted_at', '=', null) // jhamela
            ->select(
                'fill_in_the_blanks_word_ver.fill_blanks_word_id as id',
                'fill_in_the_blanks_word_ver.incomplete_word as TaskWord',
                'bangla_resource.word as Meaning',
                'english_resource.word as CompleteWord',
                'fill_in_the_blanks_word_ver.created_at as created_at'
            )
            ->limit(100)->get();
        }else
        {
            $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('fill_in_the_blanks_word_ver', 'fill_in_the_blanks_word_ver.dictionary_id', '=', 'dictionary.dictionary_id')
            ->where('fill_in_the_blanks_word_ver.fill_blanks_word_id', '>=', $id)
            ->where('fill_in_the_blanks_word_ver.quiz_id', '=', null)
            ->where('fill_in_the_blanks_word_ver.deleted_at', '=', null)
            ->where('dictionary.deleted_at', '=', null) // jhamela
            ->where('fill_in_the_blanks_word_ver.user_id', '=', Auth::user()->user_id)
            ->select(
                'fill_in_the_blanks_word_ver.fill_blanks_word_id as id',
                'fill_in_the_blanks_word_ver.incomplete_word as TaskWord',
                'bangla_resource.word as Meaning',
                'english_resource.word as CompleteWord',
                'fill_in_the_blanks_word_ver.created_at as created_at'
                
            )
            ->limit(100)->get();
        }
        

        // return view('words.dictionarytable',compact('data'));

        return $data;
    }


    public function getLimitedFillInTheBlanksWordVerWithUI($id)
    {

        $data = app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksWordVerController')->getLimitedFillInTheBlanksWordVer($id);

        return View::make('word.fillInTheBlanks')->with('fillintheblankswordver', $data );
    }

    public function getLimitedFillInTheBlanksWordVerQuiz($id)
    {

        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('fill_in_the_blanks_word_ver', 'fill_in_the_blanks_word_ver.dictionary_id', '=', 'dictionary.dictionary_id')
            ->where('fill_in_the_blanks_word_ver.deleted_at', '=', null)
            ->where('fill_in_the_blanks_word_ver.quiz_id', '>=', $id)
            ->where('dictionary.deleted_at', '=', null) // jhamela
            ->select(
                'fill_in_the_blanks_word_ver.fill_blanks_word_id as id',
                'fill_in_the_blanks_word_ver.incomplete_word as TaskWord',
                'bangla_resource.word as Meaning',
                'english_resource.word as CompleteWord',
                'fill_in_the_blanks_word_ver.created_at as created_at',
                'fill_in_the_blanks_word_ver.quiz_id as quiz_id'
            )
            ->limit(100)->get();
        }else
        {
            $data = \DB::table('dictionary')
            ->join('bangla_resource', 'bangla_resource.bangla_word_id', '=', 'dictionary.bangla_word_id')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('fill_in_the_blanks_word_ver', 'fill_in_the_blanks_word_ver.dictionary_id', '=', 'dictionary.dictionary_id')
            ->where('fill_in_the_blanks_word_ver.deleted_at', '=', null)
            ->where('fill_in_the_blanks_word_ver.quiz_id', '>=', $id)
            ->where('dictionary.deleted_at', '=', null) // jhamela
            ->where('fill_in_the_blanks_word_ver.user_id', '=', Auth::user()->user_id)
            ->select(
                'fill_in_the_blanks_word_ver.fill_blanks_word_id as id',
                'fill_in_the_blanks_word_ver.incomplete_word as TaskWord',
                'bangla_resource.word as Meaning',
                'english_resource.word as CompleteWord',
                'fill_in_the_blanks_word_ver.created_at as created_at',
                'fill_in_the_blanks_word_ver.quiz_id as quiz_id'
            )
            ->limit(100)->get();
        }
        

        // return view('words.dictionarytable',compact('data'));

        return $data;
    }


    public function getLimitedFillInTheBlanksWordVerWithUIQuiz($id)
    {

        $data = app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksWordVerController')->getLimitedFillInTheBlanksWordVerQuiz($id);

        return View::make('word.fillInTheBlanks')->with('fillintheblankswordver', $data );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedTaskWord' => 'required',
            'updatedCompleteWord' => 'required',
            'updatedMeaning' => 'required',
        ]);

        $fillWordVerData = FillInTheBlanksWordVer::find($id);

        $inputTaskWord = $request->updatedTaskWord; //gapped word

        $inputCompleteWord = $request->updatedCompleteWord; // english word
        $inputMeaning = $request->updatedMeaning; // bangla word

        $dict_id = app('App\Http\Controllers\ADMIN_WEB\DictionaryController')->update_A_Particular_Entry($fillWordVerData->dictionary_id, $inputCompleteWord, $inputMeaning);

        $fillWordVerData->incomplete_word = $inputTaskWord;
        $fillWordVerData->save();

        return redirect()->back();
    }

    public function deleteWordGap( $id)
    {
        $image = FillInTheBlanksWordVer::where('fill_blanks_word_id', $id)->first()->delete();
        return redirect()->back();
    }
}
