<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use View;
use Carbon;

class UserController extends Controller
{
    public function getUserData($id)
    {
        $allUser = \DB::table('users')
        ->where('users.user_id', '>=' , $id)
        ->limit(100)->get();

        return View::make('user.userTable')
            ->with('allUser', User::all());
    }

    public function getAdminsData($id)
    {
        $allUser = \DB::table('users')
        ->where('users.user_id', '>=' , $id)
        ->where('users.privilege', '>=' , 10)
        ->limit(100)->get();

        return  $allUser;
    }

    public function getAdminsDataWihUI($id)
    {
        return View::make('user.userTable')
            ->with('allUser', $this->getAdminsData($id));
    }


    public function getTeacherDataWithUI($id)
    {
        return View::make('user.userTable')
            ->with('allUser',  $this->getTeacherData($id));
    }

    public function getTeacherData($id)
    {
        $allUser = \DB::table('users')
        ->where('users.user_id', '>=' , $id)
        ->where('users.privilege', '=' , 3)
        ->limit(100)->get();

        return $allUser;
    }
    public function getNewDataWithUI($id)
    {
        return View::make('user.userTable')
            ->with('allUser',  $this->getNewData($id));
    }

    public function getNewData($id)
    {
        $allUser = \DB::table('users')
        ->where('users.user_id', '>=' , $id)
        ->where('users.privilege', '=' , 2)
        ->limit(100)->get();

        return $allUser;
    }

    public function getStudentDataWithUI($id)
    {

        return View::make('user.userTable')
            ->with('allUser',  $this->getStudentData($id));
    }

    public function getStudentData($id)
    {
        $allUser = \DB::table('users')
        ->where('users.user_id', '>=' , $id)
        ->where('users.privilege', '=' , 1)
        ->limit(100)->get();

        return   $allUser;
    }

    public function getAdminNumber_created_at()
    {
        $admindata = \DB::table('users')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            ->where('users.privilege', '>=' , 10)
            ->select(\DB::raw('DATE(users.created_at) as date'), \DB::raw('count(*) as total'))
            ->groupBy('date')
            ->limit(25)->get();

        
        return $admindata;
    }


    public function getStudentNumber_created_at()
    {
        $studentdata = \DB::table('users')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            ->where('users.privilege', '=' , 1)
            ->select(\DB::raw('DATE(users.created_at) as date'), \DB::raw('count(*) as total'))
            ->groupBy('date')
            ->limit(25)->get();

        
        return $studentdata;
    }

    public function getTeacherNumber_created_at()
    {
        $teacherdata = \DB::table('users')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            ->where('users.privilege', '=' , 3)
            ->select(\DB::raw('DATE(users.created_at) as date'), \DB::raw('count(*) as total'))
            ->groupBy('date')
            ->limit(25)->get();

        
        return $teacherdata;
    }


    public function gettaskwiseuserperformance()
    {
        $taskperformancedata = \DB::table('user_performance')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            
            ->where('user_performance.solved', '=' , 1)
            
            ->select(\DB::raw('user_performance.task_id as task'), \DB::raw('count(*) as total'))
            ->groupBy('task')
            ->limit(25)->get();

        
        return $taskperformancedata;
    }

    public function gettaskwiseuserfailure()
    {
        $taskfailuredata = \DB::table('user_performance')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            
            ->where('user_performance.solved', '=' , 0)
            ->select(\DB::raw('user_performance.task_id as task'), \DB::raw('count(*) as total'))
            ->groupBy('task')
            ->limit(25)->get();

        
        return $taskfailuredata;
    }

    public function getuserperformance($id)
    {
        $performancedata = \DB::table('user_performance')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            ->where('user_performance.task_id', '=' , $id)
            ->where('user_performance.solved', '=' , 1)
            ->select(\DB::raw('user_performance.level_id as level'), \DB::raw('count(*) as total'))
            ->groupBy('level')
            ->limit(25)->get();

        
        return $performancedata;
    }


    public function getuserfailure($id)
    {
        $failuredata = \DB::table('user_performance')
        //    ->select(Carbon\Carbon::createFromFormat('Y-m-d', 'users.created_at') , \DB::raw('count(*) as total'))
            // ->select('users.created_at'->format('d') , \DB::raw('count(*) as total'))
            // ->groupBy('day')
            // ->groupBy('users.created_at')
            ->where('user_performance.task_id', '=' , $id)
            ->where('user_performance.solved', '=' , 0)
            ->select(\DB::raw('user_performance.level_id as level'), \DB::raw('count(*) as total'))
            ->groupBy('level')
            ->limit(25)->get();

        
        return $failuredata;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedPrivilege' => 'required',
        ]);

        $userData = User::find($id);

        $inputPrivilege = $request->updatedPrivilege;

        $userData->privilege = $inputPrivilege;

        $userData->save();

        return redirect()->back();
    }

    public function updateYourProfile(Request $request, $id)
    {

        $this->validate($request, [
            'updatedFullName' => 'required',
            'updatedMobileNo' => 'required',
            'updatedAge' => 'required',
            'updatedInstitution' => 'required',
        ]);


        //Mobile No update kora hoy nai. database e dhukano lagbe.
        $userData = User::find($id);

        $userData->name = $request->updatedFullName;
        $userData->age = $request->updatedAge;
        $userData->mobile_number = $request->updatedMobileNo;
        $userData->institution = $request->updatedInstitution;
        $userData->present_address = $request->updatedAddress;


        $userData->save();


        return redirect()->back();
    }



    public function delete($id)
    {
        $image = User::where('user_id', $id)->first()->delete();
        return redirect()->back();
    }

}
