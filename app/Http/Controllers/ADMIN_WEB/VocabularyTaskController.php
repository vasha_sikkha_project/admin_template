<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
class VocabularyTaskController extends Controller
{
    public function getLimitedVocabulary( $id )
    {
        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('dictionary')
                ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
                ->join('english_resource','english_resource.english_word_id','=','dictionary.another_word_id')
                ->join('vocabulary_task','vocabulary_task.dictionary_id','=','dictionary.dictionary_id')
                ->where('vocabulary_task.vocabulary_id', '>=' , $id)
                ->where('vocabulary_task.deleted_at', '=' , null)
                ->select('vocabulary_task.vocabulary_id as id','bangla_resource.word as BanglaWord',
                'english_resource.word as EnglishWord' , 'vocabulary_task.created_at as created_at')
                ->limit(25)->get();
        }else
        {
            $data = \DB::table('dictionary')
                ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
                ->join('english_resource','english_resource.english_word_id','=','dictionary.another_word_id')
                ->join('vocabulary_task','vocabulary_task.dictionary_id','=','dictionary.dictionary_id')
                ->where('vocabulary_task.vocabulary_id', '>=' , $id)
                ->where('vocabulary_task.deleted_at', '=' , null)
                ->where('vocabulary_task.user_id', '=' , Auth::user()->user_id)
                ->select('vocabulary_task.vocabulary_id as id','bangla_resource.word as BanglaWord',
                'english_resource.word as EnglishWord' , 'vocabulary_task.created_at as created_at')
                ->limit(25)->get();
        }
        

        // return view('words.dictionarytable',compact('data')); 
        
        return $data;
        
    }

    public function getLimitedVocabularyWithUI($id)
    {
        $vocabulary = app('App\Http\Controllers\ADMIN_WEB\VocabularyTaskController')->getLimitedVocabulary($id);

        return view('word.vocabulary', compact('vocabulary'));
    }

    public function getLimitedVocabularyQuiz( $id )
    {
        if(Auth::user()->privilege >= 10)
        {
            $data = \DB::table('dictionary')
                ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
                ->join('english_resource','english_resource.english_word_id','=','dictionary.another_word_id')
                ->join('vocabulary_task','vocabulary_task.dictionary_id','=','dictionary.dictionary_id')
                ->where('vocabulary_task.quiz_id', '>=' , $id)
                ->where('vocabulary_task.deleted_at', '=' , null)
                ->select('vocabulary_task.vocabulary_id as id','bangla_resource.word as BanglaWord',
                'english_resource.word as EnglishWord' , 'vocabulary_task.created_at as created_at')
                ->limit(25)->get();
        }else
        {
            $data = \DB::table('dictionary')
                ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
                ->join('english_resource','english_resource.english_word_id','=','dictionary.another_word_id')
                ->join('vocabulary_task','vocabulary_task.dictionary_id','=','dictionary.dictionary_id')
                ->where('vocabulary_task.quiz_id', '>=' , $id)
                ->where('vocabulary_task.deleted_at', '=' , null)
                ->where('vocabulary_task.user_id', '=' , Auth::user()->user_id)
                ->select('vocabulary_task.vocabulary_id as id','bangla_resource.word as BanglaWord',
                'english_resource.word as EnglishWord' , 'vocabulary_task.created_at as created_at')
                ->limit(25)->get();
        }
        

        // return view('words.dictionarytable',compact('data')); 
        
        return $data;
        
    }

    public function getLimitedVocabularyWithUIQuiz($id)
    {
        $vocabulary = app('App\Http\Controllers\ADMIN_WEB\VocabularyTaskController')->getLimitedVocabularyQuiz($id);

        return view('word.vocabulary', compact('vocabulary'));
    }
   
}
