<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\EnglishResources;
use View;

class EnglishResourcesController extends Controller
{
    public function insertAndReturnID($word)
    {
        $data = EnglishResources::where('word', '=', $word)->first();

        if ($data == null) {
            $data = EnglishResources::create(
                [
                    'word' => $word,
                    'image_link' => 'ni IMG'
                ]
            );
        } else if ($data->deleted_at != '') {
            $data->deleted_at = '';
        }

        return $data->english_word_id;
    }


    public function getLimitedEnglishWords($id)
    {
        $english = \DB::table('english_resource')
            ->where('english_resource.english_word_id', '>=', $id)
            ->where('english_resource.deleted_at', '=', null)
            ->limit(100)->get();

        return $english;
    }


    public function getLimitedEnglishWordsWithUI($id)
    {
        $dataEnglish = \DB::table('english_resource')
            ->where('english_resource.english_word_id', '>=', $id)
            ->where('english_resource.deleted_at', '=', null)
            ->limit(200)->get();

            return View::make('word.englishWord')
                         ->with('dataEnglish', $dataEnglish );
            // return view('word.englishWord',compact('dataEnglish'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedEnglishWord' => 'required'
        ]);

        $inputWord = $request->input('updatedEnglishWord');

        \DB::update('update english_resource set word = ? where english_word_id = ?', [$inputWord, $id]);
        return redirect()->back();
    }

    public function deleteEnglishWord(Request $request, $id)
    {
        $image = EnglishResources::where('english_word_id', $id)->first()->delete();
        return redirect()->back();
    }



    /**
    *
    * @update_A_Particular_English_Word() - this function is needed for dictionary words update.
    */

    public function update_A_Particular_English_Word($word)
    {
        $data = EnglishResources::where('word', '=', $word)->first();
        $data->word = $word;
        $data->save();
        return $data;
    }

    public function updateAnEntry($id, $word)
    {
        $data = EnglishResources::find($id);
        $data->word = $word;
        $data->save();
        return $id;
    }
}
