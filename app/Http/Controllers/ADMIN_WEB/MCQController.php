<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;
use App\Models\MCQ;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use View;
use Auth;
class MCQController extends Controller
{
    public function getLimitedMCQ($id)
    {
        if(Auth::user()->privilege >= 10)
        {
            $mcq = \DB::table('mcq')
            ->where('mcq.mcq_id', '>=', $id)
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq.answer')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
            ->where('mcq.deleted_at', '=', null)
            ->where('mcq.quiz_id', '=', null)
            ->select('mcq.mcq_id as id','mcq.question as question','english_resource.word as answer','bangla_resource.word as meaning')
            ->limit(25)->get();
        }else
        {
            $mcq = \DB::table('mcq')
            ->where('mcq.mcq_id', '>=', $id)
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq.answer')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
            ->where('mcq.deleted_at', '=', null)
            ->where('mcq.quiz_id', '=', null)
            ->where('mcq.user_id', '=', Auth::user()->user_id)
            ->select('mcq.mcq_id as id','mcq.question as question','english_resource.word as answer','bangla_resource.word as meaning')
            ->limit(25)->get();
        }
        

        
        return $mcq;
    }
    public function getLimitedMCQWithUI($id)
    {
        $mcq = app('App\Http\Controllers\ADMIN_WEB\MCQController')->getLimitedMCQ($id);

        return View::make('word.mcq')->with('mcq', $mcq );

    
    }


    public function getLimitedMCQQuiz($id)
    {
        if(Auth::user()->privilege >= 10)
        {
            $mcq = \DB::table('mcq')
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq.answer')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
            ->where('mcq.deleted_at', '=', null)
            ->where('mcq.quiz_id', '>=', $id)
            ->select('mcq.mcq_id as id','mcq.question as question','english_resource.word as answer','bangla_resource.word as meaning','mcq.quiz_id as quiz_id')
            ->limit(200)->get();
        }else
        {
            $mcq = \DB::table('mcq')
            ->join('dictionary', 'dictionary.dictionary_id', '=', 'mcq.answer')
            ->join('english_resource', 'english_resource.english_word_id', '=', 'dictionary.another_word_id')
            ->join('bangla_resource','bangla_resource.bangla_word_id','=','dictionary.bangla_word_id')
            ->where('mcq.deleted_at', '=', null)
            ->where('mcq.quiz_id', '>=', $id)
            ->where('mcq.user_id', '=', Auth::user()->user_id)
            ->select('mcq.mcq_id as id','mcq.question as question','english_resource.word as answer','bangla_resource.word as meaning', 'mcq.quiz_id as quiz_id')
            ->limit(200)->get();
        }
        

        
        return $mcq;
    }
    public function getLimitedMCQWithUIQuiz($id)
    {
        $mcq = app('App\Http\Controllers\ADMIN_WEB\MCQController')->getLimitedMCQQuiz($id);

        return View::make('word.mcq')->with('mcq', $mcq );

    
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'updatedMCQQuestion' => 'required',
            'updatedAnswer' => 'required',
            'updatedMeaning' => 'required',
            
        ]);

        $MCQData = MCQ::find($id);

        $inputMCQQuestion = $request->updatedMCQQuestion; //gapped word

        $inputAnswer = $request->updatedAnswer; // english word

        $inputMeaning = $request->updatedMeaning;
       

        $dict_id = app('App\Http\Controllers\ADMIN_WEB\DictionaryController')->update_A_Particular_Entry($MCQData->mcq_id, $inputAnswer, $inputMeaning);

        $MCQData->question = $inputMCQQuestion;
        $MCQData->save();

        return redirect()->back();
    }

    public function deleteMCQ( $id)
    {
        $image = MCQ::where('mcq_id', $id)->first()->delete();
        return redirect()->back();
    }

}
