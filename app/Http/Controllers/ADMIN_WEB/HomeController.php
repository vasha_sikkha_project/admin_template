<?php

namespace App\Http\Controllers\ADMIN_WEB;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = User::all();

        return View::make('home')
        ->with('data', $data );
    }
}
