<?php

namespace App\Http\Controllers\ADMIN_WEB\csvController;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Support\Facades\Input;

class csvForFillInTheBlankController extends Controller
{
   public function index()
   {
      $topics = \DB::table('topic')->get();
      return view('csvUpload.csvForFillInTheBlank', compact('topics'));
   }
   public function showUploadFile(Request $request)
   {
      $topic_id = Input::get('topic');
      $file = $request->file('csv');
      $quiz = $request->quizCk;

      //Display File Name
      echo 'File Name: ' . $file->getClientOriginalName();
      echo '<br>';

      //Display File Extension
      echo 'File Extension: ' . $file->getClientOriginalExtension();
      echo '<br>';

      //Display File Real Path
      echo 'File Real Path: ' . $file->getRealPath();
      echo '<br>';

      //Display File Size
      echo 'File Size: ' . $file->getSize();
      echo '<br>';

      //Display File Mime Type
      echo 'File Mime Type: ' . $file->getMimeType();
      echo '<br>';
      // Get uploaded CSV file
      $file1 = $request->file('csv');


      // Create a CSV reader instance
      $reader = Reader::createFromFileObject($file1->openFile());
      // Create a customer from each row in the CSV file

      $count = 0;
      $linenum = 0;
      $task_count = \DB::table('task')
         ->where('task_name', "fill in the blank (word)")
         ->count();

      if ($task_count == 0) {
         $task1 = \DB::table('task')->pluck("task_id");
         $var = 0;
         foreach ($task1 as $task) {
            if ($var < $task) {
               $var = $task;
            }
         }
         $var++;
         $data = array('task_id' => $var, "task_name" => "fill in the blank (word)", "wordOrsentence" => 0, "created_at" => Carbon::now());
         \DB::table('task')->insert($data);
      }
      $task_id = \DB::table('task')
         ->where('task_name', "fill in the blank (word)")
         ->value('task_id');


      foreach ($reader as $index => $row) {
         $linenum++;
         if ($linenum == 1) {
            continue;
         }
         $j = 0;
         $sentence = "";
         $answer = "";
         $meaning = "";
         $difficulty = "";
         $experience = "";
         $explanation = "";
         foreach ($row as $line) {
            $j++;
            if ($j == 1) {
               $sentence = $line;
            } else if ($j == 2) {
               $answer = $line;
            } else if ($j == 3) {
               $meaning = $line;
            } else if ($j == 4) {
               $difficulty= $line;
            } else if ($j == 5) {
               $experience = $line;
            } else {
               $explanation = $line;
            }
         }
         $targetusers = explode("#", $experience );
         $targetusers=\DB::table('experience')->where('experience_type', $targetusers[0])->value('experience_id');
         //echo $targetusers;
   
         $word_task_count=\DB::table('word_task')
                                    ->where('task_id',$task_id)
                                    ->where('topic_id',$topic_id)
                                    ->where('level_id',$difficulty)
                                    ->where('experience_id',$targetusers)
                                    ->count();
   
         if($word_task_count==0){
            $task1=\DB::table('word_task')->pluck("word_task_id");
            $var=0;
           foreach($task1 as $task)
           {
               if($var<$task){
                   $var=$task;
               }
               
           }
           $var++;
         $data=array('word_task_id'=>$var,"task_id"=>$task_id,"topic_id"=>$topic_id,"level_id"=>$difficulty,"experience_id"=>$targetusers,"created_at"=>Carbon::now());
           \DB::table('word_task')->insert($data);
         }
         $word_task_id=\DB::table('word_task')
                                    ->where('task_id',$task_id)
                                    ->where('topic_id',$topic_id)
                                    ->where('level_id',$difficulty)
                                    ->where('experience_id',$targetusers)
                                    ->value('word_task_id');
         $user_id = Auth::user()->user_id;


         $bangla_word_count = \DB::table('bangla_resource')
            ->where('word', $meaning)
            ->count();
         if ($bangla_word_count == 0) {
            $task1 = \DB::table('bangla_resource')->pluck("bangla_word_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('bangla_word_id' => $var, "word" => $meaning, "created_at" => Carbon::now());
            \DB::table('bangla_resource')->insert($data);
         }


         $bangla_id = \DB::table('bangla_resource')
            ->where('word', $meaning)->pluck('bangla_word_id')->first();


         $english_word_count = \DB::table('english_resource')
            ->where('word', $answer)
            ->count();
         if ($english_word_count == 0) {
            $task1 = \DB::table('english_resource')->pluck("english_word_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('english_word_id' => $var, "word" => $answer, "image_link" => "null", "created_at" => Carbon::now());
            \DB::table('english_resource')->insert($data);
         }


         //echo $bangla_id;
         $english_id = \DB::table('english_resource')
            ->where('word', $answer)->pluck('english_word_id')->first();
         //echo $english_id;


         $dictionary_count = \DB::table('dictionary')
            ->where('another_word_id', $english_id)
            ->where('bangla_word_id', $bangla_id)
            ->count();

         if ($dictionary_count == 0) {
            $task1 = \DB::table('dictionary')->pluck("dictionary_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;
            $data = array('dictionary_id' => $var, "another_word_id" => $english_id, "bangla_word_id" => $bangla_id, "language_id" => 1, "created_at" => Carbon::now());
            \DB::table('dictionary')->insert($data);
         }
         $dictionary_id = \DB::table('dictionary')
            ->where('another_word_id', $english_id)
            ->where('bangla_word_id', $bangla_id)
            ->value('dictionary_id');

         $task1 = \DB::table('fill_in_the_blanks_word_ver')->pluck("fill_blanks_word_id");
         $var = 0;
         foreach ($task1 as $task) {
            if ($var < $task) {
               $var = $task;
            }
         }
         $var++;
         $data=array('fill_blanks_word_id'=>$var,"word_task_id"=>$word_task_id,"dictionary_id"=>$dictionary_id,"incomplete_word"=>$sentence,"user_id"=>$user_id,"created_at"=>Carbon::now());
      \DB::table('fill_in_the_blanks_word_ver')->insert($data);
      }

      //Move Uploaded File
      $destinationPath = 'uploads\FillInTheBlank';
      $file->move($destinationPath, $file->getClientOriginalName());
   }
}
