<?php
namespace App\Http\Controllers\ADMIN_WEB\csvController;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Support\Facades\Input;
class csvForWordPicture extends Controller {
   public function index() {
      
      $topics = \DB::table('topic')->get();
      return view('csvUpload.csvForWordPicture',compact('topics'));
   }
   public function showUploadFile(Request $request) {

      $topic_id=Input::get('topic');
     

      echo "<br>";
      $file = $request->file('csv');
      $quiz = $request->quizCk;
   
      //Display File Name
      echo 'File Name: '.$file->getClientOriginalName();
      echo '<br>';
   
      //Display File Extension
      echo 'File Extension: '.$file->getClientOriginalExtension();
      echo '<br>';
   
      //Display File Real Path
      echo 'File Real Path: '.$file->getRealPath();
      echo '<br>';
   
      //Display File Size
      echo 'File Size: '.$file->getSize();
      echo '<br>';
   
      //Display File Mime Type
      echo 'File Mime Type: '.$file->getMimeType();
      echo '<br>';
       // Get uploaded CSV file
     $file1 = $request->file('csv');


    // Create a CSV reader instance
     $reader = Reader::createFromFileObject($file1->openFile());
    // Create a customer from each row in the CSV file

    $count = 0;
    $linenum = 0;
    $task_count=\DB::table('task')
                                 ->where('task_name',"word-picture")
                                 ->count();

      if($task_count==0){
         $task1=\DB::table('task')->pluck("task_id");
         $var=0;
        foreach($task1 as $task)
        {
            if($var<$task){
                $var=$task;
            }
            
        }
        $var++;
      $data=array('task_id'=>$var,"task_name"=>"word-picture","wordOrsentence"=>0,"created_at"=>Carbon::now());
        \DB::table('task')->insert($data);
      }
      $task_id=\DB::table('task')
                                 ->where('task_name',"word-picture")
                                 ->value('task_id');
    
   foreach ($reader as $index => $row)
   {
      $linenum++;
      if($linenum==1){
         continue;
      }
      $j=0;
      $word = "";
      $numofoption = 4;
      $option="";
      $option_meaning="";
      $difficulty=0;
      $targetuser="";
      foreach( $row as $line )
      {
         $j++;
         if($j==1){
            $word=$line;
         }
         if($j==2){
            $option=$line;      
         }
         if($j==3){
            $option_meaning=$line;
         }
         if($j==4){
            $difficulty=$line;
         }
         if($j==5){
            $targetuser=$line;
         }

     ///  echo $line;
         
      }
      //echo $sentence;
      //echo $sentence_meaning;
     // echo $numofoption;
      $options = explode("#", $option);
      foreach($options as $opt){
       ///  echo $opt;
      }
      
      
      $options_meaning = explode("#", $option_meaning);
      foreach($options_meaning as $opt){
       ///  echo $opt;
      }
   //   $difficulty=\DB::table('level')->where('difficulty', $difficulty)->value('level_id');
      //echo $difficulty;
      $targetusers = explode("#", $targetuser);
      $targetusers=\DB::table('experience')->where('experience_type', $targetusers[0])->value('experience_id');
      //echo $targetusers;

      $word_task_count=\DB::table('word_task')
                                 ->where('task_id',$task_id)
                                 ->where('topic_id',$topic_id)
                                 ->where('level_id',$difficulty)
                                 ->where('experience_id',$targetusers)
                                 ->count();

      if($word_task_count==0){
         $task1=\DB::table('word_task')->pluck("word_task_id");
         $var=0;
        foreach($task1 as $task)
        {
            if($var<$task){
                $var=$task;
            }
            
        }
        $var++;
      $data=array('word_task_id'=>$var,"task_id"=>$task_id,"topic_id"=>$topic_id,"level_id"=>$difficulty,"experience_id"=>$targetusers,"created_at"=>Carbon::now());
        \DB::table('word_task')->insert($data);
      }
      $word_task_id=\DB::table('word_task')
                                 ->where('task_id',$task_id)
                                 ->where('topic_id',$topic_id)
                                 ->where('level_id',$difficulty)
                                 ->where('experience_id',$targetusers)
                                 ->value('word_task_id');
      
      $user_id= Auth::user()->user_id ;
      echo $word_task_id;

      $task1=\DB::table('word_picture')->pluck("word_picture_id");
      $var=0;
      foreach($task1 as $task)
      {
         if($var<$task){
            $var=$task;
         }
            
      }
      $var++;
      echo $var;


      for ($i = 0; $i < $numofoption; $i++){

         $bangla_word_count=\DB::table('bangla_resource')
         ->where('word',$options_meaning[$i])
         ->count();
         if($bangla_word_count==0){
            $task1=\DB::table('bangla_resource')->pluck("bangla_word_id");
            $var=0;
            foreach($task1 as $task)
            {
               if($var<$task){
                  $var=$task;
            }
            
         }
          $var++;

         $data=array('bangla_word_id'=>$var,"word"=>$options_meaning[$i],"created_at"=>Carbon::now());
         \DB::table('bangla_resource')->insert($data);


         }


         $bangla_id=\DB::table('bangla_resource')
                     ->where('word',$options_meaning[$i])->pluck('bangla_word_id')->first();


         $english_word_count=\DB::table('english_resource')
                     ->where('word',$options[$i])
                     ->count();
          if($english_word_count==0){
                        $task1=\DB::table('english_resource')->pluck("english_word_id");
                        $var=0;
                        foreach($task1 as $task)
                        {
                           if($var<$task){
                              $var=$task;
                        }
                        
                     }
                      $var++;
            
                     $data=array('english_word_id'=>$var,"word"=>$options[$i],"image_link"=>"null","created_at"=>Carbon::now());
                     \DB::table('english_resource')->insert($data);
            
            
      }
            

         //echo $bangla_id;
         $english_id=\DB::table('english_resource')
                     ->where('word',$options[$i])->pluck('english_word_id')->first();
         //echo $english_id;







         $dictionary_count=\DB::table('dictionary')
                                 ->where('another_word_id',$english_id)
                                 ->where('bangla_word_id',$bangla_id)
                                 ->count();

      if($dictionary_count==0){
         $task1=\DB::table('dictionary')->pluck("dictionary_id");
         $var=0;
        foreach($task1 as $task)
        {
            if($var<$task){
                $var=$task;
            }
            
        }
        $var++;
      $data=array('dictionary_id'=>$var,"another_word_id"=>$english_id,"bangla_word_id"=>$bangla_id,"language_id"=>1,"created_at"=>Carbon::now());
      \DB::table('dictionary')->insert($data);

      }
   }

   $english_id=\DB::table('english_resource')
                     ->where('word',$word)->pluck('english_word_id')->first();
    $dictionary_id=\DB::table('dictionary')
                     ->where('another_word_id',$english_id)
                     ->value('dictionary_id');
     $task1=\DB::table('word_picture')->pluck("word_picture_id");
     $var=0;
                     foreach($task1 as $task)
                     {
                        if($var<$task){
                           $var=$task;
                        }
                           
                     }
                     $var++;

   $data=array('word_picture_id'=>$var,"word_task_id"=>$word_task_id,"dictionary_id"=>$dictionary_id,"user_id"=>$user_id,"created_at"=>Carbon::now());
      \DB::table('word_picture')->insert($data);
      $match_id=$var;

      //////////


   for ($i = 0; $i < $numofoption; $i++){
      $bangla_id=\DB::table('bangla_resource')
      ->where('word',$options_meaning[$i])->pluck('bangla_word_id')->first();
      $english_id=\DB::table('english_resource')
                     ->where('word',$options[$i])->pluck('english_word_id')->first();
      $dictionary_id=\DB::table('dictionary')
                     ->where('another_word_id',$english_id)
                     ->where('bangla_word_id',$bangla_id)
                     ->value('dictionary_id');

      $task1=\DB::table('word_picture_options')->pluck("options_id");
      $var=0;
      foreach($task1 as $task)
      {
         if($var<$task){
             $var=$task;
         }
                              
      }
      $var++;

      $data=array('options_id'=>$var,"word_picture_id"=>$match_id,"options"=>$dictionary_id,"created_at"=>Carbon::now());
      \DB::table('word_picture_options')->insert($data);

      }
      

      }//Move Uploaded File
      $destinationPath = 'uploads\WordPicture';
     // public\uploads\MCQ
      $file->move($destinationPath,$file->getClientOriginalName());

   }
      
      



      
   }
