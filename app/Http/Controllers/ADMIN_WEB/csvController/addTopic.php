<?php

namespace App\Http\Controllers\ADMIN_WEB\csvController;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class addTopic extends Controller
{
    public function index() {
        $topics = \DB::table('topic')->pluck("topic_name","topic_id")
                  ->where('topic.deleted_at', '=', null);
        return view('csvUpload.addTopic',compact('topics'));
     }

     public function addNewTopic(Request $request) {
        
        


        $newtopic = Input::get('newtopic');
        
        $topics1=\DB::table('topic')->pluck("topic_id");
        $var=-1;
        foreach($topics1 as $topic)
        {
            if($var<$topic){
                $var=$topic;
            }
            
        }
        $file = $request->file('csv');
   
        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';
     
        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';
     
        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';
     
        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';
     
        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();
        echo '<br>';
        $foo = $file->getClientOriginalExtension();
         
        
        //Move Uploaded File
        $date = Carbon::now();
        $newfilename=$newtopic.".".$foo;
        $destinationPath = 'uploads\TopicImage';
        $file->move($destinationPath,$newfilename);
        

        $data=array('topic_id'=>$var+1,"topic_name"=>$newtopic,"topic_image"=>$newfilename,"created_at"=>Carbon::now());
        \DB::table('topic')->insert($data);
        $topics = \DB::table('topic')->pluck("topic_name","topic_id");
       return view('csvUpload.addTopic',compact('topics'));
     }
}
