<?php

namespace App\Http\Controllers\ADMIN_WEB\csvController;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Support\Facades\Input;

class csvForSynAntonyms extends Controller
{
    //
    public function index() {
      
        $topics = \DB::table('topic')->get();
        return view('csvUpload.csvForSynAntonyms',compact('topics'));
     }

     public function showUploadFile(Request $request) {

        $topic_id=Input::get('topic');
       
  
        echo "<br>";
        $file = $request->file('csv');
        $quiz = $request->quizCk;
     
        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';
     
        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';
     
        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';
     
        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';
     
        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();
        echo '<br>';
         // Get uploaded CSV file
       $file1 = $request->file('csv');
  
  
      // Create a CSV reader instance
       $reader = Reader::createFromFileObject($file1->openFile());
      // Create a customer from each row in the CSV file
  
      $count = 0;
      $linenum = 0;

      $task_count=\DB::table('task')
                                 ->where('task_name',"synonym_antonym")
                                 ->count();

      if($task_count==0){
         $task1=\DB::table('task')->pluck("task_id");
         $var=0;
        foreach($task1 as $task)
        {
            if($var<$task){
                $var=$task;
            }
            
        }
        $var++;
      $data=array('task_id'=>$var,"task_name"=>"synonym_antonym","wordOrsentence"=>0,"created_at"=>Carbon::now());
        \DB::table('task')->insert($data);
      }
      $task_id=\DB::table('task')
                                 ->where('task_name',"synonym_antonym")
                                 ->value('task_id');
      
     foreach ($reader as $index => $row)
     {
        $linenum++;
        if($linenum==1){
           continue;
        }
        $j=0;
        $word="";
        $word_meaning="";
        $synonyms="";
        $syn_meaning="";
        $antonyms="";
        $ant_meaning="";

        $difficulty="";
        $targer_user="";


        foreach( $row as $line )
        {
           $j++;
           if($j==1){
              $word=$line;

           }
           else if($j==2){
               $word_meaning=$line;
           }
           else if($j==3){
            $synonyms=$line;
         }
         else if($j==4){
            $syn_meaning=$line;
         }
         else if($j==5){
            $antonyms=$line;
         }
         else if($j==6){
            $ant_meaning=$line;
         }
         else if($j==7){
            $difficulty=$line;
         }
         else if($j==8){
            $targer_user=$line;
         }
  
         //  echo $line;
           
        }
      //   echo $word;
      //   echo $word_meaning;
      //   echo $synonyms;
      //   echo $syn_meaning;
      //   echo $antonyms;
      //   echo $ant_meaning;
      //   echo $difficulty;
      //   echo $targer_user;
      $targetusers = explode("#", $targer_user);
      $targetusers=\DB::table('experience')->where('experience_type', $targetusers[0])->value('experience_id');
      //echo $targetusers;

      $word_task_count=\DB::table('word_task')
                                 ->where('task_id',$task_id)
                                 ->where('topic_id',$topic_id)
                                 ->where('level_id',$difficulty)
                                 ->where('experience_id',$targetusers)
                                 ->count();

      if($word_task_count==0){
         $task1=\DB::table('word_task')->pluck("word_task_id");
         $var=0;
        foreach($task1 as $task)
        {
            if($var<$task){
                $var=$task;
            }
            
        }
        $var++;
      $data=array('word_task_id'=>$var,"task_id"=>$task_id,"topic_id"=>$topic_id,"level_id"=>$difficulty,"experience_id"=>$targetusers,"created_at"=>Carbon::now());
        \DB::table('word_task')->insert($data);
      }
      $word_task_id=\DB::table('word_task')
                                 ->where('task_id',$task_id)
                                 ->where('topic_id',$topic_id)
                                 ->where('level_id',$difficulty)
                                 ->where('experience_id',$targetusers)
                                 ->value('word_task_id');
      
      $user_id= Auth::user()->user_id ;
     /// echo $word_task_id;
     $bangla_word_count = \DB::table('bangla_resource')
     ->where('word', $word_meaning)
     ->count();
  if ($bangla_word_count == 0) {
     $task1 = \DB::table('bangla_resource')->pluck("bangla_word_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;

     $data = array('bangla_word_id' => $var, "word" => $word_meaning, "created_at" => Carbon::now());
     \DB::table('bangla_resource')->insert($data);
  }


  $bangla_id = \DB::table('bangla_resource')
     ->where('word', $word_meaning)->pluck('bangla_word_id')->first();


  $english_word_count = \DB::table('english_resource')
     ->where('word', $word)
     ->count();
  if ($english_word_count == 0) {
     $task1 = \DB::table('english_resource')->pluck("english_word_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;

     $data = array('english_word_id' => $var, "word" => $word, "image_link" => "null", "created_at" => Carbon::now());
     \DB::table('english_resource')->insert($data);
  }


  //echo $bangla_id;
  $english_id = \DB::table('english_resource')
     ->where('word', $word)->pluck('english_word_id')->first();
  //echo $english_id;


  $dictionary_count = \DB::table('dictionary')
     ->where('another_word_id', $english_id)
     ->where('bangla_word_id', $bangla_id)
     ->count();

  if ($dictionary_count == 0) {
     $task1 = \DB::table('dictionary')->pluck("dictionary_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;
     $data = array('dictionary_id' => $var, "another_word_id" => $english_id, "bangla_word_id" => $bangla_id, "language_id" => 1, "created_at" => Carbon::now());
     \DB::table('dictionary')->insert($data);
  }
  $dictionary_id1 = \DB::table('dictionary')
     ->where('another_word_id', $english_id)
     ->where('bangla_word_id', $bangla_id)
     ->value('dictionary_id');


     $bangla_word_count = \DB::table('bangla_resource')
     ->where('word', $syn_meaning)
     ->count();
  if ($bangla_word_count == 0) {
     $task1 = \DB::table('bangla_resource')->pluck("bangla_word_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;

     $data = array('bangla_word_id' => $var, "word" => $syn_meaning, "created_at" => Carbon::now());
     \DB::table('bangla_resource')->insert($data);
  }


  $bangla_id = \DB::table('bangla_resource')
     ->where('word', $syn_meaning)->pluck('bangla_word_id')->first();


  $english_word_count = \DB::table('english_resource')
     ->where('word', $synonyms)
     ->count();
  if ($english_word_count == 0) {
     $task1 = \DB::table('english_resource')->pluck("english_word_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;

     $data = array('english_word_id' => $var, "word" => $synonyms, "image_link" => "null", "created_at" => Carbon::now());
     \DB::table('english_resource')->insert($data);
  }


  //echo $bangla_id;
  $english_id = \DB::table('english_resource')
     ->where('word', $synonyms)->pluck('english_word_id')->first();
  //echo $english_id;


  $dictionary_count = \DB::table('dictionary')
     ->where('another_word_id', $english_id)
     ->where('bangla_word_id', $bangla_id)
     ->count();

  if ($dictionary_count == 0) {
     $task1 = \DB::table('dictionary')->pluck("dictionary_id");
     $var = 0;
     foreach ($task1 as $task) {
        if ($var < $task) {
           $var = $task;
        }
     }
     $var++;
     $data = array('dictionary_id' => $var, "another_word_id" => $english_id, "bangla_word_id" => $bangla_id, "language_id" => 1, "created_at" => Carbon::now());
     \DB::table('dictionary')->insert($data);
  }
  $dictionary_id2 = \DB::table('dictionary')
     ->where('another_word_id', $english_id)
     ->where('bangla_word_id', $bangla_id)
     ->value('dictionary_id');


     $bangla_word_count = \DB::table('bangla_resource')
            ->where('word', $ant_meaning)
            ->count();
         if ($bangla_word_count == 0) {
            $task1 = \DB::table('bangla_resource')->pluck("bangla_word_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('bangla_word_id' => $var, "word" => $ant_meaning, "created_at" => Carbon::now());
            \DB::table('bangla_resource')->insert($data);
         }


         $bangla_id = \DB::table('bangla_resource')
            ->where('word', $ant_meaning)->pluck('bangla_word_id')->first();


         $english_word_count = \DB::table('english_resource')
            ->where('word', $antonyms)
            ->count();
         if ($english_word_count == 0) {
            $task1 = \DB::table('english_resource')->pluck("english_word_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('english_word_id' => $var, "word" => $antonyms, "image_link" => "null", "created_at" => Carbon::now());
            \DB::table('english_resource')->insert($data);
         }


         //echo $bangla_id;
         $english_id = \DB::table('english_resource')
            ->where('word', $antonyms)->pluck('english_word_id')->first();
         //echo $english_id;


         $dictionary_count = \DB::table('dictionary')
            ->where('another_word_id', $english_id)
            ->where('bangla_word_id', $bangla_id)
            ->count();

         if ($dictionary_count == 0) {
            $task1 = \DB::table('dictionary')->pluck("dictionary_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;
            $data = array('dictionary_id' => $var, "another_word_id" => $english_id, "bangla_word_id" => $bangla_id, "language_id" => 1, "created_at" => Carbon::now());
            \DB::table('dictionary')->insert($data);
         }
         $dictionary_id3 = \DB::table('dictionary')
            ->where('another_word_id', $english_id)
            ->where('bangla_word_id', $bangla_id)
            ->value('dictionary_id');
            $task1 = \DB::table('synonyms_antonyms')->pluck("synonym_antonym_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;
            $data=array('synonym_antonym_id'=>$var,"word_task_id"=>$word_task_id,"base_word_id"=>$dictionary_id1,"synonym_word_id"=>$dictionary_id2,"antonym_word_id"=>$dictionary_id3,"user_id"=>$user_id,"created_at"=>Carbon::now());
         \DB::table('synonyms_antonyms')->insert($data);

        }//Move Uploaded File
        $destinationPath = 'uploads\SynandAntonyms';
       // public\uploads\MCQ
        $file->move($destinationPath,$file->getClientOriginalName());
  
     }
}
