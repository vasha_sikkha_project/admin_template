<?php

namespace App\Http\Controllers\ADMIN_WEB\csvController;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Support\Facades\Input;
use Auth;

class csvForJSentenceController extends Controller
{
   public function index()
   {
      $topics = \DB::table('topic')->get();
      return view('csvUpload.csvForJSentence', compact('topics'));
   }
   public function showUploadFile(Request $request)
   {
      $topic_id = Input::get('topic');
      $file = $request->file('csv');
      $quiz = $request->quizCk;

      //Display File Name
      echo 'File Name: ' . $file->getClientOriginalName();
      echo '<br>';

      //Display File Extension
      echo 'File Extension: ' . $file->getClientOriginalExtension();
      echo '<br>';

      //Display File Real Path
      echo 'File Real Path: ' . $file->getRealPath();
      echo '<br>';

      //Display File Size
      echo 'File Size: ' . $file->getSize();
      echo '<br>';

      //Display File Mime Type
      echo 'File Mime Type: ' . $file->getMimeType();
      echo '<br>';
      // Get uploaded CSV file
      $file1 = $request->file('csv');


      // Create a CSV reader instance
      $reader = Reader::createFromFileObject($file1->openFile());
      // Create a customer from each row in the CSV file

      $count = 0;
      $linenum = 0;

      $task_count = \DB::table('task')
         ->where('task_name', "jumbled_sentence")
         ->count();

      if ($task_count == 0) {
         $task1 = \DB::table('task')->pluck("task_id");
         $var = 0;
         foreach ($task1 as $task) {
            if ($var < $task) {
               $var = $task;
            }
         }
         $var++;
         $data = array('task_id' => $var, "task_name" => "jumbled_sentence", "wordOrsentence" => 1, "created_at" => Carbon::now());
         \DB::table('task')->insert($data);
      }
      $task_id = \DB::table('task')
         ->where('task_name', "jumbled_sentence")
         ->value('task_id');

      foreach ($reader as $index => $row) {
         $linenum++;
         if ($linenum == 1) {
            continue;
         }
         $question = "";
         $meaning = "";
         $jsentence = "";
         $level = "";
         $experience = "";
         $explanation = "";
         $j = 0;
         foreach ($row as $line) {
            $j++;
            if ($j == 1) {
               $question = $line;
            }
            if ($j == 2) {
               $meaning = $line;
            }
            if ($j == 3) {
               $jsentence = $line;
            }
            if ($j == 4) {
               $level = $line;
            }
            if ($j == 5) {
               $experience = $line;
            }
            if ($j == 6) {
               $explanation = $line;
            }
         }
         // echo $question;
         // echo $meaning;
         // echo $jsentence;
         // echo $level;
         // echo $experience;
         // echo $explanation;



         $proffesions = explode("#", $experience);
         $proffesion = \DB::table('experience')->where('experience_type', $proffesions[0])->value('experience_id');
         echo $proffesion;


         $sentence_task_count = \DB::table('sentence_task')
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('level_id', $level)
            ->where('experience_id', $proffesion)
            ->count();

         if ($sentence_task_count == 0) {
            $task1 = \DB::table('sentence_task')->pluck("sentence_task_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;
            $data = array('sentence_task_id' => $var, "task_id" => $task_id, "topic_id" => $topic_id, "level_id" => $level, "experience_id" => $proffesion, "created_at" => Carbon::now());
            \DB::table('sentence_task')->insert($data);
         }
         $sentence_task_id = \DB::table('sentence_task')
            ->where('task_id', $task_id)
            ->where('topic_id', $topic_id)
            ->where('level_id', $level)
            ->where('experience_id', $proffesion)
            ->value('sentence_task_id');
         echo $sentence_task_id;
         $user_id = Auth::user()->user_id;
         $bangla_word_count = \DB::table('bangla_sentence')
            ->where('bangla_sentence', $meaning)
            ->count();
         if ($bangla_word_count == 0) {
            $task1 = \DB::table('bangla_sentence')->pluck("bangla_sentence_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('bangla_sentence_id' => $var, "bangla_sentence" => $meaning, "created_at" => Carbon::now());
            \DB::table('bangla_sentence')->insert($data);
         }

         $bangla_id = \DB::table('bangla_sentence')
            ->where('bangla_sentence', $meaning)->pluck('bangla_sentence_id')->first();
         echo $bangla_id;


         $english_word_count = \DB::table('english_sentence')
            ->where('english_sentence', $question)
            ->count();
         if ($english_word_count == 0) {
            $task1 = \DB::table('english_sentence')->pluck("english_sentence_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;

            $data = array('english_sentence_id' => $var, "english_sentence" => $question, "created_at" => Carbon::now());
            \DB::table('english_sentence')->insert($data);
         }
         $english_id = \DB::table('english_sentence')
            ->where('english_sentence', $question)->pluck('english_sentence_id')->first();
         //echo $english_id;
         $translation_count = \DB::table('translation')
            ->where('another_sentence_id', $english_id)
            ->where('bangla_sent_id', $bangla_id)
            ->count();

         if ($translation_count == 0) {
            $task1 = \DB::table('translation')->pluck("translation_id");
            $var = 0;
            foreach ($task1 as $task) {
               if ($var < $task) {
                  $var = $task;
               }
            }
            $var++;
            $data = array('translation_id' => $var, "another_sentence_id" => $english_id, "bangla_sent_id" => $bangla_id, "user_id" => $user_id, "language_id" => 1, "created_at" => Carbon::now());
            \DB::table('translation')->insert($data);
         }
         $translation_id = \DB::table('translation')
            ->where('another_sentence_id', $english_id)
            ->where('bangla_sent_id', $bangla_id)
            ->value('translation_id');
         echo $translation_id;

         $task1 = \DB::table('fix_jumbled_sentence')->pluck("fix_jum_sen_id");
         $var = 0;
         foreach ($task1 as $task) {
            if ($var < $task) {
               $var = $task;
            }
         }
         $var++;

         $data = array('fix_jum_sen_id' => $var, "sentence_task_id" => $sentence_task_id, "translation_id" => $translation_id, "question" => $jsentence, "user_id" => $user_id, "created_at" => Carbon::now());
         \DB::table('fix_jumbled_sentence')->insert($data);
      }

      //Move Uploaded File
      $destinationPath = 'uploads\JSentence';
      $file->move($destinationPath, $file->getClientOriginalName());
   }
}
