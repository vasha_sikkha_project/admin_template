<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\BanglaSentences;
use App\Models\EnglishSentence;
// use App\Http\Controllers\TranslationController;
use View;

class SentenceTaskController extends Controller
{
    public $dataBangla;
    public $dataEnglish;
    public $dataTranslartion;
    public $fillInTheBlanks;
    public $sentenceMatching;

    public function getData()
    {
        $dataBangla = BanglaSentences::all();
        $dataEnglish = EnglishSentence::all();
        $dataTranslartion = app('App\Http\Controllers\ADMIN_WEB\TranslationController')->getData(0);
        $fillInTheBlanksSentence = app('App\Http\Controllers\ADMIN_WEB\FillInTheBlanksController')->getLimitedFillInTheBlanks(0);
        $fixJumble = app('App\Http\Controllers\ADMIN_WEB\FixJumbledSentencesController')->getLimitedFixJumbledSentences(0);
        $senMatch = app('App\Http\Controllers\ADMIN_WEB\SentenceMatchingController')->getLimitedSentenceMatching(0);

        return View::make('sentence.sentenceTable')
            ->with('dataBangla', $dataBangla)
            ->with('dataEnglish', $dataEnglish)
            ->with('dataTranslartion', $dataTranslartion)
            ->with('fillInTheBlanksSentence', $fillInTheBlanksSentence)
            ->with('fixJumble', $fixJumble)
            ->with('senMatch', $senMatch);

    }

}
