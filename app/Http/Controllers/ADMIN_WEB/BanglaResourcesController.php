<?php

namespace App\Http\Controllers\ADMIN_WEB;

use App\Http\Controllers\Controller;
use App\Models\BanglaResources;
use Illuminate\Http\Request;

class BanglaResourcesController extends Controller
{

    public function insertAndReturnID($word)
    {
        $data = BanglaResources::where('word', '=', $word)->first();

        if ($data == null) {
            $data = BanglaResources::create(
                [
                    'word' => $word,
                ]
            );
        }

        return $data->bangla_word_id;
    }

    public function inserted(Request $request, $id)
    {
        $this->validate($request, [
            'word' => 'required'
        ]);

        $data = BanglaResources::where('word', '=', $request->word)->first();

        if ($data == null) {
            $data = BanglaResources::create(
                [
                    'word' => $request->word,
                ]
            );
        }

        return redirect()->back();
    }

    public function getLimitedBanglaWords($id)
    {
        
       
            $bangla = \DB::table('bangla_resource')
            ->where('bangla_resource.bangla_word_id', '>=', $id)
            ->where('bangla_resource.deleted_at', '=', null)
            ->limit(25)->get();
        
        

        return $bangla;
    }

    public function getLimitedBanglaWordsWithUI($id)
    {
        $dataBangla = \DB::table('bangla_resource')
            ->where('bangla_resource.bangla_word_id', '>=', $id)
            ->where('bangla_resource.deleted_at', '=', null)
            ->limit(200)->get();

        return view('word.banglaWord', compact('dataBangla'));
    }

    public function update(Request $request, $id)
    {
        $wrd = $request->input('updatedBanglaWord');
        $op = $id;

        \DB::update('update bangla_resource set word = ? where bangla_word_id = ?', [$wrd, $op]);
        return redirect()->back();
    }

    public function deleteBanglaWord(Request $request, $id)
    {
        $image = BanglaResources::where('bangla_word_id', $id)->first()->delete();
        return redirect()->back();
    }

    /**
     *
     * updateAnEntry() - needed for dictionary
     * @update_A_Particular_English_Word() - this function is needed for dictionary words update.
     */

    public function update_A_Particular_Bangla_Word($word)
    {
        $data = BanglaResources::where('word', '=', $word)->first();
        $data->word = $word;
        $data->save();
        return $data;
    }

    public function updateAnEntry($id, $word)
    {
        $data = BanglaResources::find($id);
        $data->word = $word;
        $data->save();
        return $id;
    }

}
