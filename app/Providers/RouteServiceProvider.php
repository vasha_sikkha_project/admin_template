<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {

        $this->mapApiRoutes();
        $this->mapWebRoutes();

        /** adding custom routes */
        /** word routes */
        $this->mapAllWordsRoutes();
        $this->mapBanglaResourcesRoutes();
        $this->mapEnglishResourcesRoutes();
        $this->mapDictionaryRoutes();
        $this->mapFillBlanksWordVerRoutes();
        $this->mapSynonymAntonymRoutes();
        $this->mapVocabularyRoutes();
        $this->mapMcqRoutes();

        /** sentence routes */

        $this->mapAllSentencesRoutes();
        $this->mapBanglaSentenceRoutes();
        $this->mapEnglishSentenceRoutes();
        $this->mapFillBlankSentenceRoutes();
        $this->mapTranslationRoutes();
        $this->mapFixJumbleSentenceRoutes();
        $this->mapSentenceMatchingRoutes();

        /**user routes */
        $this->mapAllUsersRoutes();
        $this->mapUserProfileRoutes();
        $this->mapAllTypeOfDashboardRoutes();

        /**csv upload routes */
        $this->mapcsvUploadRoutes();
        $this->mapTopicRoutes();


        /** Custom API Routes */
        $this->mapMobileApiRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapTopicRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/topicRoutes/topic.php'));
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**Custom route file for mobile APIs. */

    protected function mapMobileApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/mobileApi.php'));
    }

    /**
     *
     * Later functions are for custom routes
     *
     */

     /** word */
    protected function mapAllWordsRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/AllWords.php'));
    }

    protected function mapBanglaResourcesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/BanglaResources.php'));
    }

    protected function mapEnglishResourcesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/EnglishResources.php'));
    }

    protected function mapDictionaryRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/Dictionary.php'));
    }

    protected function mapFillBlanksWordVerRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/FillBlanksWordVer.php'));
    }

    protected function mapSynonymAntonymRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/SynonymAntonym.php'));
    }

    protected function mapVocabularyRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/Vocabulary.php'));
    }

    protected function mapMcqRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/wordRoutes/Mcq.php'));
    }






    /** sentence */

    protected function mapAllSentencesRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/AllSentences.php'));
    }

    protected function mapBanglaSentenceRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/BanglaSentence.php'));
    }

    protected function mapEnglishSentenceRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/EnglishSentence.php'));
    }

    protected function mapFillBlankSentenceRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/FillInTheBlanks.php'));
    }

    protected function mapTranslationRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/Translation.php'));
    }

    protected function mapFixJumbleSentenceRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/FixJumbledSentence.php'));
    }

    protected function mapSentenceMatchingRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/sentenceRoutes/SentenceMatching.php'));
    }



    /**users */

    protected function mapAllUsersRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/userRoutes/AllUsers.php'));
    }


    protected function mapUserProfileRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/userRoutes/UserProfile.php'));
    }


    protected function mapAllTypeOfDashboardRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/dashboard/adminDashboard.php'));
            
    }
    protected function mapcsvUploadRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/adminWebsite/csvRoutes/csvUpload.php'));
    }

}
