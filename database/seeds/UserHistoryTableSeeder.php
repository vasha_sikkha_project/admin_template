<?php

use Illuminate\Database\Seeder;

class UserHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 15; ++$i)
        {
            DB::table('user_history')->insert([
                'userhistory_id' => $i,
                'user_id' => rand(1, 2),
                'topic_id' => rand(1, 3),
                'task_id' => rand(1, 5),
                'level_id' => rand(1, 3),
                'progress' => rand(0, 100)
            ]);
        }
    }
}
