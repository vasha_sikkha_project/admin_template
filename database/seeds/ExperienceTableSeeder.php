<?php

use Illuminate\Database\Seeder;

class ExperienceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 1; ++$i)
        {
            DB::table('experience')->insert([
                'experience_id' => $i,
                'experience_type' => Str::random(10)
            ]);
        }
    }
}
