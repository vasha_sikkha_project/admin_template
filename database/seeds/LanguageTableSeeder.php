<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 2; ++$i)
        {
            DB::table('language')->insert([
                'language_id' => $i,
                'language_name' => Str::random(10)
            ]);
        }
    }
}
