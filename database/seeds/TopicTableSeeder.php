<?php

use Illuminate\Database\Seeder;

class TopicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('topic')->insert([
                'topic_id' => $i,
                'topic_name' => Str::random(10),
                'topic_image' => 'localhost.com\\'.Str::random(10)
            ]);
        }
    }
}
