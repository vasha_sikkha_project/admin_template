<?php

use Illuminate\Database\Seeder;

class FillInTheBlanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('fill_in_the_blanks')->insert([
                'fill_blanks_id' => $i,
                'sentence_task_id' => rand(1, 5),
                'translation_id' => rand(1, 3),
                'user_id' => rand(1, 2),
                'incomplete_sentence' => Str::random(10),
                'explanation' => Str::random(10)
            ]);
        }
    }
}
