<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        /**** Before using these seeders, make sure you have done the followings -
         * 1. php artisan migrate:fresh
         * 2. php artisan passport:install
         * 3. insert at least two users in your database table
         * 4. make sure your local database is up and running
         * 5. composer dump-autoload
         * 6. php artisan db:seed
         **** this will seed random data in your local database ******/


        $this->call(TopicTableSeeder::class);
        $this->call(TaskTableSeeder::class);
        $this->call(LevelTableSeeder::class);
        $this->call(LovedTopicTableSeeder::class);
        $this->call(UserHistoryTableSeeder::class);
        $this->call(BanglaResourceTableSeeder::class);
        $this->call(BanglaSentenceTableSeeder::class);
        $this->call(EnglishResourceTableSeeder::class);
        $this->call(EnglishSentenceTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(DictionaryTableSeeder::class);
        $this->call(TranslationTableSeeder::class);
        $this->call(ExperienceTableSeeder::class);
        $this->call(WordTaskTableSeeder::class);
        $this->call(SentenceTaskTableSeeder::class);
        $this->call(MCQTableSeeder::class);
        $this->call(MCQOptionsTableSeeder::class);
        $this->call(FillInTheBlanksTableSeeder::class);
        $this->call(FbOptionsTableSeeder::class);
        $this->call(FbAnswerTableSeeder::class);
    }
}
