<?php

use Illuminate\Database\Seeder;

class DictionaryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; ++$i)
        {
            DB::table('dictionary')->insert([
                'dictionary_id' => $i,
                'another_word_id' => rand(1, 50),
                'bangla_word_id' => rand(1, 50),
                'language_id' => rand(1, 2)
            ]);
        }
    }
}
