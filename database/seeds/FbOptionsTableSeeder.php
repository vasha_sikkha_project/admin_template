<?php

use Illuminate\Database\Seeder;

class FbOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('fb_options')->insert([
                'options_id' => $i,
                'fill_blanks_id' => rand(1, 10),
                'options' => rand(1, 100)
            ]);
        }
    }
}
