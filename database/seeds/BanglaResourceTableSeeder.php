<?php

use Illuminate\Database\Seeder;

class BanglaResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('bangla_resource')->insert([
                'bangla_word_id' => $i,
                'word' => Str::random(10)
            ]);
        }
    }
}
