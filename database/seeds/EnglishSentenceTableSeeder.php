<?php

use Illuminate\Database\Seeder;

class EnglishSentenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('english_sentence')->insert([
                'english_sentence_id' => $i,
                'english_sentence' => Str::random(10)
            ]);
        }
    }
}
