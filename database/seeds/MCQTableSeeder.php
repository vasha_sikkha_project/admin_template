<?php

use Illuminate\Database\Seeder;

class MCQTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('mcq')->insert([
                'mcq_id' => $i,
                'word_task_id' => rand(1, 5),
                'user_id' => rand(1, 2),
                'explanation' => Str::random(10),
                'answer' => rand(1, 100),
                'question' => Str::random(10)
            ]);
        }
    }
}
