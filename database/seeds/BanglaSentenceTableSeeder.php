<?php

use Illuminate\Database\Seeder;

class BanglaSentenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('bangla_sentence')->insert([
                'bangla_sentence_id' => $i,
                'bangla_sentence' => Str::random(10)
            ]);
        }
    }
}
