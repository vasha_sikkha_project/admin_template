<?php

use Illuminate\Database\Seeder;

class WordTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('word_task')->insert([
                'word_task_id' => $i,
                'topic_id' => rand(1, 2),
                'task_id' => rand(1, 3),
                'level_id' => rand(1, 2),
                'experience_id' => rand(1, 1)
            ]);
        }
    }
}
