<?php

use Illuminate\Database\Seeder;

class TranslationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; ++$i)
        {
            DB::table('translation')->insert([
                'translation_id' => $i,
                'another_sentence_id' => rand(1, 50),
                'bangla_sent_id' => rand(1, 50),
                'user_id' => rand(1, 2),
                'language_id' => rand(1, 2)
            ]);
        }
    }
}
