<?php

use Illuminate\Database\Seeder;

class MCQOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('mcq_options')->insert([
                'options_id' => $i,
                'mcq_id' => rand(1, 10),
                'dictionary_id' => rand(1, 100)
            ]);
        }
    }
}
