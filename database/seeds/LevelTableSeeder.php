<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; ++$i)
        {
            DB::table('level')->insert([
                'level_id' => $i,
                'difficulty' => rand(1, 3),
                'score_required' => 20
            ]);
        }
    }
}
