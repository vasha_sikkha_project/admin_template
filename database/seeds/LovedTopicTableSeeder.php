<?php

use Illuminate\Database\Seeder;

class LovedTopicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('loved_topic')->insert([
                'favourite_topic_id' => $i,
                'topic_id' => rand(1, 3),
                'user_id' => rand(1, 2)
            ]);
        }
    }
}
