<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; ++$i)
        {
            DB::table('task')->insert([
                'task_id' => $i,
                'task_name' => Str::random(5),
                'wordOrSentence' => rand(0, 1)
            ]);
        }
    }
}
