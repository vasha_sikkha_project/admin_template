<?php

use Illuminate\Database\Seeder;

class SentenceTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; ++$i)
        {
            DB::table('sentence_task')->insert([
                'sentence_task_id' => $i,
                'topic_id' => rand(1, 2),
                'task_id' => rand(1, 2),
                'level_id' => rand(1, 2),
                'experience_id' => rand(1, 1)
            ]);
        }
    }
}
