<?php

use Illuminate\Database\Seeder;

class EnglishResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i)
        {
            DB::table('english_resource')->insert([
                'english_word_id' => $i,
                'word' => Str::random(10),
                'image_link' => Str::random(10)
            ]);
        }
    }
}
