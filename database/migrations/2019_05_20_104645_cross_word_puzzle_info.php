<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrossWordPuzzleInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cross_word_puzzle_info', function (Blueprint $table) {
            $table->increments('info_id')->unsigned();
            $table->integer('cross_id')->unsigned();
            $table->foreign('cross_id')->references('cross_id')->on('cross_word_puzzle');
            $table->integer('dictionary_id')->unsigned();
            $table->foreign('dictionary_id')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cross_word_puzzle_info');
    }
}
