<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SentenceTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentence_task', function (Blueprint $table) {
            $table->increments('sentence_task_id');
            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')->references('task_id')->on('task');
            $table->integer('topic_id')->unsigned();
            $table->foreign('topic_id')->references('topic_id')->on('topic');
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('level_id')->on('level');
            $table->integer('experience_id')->unsigned();
            $table->foreign('experience_id')->references('experience_id')->on('experience');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sentence_task');
    }
}
