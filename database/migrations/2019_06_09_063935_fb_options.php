<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FbOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_options', function (Blueprint $table) {
            $table->increments('options_id');
            $table->integer('fill_blanks_id')->unsigned();
            $table->foreign('fill_blanks_id')->references('fill_blanks_id')->on('fill_in_the_blanks');
            $table->integer('options')->unsigned();
            $table->foreign('options')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
        });
    }
}
