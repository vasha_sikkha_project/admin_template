<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Translation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation', function (Blueprint $table) {
            $table->increments('translation_id');
            $table->integer('another_sentence_id')->unsigned();
            $table->foreign('another_sentence_id')->references('english_sentence_id')->on('english_sentence');
            $table->integer('bangla_sent_id')->unsigned();
            $table->foreign('bangla_sent_id')->references('bangla_sentence_id')->on('bangla_sentence');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('language_id')->on('language');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation');
    }
}
