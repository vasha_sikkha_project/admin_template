<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FbAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_answer', function (Blueprint $table) {
            $table->increments('answer_id');
            $table->integer('fill_blanks_id')->unsigned();
            $table->foreign('fill_blanks_id')->references('fill_blanks_id')->on('fill_in_the_blanks');
            $table->integer('answer')->unsigned();
            $table->foreign('answer')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_answers');
    }
}
