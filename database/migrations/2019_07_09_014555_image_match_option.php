<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImageMatchOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_match_option', function (Blueprint $table) {
            $table->increments('options_id');
            $table->integer('matchword_id')->unsigned();
            $table->foreign('matchword_id')->references('matchword_id')->on('image_match');
            $table->integer('option')->unsigned();
            $table->foreign('option')->references('dictionary_id')->on('dictionary');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
