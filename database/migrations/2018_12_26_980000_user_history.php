<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_history', function (Blueprint $table) {
            $table->increments('userhistory_id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('topic_id')->unsigned();
            $table->foreign('topic_id')->references('topic_id')->on('topic');
            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')->references('task_id')->on('task');
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('level_id')->on('level');
            $table->double('progress');
            $table->timestamp('upload_time')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_history');
    }
}
