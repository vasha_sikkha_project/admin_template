<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dictionary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary', function (Blueprint $table) {
            $table->increments('dictionary_id');
            $table->integer('another_word_id')->unsigned();
            $table->foreign('another_word_id')->references('english_word_id')->on('english_resource');
            $table->integer('bangla_word_id')->unsigned();
            $table->foreign('bangla_word_id')->references('bangla_word_id')->on('bangla_resource');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('language_id')->on('language');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary');
    }
}
