<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WordPicture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_picture', function (Blueprint $table) {
            $table->increments('word_picture_id');
            $table->integer('word_task_id')->unsigned();
            $table->foreign('word_task_id')->references('word_task_id')->on('word_task');
            $table->integer('dictionary_id')->unsigned();
            $table->foreign('dictionary_id')->references('dictionary_id')->on('dictionary');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('word_picture');
    }
}
