<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MCQ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcq', function (Blueprint $table) {
            $table->increments('mcq_id');
            $table->integer('word_task_id')->unsigned();
            $table->foreign('word_task_id')->references('word_task_id')->on('word_task');
            $table->string('explanation');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->string('question');
            $table->integer('answer')->unsigned();
            $table->foreign('answer')->references('dictionary_id')->on('dictionary');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcq');
    }
}
