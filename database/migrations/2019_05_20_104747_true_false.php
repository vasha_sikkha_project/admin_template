<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrueFalse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('true_false', function (Blueprint $table) {
            $table->increments('true_false_id');
            $table->integer('sentence_task_id')->unsigned();
            $table->foreign('sentence_task_id')->references('sentence_task_id')->on('sentence_task');
            $table->string('answer');
            $table->string('explanation');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('translation_id')->unsigned();
            $table->foreign('translation_id')->references('translation_id')->on('translation');
            $table->string('question');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('true_false');
    }
}
