<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class McqOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcq_options', function (Blueprint $table) {
            $table->increments('options_id');
            $table->integer('mcq_id')->unsigned();
            $table->foreign('mcq_id')->references('mcq_id')->on('mcq');
            $table->integer('dictionary_id')->unsigned();
            $table->foreign('dictionary_id')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcq_options');
    }
}
