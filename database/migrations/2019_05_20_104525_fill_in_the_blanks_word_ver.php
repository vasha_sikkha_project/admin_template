<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillInTheBlanksWordVer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fill_in_the_blanks_word_ver', function (Blueprint $table) {
            $table->increments('fill_blanks_word_id');
            $table->integer('word_task_id')->unsigned();
            $table->foreign('word_task_id')->references('word_task_id')->on('word_task');
            $table->integer('dictionary_id')->unsigned();
            $table->foreign('dictionary_id')->references('dictionary_id')->on('dictionary');
            $table->string('incomplete_word');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fill_in_the_blanks_word_ver');
    }
}
