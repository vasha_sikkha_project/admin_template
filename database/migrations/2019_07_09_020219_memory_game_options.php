<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MemoryGameOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_game_options', function (Blueprint $table) {
            $table->increments('options_id');
            $table->integer('memory_game_id')->unsigned();
            $table->foreign('memory_game_id')->references('memory_game_id')->on('memory_game');
            $table->integer('options')->unsigned();
            $table->foreign('options')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_game_options');
    }
}
