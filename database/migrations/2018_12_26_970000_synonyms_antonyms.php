<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SynonymsAntonyms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('synonyms_antonyms', function (Blueprint $table) {
            $table->increments('synonym_antonym_id');
            $table->integer('word_task_id')->unsigned();
            $table->foreign('word_task_id')->references('word_task_id')->on('word_task');
            $table->integer('base_word_id')->unsigned();
            $table->foreign('base_word_id')->references('dictionary_id')->on('dictionary');
            $table->integer('quiz_id')->unsigned()->nullable();
            // $table->foreign('base_word_id')->references('english_word_id')->on('english_resource');
            $table->integer('synonym_word_id')->unsigned();
            $table->foreign('synonym_word_id')->references('dictionary_id')->on('dictionary');
            // $table->foreign('synonym_word_id')->references('english_word_id')->on('english_resource');
            $table->integer('antonym_word_id')->unsigned();
            $table->foreign('antonym_word_id')->references('dictionary_id')->on('dictionary');
            // $table->foreign('antonym_word_id')->references('english_word_id')->on('english_resource');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('synonyms_antonyms');
    }
}
