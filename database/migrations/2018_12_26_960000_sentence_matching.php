<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SentenceMatching extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentence_matching', function (Blueprint $table) {
            $table->increments('sen_match_id');
            $table->integer('sentence_task_id')->unsigned();
            $table->foreign('sentence_task_id')->references('sentence_task_id')->on('sentence_task');
            $table->integer('translation_id')->unsigned();
            $table->foreign('translation_id')->references('translation_id')->on('translation');
            $table->string('broken_sentence');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sentence_matching');
    }
}
