 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('age')->nullable();
            $table->string('password');
            $table->boolean('privilege')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('institution')->nullable();
            $table->string('present_address')->nullable();
            $table->string('latest_image')->nullable();
            $table->integer('experience_id')->unsigned()->nullable();
            $table->foreign('experience_id')->references('experience_id')->on('experience');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
