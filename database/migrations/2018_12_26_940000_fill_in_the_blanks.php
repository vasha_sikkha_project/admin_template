<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillInTheBlanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fill_in_the_blanks', function (Blueprint $table) {
            $table->increments('fill_blanks_id');
            $table->integer('sentence_task_id')->unsigned();
            $table->foreign('sentence_task_id')->references('sentence_task_id')->on('sentence_task');
            $table->integer('translation_id')->unsigned();
            $table->foreign('translation_id')->references('translation_id')->on('translation');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('quiz_id')->unsigned()->nullable();
            $table->string('incomplete_sentence');
            $table->string('explanation');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fill_in_the_blank');
    }
}
