<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WordPictureOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_picture_options', function (Blueprint $table) {
            $table->increments('options_id');
            $table->integer('word_picture_id')->unsigned();
            $table->foreign('word_picture_id')->references('word_picture_id')->on('word_picture');
            $table->integer('options')->unsigned();
            $table->foreign('options')->references('dictionary_id')->on('dictionary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('word_picture_options');
    }
}
