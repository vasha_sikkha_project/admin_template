getting started:

1. clone

2. inside the project folder do <i>composer install</i> -> manages composer dependencies

3. copy a .env.example, rename it to .env and edit it

4. <i>php artisan key:generate</i> generates an app key

5. <i>php artisan migrate</i> to create the tables

6. <i>php artisan passport:install</i> to install passport(creates 2 clients through which access token is acquired)

7. in your .env do:

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

the above three is already set

the below 3 will be db name, username and password

DB_DATABASE=makeAPI

DB_USERNAME=root

DB_PASSWORD=

#CORS solution: run the following commands
1. composer require barryvdh/laravel-cors
2. php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"


#FOR CSV FILE 
#Please change in php.ini file these three parameters. php.ini is in xampp or wamp file, inside php folder. 

upload_max_filesize = 60M
post_max_size = 100M
max_execution_time = 300