<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Bangla Sentence</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Products</option>
                        <option value="">Services</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                    <select class="js-select2 au-select-dark" name="time">
                        <option selected="selected">All Time</option>
                        <option value="">By Month</option>
                        <option value="">By Day</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>

                            <th>id</th>
                            <th>bangla sentence</th>
                            <th>created at</th>
                            <th>updated at</th>
                            <!-- <th>status</th> -->
                            <!-- <th>price</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataBangla as $value)
                        <tr class="tr-shadow">

                            <td>{{ $value->bangla_sentence_id }}</td>
                            <td>
                                <span class="block-email">{{ $value->bangla_sentence }}</span>
                            </td>
                            <td class="desc">{{ $value->created_at }}</td>
                            <td>
                                <span class="status--process">{{ $value->updated_at }}</span>
                            </td>
                            @if( Auth::user()->privilege >= 10 ) 
                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-placement="top" title="Edit" data-toggle="modal"
                                        data-target="#editModal-{{ $value->bangla_sentence_id }}">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>

                                    <!-- #################### -->

                                    <!-- Bangla Edit Pop-up Modal Starts Here. DON'T TOUCH IT. JUST DON'T. -->
                                    <div class="modal fade" id="editModal-{{ $value->bangla_sentence_id }}"
                                        tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
                                        aria-hidden="true" data-backdrop="false">
                                        <!-- data-backdrop causes backdrop-shadow fading problem and our form goes behind a backdrop. no other solution found other than taking it all outside but $value->id can't be passed to outer segments. try not to use backdrop. -->
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update
                                                        Bangla Sentence
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form
                                                    action="{{ route('update-bangla-sentences',['id' => $value->bangla_sentence_id]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Current Bangla Sentence</label>
                                                            <input class="form-control" id="currentBanglaWord"
                                                                type="text"
                                                                placeholder="<?php echo $value->bangla_sentence; ?>"
                                                                readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Bangla Sentence</label>
                                                            <input type="text" name="updatedBanglaWord"
                                                                id="updatedBanglaWordInput"
                                                                value="<?php echo $value->bangla_sentence; ?>"
                                                                class="form-control">
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Bangla Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- #everythong about delete and delete modal -->

                                    @include('modals.deleteModal', ['routeName' => 'delete-bangla-sentence', 'idx' =>
                                    $value->bangla_sentence_id])


                                </div>
                            </td>
                            @endif
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
            <?php
                        if(sizeof($dataBangla)>=1){
                                $index = $dataBangla[sizeof($dataBangla)-1];
                                $test = $index->bangla_sentence_id+1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                     <form action="{{ url('/sentence/bangla-sentences/'.$test) }}">
                        <button class="au-btn au-btn-load" type="submit">load more</button>
                    </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
