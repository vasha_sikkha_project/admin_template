<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Fix Jumbled Sentences</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">
                    <div class="table-data__tool-right">
                        
                        <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                            <a href="{{ route('Instruction-for-jumble-sentence') }}"
                                style="text-decorations:none; color:inherit;">
                                <i class="zmdi zmdi-cloud"></i>Import</a></button>

                    </div>
                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Products</option>
                        <option value="">Services</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                    <select class="js-select2 au-select-dark" name="time">
                        <option selected="selected">All Time</option>
                        <option value="">By Month</option>
                        <option value="">By Day</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <!-- <th>
                                        <label class="au-checkbox">
                                            <input type="checkbox">
                                            <span class="au-checkmark"></span>
                                        </label>
                                    </th> -->
                            <th>id</th>
                            <th>english sentence</th>
                            <th>bangla sentence</th>
                            <th>jumbled sentence</th>
                            <th>created at</th>
                            <!-- <th>status</th> -->
                            <!-- <th>price</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fixJumbledSentences as $value)
                        <tr class="tr-shadow">


                            <!-- <td>
                                        <label class="au-checkbox">
                                            <input type="checkbox">
                                            <span class="au-checkmark"></span>
                                        </label>
                                    </td> -->
                            <td>{{ $value->id }}</td>
                            <td>
                                <span class="block-email">{{ $value->englishSentence }}</span>
                            </td>
                            <td>
                                <span class="block-email">{{ $value->banglaSentence }}</span>
                            </td>
                            <td>
                                <span class="block-email">{{ $value->jumbledSentence }}</span>
                            </td>
                            <td class="desc">{{ $value->created_at }}</td>

                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-toggle="modal" data-placement="top"
                                        data-target="#editModalJumble-{{ $value->id }}" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>

                                    <!-- #################### -->

                                    <!-- Dictionary Edit Pop-up Modal Starts Here. DON'T TOUCH IT. -->
                                    <div class="modal fade" id="editModalJumble-{{ $value->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true"
                                        data-backdrop="false">
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update
                                                        Fixed Jumbled Sentence
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form
                                                    action="{{ route('update-fixed-jumble',['dictionaryid' => $value->id]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}
                                                    {{-- {{ method_field('PATCH') }} --}}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Correct English Sentence</label>
                                                            <input class="form-control" type="text"
                                                                placeholder="<?php echo $value->englishSentence; ?>"
                                                                readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Bangla Meaning</label>
                                                            <input class="form-control" type="text"
                                                                placeholder="<?php echo $value->banglaSentence; ?>"
                                                                readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Jumbled Sentence</label>
                                                            <input type="text" name="updatedJumbledSentence"
                                                                id="updatedinc"
                                                                value="<?php echo $value->jumbledSentence; ?>"
                                                                class="form-control">
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Dictionary Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- ################## -->

                                    @include('modals.deleteModal', ['routeName' => 'delete-fixjumble-sentence',
                                    'idx' => $value->id])



                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
                <?php
                        if(sizeof($fixJumbledSentences)>=1){
                                $index = $fixJumbledSentences[sizeof($fixJumbledSentences)-1];
                                $test = $index->id+1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                <form action="{{ url('/sentence/jumbled_sentence/'.$test) }}">
                    <button class="au-btn au-btn-load" type="submit">load more</button>
                </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
