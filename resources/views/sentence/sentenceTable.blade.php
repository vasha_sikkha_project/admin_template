@extends('layouts.navbarLayout')
<!-- MAIN CONTENT-->

@section('tableContent')

@include('sentence.singleTable.banglaSentenceTable', ['dataBangla' => $dataBangla])

@include('sentence.singleTable.englishSentenceTable', ['dataEnglish' => $dataEnglish])

@include('sentence.singleTable.translationTable', ['dataTranslartion' => $dataTranslartion])

@include('sentence.singleTable.fillInTheGapsTable', ['fillInTheBlanks' => $fillInTheBlanksSentence])

@include('sentence.singleTable.jumbledSentenceTable', ['fixJumbledSentences' => $fixJumble])

@include('sentence.singleTable.sentenceMatchingTable', ['sentenceMatching' => $senMatch])


@stop
