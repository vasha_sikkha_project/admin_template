@extends('layouts.navbarLayout')
<!-- MAIN CONTENT-->

@section('tableContent')


<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>All Users</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">
                    <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                        <i class="zmdi zmdi-plus"></i>add item</button>
                    <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                        <select class="js-select2" name="type">
                            <option selected="selected">Export</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Products</option>
                        <option value="">Services</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                    <select class="js-select2 au-select-dark" name="time">
                        <option selected="selected">All Time</option>
                        <option value="">By Month</option>
                        <option value="">By Day</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <!-- <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th> -->
                            <th>id</th>
                            <th>Username</th>
                            <th>email</th>
                            <th>age</th>
                            <th>mobile number</th>
                            <th>privilege</th>
                            <th>created at</th>
                            <!-- <th>price</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allUser as $value)
                        <tr class="tr-shadow">


                            <!-- <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td> -->
                            <td>{{ $value->user_id }}</td>
                            <td>
                                <span class="block-email">{{ $value->name }}</span>
                            </td>
                            <td class="desc">{{ $value->email }}</td>
                            <td class="desc">{{ $value->age }}</td>
                            <td class="desc">{{ $value->mobile_number }}</td>
                            <td>
                            @if(  $value->privilege >= 10 ) 
                                <span class="status--process">ADMIN</span>
                            @elseif(  $value->privilege >= 3 ) 
                                <span class="status--process">TEACHER</span>
                            @else
                                <span class="status--process">STUDENT</span>
                            @endif

                            </td>
                            <td>
                                <span class="status--process">{{ $value->created_at }}</span>
                            </td>
                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-placement="top" title="Edit" data-toggle="modal"
                                        data-target="#editModaluser-{{ $value->user_id }}">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>
                                    <!-- #################### -->

                                    <!-- Bangla Edit Pop-up Modal Starts Here. DON'T TOUCH IT. JUST DON'T. -->
                                    <div class="modal fade" id="editModaluser-{{ $value->user_id }}"
                                        tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
                                        aria-hidden="true" data-backdrop="false">
                                        <!-- data-backdrop causes backdrop-shadow fading problem and our form goes behind a backdrop. no other solution found other than taking it all outside but $value->id can't be passed to outer segments. try not to use backdrop. -->
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update User
                                                        Data
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form action="{{ route('update-user-data',['id' => $value->user_id]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label><b>User Name</b></label>
                                                            <input class="form-control" type="text"
                                                                placeholder="<?php echo $value->name; ?>" readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><b>Email</b></label>
                                                            <input class="form-control" type="text"
                                                                placeholder="<?php echo $value->email; ?>" readonly>
                                                        </div>

                                                        

                                                        <div class="form-group">
                                                            <label><b>Privilege</b></label>
                                                            <!-- <input type="text" name="updatedPrivilege" id="updatedprvlg"
                                                                
                                                                class="form-control"> -->

                                                                <div class="rs-select2--light rs-select2--sm">
                                                                    <select class="js-select2" name="updatedPrivilege" id="updatedprvlg">
                                                                        
                                                                            <option value="10">ADMIN</option>
                                                                            <option value="3">TEACHER</option>
                                                                            <option value="1">STUDENT</option>
                                                                        
                                                                    </select>
                                                                    <div class="dropDownSelect2"></div>
                                                                </div>
                                                                
                                                        </div>

                                                        <div class="form-group">
                                                            <label><b>Age</b></label>
                                                            <input class="form-control" type="text"
                                                                placeholder="<?php echo $value->age; ?>" readonly>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Bangla Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- #everythong about delete and delete modal -->

                                    @include('modals.deleteModal', ['routeName' => 'delete-user-data', 'idx' =>
                                    $value->user_id])




                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
                <button class="au-btn au-btn-load">load more</button>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>

@stop
