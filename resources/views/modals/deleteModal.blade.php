<button type="submit" class="item" data-toggle="modal" data-target="#deleteModal-{{ $routeName }}-{{ $idx }}"
    data-placement="top" title="Delete">
    <i class="zmdi zmdi-delete"></i>
</button>

<!-- delete modal -->

{{-- jhamela:: attention:: <!-- id="deleteModal-{{ $routeName }}-{{ $idx }}" - eita emne
naam deyar karon hocche shomosto delete modal er name totally unique rakha.
naile kaj korbe na thik moto. --> --}}

<div class="modal fade" id="deleteModal-{{ $routeName }}-{{ $idx }}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>Are you sure you want to delete it? </p>
            </div>

            <form action=" {{ route($routeName ,['id' => $idx]) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
