@extends('layouts.navbarLayout')

@section('tableContent')

@include('word.singleTable.vocabularyTable', ['vocabulary' => $vocabulary])

@stop