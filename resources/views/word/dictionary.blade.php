@extends('layouts.navbarLayout')

@section('tableContent')

@include('word.singleTable.dictionaryTable', ['dictionary' => $dictionary])

@stop
