@extends('layouts.navbarLayout')
<!-- MAIN CONTENT-->

@section('tableContent')

@include('word.singleTable.banglaTable', ['dataBangla' => $dataBangla])

@include('word.singleTable.englishTable', ['dataEnglish' => $dataEnglish])

@include('word.singleTable.dictionaryTable', ['dataEnglish' => $dataEnglish])

@include('word.singleTable.vocabularyTable', ['vocabulary' => $vocabulary])

@include('word.singleTable.fillInTheBlanksTable', ['fillintheblankswordver' => $fillintheblankswordver])

@include('word.singleTable.synonymAntonymTable', ['synonymantonym' => $synonymantonym])

@include('word.singleTable.mcqTable', ['mcq' => $mcq])

@stop
