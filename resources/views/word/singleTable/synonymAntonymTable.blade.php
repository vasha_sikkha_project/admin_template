<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Synonym Antonym</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">

                    <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                        <a href="{{ route('Instruction-for-Synonyms and antonyms') }}"
                            style="text-decorations:none; color:inherit;">
                            <i class="zmdi zmdi-cloud"></i>Import</a></button>

                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">

                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <!-- <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th> -->
                            <th>id</th>
                            <th>Base word</th>
                            <th>Synonym word</th>
                            <th>Antonym word</th>
                            <th>created at</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($synonymantonym as $value)
                        <tr class="tr-shadow">

                            <td>{{ $value['id'] }}</td>
                            <td>
                                <span class="block-email">{{ $value['baseWord'] }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value['synonymWord'] }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value['antonymWord'] }}</span>
                            </td>
                            <td>{{ $value['created_at'] }}</td>
                            <td>
                                <div class="table-data-feature">
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                        <i class="zmdi zmdi-mail-send"></i>
                                    </button>
                                    <button class="item" data-toggle="modal" data-placement="top"
                                        data-target="#editModalSynAnt-{{ $value['id'] }}" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>


                                    <!-- #################### -->

                                    <!--  Edit Pop-up Modal Starts Here. DON'T TOUCH IT. -->
                                    <div class="modal fade" id="editModalSynAnt-{{ $value['id'] }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true"
                                        data-backdrop="false">
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update
                                                        Synonym & Antonym
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form action="{{ route('update-syn-ant',['id' => $value['id'] ]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}
                                                    {{-- {{ method_field('PATCH') }} --}}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Enter Updated Base Word</label>
                                                            <input type="text" name="updatedBaseWord"
                                                                id="updatedBaseWordInput"
                                                                value="<?php echo $value['baseWord'] ; ?>"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Synonym Word</label>
                                                            <input type="text" name="updatedSynWord"
                                                                id="updatedSynWordInput"
                                                                value="<?php echo $value['synonymWord']; ?>"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Antonym Word</label>
                                                            <input type="text" name="updatedAntWord"
                                                                id="updatedAntWordInput"
                                                                value="<?php echo $value['antonymWord']; ?>"
                                                                class="form-control">
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- ################## -->






                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                                        <i class="zmdi zmdi-more"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
                <?php
                        if(sizeof($synonymantonym)>=1){
                                $index = $synonymantonym[sizeof($synonymantonym)-1];
                                $test = $index['id']+1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                <form action="{{ url('/word/syn_ant/'.$test) }}">
                    <button class="au-btn au-btn-load" type="submit">load more</button>
                </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
