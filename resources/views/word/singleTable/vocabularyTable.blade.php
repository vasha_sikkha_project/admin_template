<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Vocabulary Task</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">

                    <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                        <a href="{{ route('Instruction-for-Synonyms and antonyms') }}"
                            style="text-decorations:none; color:inherit;">
                            <i class="zmdi zmdi-cloud"></i>Import</a></button>

                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">

                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <!-- <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th> -->
                            <th>id</th>
                            <th>English Word</th>
                            <th>Bangla Word</th>
                            <th>created at</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($vocabulary as $value)
                        <tr class="tr-shadow">
                            <td>{{ $value->id }}</td>
                            <td>
                                <span class="block-email">{{ $value->EnglishWord }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value->BanglaWord }}</span>
                            </td>
                            <td>{{ $value->created_at }}</td>
                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>

                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
                <?php
                        if(sizeof($vocabulary)>=1){
                                $index = $vocabulary[sizeof($vocabulary)-1];
                                $test = $index->id + 1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                <form action="{{ url('/word/vocabulary/'.$test) }}">
                    <button class="au-btn au-btn-load" type="submit">load more</button>
                </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
