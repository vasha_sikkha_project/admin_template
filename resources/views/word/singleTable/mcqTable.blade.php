<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>M.C.Q</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">
                    <div class="table-data__tool-right">

                        <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                            <a href="{{ route('Instruction-for-MCQ') }}" style="text-decorations:none; color:inherit;">
                                <i class="zmdi zmdi-cloud"></i>Import</a></button>

                    </div>
                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">

                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>

                            <th>id</th>
                            <th>question</th>
                            <th>answer</th>
                            <th>meaning</th>
                            <th> options </th>
                            <!-- <th> options </th> -->


                            <th></th>
                        </tr>
                    </thead>
                    <tbody>



                        @foreach($mcq as $value)

                        <tr class="tr-shadow">


                            <td>
                                <span class="block-email">{{ $value->id }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value->question }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value->answer }}</span>
                            </td>
                            <td>
                                <span class="status--process">{{ $value->meaning }}</span>
                            </td>

                            <td>
                                <?php
                        $options = app('App\Http\Controllers\ADMIN_WEB\MCQOptionsController')->getMCQOptions($value->id);  
                ?>

                                <div class="rs-select2--light rs-select2--sm">
                                    <select class="js-select2" name="time">
                                        @foreach($options as $val)
                                        <option value="">{{ $val->options_of_mcq }}</option>
                                        @endforeach
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                            </td>

                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-toggle="modal"
                                        data-target="#editModalMcq-{{ $value->id }}" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>

                                    <!-- #################### -->

                                    <!--  Edit Pop-up Modal Starts Here. DON'T TOUCH IT. -->
                                    <div class="modal fade" id="editModalMcq-{{ $value->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true"
                                        data-backdrop="false">
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update MCQ
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form action="{{ route('update-mcq',['id' => $value->id]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}


                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Enter Updated MCQ</label>
                                                            <input type="text" name="updatedMCQQuestion"
                                                                id="updatedMCQQuestion"
                                                                value="<?php echo  $value->question; ?>"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Answer</label>
                                                            <input type="text" name="updatedAnswer" id="updatedAnswer"
                                                                value="<?php echo $value->answer; ?>"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Meaning</label>
                                                            <input type="text" name="updatedMeaning" id="updatedMeaning"
                                                                value="<?php echo $value->meaning; ?>"
                                                                class="form-control">
                                                        </div>



                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- ################## -->


                                    @include('modals.deleteModal', ['routeName' => 'delete-mcq-entry', 'idx' =>
                                    $value->id])



                                </div>
                            </td>


                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
                <?php
                        if(sizeof($mcq)>=1){
                                $index = $mcq[sizeof($mcq)-1];
                                $test = $index->id+1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                <form action="{{ url('/word/Mcq/'.$test) }}">
                    <button class="au-btn au-btn-load" type="submit">load more</button>
                </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
