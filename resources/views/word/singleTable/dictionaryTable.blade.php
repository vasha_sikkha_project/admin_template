<div class="row">
    <div class="col-lg-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Dictionary Word</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">
                    


                    <form action="/uploadfile" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                               
                                                <div class="form-group">
                                                    <input type="file" class="form-control" name="csv" />
                                                    <br/>
                                                    <input type="submit" class="btn btn-primary" value="ADD DICTIONARY" />
                                                </div>
                                                
                                                
                    </form>

                    
                </div>
            </div>
            <div class="filters m-b-45">
                <div class="rs-select2--dark rs-select2--sm rs-select2--border">

                    <div class="dropDownSelect2"></div>
                </div>
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <!-- <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th> -->
                            <th>id</th>
                            <th>bangla word</th>
                            <th>english word</th>
                            <th>created at</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dictionary as $value)
                        <tr class="tr-shadow">

                            <td>{{ $value->id }}</td>
                            <td>
                                <span class="block-email">{{ $value->banglaWord }}</span>
                            </td>


                            <td>
                                <span class="status--process">{{ $value->englishWord }}</span>
                            </td>
                            <td>{{ $value->created_at }}</td>
                            @if( Auth::user()->privilege >= 10 ) 
                            <td>
                                <div class="table-data-feature">

                                    <button class="item" data-toggle="modal" data-placement="top"
                                        data-target="#editModalDictionary-{{ $value->id }}" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>

                                    <!-- #################### -->

                                    <!-- Dictionary Edit Pop-up Modal Starts Here. DON'T TOUCH IT. -->
                                    <div class="modal fade" id="editModalDictionary-{{ $value->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true"
                                        data-backdrop="false">
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update
                                                        Dictionary Entry
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form
                                                    action="{{ route('update-dictionary',['dictionaryid' => $value->id]) }}"
                                                    method="POST" id="editForm">

                                                    {{ csrf_field() }}
                                                    {{-- {{ method_field('PATCH') }} --}}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Enter Updated Bangla Word</label>
                                                            <input type="text" name="updatedBanglaWord"
                                                                id="updatedBanglaWordInput"
                                                                value="<?php echo $value->banglaWord; ?>"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated English Word</label>
                                                            <input type="text" name="updatedEnglishWord"
                                                                id="updatedEnglishWordInput"
                                                                value="<?php echo $value->englishWord; ?>"
                                                                class="form-control">
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Dictionary Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- ################## -->

                                    @include('modals.deleteModal', ['routeName' => 'delete-dictionary-entry', 'idx' =>
                                    $value->id])


                                </div>
                            </td>
                            @endif
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="user-data__footer">
            <?php
                        if(sizeof($dictionary)>=1){
                                $index = $dictionary[sizeof($dictionary)-1];
                                $test = $index->id+1;
                            }else
                            {
                                $test = 1;
                            }
                    ?>
                     <form action="{{ url('/word/dictionary/'.$test) }}">
                        <button class="au-btn au-btn-load" type="submit">load more</button>
                    </form>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>
