@extends('layouts.navbarLayout')

@section('tableContent')
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">overview</h2>
                    <button class="au-btn au-btn-icon au-btn--blue">
                        <i class="zmdi zmdi-plus"></i>add item</button>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-pin-account"></i>
                            </div>
                            <div class="text">
                                <h2 id="teacherText"></h2>
                                <span>Total Teachers</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart1"></canvas>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-assignment-account"></i>
                            </div>
                            <div class="text">
                                <h2 id="adminText"></h2>
                                <span>Total Admins</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c3">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-graduation-cap"></i>
                            </div>
                            <div class="text">
                                <h2 id="StudentText"></h2>
                                <span>Total Students</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart3"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            
          
        </div>
        
        <div class="row">
          <div class="col-md-12">
            <div class="overview-wrap">
               <h2 class="title-1">Performance Statistics</h2>
               
            </div>
          </div>
        </div>
        <!---<div class="row m-t-25">
      <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c4">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-money"></i>
                            </div>
                            <div class="text">
                                <h2 id="topicpeformanceText"></h2>
                                <span>All Topic Performace</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart4"></canvas>
                        </div>

                    </div>
                </div>
            </div>-->

       <!--- <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-trending-up"></i>
                            </div>
                            <div class="text">
                                <h2 id="MCQPerformanceText"></h2>
                                <span>MCQ Performance</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart5"></canvas>
                            
                        </div>
                    </div>
                </div>
       </div>!--->
        




        <!--<div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c3">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                
                                <i class="zmdi zmdi-trending-up"></i>
                            </div>
                            <div class="text">
                                <h2 id="fillintheblanksPerformanceText"></h2>
                                <span>Fill in the blanks Performance</span>
                            </div>
                        </div>
                        <div class="overview-chart">
                            <canvas id="widgetChart6"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>!--->


        <div class="row m-t-25">
          <div class="col-lg-6">
             <div class="au-card m-b-30">
                <div class="au-card-inner">
                  <h3 class="title-2 m-b-40">Taskwise Performance</h3>
                    <canvas id="sales-chart"></canvas>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="overview-wrap">
               <h2 class="title-1">Levelwise Performance Statistics</h2>
               
            </div>
          </div>
        </div>


        <div class="row m-t-25">
            <div class="col-lg-6">
                <div class="au-card recent-report">
                    <div class="au-card-inner">
                        <h3 class="title-2">MCQ Performance Reports</h3>
                        <div class="chart-info">
                            <div class="chart-info__left">
                                <div class="chart-note">
                                    <span class="dot dot--red"></span>
                                    <span>Failure</span>
                                </div>
                                <div class="chart-note mr-0">
                                    <span class="dot dot--green"></span>
                                    <span>Success</span>
                                </div>
                            </div>
                            <div class="chart-info__right">
                                <div class="chart-statis">
                                    
                                    <span class="index incre">
                                        <i class="zmdi zmdi-long-arrow-up"></i><span id="successrate">%</span></span>
                                    <span class="label">Success(%)</span>
                                </div>
                                <div class="chart-statis mr-0">
                                    <span class="index decre">
                                        <i class="zmdi zmdi-long-arrow-down"></i><span id="failurerate">%</span></span>
                                    <span class="label">Failure(%)</span>
                                </div>
                            </div>
                        </div>
                        <div class="recent-report__chart">
                            <canvas id="recent-rep-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6">
                <div class="au-card recent-report">
                    <div class="au-card-inner">
                        <h3 class="title-2">Fill In the Blanks Reports</h3>
                        <div class="chart-info">
                            <div class="chart-info__left">
                                <div class="chart-note">
                                    <span class="dot dot--red"></span>
                                    <span>Failure</span>
                                </div>
                                <div class="chart-note mr-0">
                                    <span class="dot dot--green"></span>
                                    <span>Success</span>
                                </div>
                            </div>
                            <div class="chart-info__right">
                                <div class="chart-statis">
                                    
                                    <span class="index incre">
                                        <i class="zmdi zmdi-long-arrow-up"></i><span id="fillsuccessrate">%</span></span>
                                    <span class="label">Success(%)</span>
                                </div>
                                <div class="chart-statis mr-0">
                                    <span class="index decre">
                                        <i class="zmdi zmdi-long-arrow-down"></i><span id="fillfailurerate">%</span></span>
                                    <span class="label">Failure(%)</span>
                                </div>
                            </div>
                        </div>
                        <div class="recent-report__chart">
                            <canvas id="recent-rep-chart-2"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row m-t-25">
            <div class="col-lg-6">
                <div class="au-card recent-report">
                    <div class="au-card-inner">
                        <h3 class="title-2">Vocabulary Performance Reports</h3>
                        <div class="chart-info">
                            <div class="chart-info__left">
                                <div class="chart-note">
                                    <span class="dot dot--red"></span>
                                    <span>Failure</span>
                                </div>
                                <div class="chart-note mr-0">
                                    <span class="dot dot--green"></span>
                                    <span>Success</span>
                                </div>
                            </div>
                            <div class="chart-info__right">
                                <div class="chart-statis">
                                    
                                    <span class="index incre">
                                        <i class="zmdi zmdi-long-arrow-up"></i><span id="vocabularysuccessrate">%</span></span>
                                    <span class="label">Success(%)</span>
                                </div>
                                <div class="chart-statis mr-0">
                                    <span class="index decre">
                                        <i class="zmdi zmdi-long-arrow-down"></i><span id="vocabularyfailurerate">%</span></span>
                                    <span class="label">Failure(%)</span>
                                </div>
                            </div>
                        </div>
                        <div class="recent-report__chart">
                            <canvas id="recent-rep-chart-3"></canvas>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6">
                <div class="au-card recent-report">
                    <div class="au-card-inner">
                        <h3 class="title-2">Synonym Antonym Reports</h3>
                        <div class="chart-info">
                            <div class="chart-info__left">
                                <div class="chart-note">
                                    <span class="dot dot--red"></span>
                                    <span>Failure</span>
                                </div>
                                <div class="chart-note mr-0">
                                    <span class="dot dot--green"></span>
                                    <span>Success</span>
                                </div>
                            </div>
                            <div class="chart-info__right">
                                <div class="chart-statis">
                                    
                                    <span class="index incre">
                                        <i class="zmdi zmdi-long-arrow-up"></i><span id="SynAntosuccessrate">%</span></span>
                                    <span class="label">Success(%)</span>
                                </div>
                                <div class="chart-statis mr-0">
                                    <span class="index decre">
                                        <i class="zmdi zmdi-long-arrow-down"></i><span id="SynAntofailurerate">%</span></span>
                                    <span class="label">Failure(%)</span>
                                </div>
                            </div>
                        </div>
                        <div class="recent-report__chart">
                            <canvas id="recent-rep-chart-4"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <div class="row">
          <div class="col-md-12">
            <div class="overview-wrap">
               <h2 class="title-1">User Statistics</h2>
               
            </div>
          </div>
        </div>

        <div class="row m-t-25">
            <div class="col-lg-6">
                <div class="au-card chart-percent-card">
                    <div class="au-card-inner">
                        <h3 class="title-2 tm-b-5">Users by %</h3>
                        <div class="row no-gutters">
                            <div class="col-xl-6">
                                <div class="chart-note-wrap">
                                    <div class="chart-note mr-0 d-block">
                                        <span class="dot dot--blue"></span>
                                        <span>Teachers</span>
                                    </div>
                                    <div class="chart-note mr-0 d-block">
                                        <span class="dot dot--red"></span>
                                        <span>Students</span>
                                    </div>
                                    <div class="chart-note mr-0 d-block">
                                        <span class="dot dot--green"></span>
                                        <span>Admins</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="percent-chart">
                                    <canvas id="percent-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <h2 class="title-1 m-b-25">Earnings By Items</h2>
                <div class="table-responsive table--no-card m-b-40">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th>date</th>
                                <th>order ID</th>
                                <th>name</th>
                                <th class="text-right">price</th>
                                <th class="text-right">quantity</th>
                                <th class="text-right">total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2018-09-29 05:57</td>
                                <td>100398</td>
                                <td>iPhone X 64Gb Grey</td>
                                <td class="text-right">$999.00</td>
                                <td class="text-right">1</td>
                                <td class="text-right">$999.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-28 01:22</td>
                                <td>100397</td>
                                <td>Samsung S8 Black</td>
                                <td class="text-right">$756.00</td>
                                <td class="text-right">1</td>
                                <td class="text-right">$756.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-27 02:12</td>
                                <td>100396</td>
                                <td>Game Console Controller</td>
                                <td class="text-right">$22.00</td>
                                <td class="text-right">2</td>
                                <td class="text-right">$44.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-26 23:06</td>
                                <td>100395</td>
                                <td>iPhone X 256Gb Black</td>
                                <td class="text-right">$1199.00</td>
                                <td class="text-right">1</td>
                                <td class="text-right">$1199.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-25 19:03</td>
                                <td>100393</td>
                                <td>USB 3.0 Cable</td>
                                <td class="text-right">$10.00</td>
                                <td class="text-right">3</td>
                                <td class="text-right">$30.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-29 05:57</td>
                                <td>100392</td>
                                <td>Smartwatch 4.0 LTE Wifi</td>
                                <td class="text-right">$199.00</td>
                                <td class="text-right">6</td>
                                <td class="text-right">$1494.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-24 19:10</td>
                                <td>100391</td>
                                <td>Camera C430W 4k</td>
                                <td class="text-right">$699.00</td>
                                <td class="text-right">1</td>
                                <td class="text-right">$699.00</td>
                            </tr>
                            <tr>
                                <td>2018-09-22 00:43</td>
                                <td>100393</td>
                                <td>USB 3.0 Cable</td>
                                <td class="text-right">$10.00</td>
                                <td class="text-right">3</td>
                                <td class="text-right">$30.00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-3">
                <h2 class="title-1 m-b-25">Top countries</h2>
                <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                    <div class="au-card-inner">
                        <div class="table-responsive">
                            <table class="table table-top-countries">
                                <tbody>
                                    <tr>
                                        <td>United States</td>
                                        <td class="text-right">$119,366.96</td>
                                    </tr>
                                    <tr>
                                        <td>Australia</td>
                                        <td class="text-right">$70,261.65</td>
                                    </tr>
                                    <tr>
                                        <td>United Kingdom</td>
                                        <td class="text-right">$46,399.22</td>
                                    </tr>
                                    <tr>
                                        <td>Turkey</td>
                                        <td class="text-right">$35,364.90</td>
                                    </tr>
                                    <tr>
                                        <td>Germany</td>
                                        <td class="text-right">$20,366.96</td>
                                    </tr>
                                    <tr>
                                        <td>France</td>
                                        <td class="text-right">$10,366.96</td>
                                    </tr>
                                    <tr>
                                        <td>Australia</td>
                                        <td class="text-right">$5,366.96</td>
                                    </tr>
                                    <tr>
                                        <td>Italy</td>
                                        <td class="text-right">$1639.32</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                    <div class="au-card-title" style="background-image:url('images/bg-title-01.jpg');">
                        <div class="bg-overlay bg-overlay--blue"></div>
                        <h3>
                            <i class="zmdi zmdi-account-calendar"></i>26 April, 2018</h3>
                        <button class="au-btn-plus">
                            <i class="zmdi zmdi-plus"></i>
                        </button>
                    </div>
                    <div class="au-task js-list-load">
                        <div class="au-task__title">
                            <p>Tasks for John Doe</p>
                        </div>
                        <div class="au-task-list js-scrollbar3">
                            <div class="au-task__item au-task__item--danger">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Meeting about plan for Admin Template 2018</a>
                                    </h5>
                                    <span class="time">10:00 AM</span>
                                </div>
                            </div>
                            <div class="au-task__item au-task__item--warning">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Create new task for Dashboard</a>
                                    </h5>
                                    <span class="time">11:00 AM</span>
                                </div>
                            </div>
                            <div class="au-task__item au-task__item--primary">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Meeting about plan for Admin Template 2018</a>
                                    </h5>
                                    <span class="time">02:00 PM</span>
                                </div>
                            </div>
                            <div class="au-task__item au-task__item--success">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Create new task for Dashboard</a>
                                    </h5>
                                    <span class="time">03:30 PM</span>
                                </div>
                            </div>
                            <div class="au-task__item au-task__item--danger js-load-item">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Meeting about plan for Admin Template 2018</a>
                                    </h5>
                                    <span class="time">10:00 AM</span>
                                </div>
                            </div>
                            <div class="au-task__item au-task__item--warning js-load-item">
                                <div class="au-task__item-inner">
                                    <h5 class="task">
                                        <a href="#">Create new task for Dashboard</a>
                                    </h5>
                                    <span class="time">11:00 AM</span>
                                </div>
                            </div>
                        </div>
                        <div class="au-task__footer">
                            <button class="au-btn au-btn-load js-load-btn">load more</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                    <div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
                        <div class="bg-overlay bg-overlay--blue"></div>
                        <h3>
                            <i class="zmdi zmdi-comment-text"></i>New Messages</h3>
                        <button class="au-btn-plus">
                            <i class="zmdi zmdi-plus"></i>
                        </button>
                    </div>
                    <div class="au-inbox-wrap js-inbox-wrap">
                        <div class="au-message js-list-load">
                            <div class="au-message__noti">
                                <p>You Have
                                    <span>2</span>

                                    new messages
                                </p>
                            </div>
                            <div class="au-message-list">
                                <div class="au-message__item unread">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-02.jpg" alt="John Smith">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">John Smith</h5>
                                                <p>Have sent a photo</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>12 Min ago</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="au-message__item unread">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap online">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-03.jpg" alt="Nicholas Martinez">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">Nicholas Martinez</h5>
                                                <p>You are now connected on message</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>11:00 PM</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="au-message__item">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap online">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-04.jpg" alt="Michelle Sims">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">Michelle Sims</h5>
                                                <p>Lorem ipsum dolor sit amet</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>Yesterday</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="au-message__item">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-05.jpg" alt="Michelle Sims">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">Michelle Sims</h5>
                                                <p>Purus feugiat finibus</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>Sunday</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="au-message__item js-load-item">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap online">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-04.jpg" alt="Michelle Sims">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">Michelle Sims</h5>
                                                <p>Lorem ipsum dolor sit amet</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>Yesterday</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="au-message__item js-load-item">
                                    <div class="au-message__item-inner">
                                        <div class="au-message__item-text">
                                            <div class="avatar-wrap">
                                                <div class="avatar">
                                                    <img src="images/icon/avatar-05.jpg" alt="Michelle Sims">
                                                </div>
                                            </div>
                                            <div class="text">
                                                <h5 class="name">Michelle Sims</h5>
                                                <p>Purus feugiat finibus</p>
                                            </div>
                                        </div>
                                        <div class="au-message__item-time">
                                            <span>Sunday</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="au-message__footer">
                                <button class="au-btn au-btn-load js-load-btn">load more</button>
                            </div>
                        </div>
                        <div class="au-chat">
                            <div class="au-chat__title">
                                <div class="au-chat-info">
                                    <div class="avatar-wrap online">
                                        <div class="avatar avatar--small">
                                            <img src="images/icon/avatar-02.jpg" alt="John Smith">
                                        </div>
                                    </div>
                                    <span class="nick">
                                        <a href="#">John Smith</a>
                                    </span>
                                </div>
                            </div>
                            <div class="au-chat__content">
                                <div class="recei-mess-wrap">
                                    <span class="mess-time">12 Min ago</span>
                                    <div class="recei-mess__inner">
                                        <div class="avatar avatar--tiny">
                                            <img src="images/icon/avatar-02.jpg" alt="John Smith">
                                        </div>
                                        <div class="recei-mess-list">
                                            <div class="recei-mess">Lorem ipsum dolor sit amet, consectetur adipiscing elit non iaculis</div>
                                            <div class="recei-mess">Donec tempor, sapien ac viverra</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="send-mess-wrap">
                                    <span class="mess-time">30 Sec ago</span>
                                    <div class="send-mess__inner">
                                        <div class="send-mess-list">
                                            <div class="send-mess">Lorem ipsum dolor sit amet, consectetur adipiscing elit non iaculis</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="au-chat-textfield">
                                <form class="au-form-icon">
                                    <input class="au-input au-input--full au-input--h65" type="text" placeholder="Type a message">
                                    <button class="au-input-icon">
                                        <i class="zmdi zmdi-camera"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
</div>

</div>



<?php
$abcd=100;
$admins=array();
foreach($admindata as $value){
  array_push($admins,$value->date);
}

$students=array();
foreach($studentdata as $value){
  array_push($students,$value->date);
}

$teachers=array();
foreach($teacherdata as $value){
  array_push($teachers,$value->date);
}



$performance_mcq=array();
foreach($mcqperformancedata as $value){
  array_push($performance_mcq,$value->level);
}

$performance_fillintheblanks=array();
foreach($fillintheblanksperformancedata as $value){
  array_push($performance_fillintheblanks,$value->level);
}



?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{asset("vendor/chartjs/Chart.bundle.min.js")}}"></script>

<script type="text/javascript">

(function ($) {
  // USE STRICT
  "use strict";

  try {
    //WidgetChart 1
    var i;
    var labelis=[];
    var datas=[];
    var teachers = {!! json_encode($teacherdata) !!};
    
    //console.log("Users ",admins[0].date)
    // console.log("Date",test)
    var ctx = document.getElementById("widgetChart1");
    var teachertotal=0;
    
    for(i=0;i < teachers.length;++i){
      labelis.push(teachers[i].date);
      datas.push(teachers[i].total);
      teachertotal=parseInt(teachertotal)+parseInt(teachers[i].total);
    }

    document.getElementById("teacherText").innerHTML = teachertotal;
    if (ctx) {
      ctx.height = 130;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          type: 'line',
          datasets: [{
            data: datas,
            label: 'Teacher ',
            backgroundColor: 'rgba(255,255,255,.1)',
            borderColor: 'rgba(255,255,255,.55)',
          },]
        },
        options: {

          maintainAspectRatio: false,
          legend: {
            display: false
          },
          responsive: true,
        tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Montserrat',
        bodyFontFamily: 'Montserrat',
        cornerRadius: 3,
       intersect: false,
        },
      scales: {
       xAxes: [{
    gridLines: {
      color: 'transparent',
      zeroLineColor: 'transparent'
    },
    ticks: {
      fontSize: 2,
      fontColor: 'transparent'
    }
  }],
  yAxes: [{
    display: false,
    ticks: {
      display: false,
    }
  }]
},
title: {
  display: false,
},
elements: {
  line: {
    borderWidth: 1
  },
  point: {
    radius: 4,
    hitRadius: 10,
    hoverRadius: 4
  }
}
}
      });
    }


    //WidgetChart 2
    var ctx = document.getElementById("widgetChart2");

    
    /*labelis.push('January');
    labelis.push('February');
    labelis.push('March');
    labelis.push('April');*/
    /*$(userdata).each(function( index ) {
  console.log( index + ": " + $( this ).text() );
    });*/
    labelis=[];
    datas=[];
    var admins = {!! json_encode($admindata) !!};
    var admintotal=0;
    
    for(i=0;i < admins.length;++i){
      labelis.push(admins[i].date);
      datas.push(admins[i].total);
      admintotal=parseInt(admintotal)+parseInt(admins[i].total);
    }

    document.getElementById("adminText").innerHTML = admintotal;
    
    if (ctx) {
      ctx.height = 130;
      var myChart = new Chart(ctx, {
        type: 'line',
        
        //labels=new Array(10),
        data: {
        
          labels: labelis,
          type: 'line',
          datasets: [{
            
            
            data: datas,
            //labels: labelis,
            label: 'Registered ',
            backgroundColor: 'transparent',
            borderColor: 'rgba(255,255,255,.55)',
          },]
        },
        options: {

          maintainAspectRatio: false,
          legend: {
            display: false
          },
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Montserrat',
            bodyFontFamily: 'Montserrat',
            cornerRadius: 3,
            intersect: false,
          },
          scales: {
            xAxes: [{
              gridLines: {
                color: 'transparent',
                zeroLineColor: 'transparent'
              },
              ticks: {
                fontSize: 2,
                fontColor: 'transparent'
              }
            }],
            yAxes: [{
              display: false,
              ticks: {
                display: false,
              }
            }]
          },
          title: {
            display: false,
          },
          elements: {
            line: {
              tension: 0.00001,
              borderWidth: 1
            },
            point: {
              radius: 4,
              hitRadius: 10,
              hoverRadius: 4
            }
          }
        }
      });
    }


    //WidgetChart 3
    var ctx = document.getElementById("widgetChart3");
    
    labelis=[];
    datas=[];
    var students = {!! json_encode($studentdata) !!};
    var studenttotal=0;
    
    for(i=0;i < students.length;++i){
      labelis.push(students[i].date);
      datas.push(students[i].total);
      studenttotal=parseInt(studenttotal)+parseInt(students[i].total);
    }

    document.getElementById("StudentText").innerHTML = studenttotal;
    
    if (ctx) {
      ctx.height = 130;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          type: 'line',
          datasets: [{
            data: datas,
            label: 'Students ',
            backgroundColor: 'transparent',
            borderColor: 'rgba(255,255,255,.55)',
          },]
        },
        options: {

          maintainAspectRatio: false,
          legend: {
            display: false
          },
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Montserrat',
            bodyFontFamily: 'Montserrat',
            cornerRadius: 3,
            intersect: false,
          },
          scales: {
            xAxes: [{
              gridLines: {
                color: 'transparent',
                zeroLineColor: 'transparent'
              },
              ticks: {
                fontSize: 2,
                fontColor: 'transparent'
              }
            }],
            yAxes: [{
              display: false,
              ticks: {
                display: false,
              }
            }]
          },
          title: {
            display: false,
          },
          elements: {
            line: {
              borderWidth: 1
            },
            point: {
              radius: 4,
              hitRadius: 10,
              hoverRadius: 4
            }
          }
        }
      });
    }

    


    /*
    //WidgetChart 5
    var ctx = document.getElementById("widgetChart5");
    
    labelis=[];
    datas=[];
    
    var mcq_performance = {!! json_encode($mcqperformancedata) !!};
    var mcq_failure = {!! json_encode($mcqfailuredata) !!};
    
    var mcq_performancetotal=0;
    
    
    for(i=0;i < mcq_performance.length;++i){
      labelis.push('Label : '+mcq_performance[i].level);
      datas.push(mcq_performance[i].total);
      mcq_performancetotal=parseInt(mcq_performancetotal)+parseInt(mcq_performance[i].total);
    }

    document.getElementById("MCQPerformanceText").innerHTML = mcq_performancetotal;
    
    if (ctx) {
      ctx.height = 130;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          type: 'line',
          datasets: [{
            data: datas,
            label: 'Solved ',
            backgroundColor: 'rgba(255,255,255,.1)',
            borderColor: 'rgba(255,255,255,.55)',
          },]
        },
        options: {

          maintainAspectRatio: false,
          legend: {
            display: false
          },
          responsive: true,
        tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Montserrat',
        bodyFontFamily: 'Montserrat',
        cornerRadius: 3,
       intersect: false,
        },
      scales: {
       xAxes: [{
    gridLines: {
      color: 'transparent',
      zeroLineColor: 'transparent'
    },
    ticks: {
      fontSize: 2,
      fontColor: 'transparent'
    }
  }],
  yAxes: [{
    display: false,
    ticks: {
      display: false,
    }
  }]
},
title: {
  display: false,
},
elements: {
  line: {
    borderWidth: 1
  },
  point: {
    radius: 4,
    hitRadius: 10,
    hoverRadius: 4
  }
}
}
      });
    }



    //WidgetChart 6
    var ctx = document.getElementById("widgetChart6");
    
    labelis=[];
    datas=[];
    
    var fillintheblanks_performance = {!! json_encode($fillintheblanksperformancedata) !!};
    var fillintheblanks_performancetotal=0;
    
    for(i=0;i <fillintheblanks_performance.length;++i){
      labelis.push('Label : '+fillintheblanks_performance[i].level);
      datas.push(fillintheblanks_performance[i].total);
      fillintheblanks_performancetotal=parseInt(fillintheblanks_performancetotal)+parseInt(fillintheblanks_performance[i].total);
    }

    document.getElementById("fillintheblanksPerformanceText").innerHTML = fillintheblanks_performancetotal;
    
    if (ctx) {
      ctx.height = 130;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          type: 'line',
          datasets: [{
            data: datas,
            label: 'Solved ',
            backgroundColor: 'rgba(255,255,255,.1)',
            borderColor: 'rgba(255,255,255,.55)',
          },]
        },
        options: {

          maintainAspectRatio: false,
          legend: {
            display: false
          },
          responsive: true,
        tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Montserrat',
        bodyFontFamily: 'Montserrat',
        cornerRadius: 3,
       intersect: false,
        },
      scales: {
       xAxes: [{
    gridLines: {
      color: 'transparent',
      zeroLineColor: 'transparent'
    },
    ticks: {
      fontSize: 2,
      fontColor: 'transparent'
    }
  }],
  yAxes: [{
    display: false,
    ticks: {
      display: false,
    }
  }]
},
title: {
  display: false,
},
elements: {
  line: {
    borderWidth: 1
  },
  point: {
    radius: 4,
    hitRadius: 10,
    hoverRadius: 4
  }
}
}
      });
    }*/



    

    try {
     var i;
    var labelis=[];
    var datas=[];
    var dataf=[];
    //Sales chart
    var ctx = document.getElementById("sales-chart");
    var task_performance = {!! json_encode($taskperformancedata) !!};
    var task_failure = {!! json_encode($taskfailuredata) !!};
    var task_performancetotal=0;
    var task_failuretotal=0;
    
    for(i=0;i < task_performance.length;++i){
      labelis.push('Task : '+task_performance[i].task);
      datas.push(task_performance[i].total);
      task_performancetotal=parseInt(task_performancetotal)+parseInt(task_performance[i].total);
    }

    for(i=0;i < task_failure.length;++i){
      
      dataf.push(task_failure[i].total);
      task_failuretotal=parseInt(task_failuretotal)+parseInt(task_failure[i].total);
    }

    

    if (ctx) {
      ctx.height = 150;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          type: 'line',
          defaultFontFamily: 'Poppins',
          datasets: [{
            label: "Unsolved",
            data: dataf,
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
          }, {
            label: "Solved",
            data: datas,
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Poppins',
            bodyFontFamily: 'Poppins',
            cornerRadius: 3,
            intersect: false,
          },
          legend: {
            display: false,
            labels: {
              usePointStyle: true,
              fontFamily: 'Poppins',
            },
          },
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: false,
                labelString: 'Month'
              },
              ticks: {
                fontFamily: "Poppins"
              }
            }],
            yAxes: [{
              
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'Value',
                fontFamily: "Poppins"

              },
              ticks: {
                fontFamily: "Poppins"
              }
            }]
          },
          title: {
            display: false,
            text: 'Normal Legend'
          }
        }
      });
    }


  } catch (error) {
    console.log(error);
  }

    // Recent Report
    const Solvedcolor = 'rgba(0,173,95,0.8)'
    const Failedcolor = 'rgba(255, 0, 0, 0.6)'

    var elements = 10
    var data1 = [ ]
    var data2 = [ ]
    labelis=[]
    

    var ctx = document.getElementById("recent-rep-chart");
    var mcq_performance = {!! json_encode($mcqperformancedata) !!};
    var mcq_failure = {!! json_encode($mcqfailuredata) !!};
    
    
    var mcq_performancetotal=0;
    var mcq_failuretotal=0;
    for(i=0;i < mcq_performance.length;++i){
      labelis.push('Level : '+mcq_performance[i].level);
      data1.push(mcq_performance[i].total);
      mcq_performancetotal=parseInt(mcq_performancetotal)+parseInt(mcq_performance[i].total);
    }

    var lev=1;
    var j;

    for(i=0;i < mcq_failure.length;++i){
      for(j=lev;j<mcq_failure[i].level;j++){
        
        data2.push(0);
      }
      lev=mcq_failure[i].level+1;
      
      data2.push(mcq_failure[i].total);
      mcq_failuretotal=parseInt(mcq_failuretotal)+parseInt(mcq_failure[i].total);
    }

    var success=((mcq_performancetotal)/(mcq_performancetotal+mcq_failuretotal))*100;
    
    var failure= 100 - (success);
    
    

    document.getElementById("successrate").innerHTML =Math.round(success)+ " %";
    document.getElementById("failurerate").innerHTML = Math.round(failure)+ " %";


    if (ctx) {
      ctx.height = 250;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          datasets: [
            {
              label: 'Solved',
              backgroundColor: Solvedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(0,255,0,0.3)',
              borderWidth: 0,
              data: data1

            },
            {
              label: 'Failed',
              backgroundColor: Failedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(255, 0, 0, 0.4)',
              borderWidth: 0,
              data: data2

            }
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              gridLines: {
                drawOnChartArea: true,
                color: '#f2f2f2'
              },
              ticks: {
                fontFamily: "Poppins",
                fontSize: 12
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true,
                maxTicksLimit: 5,
                stepSize: .5,
                max: 5,
                fontFamily: "Poppins",
                fontSize: 12
              },
              gridLines: {
                display: true,
                color: '#f2f2f2'

              }
            }]
          },
          elements: {
            point: {
              radius: 0,
              hitRadius: 10,
              hoverRadius: 4,
              hoverBorderWidth: 3
            }
          }


        }
      });
    }



    //Recent Report 2
    try{
      const Solvedcolor = 'rgba(0,173,95,0.8)'
    const Failedcolor = 'rgba(255, 0, 0, 0.6)'

    var elements = 10
    var data1 = [ ]
    var data2 = [ ]
    labelis=[]
    

    var ctx = document.getElementById("recent-rep-chart-2");
    var fillintheblanks_performance = {!! json_encode($fillintheblanksperformancedata) !!};
    var fillintheblanks_failure = {!! json_encode($fillintheblanksfailuredata) !!};
    
    
    var fillintheblanks_performancetotal=0;
    var fillintheblanks_failuretotal=0;
    for(i=0;i < fillintheblanks_performance.length;++i){
      labelis.push('Level : '+fillintheblanks_performance[i].level);
      data1.push(fillintheblanks_performance[i].total);
      fillintheblanks_performancetotal=parseInt(fillintheblanks_performancetotal)+parseInt(fillintheblanks_performance[i].total);
    }
    var lev=1;
    var j;

    for(i=0;i < fillintheblanks_failure.length;++i){
      for(j=lev;j<fillintheblanks_failure[i].level;j++){
        data2.push(0);
      }
      lev=fillintheblanks_failure[i].level+1;
      
      data2.push(fillintheblanks_failure[i].total);
      fillintheblanks_failuretotal=parseInt(fillintheblanks_failuretotal)+parseInt(fillintheblanks_failure[i].total);
    }

    var success=((fillintheblanks_performancetotal)/(fillintheblanks_performancetotal+fillintheblanks_failuretotal))*100;
    
    var failure= 100 - (success);
    
    

    document.getElementById("fillsuccessrate").innerHTML =Math.round(success)+ " %";
    document.getElementById("fillfailurerate").innerHTML = Math.round(failure)+ " %";


    if (ctx) {
      ctx.height = 250;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          datasets: [
            {
              label: 'Solved',
              backgroundColor: Solvedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(0,255,0,0.3)',
              borderWidth: 0,
              data: data1

            },
            {
              label: 'Failed',
              backgroundColor: Failedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(255, 0, 0, 0.4)',
              borderWidth: 0,
              data: data2

            }
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              gridLines: {
                drawOnChartArea: true,
                color: '#f2f2f2'
              },
              ticks: {
                fontFamily: "Poppins",
                fontSize: 12
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true,
                maxTicksLimit: 5,
                stepSize: .5,
                max: 5,
                fontFamily: "Poppins",
                fontSize: 12
              },
              gridLines: {
                display: true,
                color: '#f2f2f2'

              }
            }]
          },
          elements: {
            point: {
              radius: 0,
              hitRadius: 10,
              hoverRadius: 4,
              hoverBorderWidth: 3
            }
          }


        }
      });
    }

  }catch (error) {
    console.log(error);
  }


  //Recent Report 3
  try{
      const Solvedcolor = 'rgba(0,173,95,0.8)'
    const Failedcolor = 'rgba(255, 0, 0, 0.6)'

    var elements = 10
    var data1 = [ ]
    var data2 = [ ]
    labelis=[]
    

    var ctx = document.getElementById("recent-rep-chart-3");
    var vocabulary_performance = {!! json_encode($vocabularyperformancedata) !!};
    var vocabulary_failure = {!! json_encode($vocabularyfailuredata) !!};
    
    
    var vocabulary_performancetotal=0;
    var vocabulary_failuretotal=0;
    for(i=0;i < vocabulary_performance.length;++i){
      labelis.push('Level : '+vocabulary_performance[i].level);
      data1.push(vocabulary_performance[i].total);
      vocabulary_performancetotal=parseInt(vocabulary_performancetotal)+parseInt(vocabulary_performance[i].total);
    }
    var lev=1;
    var j;
    for(i=0;i < vocabulary_failure.length;++i){
      for(j=lev;j<vocabulary_failure[i].level;j++){
        data2.push(0);
      }
      lev=vocabulary_failure[i].level+1;

      //labelis.push('Level : '+vocabulary_failure[i].level);
      data2.push(vocabulary_failure[i].total);
      vocabulary_failuretotal=parseInt(vocabulary_failuretotal)+parseInt(vocabulary_failure[i].total);
    }

    var success=((vocabulary_performancetotal)/(vocabulary_performancetotal+vocabulary_failuretotal))*100;
    
    var failure= 100 - (success);
    
    

    document.getElementById("vocabularysuccessrate").innerHTML =Math.round(success)+ " %";
    document.getElementById("vocabularyfailurerate").innerHTML = Math.round(failure)+ " %";


    if (ctx) {
      ctx.height = 250;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          datasets: [
            {
              label: 'Solved',
              backgroundColor: Solvedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(0,255,0,0.3)',
              borderWidth: 0,
              data: data1

            },
            {
              label: 'Failed',
              backgroundColor: Failedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(255, 0, 0, 0.4)',
              borderWidth: 0,
              data: data2

            }
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              gridLines: {
                drawOnChartArea: true,
                color: '#f2f2f2'
              },
              ticks: {
                fontFamily: "Poppins",
                fontSize: 12
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true,
                maxTicksLimit: 5,
                stepSize: .5,
                max: 5,
                fontFamily: "Poppins",
                fontSize: 12
              },
              gridLines: {
                display: true,
                color: '#f2f2f2'

              }
            }]
          },
          elements: {
            point: {
              radius: 0,
              hitRadius: 10,
              hoverRadius: 4,
              hoverBorderWidth: 3
            }
          }


        }
      });
    }

  }catch (error) {
    console.log(error);
  }

  //Recent Report 4
  try{
      const Solvedcolor = 'rgba(0,173,95,0.8)'
    const Failedcolor = 'rgba(255, 0, 0, 0.6)'

    var elements = 10
    var data1 = [ ]
    var data2 = [ ]
    labelis=[]
    

    var ctx = document.getElementById("recent-rep-chart-4");
    var SynonymAntonym_performance = {!! json_encode($SynonymAntonymperformancedata) !!};
    var SynonymAntonym_failure = {!! json_encode($SynonymAntonymfailuredata) !!};
    
    
    var SynonymAntonym_performancetotal=0;
    var SynonymAntonym_failuretotal=0;
    for(i=0;i < SynonymAntonym_performance.length;++i){
      labelis.push('Level : '+SynonymAntonym_performance[i].level);
      data1.push(SynonymAntonym_performance[i].total);
      SynonymAntonym_performancetotal=parseInt(SynonymAntonym_performancetotal)+parseInt(SynonymAntonym_performance[i].total);
    }
    var lev=1;
    var j;

    for(i=0;i < SynonymAntonym_failure.length;++i){
      for(j=lev;j<SynonymAntonym_failure[i].level;j++){
        data2.push(0);
      }
      lev=SynonymAntonym_failure[i].level+1;
      
      data2.push(SynonymAntonym_failure[i].total);
      SynonymAntonym_failuretotal=parseInt(SynonymAntonym_failuretotal)+parseInt(SynonymAntonym_failure[i].total);
    }

    var success=((SynonymAntonym_performancetotal)/(SynonymAntonym_performancetotal+SynonymAntonym_failuretotal))*100;
    
    var failure= 100 - (success);
    
    

    document.getElementById("SynAntosuccessrate").innerHTML =Math.round(success)+ " %";
    document.getElementById("SynAntofailurerate").innerHTML = Math.round(failure)+ " %";


    if (ctx) {
      ctx.height = 250;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labelis,
          datasets: [
            {
              label: 'Solved',
              backgroundColor: Solvedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(0,255,0,0.3)',
              borderWidth: 0,
              data: data1

            },
            {
              label: 'Failed',
              backgroundColor: Failedcolor,
              borderColor: 'transparent',
              pointHoverBackgroundColor: 'rgba(255, 0, 0, 0.4)',
              borderWidth: 0,
              data: data2

            }
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              gridLines: {
                drawOnChartArea: true,
                color: '#f2f2f2'
              },
              ticks: {
                fontFamily: "Poppins",
                fontSize: 12
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true,
                maxTicksLimit: 5,
                stepSize: .5,
                max: 5,
                fontFamily: "Poppins",
                fontSize: 12
              },
              gridLines: {
                display: true,
                color: '#f2f2f2'

              }
            }]
          },
          elements: {
            point: {
              radius: 0,
              hitRadius: 10,
              hoverRadius: 4,
              hoverBorderWidth: 3
            }
          }


        }
      });
    }

  }catch (error) {
    console.log(error);
  }




    // Percent Chart
    var ctx = document.getElementById("percent-chart");
    if (ctx) {
      ctx.height = 280;
      var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
          datasets: [
            {
              label: "My First dataset",
              data: [teachertotal,studenttotal, admintotal],
              backgroundColor: [
                '#00b5e9',
                '#fa4251',
                '#109b04'
              ],
              hoverBackgroundColor: [
                '#00b5e9',
                '#fa4251',
                '#109b04'
              ],
              borderWidth: [
                0, 0,0
              ],
              hoverBorderColor: [
                'transparent',
                'transparent',
                'tranparent'
              ]
            }
          ],
          labels: [
            'Teachers',
            'Students',
            'Admins'
          ]
        },
        options: {
          maintainAspectRatio: false,
          responsive: true,
          cutoutPercentage: 55,
          animation: {
            animateScale: true,
            animateRotate: true
          },
          legend: {
            display: false
          },
          tooltips: {
            titleFontFamily: "Poppins",
            xPadding: 15,
            yPadding: 10,
            caretPadding: 0,
            bodyFontSize: 16
          }
        }
      });
    }
  

  } catch (error) {
    console.log(error);
  }


})(jQuery);


</script>
@stop