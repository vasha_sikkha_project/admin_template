@extends('layouts.navbarLayout')
@section('tableContent')
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong class="card-title mb-3">Current Topics</strong>
                                        </div>
                                        <div class="card-body">
                                        <div class="mx-auto d-block">
                                            <ol>
                                                {{-- <option value="">--- Select Topic ---</option> --}}
                                                @foreach ($topics as $key => $value)
                                                <li>{{ $value }}</li>
                                                @endforeach
                                            </ol>
                                        </div>
                                        </div>
                                    </div>
                    </div>
                </div>

                <div class="row">
                    
                            <div class="col-md-12">
                        


                                
                    

                                            <form action="/addTopic" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="Product Name">Enter new topic</label>
                                                    <input type="text" name="newtopic" class="form-control" >
                                                </div>

                                                <div class="form-group">
                                                    <input type="file" class="form-control" name="csv" />
                                                    <br/>
                                                    <input type="submit" class="btn btn-primary" value="ADD NEW TOPIC" />
                                                </div>
                                                
                                                
                                            </form>

    
                            </div>
                            
                </div>
                </div></div>

</div>
@stop