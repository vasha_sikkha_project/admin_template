@extends('layouts.navbarLayout')
@section('tableContent')
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong class="card-title mb-3">Instructions</strong>
                                        </div>
                                        <div class="card-body">
                                        <div class="mx-auto d-block">
                                           <ol type="1">
                                               <li>You should use the template csv to upload data.</li>
                                               <li>The blank word will be denoted by "#" . </li>
                                               <li>You give the right word.</li>
                                               <li>You should give the meaning of the sentence.</li>
                                               <li>There should be a difficulty level for each question and it should be between 1-100</li>
                                               <li>You should assign the profession for which the question is suitable.If it is suitable for multiple professions, then the professions will be separated by “#”</li>
                                               <li>You give an explanation for the question but it is optional.</li>
                                           
                                           </ol>
                                           <h4>See this sample image for better understanding</h4><br>
                                           <img src="/images/Sample/fillintheblank.png">
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
                <div class="row">

                        <div class="col-md-6">
                        


                            <div class="card">
                                <button type="button" class="btn btn-outline-link">
                                <br/><br/>
                                    <i class="fa fa-download"></i>&nbsp;<a href="{{ route('Download-Fill-in-the-blank-sample') }}"  target="_blank">Sample csv for fill in the blank</a></button><hr>
                                    <br/>
                                    <button type="button" class="btn btn-outline-link">

                                            <i class="fa fa-plus"></i>&nbsp;<a href="{{ route('Add-topic-UI') }}" target="_blank">Add new topic</a></button>
                               
                        </div></div>
                        <div class="col-md-6">
                        


                                <div class="card">
                                           
                                            <form action="/uploadfillintheblank" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="rs-select2--light rs-select2--md">
                                                    <select class="js-select2" name="quizCK">
                                                            <option value="0">NORMAL</option>
                                                            <option value="-1">QUIZ</option>
                                                        </select>
                                                    <div class="dropDownSelect2"></div>
                                                </div>

                                                <div class="rs-select2--light rs-select2--md">
                                                    <select class="js-select2" name="topic">
                                                    @foreach($topics as $topic)
                                                        <option value="{{$topic->topic_id}}">{{$topic->topic_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    <div class="dropDownSelect2"></div>
                                                </div>

                                                <br/><br/>
                                                <div class="form-group">
                                                    <input type="file" name="csv" />
                                                    <br/><br/>
                                                    <input type="submit" class="btn btn-primary" value="ADD FILL-IN-THE-BLANKS" />
                                                </div>

                                                
                                                
                                                
                                            </form>

    
    
                            </div></div>
                    </div></div></div>

</div>
@stop