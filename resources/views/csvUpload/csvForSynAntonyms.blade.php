@extends('layouts.navbarLayout')
@section('tableContent')
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong class="card-title mb-3">Instructions</strong>
                                        </div>
                                        <div class="card-body">
                                        <div class="mx-auto d-block">
                                           <ol type="1">
                                               <li>You should use the template csv to upload data.</li>
                                               <li>Give the word with synonyms and antonyms with their meaning</li>
                                               <li>You should give difficulty level and targeted profession for this question.</li>
                                               <li>Select a topic from the dropdown box.</li>
                                               <li>Reload after adding a new topic.</li>
                                           </ol>
                                           <h4>See this sample image for better understanding</h4><br>
                                           <img src="/images/Sample/synantonyms.png">
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        


                            <div class="card">
                                <button type="button" class="btn btn-outline-link">
                                <br/> <br/>  
                                    <i class="fa fa-download"></i>&nbsp;<a href="{{ route('Download-SynAntonyms-sample') }}" target="_blank">Sample csv for synonyms and antonyms</a></button><hr>
                                    <br/>
                                    <button type="button" class="btn btn-outline-link">

                                            <i class="fa fa-plus"></i>&nbsp;<a href="{{ route('Add-topic-UI') }}" target="_blank">Add new topic</a></button>
                               
                        </div></div>
                        

                       
                        <div class="col-md-6">
                        


                                <div class="card">
                                            
                                            <form action="/uploadSMatching" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="rs-select2--light rs-select2--md">
                                                    <select class="js-select2" name="quizCK">
                                                            <option value="0">NORMAL</option>
                                                            <option value="-1">QUIZ</option>
                                                        </select>
                                                    <div class="dropDownSelect2"></div>
                                                </div>

                                                <div class="rs-select2--light rs-select2--md">
                                                    <select class="js-select2" name="topic">
                                                    @foreach($topics as $topic)
                                                        <option value="{{$topic->topic_id}}">{{$topic->topic_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    <div class="dropDownSelect2"></div>
                                                </div>

                                                <br/> <br/>
                                                <div class="form-group">
                                                    <input type="file" name="csv" />
                                                    <br/> <br/>
                                                    <input type="submit" class="btn btn-primary" value="ADD FILL-IN-THE-BLANKS" />
                                                </div> 
                                            </form>
    
                            </div></div>
                    </div><br><br><br></div></div>

</div>
@stop