@extends('layouts.navbarLayout')

@section('tableContent')
<div class="row">
    <div class="col-lg-12">
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>All Topics</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">

                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                </div>
                <div class="table-data__tool-right">

                <a href="{{ route('Add-topic-UI') }}">
                    <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                        <i class="zmdi zmdi-cloud"></i>Import
                    </button>
                </a>
                        

                </div>
            </div>
           
            <div class="table-responsive table-data">
                <table id="datatable" class="table table-data2">
                    <thead>
                        <tr>

                            <th>id</th>
                            <th>Topic name</th>
                            <th>topic image</th>

                            <!-- <th>status</th> -->
                            <!-- <th>price</th> -->
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($allTopics as $value)
                        <tr class="tr-shadow">

                            <td>{{ $value->topic_id }}</td>
                            <td>
                                <span class="block-email">{{ $value->topic_name }}</span>
                            </td>
                            <td class="desc">

                            <img src="{{URL::asset('/uploads/TopicImage/'.$value->topic_image)}}" alt="profile Pic" height="100" width="100">


                            </td>

                            <td>
                                <div class="table-data-feature">

                                    <!-- <button class="item" data-placement="top" title="Edit" data-toggle="modal" data-target="#editModal-{{ $value->bangla_word_id }}">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button> -->
                                    <!-- #################### -->

                                    <!-- Bangla Edit Pop-up Modal Starts Here. DON'T TOUCH IT. JUST DON'T. -->
                                    <div class="modal fade" id="editModal-{{ $value->bangla_word_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true" data-backdrop="false">
                                        <!-- data-backdrop causes backdrop-shadow fading problem and our form goes behind a backdrop. no other solution found other than taking it all outside but $value->id can't be passed to outer segments. try not to use backdrop. -->
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update Bangla Word
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>



                                                <!--edit form-->

                                                <form action="{{ route('update-bangla-resources',['id' => $value->bangla_word_id]) }}" method="POST" id="editForm">

                                                    {{ csrf_field() }}

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label>Current Bangla Word</label>
                                                            <input class="form-control" id="currentBanglaWord" type="text" placeholder="<?php echo $value->word; ?>" readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Enter Updated Bangla Word</label>
                                                            <input type="text" name="updatedBanglaWord" id="updatedBanglaWordInput" value="<?php echo $value->word; ?>" class="form-control">
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                        <button type="submit" class="btn btn-primary">Update
                                                            Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Bangla Edit Pop-up Modal Ends Here. DON'T TOUCH IT. JUST DON'T. -->

                                    <!-- #everythong about delete and delete modal -->

                                    @include('modals.deleteModal', ['routeName' => 'delete-topic', 'idx' => $value->topic_id])

                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
</div>

@stop
