
@if( Auth::user()->privilege >= 3 ) 
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Pop up modal CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Title Page-->
    @if( Auth::user()->privilege == 3 ) 
    <title>Vasha Shikkha->TEACHER</title>
    @else
    <title>Vasha Shikkha->ADMIN</title>
    @endif



    <!-- Fontfaces CSS-->
    <link href="{{asset("css/font-face.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/font-awesome-4.7/css/font-awesome.min.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/font-awesome-5/css/fontawesome-all.min.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/mdi-font/css/material-design-iconic-font.min.css")}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <!--Datatable CSS-->
    <link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

    <!-- Vendor CSS-->
    <link href="{{asset("vendor/animsition/animsition.min.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet"
        media="all">
    <link href="{{asset("vendor/wow/animate.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/css-hamburgers/hamburgers.min.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/slick/slick.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/select2/select2.min.css")}}" rel="stylesheet" media="all">
    <link href="{{asset("vendor/perfect-scrollbar/perfect-scrollbar.css")}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="/images/dimik.png" alt="CoolAdmin" height="75" width="75" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="index.html">Dashboard 1</a>
                                </li>
                                <li>
                                    <a href="index2.html">Dashboard 2</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
            <a href="{{ route('home-page') }}">
                    <img src="/images/dimik.png" alt="Cool Admin" height="85" width="85" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>DASHBOARD</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

                            @if( Auth::user()->privilege >= 10 )
                                <li>
                                    <a href="{{ route('show-all-dashboard-data') }}">ADMIN DASHBOARD</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-all-chart-data') }}">ADMIN CHARTS</a>
                                </li>
                            @endif
                                <li>
                                    <a href="#">TEACHER DASHBOARD</a>
                                </li>
                                <li>
                                    <a href="#">TEACHER CHART</a>
                                </li>
                                
                            </ul>
                        </li>

                        @if( Auth::user()->privilege >= 10 ) 
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>USERS</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ url('/user/1') }}">All Users</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-all-admins', ['id' => '1']) }}">Admins</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-all-teachers', ['id' => '1']) }}">Teachers</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-all-students', ['id' => '1']) }}">Students</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-new-teachers', ['id' => '1']) }}">Pending Requests</a>
                                </li>
                            </ul>
                        </li>
                        @endif


                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>SENTENCE</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route('show-all-sentence') }}">All Tables</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-bangla-sentence', ['id' => '1']) }}">Bangla Sentence</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-english-sentence', ['id' => '1']) }}">English Sentence</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-translations', ['id' => '1']) }}">English To Bangla Translation</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-fill-blank-sentence', ['id' => '1']) }}">Fill In The Gaps</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-fixed-jumble', ['id' => '1']) }}">Jumbled Sentence</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-sentence-matching', ['id' => '1']) }}">Sentence Matching</a>
                                </li>

                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>WORD</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route('show-word-all') }}">All Tables</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-bangla-words' , ['id' => '1']) }}">Bangla Words</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-english-words' , ['id' => '1']) }}">English Words</a>
                                </li>
                                <li>
                                    
                                    <a href="{{ route('fill-blank-word-ver' , ['id' => '1']) }}">Word Gaps</a>

                                </li>
                                <li>
                                    <a href="{{ route('show-dictionary-all' , ['id' => '1']) }}">Dictionary</a>
                                </li>

                                <li>
                                    <a href="{{ route('show-mcq-all' , ['id' => '1']) }}">M.C.Q</a>
                                </li>
                                <li>
                                   
                                    <a href="{{ route('show-vocabulary-all' , ['id' => '1']) }}">Vocabulary</a>

                                </li>

                                <li>
                                   
                                    <a href="{{ route('show-synonymAntonym-all' , ['id' => '1']) }}">Synonym Antonyms</a>

                                </li>
                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>SENTENCE QUIZ</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route('show-fill-blank-sentence-quiz', ['id' => '1']) }}">Fill In The Gaps</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-fixed-jumble-quiz', ['id' => '1']) }}">Jumbled Sentence</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-sentence-matching-quiz', ['id' => '1']) }}">Sentence Matching</a>
                                </li>

                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>WORD QUIZ</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                
                                <li> 
                                    <a href="{{ route('fill-blank-word-ver-quiz' , ['id' => '1']) }}">Word Gaps</a>
                                </li>
                                

                                <li>
                                    <a href="{{ route('show-mcq-all-quiz' , ['id' => '1']) }}">M.C.Q</a>
                                </li>
                                <li>
                                    <a href="{{ route('show-vocabulary-all-quiz' , ['id' => '1']) }}">Vocabulary</a>
                                </li>

                                <li> 
                                    <a href="{{ route('show-synonymAntonym-all-quiz' , ['id' => '1']) }}">Synonym Antonyms</a>
                                </li>
                            </ul>
                        </li>

                        

                        

                        <li class="has-sub">
                            <a class="js-arrow" href="{{ route('show-topic') }}">
                                <i class="fas fa-tachometer-alt"></i>ALL TOPICS</a>
                        </li>

                        @if( Auth::user()->privilege >= 10 )

                        <li class="has-sub">
                            <a class="js-arrow" href="{{ url('/log-viewer') }}">
                                <i class="fas fa-tachometer-alt"></i>SYSTEM LOG</a>
                        </li>

                        @endif

                        <li class="has-sub">
                            <a class="js-arrow" href="{{ route('project-developers') }}">
                                <i class="fas fa-tachometer-alt"></i>DEVELOPERS</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <input class="au-input au-input--xl" type="text" name="search"
                                    placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                        <div class="mess-dropdown js-dropdown">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Michelle Moreno" />
                                                </div>
                                                <div class="content">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Diane Myers" />
                                                </div>
                                                <div class="content">
                                                    <h6>Diane Myers</h6>
                                                    <p>You are now connected on message</p>
                                                    <span class="time">Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="mess__footer">
                                                <a href="#">View all messages</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have 3 New Emails</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-05.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">See all emails</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="https://ui-avatars.com/api/?name={{Auth::user()->name}}&background=992417&color=fff&rounded=true" alt="{{Auth::user()->name}}" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="https://ui-avatars.com/api/?name={{Auth::user()->name}}&background=992417&color=fff&rounded=true" alt="{{Auth::user()->name}}" />
                                                        
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{ Auth::user()->name }}</a>
                                                    </h5>
                                                    <span class="email">{{ Auth::user()->email }}</span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                <a href="{{ route('show-profile') }}">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <!-- <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a> -->
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <!-- <a href="#">
                                                    <i class="zmdi zmdi-power"></i>Logout</a> -->



                                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-power"></i>{{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                    style="display: none;">
                                                    @csrf
                                                </form>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <div class="main-content">

                <div class="section__content section__content--p30">

                    <div class="container-fluid">

                        @yield('tableContent')

                    </div>

                </div>

            </div>

        </div>
    </div>




    <!-- Jquery JS-->
    <script src="{{asset("vendor/jquery-3.2.1.min.js")}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset("vendor/bootstrap-4.1/popper.min.js")}}"></script>
    <script src="{{asset("vendor/bootstrap-4.1/bootstrap.min.js")}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset("vendor/slick/slick.min.js")}}">
    </script>
    <script src="{{asset("vendor/wow/wow.min.js")}}"></script>
    <script src="{{asset("vendor/animsition/animsition.min.js")}}"></script>
    <script src="{{asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js")}}">
    </script>
    <script src="{{asset("vendor/counter-up/jquery.waypoints.min.js")}}"></script>
    <script src="{{asset("vendor/counter-up/jquery.counterup.min.js")}}">
    </script>
    <script src="{{asset("vendor/circle-progress/circle-progress.min.js")}}"></script>
    <script src="{{asset("vendor/perfect-scrollbar/perfect-scrollbar.js")}}"></script>
    <script src="{{asset("vendor/chartjs/Chart.bundle.min.js")}}"></script>
    <script src="{{asset("vendor/select2/select2.min.js")}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset("js/main.js")}}"></script>


    <!-- pop up modal-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!--datatable-->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>



</body>

</html>
<!-- end document-->
@endif