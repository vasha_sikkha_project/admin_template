@extends('layouts.navbarLayout')


@section('tableContent')


<div class="row">
    <div class="col-md-6 offset-md-3 mr-auto ml-auto">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">Admin Profile</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    @if(Auth::user()->latest_image == null)
                    <img class="rounded-circle mx-auto d-block" src="https://ui-avatars.com/api/?name={{Auth::user()->name}}&background=992417&color=fff&rounded=true" alt="Card image cap">
                    @else
                    <img class="rounded-circle mx-auto d-block" src="{{URL::asset('/uploads/profilePictures/'.Auth::user()->latest_image)}}" alt="Card image cap">
                    @endif

                    <h5 class="text-sm-center mt-2 mb-1">{{ Auth::user()->name }}</h5>
                    <div class="location text-sm-center">
                    @if(Auth::user()->present_address != null)
                        <i class="fa fa-map-marker"></i> {{Auth::user()->present_address}}</div>
                    @else
                    <i class="fa fa-map-marker"></i> Bangladesh</div>
                    @endif
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <table class="table table-data">

                        <tr class="tr-shadow">
                            <td>Full name</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->name }}</span>
                            </td>
                        </tr>
                        <tr class="tr-shadow">
                            <td>Email</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->email }}</span>
                            </td>
                        </tr>
                        <tr class="tr-shadow">
                            <td>Mobile Number</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->mobile_number }}</span>
                            </td>
                        </tr>
                        <tr class="tr-shadow">
                            <td>Institution</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->institution }}</span>
                            </td>
                        </tr>
                        <tr class="tr-shadow">
                            <td>Age</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->age }}</span>
                            </td>
                        </tr>
                        <tr class="tr-shadow">
                            <td>Present Address</td>
                            <td>
                                <span class="block-email">{{ Auth::user()->present_address }}</span>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div class="mx-auto d-block">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-placement="top"
                    data-target="#editModalProfile-{{ Auth::user()->user_id }}">Update Profile</button>

                    </div>
                    <br><br>

                    <!-- #################### -->

                    <!-- Dictionary Edit Pop-up Modal Starts Here. DON'T TOUCH IT. -->


                    <div class="modal fade" id="editModalProfile-{{ Auth::user()->user_id }}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalScrollableTitle" aria-hidden="true" data-backdrop="false">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Update
                                        Your Profile
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>



                                <!--edit form-->

                                <form action="{{ route('update-your-profile',['dictionaryid' =>  Auth::user()->user_id ]) }}"
                                    method="POST" id="editForm">

                                    {{ csrf_field() }}
                                    {{-- {{ method_field('PATCH') }} --}}

                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label>Enter Updated Full Name</label>
                                            <input type="text" name="updatedFullName" id="updatedname"
                                                value="<?php echo Auth::user()->name; ?>" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="text"
                                                placeholder="<?php echo Auth::user()->email; ?>" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Enter Updated Mobile Number</label>
                                            <input type="text" name="updatedMobileNo" id="updatedmobile"
                                                value="<?php echo Auth::user()->mobile_number ; ?>" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Enter Institution</label>
                                            <input type="text" name="updatedInstitution" id="updatedInstitution"
                                                value="<?php echo Auth::user()->institution ; ?>" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Enter Updated Age</label>
                                            <input type="text" name="updatedAge" id="updatedage"
                                                value="<?php echo Auth::user()->age; ?>" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Enter Updated Address</label>
                                            <input type="text" name="updatedAddress" id="updatedAddress"
                                                value="<?php echo Auth::user()->present_address; ?>" class="form-control">
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>

                                        <button type="submit" class="btn btn-primary">Update
                                            Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>





                </div>
            </div>
        </div>
    </div>

</div>

@stop
