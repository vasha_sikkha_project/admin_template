
--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`experience_id`, `experience_type`) VALUES
(1, 'JYW7duSff2');

-- --------------------------------------------------------



--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `age`, `password`, `privilege`, `mobile_number`, `institution`, `present_address`, `latest_image`, `experience_id`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'abc', 'abc@gamil.com', NULL, '12355678', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'aehkbd', 'asdjb@hdkc', 21, 'sfhdbjsn', 1, NULL, NULL, NULL, 'sfhbd', NULL, NULL, NULL, NULL, NULL),
(3, 'messi', 'bca32@gmil.com', NULL, '$2y$10$FLtqhOf..2WoD0Tw4.qW3eGEaJ4LaMT42Ja6ZK46d1Av3A.vhdnsG', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-25 00:55:03', '2019-06-25 00:55:03'),
(4, 'Navid', 'glitchbox29@gmail.com', NULL, '$2y$10$javjTCF4Lm8I/9WyAyi8cuFK8hiKggc0YAhtw8Q7muU3sLWDrTg0K', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-25 01:11:35', '2019-06-25 01:11:35'),
(5, 'Rumman', 'rumman@gmail.com', NULL, '$2y$10$Gv5P8EJl5WO8ADnLsK10Quyloma30/wz.M5HSgfyg3P49Rt.CMxie', 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-26 02:40:28', '2019-06-26 02:40:28'),
(6, 'rumman', 'mann@gmail.com', NULL, '$2y$10$Pf1X8J.WzzVsc4S5fA69Mu.b/mdx6rsdN0T.AnUPZcnNxXZVa8IDW', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-29 09:15:28', '2019-06-29 09:15:28'),
(7, 'montu', 'kokiko@gmail.com', NULL, '$2y$10$noIsCQ99P/e52BEIHdBoX.CcuT0uRwvYlYPH/p2x9FmfSCd8OLfQG', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-06 01:39:03', '2019-07-06 01:39:03');

-- --------------------------------------------------------




--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('19b080f3b1cfbf42866dc300e38bc64f64d93e692b2ba5d2894a8ba6d0e570af54c7a0464b3bf9f1', 3, 1, 'Personal Access Token', '[]', 0, '2019-06-25 00:55:12', '2019-06-25 00:55:12', '2020-06-25 06:55:12'),
('4ef97c5c55cc83abc04b1b9c3cc6386577c7d5e64d5fa98314743eb233be2ca202830f486568460e', 6, 1, 'Personal Access Token', '[]', 0, '2019-06-30 09:10:37', '2019-06-30 09:10:37', '2020-06-30 15:10:37'),
('63c8842176d0841f81fdcafa753189de17a2ef5ccda570ec2fcf0aa16ceaa886e59b8eea8ff29706', 4, 1, 'Personal Access Token', '[]', 0, '2019-06-25 01:11:50', '2019-06-25 01:11:50', '2020-06-25 07:11:50'),
('aaac351724f5782f6bb8f0ac4080aba10e007ab7083cc98b123a80a063c0e47a00a2a315ac9a96aa', 6, 1, 'Personal Access Token', '[]', 0, '2019-07-01 22:04:05', '2019-07-01 22:04:05', '2020-07-02 04:04:05'),
('c60038a64f954ff8ae0eb9ef694c2cc61a6f2d50c3dc941c64cd9c5c6a1ecda459ecb569bf4f9c19', 6, 1, 'Personal Access Token', '[]', 0, '2019-06-29 09:15:34', '2019-06-29 09:15:34', '2020-06-29 15:15:34'),
('cfa67f1b079f9b2fff0eac3b70421882ee0b255880888bf8d034b3ab5b09cf302936c9703f0d3a11', 4, 1, 'Personal Access Token', '[]', 0, '2019-06-25 01:12:27', '2019-06-25 01:12:27', '2020-06-25 07:12:27'),
('e2aa788f5e2cb1deb0fe01660a19221fedf063461510bd84a9a9ca614f91d7791f65b0acdfcfbd6d', 6, 1, 'Personal Access Token', '[]', 0, '2019-07-01 22:03:41', '2019-07-01 22:03:41', '2020-07-02 04:03:41'),
('eadf276ad2c89928bb9a8bf52e2c939c3d851c4fa1179543f1dddb362a65de41318d95d08c949c1e', 4, 1, 'Personal Access Token', '[]', 0, '2019-07-07 07:41:33', '2019-07-07 07:41:33', '2020-07-07 13:41:33');

-- --------------------------------------------------------


--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'TT6zbIzblZGB12paaKOxwEoqNeW5sREoNDMG4AYK', 'http://localhost', 1, 0, 0, '2019-06-25 00:51:49', '2019-06-25 00:51:49'),
(2, NULL, 'Laravel Password Grant Client', 'sZ2LTEaUi7vDtrSgUtCjwmaFr425ErdymPIqp6pv', 'http://localhost', 0, 1, 0, '2019-06-25 00:51:49', '2019-06-25 00:51:49');

-- --------------------------------------------------------


--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-06-25 00:51:49', '2019-06-25 00:51:49');

-- --------------------------------------------------------




--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_name`, `wordOrsentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sentenceMatching', 0, NULL, NULL, NULL),
(2, 'fillintheblanks', 1, NULL, NULL, NULL),
(3, 'truefalse', 1, NULL, NULL, NULL),
(4, 'sentenceMatchingEngToEng', 1, NULL, NULL, NULL),
(5, 'mcq', 1, NULL, NULL, NULL),
(6, 'memorygame', 2, NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topic_id`, `topic_name`, `topic_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'people', 'localhost.com\\oKwSOJFGaj', NULL, NULL, NULL),
(2, 'BdIhosYevM', 'localhost.com\\hhwlTGbRmw', NULL, NULL, NULL),
(3, 'j0G1OkLfiM', 'localhost.com\\JOIki1r22G', NULL, NULL, NULL),
(4, '9cVpRv2Z8v', 'localhost.com\\rp3ESehghi', NULL, NULL, NULL),
(5, 'fuVtxMDY9j', 'localhost.com\\8GiR6DKg0K', NULL, NULL, NULL),
(6, 'seHznonRT5', 'localhost.com\\IEAPjWYhv2', NULL, NULL, NULL),
(7, 'uoV0jHwx26', 'localhost.com\\1Q8ywOVfQL', NULL, NULL, NULL),
(8, 'JLy6uKzdEG', 'localhost.com\\2ExAd4Czuu', NULL, NULL, NULL),
(9, 'qSLUlxdK1S', 'localhost.com\\5K8r7PBNtR', NULL, NULL, NULL),
(10, 'kJPBrwqBl9', 'localhost.com\\1u8skil69F', NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'C6BqHa4JY8', NULL, NULL, NULL),
(2, 'eWPBR7HKtd', NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `level`
--

INSERT INTO `level` (`level_id`, `difficulty`, `score_required`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 20, NULL, NULL, NULL),
(2, 2, 20, NULL, NULL, NULL),
(3, 2, 20, NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `loved_topic`
--

INSERT INTO `loved_topic` (`favourite_topic_id`, `topic_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, NULL, NULL, NULL),
(2, 3, 1, NULL, NULL, NULL),
(3, 2, 1, NULL, NULL, NULL),
(4, 2, 1, NULL, NULL, NULL),
(5, 3, 1, NULL, NULL, NULL),
(6, 1, 2, NULL, NULL, NULL),
(7, 1, 2, NULL, NULL, NULL),
(8, 3, 2, NULL, NULL, NULL),
(9, 1, 2, NULL, NULL, NULL),
(10, 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------




--
-- Dumping data for table `word_task`
--

INSERT INTO `word_task` (`word_task_id`, `task_id`, `topic_id`, `level_id`, `experience_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 2, 2, 1, NULL, NULL, NULL),
(2, 3, 1, 2, 1, NULL, NULL, NULL),
(3, 3, 2, 2, 1, NULL, NULL, NULL),
(4, 3, 1, 1, 1, NULL, NULL, NULL),
(5, 2, 2, 1, 1, NULL, NULL, NULL),
(6, 3, 1, 1, 1, NULL, NULL, NULL),
(7, 3, 2, 1, 1, NULL, NULL, NULL),
(8, 1, 1, 1, 1, NULL, NULL, NULL),
(9, 5, 1, 1, 1, NULL, NULL, NULL),
(10, 1, 2, 2, 1, NULL, NULL, NULL),
(11, 6, 1, 1, 1, NULL, NULL, NULL);

-- ---------------------------------------------------------



--
-- Dumping data for table `sentence_task`
--

INSERT INTO `sentence_task` (`sentence_task_id`, `task_id`, `topic_id`, `level_id`, `experience_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, 1, 1, NULL, NULL, NULL),
(2, 2, 1, 1, 1, NULL, NULL, NULL),
(3, 1, 2, 1, 1, NULL, NULL, NULL),
(4, 2, 2, 2, 1, NULL, NULL, NULL),
(5, 3, 1, 1, 1, NULL, NULL, NULL),
(6, 1, 1, 1, 1, NULL, NULL, NULL),
(7, 1, 1, 2, 1, NULL, NULL, NULL),
(8, 3, 1, 1, 1, NULL, NULL, NULL),
(9, 1, 2, 1, 1, NULL, NULL, NULL),
(10, 3, 1, 1, 1, NULL, NULL, NULL),
(11, 4, 1, 1, 1, NULL, NULL, NULL),
(15, 5, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `user_history`
--

INSERT INTO `user_history` (`userhistory_id`, `user_id`, `topic_id`, `task_id`, `level_id`, `progress`, `upload_time`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 3, 2, 70, NULL, NULL, NULL, NULL, NULL),
(2, 1, 3, 1, 3, 23, NULL, NULL, NULL, NULL, NULL),
(3, 2, 3, 2, 3, 53, NULL, NULL, NULL, NULL, NULL),
(4, 2, 3, 1, 2, 15, NULL, NULL, NULL, NULL, NULL),
(5, 3, 3, 1, 1, 40, NULL, NULL, NULL, NULL, NULL),
(6, 1, 3, 2, 1, 73, NULL, NULL, NULL, NULL, NULL),
(7, 4, 1, 4, 1, 15, NULL, NULL, NULL, NULL, NULL),
(8, 2, 3, 2, 1, 31, NULL, NULL, NULL, NULL, NULL),
(9, 1, 2, 5, 3, 10, NULL, NULL, NULL, NULL, NULL),
(10, 2, 2, 2, 3, 72, NULL, NULL, NULL, NULL, NULL),
(11, 1, 1, 5, 1, 98, NULL, NULL, NULL, NULL, NULL),
(12, 1, 2, 3, 2, 30, NULL, NULL, NULL, NULL, NULL),
(13, 1, 2, 3, 1, 18, NULL, NULL, NULL, NULL, NULL),
(14, 1, 3, 2, 1, 99, NULL, NULL, NULL, NULL, NULL),
(15, 1, 1, 3, 1, 28, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------




--
-- Dumping data for table `bangla_resource`
--

INSERT INTO `bangla_resource` (`bangla_word_id`, `word`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'yQpa8sIGg1', NULL, NULL, NULL),
(2, '7rTdOMFmfl', NULL, NULL, NULL),
(3, 'JdFe5C97Mz', NULL, NULL, NULL),
(4, 'QhxfcxMX88', NULL, NULL, NULL),
(5, 'K18hvHr71c', NULL, NULL, NULL),
(6, 'W114NSswRV', NULL, NULL, NULL),
(7, 'E7bggvyCn1', NULL, NULL, NULL),
(8, 'DXT0aJnezR', NULL, NULL, NULL),
(9, '43M7gIgmxA', NULL, NULL, NULL),
(10, 'r4k5UDSJHT', NULL, NULL, NULL),
(11, '9zdsF6IPR6', NULL, NULL, NULL),
(12, 'WpJrVWuR5h', NULL, NULL, NULL),
(13, 'ULJPSg3maQ', NULL, NULL, NULL),
(14, 'pXsg6HSrNp', NULL, NULL, NULL),
(15, 'XMxwUGMSjq', NULL, NULL, NULL),
(16, 'PF76neajIc', NULL, NULL, NULL),
(17, 'ANRYpRucMC', NULL, NULL, NULL),
(18, 'hysVcextd3', NULL, NULL, NULL),
(19, 'বালক', NULL, NULL, NULL),
(20, 'dJpXgpmLik', NULL, NULL, NULL),
(21, 'পুরুষ', NULL, NULL, NULL),
(22, 'oc72PwwJRv', NULL, NULL, NULL),
(23, 'Sjpy4XSSKa', NULL, NULL, NULL),
(24, 'y4JGdq63sE', NULL, NULL, NULL),
(25, '2lCtVlShdh', NULL, NULL, NULL),
(26, 'lioaUZdGZi', NULL, NULL, NULL),
(27, 'Egmf2D8o2T', NULL, NULL, NULL),
(28, 'JUWeK1jOV5', NULL, NULL, NULL),
(29, 'ol9UCMZIOv', NULL, NULL, NULL),
(30, 'w3Xx5wSfZG', NULL, NULL, NULL),
(31, 'GbFVsCo6n3', NULL, NULL, NULL),
(32, 'পৃথিবী', NULL, NULL, NULL),
(33, 'SckdEWGdbK', NULL, NULL, NULL),
(34, 't1OWuFLm4m', NULL, NULL, NULL),
(35, 'jcJiaVccKf', NULL, NULL, NULL),
(36, 'QPmWOJELhm', NULL, NULL, NULL),
(37, 'RkRtOQllZH', NULL, NULL, NULL),
(38, 'OnMRQZNQY9', NULL, NULL, NULL),
(39, 'HJl1Tv9dR5', NULL, NULL, NULL),
(40, 'tdcT7czreJ', NULL, NULL, NULL),
(41, 'nyLCxNpJbC', NULL, NULL, NULL),
(42, 'cz0OpkUg1T', NULL, NULL, NULL),
(43, 'ZErZ7YZGxN', NULL, NULL, NULL),
(44, 'Fjl5evn17S', NULL, NULL, NULL),
(45, 'PQFOIRU8lX', NULL, NULL, NULL),
(46, 'JXKBGlOG2X', NULL, NULL, NULL),
(47, '0F1ikZ8N1k', NULL, NULL, NULL),
(48, 'MMFzHWLyeE', NULL, NULL, NULL),
(49, 'XX4Mbud2fT', NULL, NULL, NULL),
(50, 'VBiCjyHgLx', NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `bangla_sentence`
--

INSERT INTO `bangla_sentence` (`bangla_sentence_id`, `bangla_sentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dDrEnFlvi5', NULL, NULL, NULL),
(2, 'আমি যুদ্ধ করেছি, হেরে গিয়েছি। এখন আমি বিদায় নিচ্ছি।', NULL, NULL, NULL),
(3, 'কক্স\'স বাজার বিশ্বের বৃহত্তম সমুদ্র সৈকত', NULL, NULL, NULL),
(4, '8z4OJV46pC', NULL, NULL, NULL),
(5, 'মিথ্যা বলার চেয়ে মৃত্যু আমার কাছে শ্রেয়', NULL, NULL, NULL),
(6, 'UxxOrJXopT', NULL, NULL, NULL),
(7, '4d8vzx0fHj', NULL, NULL, NULL),
(8, '47KFKi5FDD', NULL, NULL, NULL),
(9, 'রাত অন্ধকার এবং ভয়ে পরিপূর্ণ।', NULL, NULL, NULL),
(10, '\'সঠিক সময়\' এর অপেক্ষায় কাজ ফেলে রেখো না।', NULL, NULL, NULL),
(11, 'QW0ja05avu', NULL, NULL, NULL),
(12, 'fo9neZ3mcd', NULL, NULL, NULL),
(13, 'সময় সবচেয়ে বড় নিরামক', NULL, NULL, NULL),
(14, 'নিজের ভেতরের বালককে বধ করো, পুরুষের জন্ম হতে দাও।', NULL, NULL, NULL),
(15, 'ফলাফল অর্জনের চেয়ে অর্জিত অভিজ্ঞতা বেশি গুরুত্বপূর্ণ। ', NULL, NULL, NULL),
(16, 'IFfFISeheS', NULL, NULL, NULL),
(17, 'ksjlklQuSO', NULL, NULL, NULL),
(18, 'ZFUG2I2cgc', NULL, NULL, NULL),
(19, 'ZVmOdBcv9x', NULL, NULL, NULL),
(20, 'SBwo8hGB34', NULL, NULL, NULL),
(21, 'uWRmNOXfge', NULL, NULL, NULL),
(22, '3KOI37n10N', NULL, NULL, NULL),
(23, 'zKZDo3NGbi', NULL, NULL, NULL),
(24, 'mv3jDDB4nq', NULL, NULL, NULL),
(25, 'zT0XN1YXqc', NULL, NULL, NULL),
(26, 'PXR6QdS9IP', NULL, NULL, NULL),
(27, 'HSYNTutTVJ', NULL, NULL, NULL),
(28, 'd14Gxdx3mM', NULL, NULL, NULL),
(29, 'gxGSxK6X3C', NULL, NULL, NULL),
(30, 'সুন্দরবন সর্ববৃহৎ ম্যানগ্রোভ বন', NULL, NULL, NULL),
(31, 'd2cI8MjP1T', NULL, NULL, NULL),
(32, 'oKYWY6nE4g', NULL, NULL, NULL),
(33, 'LHARuFDHZD', NULL, NULL, NULL),
(34, '1U9sadLH69', NULL, NULL, NULL),
(35, 'yAgRLFTkjC', NULL, NULL, NULL),
(36, '7bg13j81bF', NULL, NULL, NULL),
(37, 'caU38typwq', NULL, NULL, NULL),
(38, 'ch9NWZZsFs', NULL, NULL, NULL),
(39, 'vESROUNoIV', NULL, NULL, NULL),
(40, 'pNlDAw7A77', NULL, NULL, NULL),
(41, 'সূর্য পূর্ব দিকে ওঠে', NULL, NULL, NULL),
(42, 'হৃদয়ে ব্যথা হলেও হাসো, হৃদয় ভেঙ্গে গেলেও হাসো।', NULL, NULL, NULL),
(43, 'মা সবসময় বলতো জীবন একটা চকলেটের বক্সের মতো। সামনে কী অপেক্ষা করছে কেউ জানে না।', NULL, NULL, NULL),
(44, 'umZx9Lwmmt', NULL, NULL, NULL),
(45, 'hpGwYdE0E0', NULL, NULL, NULL),
(46, 'bYKEoBaEdS', NULL, NULL, NULL),
(47, 'কখনো নিজের পরিচয় ভুলো না। কারণ পৃথিবী ভুলবে না। বর্মের মতো এটা পরিধান করো যাতে কেউ এটা দিয়ে আঘাত না করতে পারে।', NULL, NULL, NULL),
(48, 'এটা আমি আমার জন্য করেছিলাম। আমি এ কাজ ভালবাসতাম। আমি এটায় সেরা ছিলাম। আমি জীবন ফিরে পেয়েছিলাম।', NULL, NULL, NULL),
(49, 'oHD91kHI80', NULL, NULL, NULL),
(50, 'এমন জীবন গঠন করো, যাতে মৃত্যুশয্যায় জীবনের দিকে তাকিয়ে স্মিত হতে পারো। ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Dumping data for table `english_resource`
--

INSERT INTO `english_resource` (`english_word_id`, `word`, `image_link`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'match', 'XPY0i7nABA', NULL, NULL, NULL),
(2, 'XHmWDHQsNl', 'hPULtcwF3i', NULL, NULL, NULL),
(3, 'jwaNiSEYqi', 'YlkPvYlhfF', NULL, NULL, NULL),
(4, 'f3turZE2jc', '9h7ks0Vrqo', NULL, NULL, NULL),
(5, 'sun', 'YunIEWYAy4', NULL, NULL, NULL),
(6, 'capable', 'v1jo0ne4ar', NULL, NULL, NULL),
(7, 'AekjsDWHas', 'zGUei7RBXB', NULL, NULL, NULL),
(8, 'traveller', '0khlJKQb64', NULL, NULL, NULL),
(9, 'hazes', 'JDUoLEtE4n', NULL, NULL, NULL),
(10, 'wjc7vHOkGs', 'cEkAvN4erp', NULL, NULL, NULL),
(11, 'H8wYh5XdBS', 'ZONEQlTxaU', NULL, NULL, NULL),
(12, 'boy', 'G06DCXS2Ko', NULL, NULL, NULL),
(13, 'man', 'ZjsJJbzdMk', NULL, NULL, NULL),
(14, 'N4T3XAwZ2w', 'LkubhYzxa0', NULL, NULL, NULL),
(15, 'criticize', '8FfaoAv9gC', NULL, NULL, NULL),
(16, 'hFV8LeRby8', 'Pt3I5M6KZN', NULL, NULL, NULL),
(17, 'zigXi6IEQD', 'Vh9oEQep7o', NULL, NULL, NULL),
(18, 'B35jdPuRSk', 'iMZOwqIkDr', NULL, NULL, NULL),
(19, '5oCbFxMvRy', 'eUwimqYTEq', NULL, NULL, NULL),
(20, 'Uz3Ahx996J', 'yRPLcQWhvc', NULL, NULL, NULL),
(21, 'born', '44gK0GJG4K', NULL, NULL, NULL),
(22, 'world', 'OtVwjAmq2B', NULL, NULL, NULL),
(23, 'make', 'aLmq4dpXwq', NULL, NULL, NULL),
(24, 'usually', 'GpXUe4cCMh', NULL, NULL, NULL),
(25, 'cover', 'toQLOWnPMo', NULL, NULL, NULL),
(26, 'understand', 'UAxNi4IXtX', NULL, NULL, NULL),
(27, 'dream', '9i7ItWrLQC', NULL, NULL, NULL),
(28, 'BGyAIzD0T5', 'WXxW5oFD9B', NULL, NULL, NULL),
(29, 'healer', '5pUNbqvw0W', NULL, NULL, NULL),
(30, 'lose', 'e46Kc44CTy', NULL, NULL, NULL),
(31, 'make', 'Zg3gbbrF5M', NULL, NULL, NULL),
(32, 'incapable', 'SehGdmh8cC', NULL, NULL, NULL),
(33, 'forgiveness', 'T5HwqF64i5', NULL, NULL, NULL),
(34, 'prefer', 'j4s8WBSIwL', NULL, NULL, NULL),
(35, 'usually', 'mUajma3ghW', NULL, NULL, NULL),
(36, 'ENntqiCszl', 'VRFEHn4q43', NULL, NULL, NULL),
(37, 'short', 'TlqT7sqxIT', NULL, NULL, NULL),
(38, 'under', 'pPA4Xrryaj', NULL, NULL, NULL),
(39, 'U84eyz95Sc', 'siqEXBW7D6', NULL, NULL, NULL),
(40, 'boy', 'cxRdervH7Y', NULL, NULL, NULL),
(41, 'xx3EWo38V7', 'zC8GwPODqE', NULL, NULL, NULL),
(42, 'qqdORFkWIA', 'X1pHp3ZCy4', NULL, NULL, NULL),
(43, 'kill', 'csuYjXLywd', NULL, NULL, NULL),
(44, 'born', 'xS4ULJDSkK', NULL, NULL, NULL),
(45, 'man', '11YtOyBP4T', NULL, NULL, NULL),
(46, 'N8hbPWsomY', 'hxqwak9qVx', NULL, NULL, NULL),
(47, 'created', 'v8Ux59mWzH', NULL, NULL, NULL),
(48, 'rush', 'qgSpuILk51', NULL, NULL, NULL),
(49, 'learn', 'uCq1MNR2oa', NULL, NULL, NULL),
(50, 'sun', 'kdYr0MNxnW', NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `english_sentence`
--

INSERT INTO `english_sentence` (`english_sentence_id`, `english_sentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'I did it for me. I liked it. I was good at it. And, I was alive.', NULL, NULL, NULL),
(2, 'Smile though your heart is aching. Smile even though it\'s breaking', NULL, NULL, NULL),
(3, 'Np12myuK60', NULL, NULL, NULL),
(4, 'A93UXXnFZj', NULL, NULL, NULL),
(5, 'Gs8Mvga3jB', NULL, NULL, NULL),
(6, 'kY8WuyZeTL', NULL, NULL, NULL),
(7, 'Cox\'s Bazar is the largest sea beach in the world', NULL, NULL, NULL),
(8, 'lVM7lNXucU', NULL, NULL, NULL),
(9, 'I would rather die than telling lie', NULL, NULL, NULL),
(10, 'PdJA02XJ7w', NULL, NULL, NULL),
(11, 'Time is a great healer', NULL, NULL, NULL),
(12, 'lA68zXWd68', NULL, NULL, NULL),
(13, '1Fdfod9CV9', NULL, NULL, NULL),
(14, 'QBs69YMJTb', NULL, NULL, NULL),
(15, 'Do not wait, the time will never be \'just right\'.', NULL, NULL, NULL),
(16, 'Th', NULL, NULL, NULL),
(17, 'The Sun rises in the East', NULL, NULL, NULL),
(18, 'QMa0U7DIsR', NULL, NULL, NULL),
(19, 'g0Ia6xWbx8', NULL, NULL, NULL),
(20, 'Rph5UacK5n', NULL, NULL, NULL),
(21, 'dUthcb7ldI', NULL, NULL, NULL),
(22, 'oxSOQcgyhy', NULL, NULL, NULL),
(23, 'CtboJcBG3s', NULL, NULL, NULL),
(24, 'tcBcuqDASl', NULL, NULL, NULL),
(25, 'SI7y6UwNW3', NULL, NULL, NULL),
(26, 'wjnM7IuJLF', NULL, NULL, NULL),
(27, 'sA7ZIvsFOp', NULL, NULL, NULL),
(28, 'GnUDywJevs', NULL, NULL, NULL),
(29, 'WE7ylv2Wrv', NULL, NULL, NULL),
(30, 'jadnt1QcaU', NULL, NULL, NULL),
(31, 'Sundarbans is the largest mangrove forest', NULL, NULL, NULL),
(32, 'It\'s not about the result it\'s all about the journey.', NULL, NULL, NULL),
(33, 'I fought, I lost. Now I rest.', NULL, NULL, NULL),
(34, 'Kill the boy let the man be born.', NULL, NULL, NULL),
(35, 'VDq1Ee2zl2', NULL, NULL, NULL),
(36, 'B0ANK7tk30', NULL, NULL, NULL),
(37, 'np3RYkwHrx', NULL, NULL, NULL),
(38, 'Never forget what you are. The rest of the world will not. Wear it like armor, and it can never be used to hurt you.', NULL, NULL, NULL),
(39, 'oS4VKqNIiR', NULL, NULL, NULL),
(40, 'grKoLaZBpm', NULL, NULL, NULL),
(41, 'j6HSKn3NCR', NULL, NULL, NULL),
(42, 'dzLa3AdVva', NULL, NULL, NULL),
(43, 'fRUOlmDETg', NULL, NULL, NULL),
(44, 'K0PXOD0z9p', NULL, NULL, NULL),
(45, 'FgqAWU7k3U', NULL, NULL, NULL),
(46, 'Mama always said life was like a box of chocolates: You never know what you’re gonna get.', NULL, NULL, NULL),
(47, 'Lead a life you can look back at and smile at your death bed', NULL, NULL, NULL),
(48, 'E0roXodxWR', NULL, NULL, NULL),
(49, 'cVL9SGAXFE', NULL, NULL, NULL),
(50, 'The night is dark and full of terror', NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `dictionary`
--

INSERT INTO `dictionary` (`dictionary_id`, `another_word_id`, `bangla_word_id`, `language_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 28, 35, 2, NULL, NULL, NULL),
(2, 48, 31, 1, NULL, NULL, NULL),
(3, 1, 17, 2, NULL, NULL, NULL),
(4, 48, 23, 2, NULL, NULL, NULL),
(5, 19, 26, 1, NULL, NULL, NULL),
(6, 16, 48, 2, NULL, NULL, NULL),
(7, 7, 10, 1, NULL, NULL, NULL),
(8, 14, 50, 1, NULL, NULL, NULL),
(9, 10, 45, 2, NULL, NULL, NULL),
(10, 40, 27, 2, NULL, NULL, NULL),
(11, 43, 47, 1, NULL, NULL, NULL),
(12, 34, 26, 2, NULL, NULL, NULL),
(13, 24, 13, 1, NULL, NULL, NULL),
(14, 41, 21, 1, NULL, NULL, NULL),
(15, 21, 9, 1, NULL, NULL, NULL),
(16, 28, 18, 1, NULL, NULL, NULL),
(17, 13, 21, 2, NULL, NULL, NULL),
(18, 12, 28, 2, NULL, NULL, NULL),
(19, 38, 40, 2, NULL, NULL, NULL),
(20, 38, 24, 2, NULL, NULL, NULL),
(21, 45, 10, 2, NULL, NULL, NULL),
(22, 12, 19, 2, NULL, NULL, NULL),
(23, 30, 16, 2, NULL, NULL, NULL),
(24, 32, 28, 2, NULL, NULL, NULL),
(25, 40, 45, 1, NULL, NULL, NULL),
(26, 40, 27, 2, NULL, NULL, NULL),
(27, 49, 26, 2, NULL, NULL, NULL),
(28, 28, 14, 2, NULL, NULL, NULL),
(29, 21, 48, 2, NULL, NULL, NULL),
(30, 22, 32, 2, NULL, NULL, NULL),
(31, 7, 50, 2, NULL, NULL, NULL),
(32, 10, 4, 2, NULL, NULL, NULL),
(33, 27, 18, 2, NULL, NULL, NULL),
(34, 6, 44, 1, NULL, NULL, NULL),
(35, 15, 17, 1, NULL, NULL, NULL),
(36, 47, 50, 1, NULL, NULL, NULL),
(37, 28, 36, 1, NULL, NULL, NULL),
(38, 31, 18, 2, NULL, NULL, NULL),
(39, 45, 26, 1, NULL, NULL, NULL),
(40, 36, 10, 2, NULL, NULL, NULL),
(41, 25, 11, 1, NULL, NULL, NULL),
(42, 46, 13, 1, NULL, NULL, NULL),
(43, 34, 42, 2, NULL, NULL, NULL),
(44, 40, 36, 1, NULL, NULL, NULL),
(45, 24, 23, 2, NULL, NULL, NULL),
(46, 48, 10, 2, NULL, NULL, NULL),
(47, 49, 45, 2, NULL, NULL, NULL),
(48, 37, 47, 1, NULL, NULL, NULL),
(49, 29, 43, 2, NULL, NULL, NULL),
(50, 50, 25, 2, NULL, NULL, NULL),
(51, 47, 26, 2, NULL, NULL, NULL),
(52, 23, 46, 2, NULL, NULL, NULL),
(53, 50, 47, 1, NULL, NULL, NULL),
(54, 40, 27, 1, NULL, NULL, NULL),
(55, 47, 40, 1, NULL, NULL, NULL),
(56, 21, 20, 2, NULL, NULL, NULL),
(57, 15, 32, 2, NULL, NULL, NULL),
(58, 12, 47, 2, NULL, NULL, NULL),
(59, 30, 50, 1, NULL, NULL, NULL),
(60, 8, 15, 1, NULL, NULL, NULL),
(61, 6, 27, 2, NULL, NULL, NULL),
(62, 1, 26, 1, NULL, NULL, NULL),
(63, 31, 45, 1, NULL, NULL, NULL),
(64, 4, 16, 1, NULL, NULL, NULL),
(65, 50, 13, 2, NULL, NULL, NULL),
(66, 27, 20, 2, NULL, NULL, NULL),
(67, 38, 46, 1, NULL, NULL, NULL),
(68, 29, 6, 1, NULL, NULL, NULL),
(69, 12, 1, 1, NULL, NULL, NULL),
(70, 16, 48, 1, NULL, NULL, NULL),
(71, 3, 8, 1, NULL, NULL, NULL),
(72, 26, 28, 1, NULL, NULL, NULL),
(73, 50, 6, 2, NULL, NULL, NULL),
(74, 11, 8, 1, NULL, NULL, NULL),
(75, 24, 42, 1, NULL, NULL, NULL),
(76, 26, 30, 2, NULL, NULL, NULL),
(77, 33, 16, 2, NULL, NULL, NULL),
(78, 11, 1, 2, NULL, NULL, NULL),
(79, 22, 31, 1, NULL, NULL, NULL),
(80, 37, 34, 2, NULL, NULL, NULL),
(81, 7, 7, 2, NULL, NULL, NULL),
(82, 8, 21, 2, NULL, NULL, NULL),
(83, 38, 36, 1, NULL, NULL, NULL),
(84, 5, 15, 1, NULL, NULL, NULL),
(85, 10, 9, 1, NULL, NULL, NULL),
(86, 29, 18, 1, NULL, NULL, NULL),
(87, 10, 19, 1, NULL, NULL, NULL),
(88, 32, 15, 2, NULL, NULL, NULL),
(89, 49, 46, 2, NULL, NULL, NULL),
(90, 9, 24, 2, NULL, NULL, NULL),
(91, 37, 16, 2, NULL, NULL, NULL),
(92, 31, 31, 2, NULL, NULL, NULL),
(93, 15, 42, 2, NULL, NULL, NULL),
(94, 44, 43, 1, NULL, NULL, NULL),
(95, 21, 2, 1, NULL, NULL, NULL),
(96, 28, 19, 1, NULL, NULL, NULL),
(97, 35, 4, 1, NULL, NULL, NULL),
(98, 7, 15, 1, NULL, NULL, NULL),
(99, 40, 48, 2, NULL, NULL, NULL),
(100, 46, 7, 1, NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`translation_id`, `another_sentence_id`, `bangla_sent_id`, `user_id`, `language_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, 50, 1, 1, NULL, NULL, NULL),
(2, 11, 13, 1, 2, NULL, NULL, NULL),
(3, 31, 30, 1, 1, NULL, NULL, NULL),
(4, 2, 42, 2, 1, NULL, NULL, NULL),
(5, 7, 3, 2, 2, NULL, NULL, NULL),
(6, 9, 5, 2, 1, NULL, NULL, NULL),
(7, 1, 48, 2, 1, NULL, NULL, NULL),
(8, 33, 2, 1, 2, NULL, NULL, NULL),
(9, 34, 14, 2, 2, NULL, NULL, NULL),
(10, 38, 47, 2, 1, NULL, NULL, NULL),
(11, 50, 12, 2, 2, NULL, NULL, NULL),
(12, 17, 41, 1, 1, NULL, NULL, NULL),
(13, 38, 46, 1, 2, NULL, NULL, NULL),
(14, 48, 49, 2, 1, NULL, NULL, NULL),
(15, 31, 16, 2, 1, NULL, NULL, NULL),
(16, 46, 16, 2, 1, NULL, NULL, NULL),
(17, 9, 34, 2, 1, NULL, NULL, NULL),
(18, 46, 30, 2, 2, NULL, NULL, NULL),
(19, 48, 8, 2, 2, NULL, NULL, NULL),
(20, 46, 43, 1, 1, NULL, NULL, NULL),
(21, 46, 42, 2, 1, NULL, NULL, NULL),
(22, 31, 1, 1, 1, NULL, NULL, NULL),
(23, 38, 5, 2, 2, NULL, NULL, NULL),
(24, 23, 31, 1, 2, NULL, NULL, NULL),
(25, 26, 18, 1, 2, NULL, NULL, NULL),
(26, 6, 35, 2, 1, NULL, NULL, NULL),
(27, 29, 49, 1, 1, NULL, NULL, NULL),
(28, 12, 39, 2, 2, NULL, NULL, NULL),
(29, 2, 3, 2, 2, NULL, NULL, NULL),
(30, 14, 22, 1, 2, NULL, NULL, NULL),
(31, 33, 29, 2, 1, NULL, NULL, NULL),
(32, 26, 34, 1, 2, NULL, NULL, NULL),
(33, 22, 37, 1, 1, NULL, NULL, NULL),
(34, 47, 24, 2, 2, NULL, NULL, NULL),
(35, 4, 8, 1, 1, NULL, NULL, NULL),
(36, 22, 17, 1, 2, NULL, NULL, NULL),
(37, 42, 26, 1, 1, NULL, NULL, NULL),
(38, 30, 27, 1, 1, NULL, NULL, NULL),
(39, 41, 45, 2, 1, NULL, NULL, NULL),
(40, 2, 42, 1, 2, NULL, NULL, NULL),
(41, 30, 17, 1, 2, NULL, NULL, NULL),
(42, 9, 32, 2, 2, NULL, NULL, NULL),
(43, 37, 38, 2, 1, NULL, NULL, NULL),
(44, 34, 14, 1, 1, NULL, NULL, NULL),
(45, 37, 36, 2, 2, NULL, NULL, NULL),
(46, 22, 9, 2, 1, NULL, NULL, NULL),
(47, 21, 27, 2, 1, NULL, NULL, NULL),
(48, 35, 45, 1, 2, NULL, NULL, NULL),
(49, 21, 30, 1, 1, NULL, NULL, NULL),
(50, 30, 15, 2, 2, NULL, NULL, NULL),
(51, 27, 2, 2, 2, NULL, NULL, NULL),
(52, 13, 16, 1, 1, NULL, NULL, NULL),
(53, 50, 9, 2, 1, NULL, NULL, NULL),
(54, 33, 26, 2, 2, NULL, NULL, NULL),
(55, 16, 13, 2, 2, NULL, NULL, NULL),
(56, 35, 43, 1, 2, NULL, NULL, NULL),
(57, 13, 49, 1, 1, NULL, NULL, NULL),
(58, 3, 32, 1, 2, NULL, NULL, NULL),
(59, 5, 3, 1, 2, NULL, NULL, NULL),
(60, 26, 19, 1, 1, NULL, NULL, NULL),
(61, 50, 23, 2, 1, NULL, NULL, NULL),
(62, 33, 39, 1, 1, NULL, NULL, NULL),
(63, 9, 2, 1, 1, NULL, NULL, NULL),
(64, 2, 33, 2, 2, NULL, NULL, NULL),
(65, 48, 23, 1, 2, NULL, NULL, NULL),
(66, 47, 36, 2, 1, NULL, NULL, NULL),
(67, 15, 10, 1, 1, NULL, NULL, NULL),
(68, 17, 47, 2, 1, NULL, NULL, NULL),
(69, 8, 49, 1, 2, NULL, NULL, NULL),
(70, 49, 17, 1, 2, NULL, NULL, NULL),
(71, 27, 46, 2, 1, NULL, NULL, NULL),
(72, 32, 15, 1, 1, NULL, NULL, NULL),
(73, 2, 20, 1, 2, NULL, NULL, NULL),
(74, 39, 27, 2, 1, NULL, NULL, NULL),
(75, 21, 43, 2, 1, NULL, NULL, NULL),
(76, 19, 10, 2, 1, NULL, NULL, NULL),
(77, 37, 41, 2, 2, NULL, NULL, NULL),
(78, 1, 46, 1, 1, NULL, NULL, NULL),
(79, 7, 32, 1, 2, NULL, NULL, NULL),
(80, 30, 10, 2, 1, NULL, NULL, NULL),
(81, 12, 49, 1, 1, NULL, NULL, NULL),
(82, 28, 6, 1, 2, NULL, NULL, NULL),
(83, 16, 12, 2, 1, NULL, NULL, NULL),
(84, 21, 5, 2, 2, NULL, NULL, NULL),
(85, 44, 36, 2, 1, NULL, NULL, NULL),
(86, 18, 8, 1, 2, NULL, NULL, NULL),
(87, 10, 29, 1, 2, NULL, NULL, NULL),
(88, 18, 25, 2, 1, NULL, NULL, NULL),
(89, 14, 24, 1, 2, NULL, NULL, NULL),
(90, 22, 33, 1, 1, NULL, NULL, NULL),
(91, 26, 9, 2, 1, NULL, NULL, NULL),
(92, 38, 12, 1, 2, NULL, NULL, NULL),
(93, 21, 39, 2, 1, NULL, NULL, NULL),
(94, 5, 5, 1, 2, NULL, NULL, NULL),
(95, 9, 3, 1, 1, NULL, NULL, NULL),
(96, 5, 22, 1, 1, NULL, NULL, NULL),
(97, 24, 40, 1, 1, NULL, NULL, NULL),
(98, 43, 14, 2, 2, NULL, NULL, NULL),
(99, 3, 14, 2, 1, NULL, NULL, NULL),
(100, 30, 14, 2, 2, NULL, NULL, NULL);

-- --------------------------------------------------------




--
-- Dumping data for table `fill_in_the_blanks`
--

INSERT INTO `fill_in_the_blanks` (`fill_blanks_id`, `sentence_task_id`, `translation_id`, `user_id`, `incomplete_sentence`, `explanation`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 9, 2, 'Kill the # let the # be #', 'Kill the boy let the man be born', NULL, NULL, NULL),
(2, 2, 2, 1, 'Time is a great #', 'Time is a great healer', NULL, NULL, NULL),
(3, 2, 1, 2, 'Don\'t # what you can\'t #', 'Don\'t criticize what you can\'t understand', NULL, NULL, NULL),
(4, 2, 2, 1, 'Death # time to grow the things that it would #.', 'Death created time to grow the things that it would kill.', NULL, NULL, NULL),
(5, 2, 2, 2, 'Wise men say only fools # in', 'Wise men say only fools rush in', NULL, NULL, NULL),
(6, 2, 2, 2, 'Small things # big days', 'Small things make big days', NULL, NULL, NULL),
(7, 2, 1, 2, 'Lose your #, you # your mind', 'Lose your dream, you lose your mind', NULL, NULL, NULL),
(8, 2, 2, 2, 'People # of guilt, # do have a good time.', 'People incapable of guilt, usually do have a good time.', NULL, NULL, NULL),
(9, 2, 1, 2, 'There is no such thing as #. People just have # memories.', 'There is no such thing as forgiveness. People just have short memories.', NULL, NULL, NULL),
(10, 3, 3, 2, 'Life’s # long enough to get good at one thing. So be # what you get good #.', 'Life’s barely long enough to get good at one thing. So be careful what you get good at.', NULL, NULL, NULL),
(11, 3, 7, 1, 'I did it for me. I # it. I was # at it. And, I was #.', 'I did it for me. I liked it. I was good at it. And, I was alive.', NULL, NULL, NULL),
(12, 3, 4, 2, 'Smile though your heart is #. # even though it\'s breaking', 'Smile though your heart is aching. Smile even though it\'s breaking', NULL, NULL, NULL),
(13, 3, 8, 1, 'I fought, I #. Now I #.', 'I fought, I lost. Now I rest.', NULL, NULL, NULL),
(14, 3, 1, 1, 'Never # what you are. The # of the world will not. Wear it like #, and it can never be used to # you.', 'Never forget what you are. The rest of the world will not. Wear it like armor, and it can never be used to hurt you.', NULL, NULL, NULL),
(15, 3, 3, 1, 'Mama always said life was like a # of #: You # know what you’re gonna get.', 'Mama always said life was like a box of chocolates: You never know what you’re gonna get.', NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `fb_answer`
--

INSERT INTO `fb_answer` (`answer_id`, `fill_blanks_id`, `answer`, `created_at`, `updated_at`) VALUES
(1, 1, 44, NULL, NULL),
(2, 1, 39, NULL, NULL),
(3, 1, 94, NULL, NULL),
(4, 2, 68, NULL, NULL),
(7, 6, 92, NULL, NULL),
(9, 7, 66, NULL, NULL),
(11, 8, 88, NULL, NULL),
(12, 9, 77, NULL, NULL),
(13, 10, 75, NULL, NULL),
(14, 10, 81, NULL, NULL),
(19, 9, 80, NULL, NULL),
(31, 10, 21, NULL, NULL),
(32, 4, 55, NULL, NULL),
(33, 5, 46, NULL, NULL),
(36, 3, 57, NULL, NULL),
(37, 10, 29, NULL, NULL),
(38, 7, 59, NULL, NULL),
(39, 4, 11, NULL, NULL),
(40, 3, 72, NULL, NULL),
(44, 8, 97, NULL, NULL),
(50, 10, 80, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `fb_options`
--

INSERT INTO `fb_options` (`options_id`, `fill_blanks_id`, `options`, `created_at`, `updated_at`) VALUES
(1, 1, 30, NULL, NULL),
(2, 1, 17, NULL, NULL),
(3, 1, 22, NULL, NULL),
(4, 2, 82, NULL, NULL),
(5, 2, 3, NULL, NULL),
(6, 2, 68, NULL, NULL),
(8, 8, 93, NULL, NULL),
(9, 10, 81, NULL, NULL),
(10, 1, 12, NULL, NULL),
(11, 9, 77, NULL, NULL),
(14, 8, 46, NULL, NULL),
(16, 10, 16, NULL, NULL),
(17, 4, 35, NULL, NULL),
(18, 6, 20, NULL, NULL),
(20, 7, 51, NULL, NULL),
(22, 3, 86, NULL, NULL),
(23, 8, 97, NULL, NULL),
(26, 8, 27, NULL, NULL),
(28, 6, 52, NULL, NULL),
(31, 6, 15, NULL, NULL),
(32, 3, 41, NULL, NULL),
(33, 8, 88, NULL, NULL),
(37, 9, 66, NULL, NULL),
(39, 1, 84, NULL, NULL),
(40, 10, 36, NULL, NULL),
(41, 9, 92, NULL, NULL),
(42, 7, 92, NULL, NULL),
(43, 7, 66, NULL, NULL),
(44, 6, 59, NULL, NULL),
(45, 1, 15, NULL, NULL),
(46, 9, 80, NULL, NULL),
(47, 10, 69, NULL, NULL),
(48, 4, 55, NULL, NULL),
(49, 3, 89, NULL, NULL),
(50, 7, 59, NULL, NULL),
(51, 3, 72, NULL, NULL),
(52, 3, 50, NULL, NULL),
(53, 3, 57, NULL, NULL),
(89, 4, 76, NULL, NULL),
(90, 4, 11, NULL, NULL),
(91, 5, 36, NULL, NULL),
(92, 5, 43, NULL, NULL),
(94, 5, 46, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `image_match`
--

INSERT INTO `image_match` (`matchword_id`, `word_task_id`, `dictionary_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 3, 1, NULL, NULL, NULL),
(2, 11, 34, 1, NULL, NULL, NULL),
(3, 11, 35, 1, NULL, NULL, NULL),
(4, 11, 45, 1, NULL, NULL, NULL),
(5, 11, 39, 1, NULL, NULL, NULL),
(6, 11, 59, 1, NULL, NULL, NULL),
(7, 11, 65, 1, NULL, NULL, NULL),
(8, 11, 45, 1, NULL, NULL, NULL),
(9, 11, 3, 1, NULL, NULL, NULL),
(10, 11, 34, 1, NULL, NULL, NULL),
(11, 11, 35, 1, NULL, NULL, NULL),
(12, 11, 45, 1, NULL, NULL, NULL),
(13, 11, 39, 1, NULL, NULL, NULL),
(14, 11, 59, 1, NULL, NULL, NULL),
(15, 11, 65, 1, NULL, NULL, NULL),
(16, 11, 45, 1, NULL, NULL, NULL),
(17, 11, 34, 1, NULL, NULL, NULL),
(18, 11, 35, 1, NULL, NULL, NULL),
(19, 11, 45, 1, NULL, NULL, NULL),
(20, 11, 39, 1, NULL, NULL, NULL),
(21, 11, 59, 1, NULL, NULL, NULL),
(22, 11, 65, 1, NULL, NULL, NULL),
(23, 11, 45, 1, NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `mcq`
--

INSERT INTO `mcq` (`mcq_id`, `word_task_id`, `explanation`, `user_id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 'The Sun rises in the East.', 2, 'In which direction does the Sun rise?', 47, NULL, NULL, NULL),
(2, 9, 'Sundarbans is the largest mangrove forest in the world.', 1, 'Which one is the largest mangrove forest?', 35, NULL, NULL, NULL),
(3, 9, 'Cox\'s Bazar is the largest sea beach.', 1, 'Which one below is the largest sea beach?', 77, NULL, NULL, NULL),
(4, 9, 'A cycle has 2 wheels. ', 1, 'How many wheels does a cycle have?', 90, NULL, NULL, NULL),
(5, 9, 'A cow has 2 legs.', 2, 'How many legs does a cow have?', 38, NULL, NULL, NULL),
(6, 9, 'The sky is blue coloured.', 1, 'What is the colour of the sky?', 2, NULL, NULL, NULL),
(7, 9, 'Dhaka is the capital of Bangladesh. ', 1, 'What is the caital city of Bangladesh?', 30, NULL, NULL, NULL),
(8, 9, 'Canada is in North America. ', 1, 'In which continent is Canada?', 94, NULL, NULL, NULL),
(9, 9, 'The sun is the only star in our solar system. ', 1, 'What is the name of the star of our solar system?', 47, NULL, NULL, NULL),
(10, 9, 'China has the largest population in the world having 1,389,618,778 people in total. ', 2, 'Which country does have the largest population in the world?', 20, NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `mcq_options`
--

INSERT INTO `mcq_options` (`options_id`, `mcq_id`, `dictionary_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 24, NULL, NULL, NULL),
(2, 4, 87, NULL, NULL, NULL),
(4, 8, 26, NULL, NULL, NULL),
(5, 4, 66, NULL, NULL, NULL),
(6, 7, 44, NULL, NULL, NULL),
(7, 6, 43, NULL, NULL, NULL),
(8, 1, 47, NULL, NULL, NULL),
(9, 3, 65, NULL, NULL, NULL),
(10, 3, 77, NULL, NULL, NULL),
(11, 5, 38, NULL, NULL, NULL),
(12, 6, 45, NULL, NULL, NULL),
(13, 5, 53, NULL, NULL, NULL),
(15, 10, 72, NULL, NULL, NULL),
(16, 9, 10, NULL, NULL, NULL),
(17, 7, 67, NULL, NULL, NULL),
(19, 1, 64, NULL, NULL, NULL),
(20, 2, 70, NULL, NULL, NULL),
(22, 7, 63, NULL, NULL, NULL),
(24, 4, 90, NULL, NULL, NULL),
(25, 10, 60, NULL, NULL, NULL),
(26, 5, 35, NULL, NULL, NULL),
(27, 2, 43, NULL, NULL, NULL),
(29, 8, 23, NULL, NULL, NULL),
(31, 1, 59, NULL, NULL, NULL),
(32, 6, 2, NULL, NULL, NULL),
(33, 9, 31, NULL, NULL, NULL),
(34, 9, 80, NULL, NULL, NULL),
(35, 4, 64, NULL, NULL, NULL),
(36, 8, 44, NULL, NULL, NULL),
(37, 7, 30, NULL, NULL, NULL),
(38, 2, 35, NULL, NULL, NULL),
(39, 5, 65, NULL, NULL, NULL),
(40, 6, 73, NULL, NULL, NULL),
(42, 8, 94, NULL, NULL, NULL),
(44, 9, 47, NULL, NULL, NULL),
(45, 3, 91, NULL, NULL, NULL),
(46, 1, 82, NULL, NULL, NULL),
(47, 3, 35, NULL, NULL, NULL),
(48, 10, 95, NULL, NULL, NULL),
(49, 10, 20, NULL, NULL, NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `sentence_matching`
--

INSERT INTO `sentence_matching` (`sen_match_id`, `sentence_task_id`, `translation_id`, `broken_sentence`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Lead a life you# can look back at and smile at your death bed', 1, NULL, NULL, NULL),
(2, 6, 2, 'Time is a# great healer', 1, NULL, NULL, NULL),
(3, 6, 3, 'Sundarbans is the# largest mangrove forest', 1, NULL, NULL, NULL),
(4, 11, 53, 'The night is dark# and full of terror', 1, NULL, NULL, NULL),
(5, 11, 67, 'Do not wait, the time will# never be \'just right\'.', 1, NULL, NULL, NULL),
(6, 11, 72, 'It\'s not about the result# it\'s all about the journey.', 1, NULL, NULL, NULL),
(7, 6, 53, 'The night is dark# and full of terror', 1, NULL, NULL, NULL),
(8, 6, 67, 'Do not wait, the time will# never be \'just right\'.', 1, NULL, NULL, NULL),
(9, 6, 72, 'It\'s not about the result# it\'s all about the journey.', 1, NULL, NULL, NULL),
(10, 11, 1, 'Lead a life you# can look back at and smile at your death bed', 1, NULL, NULL, NULL),
(11, 11, 2, 'Time is a# great healer', 1, NULL, NULL, NULL),
(12, 11, 3, 'Sundarbans is the# largest mangrove forest', 1, NULL, NULL, NULL),
(13, 6, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(14, 6, 8, 'I fought,# I lost. Now I rest.', 1, NULL, NULL, NULL),
(15, 6, 12, 'The Sun rises# in the East', 1, NULL, NULL, NULL),
(16, 11, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(17, 11, 8, 'I fought,# I lost. Now I rest.', 1, NULL, NULL, NULL),
(18, 11, 12, 'The Sun rises# in the East', 1, NULL, NULL, NULL);

-- --------------------------------------------------------



--
-- Dumping data for table `true_false`
--

INSERT INTO `true_false` (`true_false_id`, `sentence_task_id`, `answer`, `explanation`, `user_id`, `translation_id`, `question`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'True', 'It\'s a constant truth.', 1, 23, 'The Sun rises in the east.', '2019-05-12 18:00:01', '2019-05-23 15:18:36', NULL),
(2, 5, 'False', 'Cows can\'t fly.', 1, 43, 'The cow is flying in the sky.', '2019-05-12 18:00:01', '2019-05-23 15:18:36', NULL),
(3, 5, 'False', 'Bill Gates is the founder of Microsoft.', 2, 57, 'Bill Gates is the founder of Google.', '2019-05-12 18:00:01', '2019-05-23 15:18:36', NULL),
(4, 5, 'True', 'Canada is in North America.', 1, 92, 'Canada is in North America.', NULL, NULL, NULL),
(5, 5, 'False', 'Sundarban is in Bangladesh and India.', 1, 32, 'Sundarban is in China.', NULL, NULL, NULL),
(6, 5, 'True', 'The fastest land animal in the world is the Cheetah.', 1, 32, 'The fastest land animal in the world is the Cheetah.', NULL, NULL, NULL),
(7, 5, 'True', 'Newton discovered gravity.', 2, 1, 'Newton discovered gravity.', NULL, NULL, NULL),
(8, 5, 'False', 'Dhaka is the capital city of Bangladesh.', 1, 3, 'Dhaka is the capital city of India.', NULL, NULL, NULL);

-- --------------------------------------------------------





