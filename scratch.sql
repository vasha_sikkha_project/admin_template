-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 14, 2019 at 05:09 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scratch`
--

-- --------------------------------------------------------

--
-- Table structure for table `bangla_resource`
--

CREATE TABLE `bangla_resource` (
  `bangla_word_id` int(10) UNSIGNED NOT NULL,
  `word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bangla_resource`
--

INSERT INTO `bangla_resource` (`bangla_word_id`, `word`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ঢাকা', NULL, NULL, NULL),
(2, '7rTdOMFmfl', NULL, NULL, NULL),
(3, 'JdFe5C97Mz', NULL, NULL, NULL),
(4, 'QhxfcxMX88', NULL, NULL, NULL),
(5, 'K18hvHr71c', NULL, NULL, NULL),
(6, 'W114NSswRV', NULL, NULL, NULL),
(7, 'E7bggvyCn1', NULL, NULL, NULL),
(8, 'DXT0aJnezR', NULL, NULL, NULL),
(9, '43M7gIgmxA', NULL, NULL, NULL),
(10, 'r4k5UDSJHT', NULL, NULL, NULL),
(11, '9zdsF6IPR6', NULL, NULL, NULL),
(12, 'WpJrVWuR5h', NULL, NULL, NULL),
(13, 'ULJPSg3maQ', NULL, NULL, NULL),
(14, 'pXsg6HSrNp', NULL, NULL, NULL),
(15, 'XMxwUGMSjq', NULL, NULL, NULL),
(16, 'PF76neajIc', NULL, NULL, NULL),
(17, 'ANRYpRucMC', NULL, NULL, NULL),
(18, 'hysVcextd3', NULL, NULL, NULL),
(19, 'বালক', NULL, NULL, NULL),
(20, 'dJpXgpmLik', NULL, NULL, NULL),
(21, 'পুরুষ', NULL, NULL, NULL),
(22, 'oc72PwwJRv', NULL, NULL, NULL),
(23, 'Sjpy4XSSKa', NULL, NULL, NULL),
(24, 'y4JGdq63sE', NULL, NULL, NULL),
(25, '2lCtVlShdh', NULL, NULL, NULL),
(26, 'lioaUZdGZi', NULL, NULL, NULL),
(27, 'Egmf2D8o2T', NULL, NULL, NULL),
(28, 'JUWeK1jOV5', NULL, NULL, NULL),
(29, 'ol9UCMZIOv', NULL, NULL, NULL),
(30, 'w3Xx5wSfZG', NULL, NULL, NULL),
(31, 'GbFVsCo6n3', NULL, NULL, NULL),
(32, 'পৃথিবী', NULL, NULL, NULL),
(33, 'SckdEWGdbK', NULL, NULL, NULL),
(34, 't1OWuFLm4m', NULL, NULL, NULL),
(35, 'jcJiaVccKf', NULL, NULL, NULL),
(36, 'QPmWOJELhm', NULL, NULL, NULL),
(37, 'RkRtOQllZH', NULL, NULL, NULL),
(38, 'OnMRQZNQY9', NULL, NULL, NULL),
(39, 'HJl1Tv9dR5', NULL, NULL, NULL),
(40, 'tdcT7czreJ', NULL, NULL, NULL),
(41, 'nyLCxNpJbC', NULL, NULL, NULL),
(42, 'cz0OpkUg1T', NULL, NULL, NULL),
(43, 'ZErZ7YZGxN', NULL, NULL, NULL),
(44, 'Fjl5evn17S', NULL, NULL, NULL),
(45, 'PQFOIRU8lX', NULL, NULL, NULL),
(46, 'JXKBGlOG2X', NULL, NULL, NULL),
(47, '0F1ikZ8N1k', NULL, NULL, NULL),
(48, 'MMFzHWLyeE', NULL, NULL, NULL),
(49, 'XX4Mbud2fT', NULL, NULL, NULL),
(50, 'VBiCjyHgLx', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bangla_sentence`
--

CREATE TABLE `bangla_sentence` (
  `bangla_sentence_id` int(10) UNSIGNED NOT NULL,
  `bangla_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bangla_sentence`
--

INSERT INTO `bangla_sentence` (`bangla_sentence_id`, `bangla_sentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'কানাডা উত্তর আমেরিকায় অবস্থিত।', NULL, NULL, NULL),
(2, 'আমি যুদ্ধ করেছি, হেরে গিয়েছি। এখন আমি বিদায় নিচ্ছি।', NULL, NULL, NULL),
(3, 'কক্স\'স বাজার বিশ্বের বৃহত্তম সমুদ্র সৈকত', NULL, NULL, NULL),
(4, 'অকল্যান্ড নিউজিল্যান্ডের রাজধানী।', NULL, NULL, NULL),
(5, 'মিথ্যা বলার চেয়ে মৃত্যু আমার কাছে শ্রেয়', NULL, NULL, NULL),
(6, 'ঢাকা বুড়িগঙ্গার তীরে অবস্থিত।', NULL, NULL, NULL),
(7, 'ঢাকা বাংলাদেশের রাজধানী।', NULL, NULL, NULL),
(8, 'ওয়াশিংটন ডিসি আমেরিকার রাজধানী।', NULL, NULL, NULL),
(9, 'রাত অন্ধকার এবং ভয়ে পরিপূর্ণ।', NULL, NULL, NULL),
(10, '\'সঠিক সময়\' এর অপেক্ষায় কাজ ফেলে রেখো না।', NULL, NULL, NULL),
(11, 'QW0ja05avu', NULL, NULL, NULL),
(12, 'fo9neZ3mcd', NULL, NULL, NULL),
(13, 'সময় সবচেয়ে বড় নিরামক', NULL, NULL, NULL),
(14, 'নিজের ভেতরের বালককে বধ করো, পুরুষের জন্ম হতে দাও।', NULL, NULL, NULL),
(15, 'ফলাফল অর্জনের চেয়ে অর্জিত অভিজ্ঞতা বেশি গুরুত্বপূর্ণ। ', NULL, NULL, NULL),
(16, 'IFfFISeheS', NULL, NULL, NULL),
(17, 'ksjlklQuSO', NULL, NULL, NULL),
(18, 'ZFUG2I2cgc', NULL, NULL, NULL),
(19, 'ZVmOdBcv9x', NULL, NULL, NULL),
(20, 'SBwo8hGB34', NULL, NULL, NULL),
(21, 'uWRmNOXfge', NULL, NULL, NULL),
(22, '3KOI37n10N', NULL, NULL, NULL),
(23, 'zKZDo3NGbi', NULL, NULL, NULL),
(24, 'mv3jDDB4nq', NULL, NULL, NULL),
(25, 'zT0XN1YXqc', NULL, NULL, NULL),
(26, 'PXR6QdS9IP', NULL, NULL, NULL),
(27, 'HSYNTutTVJ', NULL, NULL, NULL),
(28, 'd14Gxdx3mM', NULL, NULL, NULL),
(29, 'gxGSxK6X3C', NULL, NULL, NULL),
(30, 'সুন্দরবন সর্ববৃহৎ ম্যানগ্রোভ বন', NULL, NULL, NULL),
(31, 'd2cI8MjP1T', NULL, NULL, NULL),
(32, 'oKYWY6nE4g', NULL, NULL, NULL),
(33, 'LHARuFDHZD', NULL, NULL, NULL),
(34, '1U9sadLH69', NULL, NULL, NULL),
(35, 'yAgRLFTkjC', NULL, NULL, NULL),
(36, '7bg13j81bF', NULL, NULL, NULL),
(37, 'caU38typwq', NULL, NULL, NULL),
(38, 'ch9NWZZsFs', NULL, NULL, NULL),
(39, 'vESROUNoIV', NULL, NULL, NULL),
(40, 'pNlDAw7A77', NULL, NULL, NULL),
(41, 'সূর্য পূর্ব দিকে ওঠে', NULL, NULL, NULL),
(42, 'হৃদয়ে ব্যথা হলেও হাসো, হৃদয় ভেঙ্গে গেলেও হাসো।', NULL, NULL, NULL),
(43, 'মা সবসময় বলতো জীবন একটা চকলেটের বক্সের মতো। সামনে কী অপেক্ষা করছে কেউ জানে না।', NULL, NULL, NULL),
(44, 'umZx9Lwmmt', NULL, NULL, NULL),
(45, 'hpGwYdE0E0', NULL, NULL, NULL),
(46, 'bYKEoBaEdS', NULL, NULL, NULL),
(47, 'কখনো নিজের পরিচয় ভুলো না। কারণ পৃথিবী ভুলবে না। বর্মের মতো এটা পরিধান করো যাতে কেউ এটা দিয়ে আঘাত না করতে পারে।', NULL, NULL, NULL),
(48, 'এটা আমি আমার জন্য করেছিলাম। আমি এ কাজ ভালবাসতাম। আমি এটায় সেরা ছিলাম। আমি জীবন ফিরে পেয়েছিলাম।', NULL, NULL, NULL),
(49, 'oHD91kHI80', NULL, NULL, NULL),
(50, 'এমন জীবন গঠন করো, যাতে মৃত্যুশয্যায় জীবনের দিকে তাকিয়ে স্মিত হতে পারো। ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cross_word_puzzle`
--

CREATE TABLE `cross_word_puzzle` (
  `cross_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cross_word_puzzle_info`
--

CREATE TABLE `cross_word_puzzle_info` (
  `info_id` int(10) UNSIGNED NOT NULL,
  `cross_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dictionary`
--

CREATE TABLE `dictionary` (
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `another_word_id` int(10) UNSIGNED NOT NULL,
  `bangla_word_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dictionary`
--

INSERT INTO `dictionary` (`dictionary_id`, `another_word_id`, `bangla_word_id`, `language_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 28, 35, 2, NULL, NULL, NULL),
(2, 48, 31, 1, NULL, NULL, NULL),
(3, 1, 17, 2, NULL, NULL, NULL),
(4, 48, 23, 2, NULL, NULL, NULL),
(5, 19, 26, 1, NULL, NULL, NULL),
(6, 16, 48, 2, NULL, NULL, NULL),
(7, 7, 10, 1, NULL, NULL, NULL),
(8, 14, 50, 1, NULL, NULL, NULL),
(9, 10, 45, 2, NULL, NULL, NULL),
(10, 40, 27, 2, NULL, NULL, NULL),
(11, 43, 47, 1, NULL, NULL, NULL),
(12, 34, 26, 2, NULL, NULL, NULL),
(13, 24, 13, 1, NULL, NULL, NULL),
(14, 41, 21, 1, NULL, NULL, NULL),
(15, 21, 9, 1, NULL, NULL, NULL),
(16, 28, 18, 1, NULL, NULL, NULL),
(17, 13, 21, 2, NULL, NULL, NULL),
(18, 12, 28, 2, NULL, NULL, NULL),
(19, 38, 40, 2, NULL, NULL, NULL),
(20, 38, 24, 2, NULL, NULL, NULL),
(21, 45, 10, 2, NULL, NULL, NULL),
(22, 12, 19, 2, NULL, NULL, NULL),
(23, 30, 16, 2, NULL, NULL, NULL),
(24, 32, 28, 2, NULL, NULL, NULL),
(25, 40, 45, 1, NULL, NULL, NULL),
(26, 40, 27, 2, NULL, NULL, NULL),
(27, 49, 26, 2, NULL, NULL, NULL),
(28, 28, 14, 2, NULL, NULL, NULL),
(29, 21, 48, 2, NULL, NULL, NULL),
(30, 22, 32, 2, NULL, NULL, NULL),
(31, 7, 50, 2, NULL, NULL, NULL),
(32, 10, 4, 2, NULL, NULL, NULL),
(33, 27, 18, 2, NULL, NULL, NULL),
(34, 6, 44, 1, NULL, NULL, NULL),
(35, 15, 17, 1, NULL, NULL, NULL),
(36, 47, 50, 1, NULL, NULL, NULL),
(37, 28, 36, 1, NULL, NULL, NULL),
(38, 31, 18, 2, NULL, NULL, NULL),
(39, 45, 26, 1, NULL, NULL, NULL),
(40, 36, 10, 2, NULL, NULL, NULL),
(41, 25, 11, 1, NULL, NULL, NULL),
(42, 46, 13, 1, NULL, NULL, NULL),
(43, 34, 42, 2, NULL, NULL, NULL),
(44, 40, 36, 1, NULL, NULL, NULL),
(45, 24, 23, 2, NULL, NULL, NULL),
(46, 48, 10, 2, NULL, NULL, NULL),
(47, 49, 45, 2, NULL, NULL, NULL),
(48, 37, 47, 1, NULL, NULL, NULL),
(49, 29, 43, 2, NULL, NULL, NULL),
(50, 50, 25, 2, NULL, NULL, NULL),
(51, 47, 26, 2, NULL, NULL, NULL),
(52, 23, 46, 2, NULL, NULL, NULL),
(53, 50, 47, 1, NULL, NULL, NULL),
(54, 40, 27, 1, NULL, NULL, NULL),
(55, 47, 40, 1, NULL, NULL, NULL),
(56, 21, 20, 2, NULL, NULL, NULL),
(57, 15, 32, 2, NULL, NULL, NULL),
(58, 12, 47, 2, NULL, NULL, NULL),
(59, 30, 50, 1, NULL, NULL, NULL),
(60, 8, 15, 1, NULL, NULL, NULL),
(61, 6, 27, 2, NULL, NULL, NULL),
(62, 1, 26, 1, NULL, NULL, NULL),
(63, 31, 45, 1, NULL, NULL, NULL),
(64, 4, 16, 1, NULL, NULL, NULL),
(65, 50, 13, 2, NULL, NULL, NULL),
(66, 27, 20, 2, NULL, NULL, NULL),
(67, 38, 46, 1, NULL, NULL, NULL),
(68, 29, 6, 1, NULL, NULL, NULL),
(69, 12, 1, 1, NULL, NULL, NULL),
(70, 16, 48, 1, NULL, NULL, NULL),
(71, 3, 8, 1, NULL, NULL, NULL),
(72, 26, 28, 1, NULL, NULL, NULL),
(73, 50, 6, 2, NULL, NULL, NULL),
(74, 11, 8, 1, NULL, NULL, NULL),
(75, 24, 42, 1, NULL, NULL, NULL),
(76, 26, 30, 2, NULL, NULL, NULL),
(77, 33, 16, 2, NULL, NULL, NULL),
(78, 11, 1, 2, NULL, NULL, NULL),
(79, 22, 31, 1, NULL, NULL, NULL),
(80, 37, 34, 2, NULL, NULL, NULL),
(81, 2, 7, 2, NULL, NULL, NULL),
(82, 8, 21, 2, NULL, NULL, NULL),
(83, 38, 36, 1, NULL, NULL, NULL),
(84, 5, 15, 1, NULL, NULL, NULL),
(85, 3, 9, 1, NULL, NULL, NULL),
(86, 29, 18, 1, NULL, NULL, NULL),
(87, 10, 19, 1, NULL, NULL, NULL),
(88, 32, 15, 2, NULL, NULL, NULL),
(89, 49, 46, 2, NULL, NULL, NULL),
(90, 9, 24, 2, NULL, NULL, NULL),
(91, 37, 16, 2, NULL, NULL, NULL),
(92, 31, 31, 2, NULL, NULL, NULL),
(93, 15, 42, 2, NULL, NULL, NULL),
(94, 44, 43, 1, NULL, NULL, NULL),
(95, 21, 2, 1, NULL, NULL, NULL),
(96, 28, 19, 1, NULL, NULL, NULL),
(97, 35, 4, 1, NULL, NULL, NULL),
(98, 7, 15, 1, NULL, NULL, NULL),
(99, 40, 48, 2, NULL, NULL, NULL),
(100, 46, 7, 1, NULL, NULL, NULL),
(101, 4, 9, 1, NULL, NULL, NULL),
(102, 7, 17, 1, NULL, NULL, NULL),
(103, 10, 9, 1, NULL, NULL, NULL),
(104, 11, 17, 1, NULL, NULL, NULL),
(105, 14, 9, 1, NULL, NULL, NULL),
(106, 7, 17, 1, NULL, NULL, NULL),
(107, 10, 9, 1, NULL, NULL, NULL),
(108, 11, 17, 1, NULL, NULL, NULL),
(109, 19, 17, 1, NULL, NULL, NULL),
(110, 20, 17, 1, NULL, NULL, NULL),
(111, 17, 17, 1, NULL, NULL, NULL),
(112, 28, 17, 1, NULL, NULL, NULL),
(113, 16, 17, 1, NULL, NULL, NULL),
(114, 41, 17, 1, NULL, NULL, NULL),
(115, 39, 17, 1, NULL, NULL, NULL),
(116, 16, 17, 1, NULL, NULL, NULL),
(117, 41, 17, 1, NULL, NULL, NULL),
(118, 36, 17, 1, NULL, NULL, NULL),
(119, 18, 17, 1, NULL, NULL, NULL),
(120, 42, 17, 1, NULL, NULL, NULL),
(121, 46, 17, 1, NULL, NULL, NULL),
(122, 51, 17, 1, NULL, NULL, NULL),
(123, 52, 17, 1, NULL, NULL, NULL),
(124, 53, 17, 1, NULL, NULL, NULL),
(125, 54, 17, 1, NULL, NULL, NULL),
(126, 55, 17, 1, NULL, NULL, NULL),
(127, 56, 17, 1, NULL, NULL, NULL),
(128, 57, 17, 1, NULL, NULL, NULL),
(129, 58, 17, 1, NULL, NULL, NULL),
(130, 59, 17, 1, NULL, NULL, NULL),
(131, 60, 17, 1, NULL, NULL, NULL),
(132, 61, 17, 1, NULL, NULL, NULL),
(133, 62, 17, 1, NULL, NULL, NULL),
(134, 63, 17, 1, NULL, NULL, NULL),
(135, 64, 17, 1, NULL, NULL, NULL),
(136, 65, 17, 1, NULL, NULL, NULL),
(137, 66, 17, 1, NULL, NULL, NULL),
(138, 67, 17, 1, NULL, NULL, NULL),
(139, 68, 17, 1, NULL, NULL, NULL),
(140, 69, 17, 1, NULL, NULL, NULL),
(141, 60, 17, 1, NULL, NULL, NULL),
(142, 61, 17, 1, NULL, NULL, NULL),
(143, 62, 17, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `english_resource`
--

CREATE TABLE `english_resource` (
  `english_word_id` int(10) UNSIGNED NOT NULL,
  `word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `english_resource`
--

INSERT INTO `english_resource` (`english_word_id`, `word`, `image_link`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'match', 'XPY0i7nABA', NULL, NULL, NULL),
(2, 'Dhaka', 'hPULtcwF3i', NULL, NULL, NULL),
(3, 'USA', 'YlkPvYlhfF', NULL, NULL, NULL),
(4, 'largest', '9h7ks0Vrqo', NULL, NULL, NULL),
(5, 'sun', 'YunIEWYAy4', NULL, NULL, NULL),
(6, 'capable', 'v1jo0ne4ar', NULL, NULL, NULL),
(7, 'world', 'zGUei7RBXB', NULL, NULL, NULL),
(8, 'traveller', '0khlJKQb64', NULL, NULL, NULL),
(9, 'hazes', 'JDUoLEtE4n', NULL, NULL, NULL),
(10, 'Canada', 'cEkAvN4erp', NULL, NULL, NULL),
(11, 'Buriganga', 'ZONEQlTxaU', NULL, NULL, NULL),
(12, 'boy', 'G06DCXS2Ko', NULL, NULL, NULL),
(13, 'man', 'ZjsJJbzdMk', NULL, NULL, NULL),
(14, 'Newzealand', 'LkubhYzxa0', NULL, NULL, NULL),
(15, 'criticize', '8FfaoAv9gC', NULL, NULL, NULL),
(16, 'Auckland', 'Pt3I5M6KZN', NULL, NULL, NULL),
(17, 'Padma', 'Vh9oEQep7o', NULL, NULL, NULL),
(18, 'China', 'iMZOwqIkDr', NULL, NULL, NULL),
(19, 'Delhi', 'eUwimqYTEq', NULL, NULL, NULL),
(20, 'USA', 'yRPLcQWhvc', NULL, NULL, NULL),
(21, 'born', '44gK0GJG4K', NULL, NULL, NULL),
(22, 'world', 'OtVwjAmq2B', NULL, NULL, NULL),
(23, 'make', 'aLmq4dpXwq', NULL, NULL, NULL),
(24, 'usually', 'GpXUe4cCMh', NULL, NULL, NULL),
(25, 'cover', 'toQLOWnPMo', NULL, NULL, NULL),
(26, 'understand', 'UAxNi4IXtX', NULL, NULL, NULL),
(27, 'dream', '9i7ItWrLQC', NULL, NULL, NULL),
(28, 'Meghna', 'WXxW5oFD9B', NULL, NULL, NULL),
(29, 'healer', '5pUNbqvw0W', NULL, NULL, NULL),
(30, 'lose', 'e46Kc44CTy', NULL, NULL, NULL),
(31, 'make', 'Zg3gbbrF5M', NULL, NULL, NULL),
(32, 'incapable', 'SehGdmh8cC', NULL, NULL, NULL),
(33, 'forgiveness', 'T5HwqF64i5', NULL, NULL, NULL),
(34, 'prefer', 'j4s8WBSIwL', NULL, NULL, NULL),
(35, 'usually', 'mUajma3ghW', NULL, NULL, NULL),
(36, 'honesty', 'VRFEHn4q43', NULL, NULL, NULL),
(37, 'short', 'TlqT7sqxIT', NULL, NULL, NULL),
(38, 'under', 'pPA4Xrryaj', NULL, NULL, NULL),
(39, 'North-America', 'siqEXBW7D6', NULL, NULL, NULL),
(40, 'boy', 'cxRdervH7Y', NULL, NULL, NULL),
(41, 'Cox\'s Bazar', 'zC8GwPODqE', NULL, NULL, NULL),
(42, 'ears', 'X1pHp3ZCy4', NULL, NULL, NULL),
(43, 'kill', 'csuYjXLywd', NULL, NULL, NULL),
(44, 'born', 'xS4ULJDSkK', NULL, NULL, NULL),
(45, 'man', '11YtOyBP4T', NULL, NULL, NULL),
(46, 'Newton', 'hxqwak9qVx', NULL, NULL, NULL),
(47, 'created', 'v8Ux59mWzH', NULL, NULL, NULL),
(48, 'rush', 'qgSpuILk51', NULL, NULL, NULL),
(49, 'learn', 'uCq1MNR2oa', NULL, NULL, NULL),
(50, 'sun', 'kdYr0MNxnW', NULL, NULL, NULL),
(51, 'two', 'csuYjXLywd', NULL, NULL, NULL),
(52, 'Einstein', 'xS4ULJDSkK', NULL, NULL, NULL),
(53, 'lying', '11YtOyBP4T', NULL, NULL, NULL),
(54, 'jealousy', 'hxqwak9qVx', NULL, NULL, NULL),
(55, 'stealing', 'v8Ux59mWzH', NULL, NULL, NULL),
(56, 'nose', 'qgSpuILk51', NULL, NULL, NULL),
(57, 'eyes', 'uCq1MNR2oa', NULL, NULL, NULL),
(58, 'hands', 'kdYr0MNxnW', NULL, NULL, NULL),
(59, 'Tesla', 'uCq1MNR2oa', NULL, NULL, NULL),
(60, 'Edison', 'kdYr0MNxnW', NULL, NULL, NULL),
(61, 'three', 'csuYjXLywd', NULL, NULL, NULL),
(62, 'four', 'xS4ULJDSkK', NULL, NULL, NULL),
(63, 'five', '11YtOyBP4T', NULL, NULL, NULL),
(64, 'Asia', 'hxqwak9qVx', NULL, NULL, NULL),
(65, 'Europe', 'v8Ux59mWzH', NULL, NULL, NULL),
(66, 'South America', 'qgSpuILk51', NULL, NULL, NULL),
(67, 'Spain', 'uCq1MNR2oa', NULL, NULL, NULL),
(68, 'Thames', 'kdYr0MNxnW', NULL, NULL, NULL),
(69, 'Islamabad', 'kdYr0MNxnW', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `english_sentence`
--

CREATE TABLE `english_sentence` (
  `english_sentence_id` int(10) UNSIGNED NOT NULL,
  `english_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `english_sentence`
--

INSERT INTO `english_sentence` (`english_sentence_id`, `english_sentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'I did it for me. I liked it. I was good at it. And, I was alive.', NULL, NULL, NULL),
(2, 'Smile though your heart is aching. Smile even though it\'s breaking', NULL, NULL, NULL),
(3, 'Canada is situated in North America.', NULL, NULL, NULL),
(4, 'Auckland is the capital of Newzealand.', NULL, NULL, NULL),
(5, 'Dhaka is situated on the bank of Buriganga.', NULL, NULL, NULL),
(6, 'Dhaka is the capital of Bangladesh.', NULL, NULL, NULL),
(7, 'Cox\'s Bazar is the largest sea beach in the world', NULL, NULL, NULL),
(8, 'Washington DC is the capital of USA.', NULL, NULL, NULL),
(9, 'I would rather die than telling lie', NULL, NULL, NULL),
(10, 'PdJA02XJ7w', NULL, NULL, NULL),
(11, 'Time is a great healer', NULL, NULL, NULL),
(12, 'lA68zXWd68', NULL, NULL, NULL),
(13, '1Fdfod9CV9', NULL, NULL, NULL),
(14, 'QBs69YMJTb', NULL, NULL, NULL),
(15, 'Do not wait, the time will never be \'just right\'.', NULL, NULL, NULL),
(16, 'Th', NULL, NULL, NULL),
(17, 'The Sun rises in the East', NULL, NULL, NULL),
(18, 'QMa0U7DIsR', NULL, NULL, NULL),
(19, 'g0Ia6xWbx8', NULL, NULL, NULL),
(20, 'Rph5UacK5n', NULL, NULL, NULL),
(21, 'dUthcb7ldI', NULL, NULL, NULL),
(22, 'oxSOQcgyhy', NULL, NULL, NULL),
(23, 'CtboJcBG3s', NULL, NULL, NULL),
(24, 'tcBcuqDASl', NULL, NULL, NULL),
(25, 'SI7y6UwNW3', NULL, NULL, NULL),
(26, 'wjnM7IuJLF', NULL, NULL, NULL),
(27, 'sA7ZIvsFOp', NULL, NULL, NULL),
(28, 'GnUDywJevs', NULL, NULL, NULL),
(29, 'WE7ylv2Wrv', NULL, NULL, NULL),
(30, 'jadnt1QcaU', NULL, NULL, NULL),
(31, 'Sundarbans is the largest mangrove forest', NULL, NULL, NULL),
(32, 'It\'s not about the result it\'s all about the journey.', NULL, NULL, NULL),
(33, 'I fought, I lost. Now I rest.', NULL, NULL, NULL),
(34, 'Kill the boy let the man be born.', NULL, NULL, NULL),
(35, 'VDq1Ee2zl2', NULL, NULL, NULL),
(36, 'B0ANK7tk30', NULL, NULL, NULL),
(37, 'np3RYkwHrx', NULL, NULL, NULL),
(38, 'Never forget what you are. The rest of the world will not. Wear it like armor, and it can never be used to hurt you.', NULL, NULL, NULL),
(39, 'oS4VKqNIiR', NULL, NULL, NULL),
(40, 'grKoLaZBpm', NULL, NULL, NULL),
(41, 'j6HSKn3NCR', NULL, NULL, NULL),
(42, 'dzLa3AdVva', NULL, NULL, NULL),
(43, 'fRUOlmDETg', NULL, NULL, NULL),
(44, 'K0PXOD0z9p', NULL, NULL, NULL),
(45, 'FgqAWU7k3U', NULL, NULL, NULL),
(46, 'Mama always said life was like a box of chocolates: You never know what you’re gonna get.', NULL, NULL, NULL),
(47, 'Lead a life you can look back at and smile at your death bed', NULL, NULL, NULL),
(48, 'E0roXodxWR', NULL, NULL, NULL),
(49, 'cVL9SGAXFE', NULL, NULL, NULL),
(50, 'The night is dark and full of terror', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `experience_id` int(10) UNSIGNED NOT NULL,
  `experience_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`experience_id`, `experience_type`) VALUES
(1, 'JYW7duSff2');

-- --------------------------------------------------------

--
-- Table structure for table `fb_answer`
--

CREATE TABLE `fb_answer` (
  `answer_id` int(10) UNSIGNED NOT NULL,
  `fill_blanks_id` int(10) UNSIGNED NOT NULL,
  `answer` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fb_answer`
--

INSERT INTO `fb_answer` (`answer_id`, `fill_blanks_id`, `answer`, `created_at`, `updated_at`) VALUES
(1, 1, 44, NULL, NULL),
(2, 1, 39, NULL, NULL),
(3, 1, 94, NULL, NULL),
(4, 2, 68, NULL, NULL),
(7, 6, 92, NULL, NULL),
(9, 7, 66, NULL, NULL),
(11, 8, 88, NULL, NULL),
(12, 9, 77, NULL, NULL),
(13, 10, 75, NULL, NULL),
(14, 10, 81, NULL, NULL),
(19, 9, 80, NULL, NULL),
(31, 10, 21, NULL, NULL),
(32, 4, 55, NULL, NULL),
(33, 5, 46, NULL, NULL),
(36, 3, 57, NULL, NULL),
(37, 10, 29, NULL, NULL),
(38, 7, 59, NULL, NULL),
(39, 4, 11, NULL, NULL),
(40, 3, 72, NULL, NULL),
(44, 8, 97, NULL, NULL),
(50, 10, 80, NULL, NULL),
(51, 16, 81, NULL, NULL),
(52, 17, 85, NULL, NULL),
(53, 18, 101, NULL, NULL),
(54, 18, 102, NULL, NULL),
(55, 19, 103, NULL, NULL),
(56, 20, 104, NULL, NULL),
(57, 21, 105, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fb_options`
--

CREATE TABLE `fb_options` (
  `options_id` int(10) UNSIGNED NOT NULL,
  `fill_blanks_id` int(10) UNSIGNED NOT NULL,
  `options` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fb_options`
--

INSERT INTO `fb_options` (`options_id`, `fill_blanks_id`, `options`, `created_at`, `updated_at`) VALUES
(1, 1, 30, NULL, NULL),
(2, 1, 17, NULL, NULL),
(3, 1, 22, NULL, NULL),
(4, 2, 82, NULL, NULL),
(5, 2, 3, NULL, NULL),
(6, 2, 68, NULL, NULL),
(8, 8, 93, NULL, NULL),
(9, 10, 81, NULL, NULL),
(10, 1, 12, NULL, NULL),
(11, 9, 77, NULL, NULL),
(14, 8, 46, NULL, NULL),
(16, 10, 16, NULL, NULL),
(17, 4, 35, NULL, NULL),
(18, 6, 20, NULL, NULL),
(20, 7, 51, NULL, NULL),
(22, 3, 86, NULL, NULL),
(23, 8, 97, NULL, NULL),
(26, 8, 27, NULL, NULL),
(28, 6, 52, NULL, NULL),
(31, 6, 15, NULL, NULL),
(32, 3, 41, NULL, NULL),
(33, 8, 88, NULL, NULL),
(37, 9, 66, NULL, NULL),
(39, 1, 84, NULL, NULL),
(40, 10, 36, NULL, NULL),
(41, 9, 92, NULL, NULL),
(42, 7, 92, NULL, NULL),
(43, 7, 66, NULL, NULL),
(44, 6, 59, NULL, NULL),
(45, 1, 15, NULL, NULL),
(46, 9, 80, NULL, NULL),
(47, 10, 69, NULL, NULL),
(48, 4, 55, NULL, NULL),
(49, 3, 89, NULL, NULL),
(50, 7, 59, NULL, NULL),
(51, 3, 72, NULL, NULL),
(52, 3, 50, NULL, NULL),
(53, 3, 57, NULL, NULL),
(89, 4, 76, NULL, NULL),
(90, 4, 11, NULL, NULL),
(91, 5, 36, NULL, NULL),
(92, 5, 43, NULL, NULL),
(94, 5, 46, NULL, NULL),
(101, 16, 103, NULL, NULL),
(102, 16, 81, NULL, NULL),
(103, 16, 109, NULL, NULL),
(104, 17, 107, NULL, NULL),
(105, 17, 105, NULL, NULL),
(106, 17, 110, NULL, NULL),
(107, 18, 101, NULL, NULL),
(108, 18, 103, NULL, NULL),
(109, 18, 106, NULL, NULL),
(110, 18, 110, NULL, NULL),
(111, 5, 43, NULL, NULL),
(112, 6, 15, NULL, NULL),
(113, 6, 52, NULL, NULL),
(114, 6, 59, NULL, NULL),
(115, 19, 109, NULL, NULL),
(116, 19, 97, NULL, NULL),
(117, 19, 107, NULL, NULL),
(118, 20, 111, NULL, NULL),
(119, 20, 110, NULL, NULL),
(120, 20, 112, NULL, NULL),
(121, 20, 104, NULL, NULL),
(122, 21, 110, NULL, NULL),
(123, 21, 105, NULL, NULL),
(124, 21, 103, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `feedbak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fill_in_the_blanks`
--

CREATE TABLE `fill_in_the_blanks` (
  `fill_blanks_id` int(10) UNSIGNED NOT NULL,
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `incomplete_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `explanation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fill_in_the_blanks`
--

INSERT INTO `fill_in_the_blanks` (`fill_blanks_id`, `sentence_task_id`, `translation_id`, `user_id`, `incomplete_sentence`, `explanation`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 9, 2, 'Kill the # let the # be #', 'Kill the boy let the man be born', NULL, NULL, NULL),
(2, 2, 2, 1, 'Time is a great #', 'Time is a great healer', NULL, NULL, NULL),
(3, 2, 1, 2, 'Don\'t # what you can\'t #', 'Don\'t criticize what you can\'t understand', NULL, NULL, NULL),
(4, 2, 2, 1, 'Death # time to grow the things that it would #.', 'Death created time to grow the things that it would kill.', NULL, NULL, NULL),
(5, 2, 2, 2, 'Wise men say only fools # in', 'Wise men say only fools rush in', NULL, NULL, NULL),
(6, 2, 2, 2, 'Small things # big days', 'Small things make big days', NULL, NULL, NULL),
(7, 2, 1, 2, 'Lose your #, you # your mind', 'Lose your dream, you lose your mind', NULL, NULL, NULL),
(8, 2, 2, 2, 'People # of guilt, # do have a good time.', 'People incapable of guilt, usually do have a good time.', NULL, NULL, NULL),
(9, 2, 1, 2, 'There is no such thing as #. People just have # memories.', 'There is no such thing as forgiveness. People just have short memories.', NULL, NULL, NULL),
(10, 3, 3, 2, 'Life’s # long enough to get good at one thing. So be # what you get good #.', 'Life’s barely long enough to get good at one thing. So be careful what you get good at.', NULL, NULL, NULL),
(11, 3, 7, 1, 'I did it for me. I # it. I was # at it. And, I was #.', 'I did it for me. I liked it. I was good at it. And, I was alive.', NULL, NULL, NULL),
(12, 3, 4, 2, 'Smile though your heart is #. # even though it\'s breaking', 'Smile though your heart is aching. Smile even though it\'s breaking', NULL, NULL, NULL),
(13, 3, 8, 1, 'I fought, I #. Now I #.', 'I fought, I lost. Now I rest.', NULL, NULL, NULL),
(14, 3, 1, 1, 'Never # what you are. The # of the world will not. Wear it like #, and it can never be used to # you.', 'Never forget what you are. The rest of the world will not. Wear it like armor, and it can never be used to hurt you.', NULL, NULL, NULL),
(15, 3, 3, 1, 'Mama always said life was like a # of #: You # know what you’re gonna get.', 'Mama always said life was like a box of chocolates: You never know what you’re gonna get.', NULL, NULL, NULL),
(16, 4, 83, 1, '# is the capital of Bangladesh.', 'Dhaka is the capital city of Bangladesh.', NULL, NULL, NULL),
(17, 4, 84, 1, 'Washington DC is the capital of #.', 'Washington DC is the capital of USA.', NULL, NULL, NULL),
(18, 4, 5, 1, 'Cox\'s Bazar is the # sea beach in the #', 'Cox\'s Bazar is the largest sea beach in the world.', NULL, NULL, NULL),
(19, 4, 80, 1, '# is situated North America.', 'Canada is situated North America.', NULL, NULL, NULL),
(20, 4, 82, 1, 'Dhaka is situated on the bank of #.', 'Dhaka is situated on the bank of Buriganga.', NULL, NULL, NULL),
(21, 4, 81, 1, 'Auckland is the capital of #.', 'Auckland is the capital of Newzealand.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fill_in_the_blanks_word_ver`
--

CREATE TABLE `fill_in_the_blanks_word_ver` (
  `fill_blanks_word_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `incomplete_word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fix_jumbled_sentence`
--

CREATE TABLE `fix_jumbled_sentence` (
  `fix_jum_sen_id` int(10) UNSIGNED NOT NULL,
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fix_jumbled_sentence`
--

INSERT INTO `fix_jumbled_sentence` (`fix_jum_sen_id`, `sentence_task_id`, `translation_id`, `question`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 16, 83, 'some question', 8, NULL, NULL, NULL),
(2, 16, 5, 'some question', 8, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_match`
--

CREATE TABLE `image_match` (
  `matchword_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_match`
--

INSERT INTO `image_match` (`matchword_id`, `word_task_id`, `dictionary_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 3, 1, NULL, NULL, NULL),
(2, 11, 34, 1, NULL, NULL, NULL),
(3, 11, 35, 1, NULL, NULL, NULL),
(4, 11, 45, 1, NULL, NULL, NULL),
(5, 11, 39, 1, NULL, NULL, NULL),
(6, 11, 59, 1, NULL, NULL, NULL),
(7, 11, 65, 1, NULL, NULL, NULL),
(8, 11, 45, 1, NULL, NULL, NULL),
(9, 11, 3, 1, NULL, NULL, NULL),
(10, 11, 34, 1, NULL, NULL, NULL),
(11, 11, 35, 1, NULL, NULL, NULL),
(12, 11, 45, 1, NULL, NULL, NULL),
(13, 11, 39, 1, NULL, NULL, NULL),
(14, 11, 59, 1, NULL, NULL, NULL),
(15, 11, 65, 1, NULL, NULL, NULL),
(16, 11, 45, 1, NULL, NULL, NULL),
(17, 11, 34, 1, NULL, NULL, NULL),
(18, 11, 35, 1, NULL, NULL, NULL),
(19, 11, 45, 1, NULL, NULL, NULL),
(20, 11, 39, 1, NULL, NULL, NULL),
(21, 11, 59, 1, NULL, NULL, NULL),
(22, 11, 65, 1, NULL, NULL, NULL),
(23, 11, 45, 1, NULL, NULL, NULL),
(50, 12, 34, 1, NULL, NULL, NULL),
(51, 12, 56, 1, NULL, NULL, NULL),
(53, 12, 89, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_match_option`
--

CREATE TABLE `image_match_option` (
  `options_id` int(10) UNSIGNED NOT NULL,
  `matchword_id` int(10) UNSIGNED NOT NULL,
  `option` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_match_option`
--

INSERT INTO `image_match_option` (`options_id`, `matchword_id`, `option`, `created_at`, `updated_at`) VALUES
(1, 50, 32, NULL, NULL),
(2, 50, 43, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(10) UNSIGNED NOT NULL,
  `language_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'C6BqHa4JY8', NULL, NULL, NULL),
(2, 'eWPBR7HKtd', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `level_id` int(10) UNSIGNED NOT NULL,
  `difficulty` int(10) UNSIGNED NOT NULL,
  `score_required` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`level_id`, `difficulty`, `score_required`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 20, NULL, NULL, NULL),
(2, 2, 20, NULL, NULL, NULL),
(3, 2, 20, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loved_topic`
--

CREATE TABLE `loved_topic` (
  `favourite_topic_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loved_topic`
--

INSERT INTO `loved_topic` (`favourite_topic_id`, `topic_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, NULL, NULL, NULL),
(2, 3, 1, NULL, NULL, NULL),
(3, 2, 1, NULL, NULL, NULL),
(4, 2, 1, NULL, NULL, NULL),
(5, 3, 1, NULL, NULL, NULL),
(6, 1, 2, NULL, NULL, NULL),
(7, 1, 2, NULL, NULL, NULL),
(8, 3, 2, NULL, NULL, NULL),
(9, 1, 2, NULL, NULL, NULL),
(10, 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mcq`
--

CREATE TABLE `mcq` (
  `mcq_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `explanation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mcq`
--

INSERT INTO `mcq` (`mcq_id`, `word_task_id`, `explanation`, `user_id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 'Honesty is a noble virtue.', 2, 'Which one is a noble virtue?', 40, NULL, NULL, NULL),
(2, 9, 'China has the largest population in the world having 1,389,618,778 people in total. ', 1, 'Which country has the largest population?', 119, NULL, NULL, NULL),
(3, 9, 'Humans hear with ears.', 1, 'By which organ does a man hear?', 120, NULL, NULL, NULL),
(4, 9, 'Newton discovered gravity. ', 1, 'Who discovered gravity?', 121, NULL, NULL, NULL),
(5, 9, 'A man has two legs.', 2, 'How many legs does a man have?', 122, NULL, NULL, NULL),
(6, 9, 'Einstein invented theory of relativity.', 1, 'Who invented theory of relativity?', 123, NULL, NULL, NULL),
(7, 3, 'Dhaka is the capital of Bangladesh. ', 1, 'What is the capital city of Bangladesh?', 30, NULL, NULL, NULL),
(8, 3, 'Canada is in North America. ', 1, 'In which continent is Canada?', 94, NULL, NULL, NULL),
(9, 3, 'The sun is the only star in our solar system. ', 1, 'What is the name of the star of our solar system?', 47, NULL, NULL, NULL),
(10, 3, 'China has the largest population in the world having 1,389,618,778 people in total. ', 2, 'Which country does have the largest population in the world?', 20, NULL, NULL, NULL),
(11, 1, 'Canada is in North America.', 2, 'In which continent is Canada?', 115, NULL, NULL, NULL),
(12, 1, 'Washington DC is the capital of USA.', 1, 'Which Country has capital named Washington DC?', 110, NULL, NULL, NULL),
(13, 1, 'Cox\'s Bazar is the largest sea beach in the world.', 1, 'Which one below is the largest sea beach?', 114, NULL, NULL, NULL),
(14, 1, 'Dhaka stands on the bank of Buriganga.', 1, 'Which rivers bank Dhaka stand on?', 104, NULL, NULL, NULL),
(15, 1, 'Dhaka is the capital of Bangladesh.', 2, 'What is the name of the capital of Bangladesh?', 81, NULL, NULL, NULL),
(16, 1, 'Auckland is the capital of Newzealand.', 1, 'Which one below is the capital of Newzealand?', 113, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mcq_options`
--

CREATE TABLE `mcq_options` (
  `options_id` int(10) UNSIGNED NOT NULL,
  `mcq_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mcq_options`
--

INSERT INTO `mcq_options` (`options_id`, `mcq_id`, `dictionary_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 110, NULL, NULL, NULL),
(2, 4, 131, NULL, NULL, NULL),
(4, 8, 26, NULL, NULL, NULL),
(5, 4, 121, NULL, NULL, NULL),
(6, 7, 44, NULL, NULL, NULL),
(7, 6, 130, NULL, NULL, NULL),
(8, 1, 126, NULL, NULL, NULL),
(9, 3, 120, NULL, NULL, NULL),
(10, 3, 127, NULL, NULL, NULL),
(11, 5, 122, NULL, NULL, NULL),
(12, 6, 123, NULL, NULL, NULL),
(13, 5, 132, NULL, NULL, NULL),
(15, 10, 72, NULL, NULL, NULL),
(16, 9, 10, NULL, NULL, NULL),
(17, 7, 67, NULL, NULL, NULL),
(19, 1, 125, NULL, NULL, NULL),
(20, 2, 119, NULL, NULL, NULL),
(22, 7, 63, NULL, NULL, NULL),
(24, 4, 123, NULL, NULL, NULL),
(25, 10, 60, NULL, NULL, NULL),
(26, 5, 133, NULL, NULL, NULL),
(27, 2, 107, NULL, NULL, NULL),
(29, 8, 23, NULL, NULL, NULL),
(31, 1, 124, NULL, NULL, NULL),
(32, 6, 121, NULL, NULL, NULL),
(33, 9, 31, NULL, NULL, NULL),
(34, 9, 80, NULL, NULL, NULL),
(35, 4, 130, NULL, NULL, NULL),
(36, 8, 44, NULL, NULL, NULL),
(37, 7, 30, NULL, NULL, NULL),
(38, 2, 105, NULL, NULL, NULL),
(39, 5, 134, NULL, NULL, NULL),
(40, 6, 131, NULL, NULL, NULL),
(42, 8, 94, NULL, NULL, NULL),
(44, 9, 47, NULL, NULL, NULL),
(45, 3, 128, NULL, NULL, NULL),
(46, 1, 40, NULL, NULL, NULL),
(47, 3, 129, NULL, NULL, NULL),
(48, 10, 95, NULL, NULL, NULL),
(49, 10, 20, NULL, NULL, NULL),
(50, 11, 136, NULL, NULL, NULL),
(51, 11, 115, NULL, NULL, NULL),
(52, 11, 137, NULL, NULL, NULL),
(53, 11, 135, NULL, NULL, NULL),
(54, 12, 138, NULL, NULL, NULL),
(55, 12, 105, NULL, NULL, NULL),
(56, 12, 107, NULL, NULL, NULL),
(57, 12, 110, NULL, NULL, NULL),
(58, 13, 139, NULL, NULL, NULL),
(59, 13, 96, NULL, NULL, NULL),
(60, 13, 111, NULL, NULL, NULL),
(61, 13, 117, NULL, NULL, NULL),
(62, 14, 104, NULL, NULL, NULL),
(63, 14, 139, NULL, NULL, NULL),
(64, 14, 96, NULL, NULL, NULL),
(65, 14, 117, NULL, NULL, NULL),
(66, 15, 70, NULL, NULL, NULL),
(67, 15, 109, NULL, NULL, NULL),
(68, 15, 81, NULL, NULL, NULL),
(69, 15, 140, NULL, NULL, NULL),
(70, 16, 70, NULL, NULL, NULL),
(71, 16, 109, NULL, NULL, NULL),
(72, 16, 81, NULL, NULL, NULL),
(73, 16, 140, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `memory_game`
--

CREATE TABLE `memory_game` (
  `memory_game_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `memory_game_options`
--

CREATE TABLE `memory_game_options` (
  `options_id` int(10) UNSIGNED NOT NULL,
  `memory_game_id` int(10) UNSIGNED NOT NULL,
  `options` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_experience', 1),
(2, '2014_10_12_110000_create_users_table', 1),
(3, '2014_10_12_120000_create_password_resets_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(7, '2016_06_01_000004_create_oauth_clients_table', 1),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2018_12_26_200000_bangla_resource', 1),
(10, '2018_12_26_300000_english_resource', 1),
(11, '2018_12_26_310000_language', 1),
(12, '2018_12_26_400000_dictionary', 1),
(13, '2018_12_26_500000_bangla_sentence', 1),
(14, '2018_12_26_600000_english_sentence', 1),
(15, '2018_12_26_700000_task', 1),
(16, '2018_12_26_800000_topic', 1),
(17, '2018_12_26_900000_level', 1),
(18, '2018_12_26_910000_word_task', 1),
(19, '2018_12_26_920000_translation', 1),
(20, '2018_12_26_930000_sentence_task', 1),
(21, '2018_12_26_940000_fill_in_the_blanks', 1),
(22, '2018_12_26_950000_fix_jumbled_sentence', 1),
(23, '2018_12_26_960000_sentence_matching', 1),
(24, '2018_12_26_970000_synonyms_antonyms', 1),
(25, '2018_12_26_980000_user_history', 1),
(26, '2018_12_26_990000_vocabulary_task', 1),
(27, '2019_05_20_104216_profile_picture', 1),
(28, '2019_05_20_104305_user_performance', 1),
(29, '2019_05_20_104339_image_match', 1),
(30, '2019_05_20_104416_word_picture', 1),
(31, '2019_05_20_104525_fill_in_the_blanks_word_ver', 1),
(32, '2019_05_20_104600_mcq', 1),
(33, '2019_05_20_104608_mcq_options', 1),
(34, '2019_05_20_104640_cross_word_puzzle', 1),
(35, '2019_05_20_104645_cross_word_puzzle_info', 1),
(36, '2019_05_20_104747_true_false', 1),
(37, '2019_05_21_155835_feedback', 1),
(38, '2019_06_09_063935_fb_options', 1),
(39, '2019_06_17_043437_loved_topic', 1),
(40, '2019_06_21_180737_fb_answer', 1),
(41, '2019_07_09_014555_image_match_option', 1),
(42, '2019_07_09_020147_memory_game', 1),
(43, '2019_07_09_020219_memory_game_options', 1),
(44, '2019_07_09_131204_word_picture_options', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0870c0e6a0cb25ce50a218e581af9785a4827c9a39f57ded54a50c52f798fc01342eeb5341da56c3', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-10 02:59:07', '2019-07-10 02:59:07', '2020-07-10 08:59:07'),
('0b745eea47eac1d7d106f1562b8b6321113a9b1a4ac43d42782342dfda8cb6a4d011fdf6ed47a65b', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:41:43', '2019-07-09 23:41:43', '2020-07-10 05:41:43'),
('164e339b075d008638e51c9b83c88c2f18f8f28ef8e9700db7016d1dbe78ee9a9f3c3caa55144b4b', 8, 1, 'Personal Access Token', '[]', 0, '2019-07-09 10:27:42', '2019-07-09 10:27:42', '2020-07-09 16:27:42'),
('193c25a9e67b4415a39b7a83a75c3f1f0a87cfb32437d95fe3f37fe90df61230997ffba26910addf', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:50:48', '2019-07-09 23:50:48', '2020-07-10 05:50:48'),
('19b080f3b1cfbf42866dc300e38bc64f64d93e692b2ba5d2894a8ba6d0e570af54c7a0464b3bf9f1', 3, 1, 'Personal Access Token', '[]', 0, '2019-06-24 18:55:12', '2019-06-24 18:55:12', '2020-06-25 06:55:12'),
('214a944c09701d43ac743c88a0e8d216dc5f84c9aad4f6c41e0312437a079c90d5cb2e8bb257b56a', 8, 1, 'Personal Access Token', '[]', 0, '2019-07-09 10:28:05', '2019-07-09 10:28:05', '2020-07-09 16:28:05'),
('2215fe8673ca8b195ff9d49a4be68823e078fe318911952da7b608179ed19e2a52cca288643cbedf', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:56:29', '2019-07-09 23:56:29', '2020-07-10 05:56:29'),
('2948486ec5c806cdde65c84664efc201d332c1aa4e7392877105e6308ad3cce48a448754e68d7484', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:54:48', '2019-07-09 23:54:48', '2020-07-10 05:54:48'),
('346e9347c4384f91a4155b1c46cc3a91341ffcb4eeb41a3908a07832356361d98a4e8505a4c0553f', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:57:41', '2019-07-09 23:57:41', '2020-07-10 05:57:41'),
('41a98de568b798b82732f404faf67edaa8c9a84370aecbe58f09a4dc779d65e0f672b97ec7e2df09', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:24:04', '2019-07-09 23:24:04', '2020-07-10 05:24:04'),
('46a148b415064fc96b15e590bfc42e9f51d371421a523096f3a39ef04878f1a80641b44a47fd9f76', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:22:29', '2019-07-09 23:22:29', '2020-07-10 05:22:29'),
('4b95cc180953ac778d65928c1e048d17130f47a3ec5d6d5d980957b2721e9405e6d84da44efcd58c', 18, 1, 'Personal Access Token', '[]', 0, '2019-07-10 10:19:33', '2019-07-10 10:19:33', '2020-07-10 16:19:33'),
('4da9141070a60bf916a0789bb183ad9d8d37486be26f135f2b8a74b6880eb30a91e76a0ff8eaa080', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:32:36', '2019-07-09 23:32:36', '2020-07-10 05:32:36'),
('4ef97c5c55cc83abc04b1b9c3cc6386577c7d5e64d5fa98314743eb233be2ca202830f486568460e', 6, 1, 'Personal Access Token', '[]', 0, '2019-06-30 03:10:37', '2019-06-30 03:10:37', '2020-06-30 15:10:37'),
('531c12555dc1cd9ec6eab08ec80d83434db7a59e4b2d23bcb699df5310c7a2f2d95be0337eed5bc1', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:55:58', '2019-07-09 23:55:58', '2020-07-10 05:55:58'),
('5ce0120cea501e1206194fa9756c5b42b9dcbaf82082fb0fe157aec11e3a9055f202b2ec7f2965bb', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:25:40', '2019-07-09 23:25:40', '2020-07-10 05:25:40'),
('63c8842176d0841f81fdcafa753189de17a2ef5ccda570ec2fcf0aa16ceaa886e59b8eea8ff29706', 4, 1, 'Personal Access Token', '[]', 0, '2019-06-24 19:11:50', '2019-06-24 19:11:50', '2020-06-25 07:11:50'),
('6c617c1c1896cdc94c96a492d2a2bd253fedd9a85c8c15097dc1bb320b7b514bf21698dbb60a7a46', 18, 1, 'Personal Access Token', '[]', 0, '2019-07-10 10:18:39', '2019-07-10 10:18:39', '2020-07-10 16:18:39'),
('7a0ea3e77cc3aa83c55fc790114fab36f3511e3db02c9f6c48c404b7f527504dc67df05cff5677d6', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:29:18', '2019-07-09 23:29:18', '2020-07-10 05:29:18'),
('880c50b13f04507157973a01715a046849d6cd996803dfa4b3babe8a4b3c305c1c5144ea8777c28e', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-10 10:12:16', '2019-07-10 10:12:16', '2020-07-10 16:12:16'),
('97991fd4a542ae70a7cec327c04c796abe71a64fb7271c06420cc0945f0ad539093525348d6b24d6', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:42:22', '2019-07-09 23:42:22', '2020-07-10 05:42:22'),
('a40eb09c77444745f49c4da42adfd356154c9104003300d5205ea0a70ea80288e6df07c6758b5c4f', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:21:11', '2019-07-09 23:21:11', '2020-07-10 05:21:11'),
('a623dab35bad5b2b9dfbfdf9099c86a4ba5d74ea2bb093899f209705b661724178c3b168233dd088', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-10 03:00:20', '2019-07-10 03:00:20', '2020-07-10 09:00:20'),
('aaac351724f5782f6bb8f0ac4080aba10e007ab7083cc98b123a80a063c0e47a00a2a315ac9a96aa', 6, 1, 'Personal Access Token', '[]', 0, '2019-07-01 16:04:05', '2019-07-01 16:04:05', '2020-07-02 04:04:05'),
('b1af673e3c9ecc17315d180e2cf74a04f885eeffbb018ca219827be3fcd55e6fef273c7e0e95b449', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-10 03:40:34', '2019-07-10 03:40:34', '2020-07-10 09:40:34'),
('b671cf523579564999f3d07cd404f5f620decd8541b209abc10474b74f6dd84192052a70d4fc7c73', 15, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:59:50', '2019-07-09 23:59:50', '2020-07-10 05:59:50'),
('c60038a64f954ff8ae0eb9ef694c2cc61a6f2d50c3dc941c64cd9c5c6a1ecda459ecb569bf4f9c19', 6, 1, 'Personal Access Token', '[]', 0, '2019-06-29 03:15:34', '2019-06-29 03:15:34', '2020-06-29 15:15:34'),
('c6a8a73940a061dcb070443b31a691f5a5cf481d4afd05bbf7977a658a6d0de9fb9582f148af5c60', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-10 02:43:39', '2019-07-10 02:43:39', '2020-07-10 08:43:39'),
('cea102f7835004098dad01ae95340829bab0156e3d4889ff257ad64df7998eb7e5f166d0a74614ab', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:19:45', '2019-07-09 23:19:45', '2020-07-10 05:19:45'),
('cfa67f1b079f9b2fff0eac3b70421882ee0b255880888bf8d034b3ab5b09cf302936c9703f0d3a11', 4, 1, 'Personal Access Token', '[]', 0, '2019-06-24 19:12:27', '2019-06-24 19:12:27', '2020-06-25 07:12:27'),
('d7887ce8a4a0d8d2acc92a007d7ea13e27e5ecc8d998e304fcd9ac453e173914d31c0410b0b4a379', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:43:34', '2019-07-09 23:43:34', '2020-07-10 05:43:34'),
('e2aa788f5e2cb1deb0fe01660a19221fedf063461510bd84a9a9ca614f91d7791f65b0acdfcfbd6d', 6, 1, 'Personal Access Token', '[]', 0, '2019-07-01 16:03:41', '2019-07-01 16:03:41', '2020-07-02 04:03:41'),
('eadf276ad2c89928bb9a8bf52e2c939c3d851c4fa1179543f1dddb362a65de41318d95d08c949c1e', 4, 1, 'Personal Access Token', '[]', 0, '2019-07-07 01:41:33', '2019-07-07 01:41:33', '2020-07-07 13:41:33'),
('ebb80090c6ff4fca43ce037862e3ca8c61700d7d40fed0cd23375dc4bfdfa1909f4e85ac8bebc114', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:16:18', '2019-07-09 23:16:18', '2020-07-10 05:16:18'),
('ec1bbb76fa24015ef06f02dd762701286640c9e64b8e406147dcc87f83402576607daa7ddf1ae51b', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:42:55', '2019-07-09 23:42:55', '2020-07-10 05:42:55'),
('f1e4c2c4c7d70a79282d5c106f3606aa96878bf77c9155d5255fd879d2db568c863d312c3fc775b0', 12, 1, 'Personal Access Token', '[]', 0, '2019-07-09 23:53:28', '2019-07-09 23:53:28', '2020-07-10 05:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'TT6zbIzblZGB12paaKOxwEoqNeW5sREoNDMG4AYK', 'http://localhost', 1, 0, 0, '2019-06-24 18:51:49', '2019-06-24 18:51:49'),
(2, NULL, 'Laravel Password Grant Client', 'sZ2LTEaUi7vDtrSgUtCjwmaFr425ErdymPIqp6pv', 'http://localhost', 0, 1, 0, '2019-06-24 18:51:49', '2019-06-24 18:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-06-24 18:51:49', '2019-06-24 18:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `profile_picture_id` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sentence_matching`
--

CREATE TABLE `sentence_matching` (
  `sen_match_id` int(10) UNSIGNED NOT NULL,
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `broken_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sentence_matching`
--

INSERT INTO `sentence_matching` (`sen_match_id`, `sentence_task_id`, `translation_id`, `broken_sentence`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Lead a life you# can look back at and smile at your death bed', 1, NULL, NULL, NULL),
(2, 6, 2, 'Time is a# great healer', 1, NULL, NULL, NULL),
(3, 6, 3, 'Sundarbans is the# largest mangrove forest', 1, NULL, NULL, NULL),
(4, 11, 53, 'The night is dark# and full of terror', 1, NULL, NULL, NULL),
(5, 11, 67, 'Do not wait, the time will# never be \'just right\'.', 1, NULL, NULL, NULL),
(6, 11, 72, 'It\'s not about the result# it\'s all about the journey.', 1, NULL, NULL, NULL),
(7, 6, 53, 'The night is dark# and full of terror', 1, NULL, NULL, NULL),
(8, 6, 67, 'Do not wait, the time will# never be \'just right\'.', 1, NULL, NULL, NULL),
(9, 6, 72, 'It\'s not about the result# it\'s all about the journey.', 1, NULL, NULL, NULL),
(10, 11, 1, 'Lead a life you# can look back at and smile at your death bed', 1, NULL, NULL, NULL),
(11, 11, 2, 'Time is a# great healer', 1, NULL, NULL, NULL),
(12, 11, 3, 'Sundarbans is the# largest mangrove forest', 1, NULL, NULL, NULL),
(13, 6, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(14, 6, 8, 'I fought,# I lost. Now I rest.', 1, NULL, NULL, NULL),
(15, 6, 12, 'The Sun rises# in the East', 1, NULL, NULL, NULL),
(16, 11, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(17, 11, 8, 'I fought,# I lost. Now I rest.', 1, NULL, NULL, NULL),
(18, 11, 12, 'The Sun rises# in the East', 1, NULL, NULL, NULL),
(30, 1, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(31, 1, 80, 'Canada is situated# in North America.', 1, NULL, NULL, NULL),
(32, 1, 81, 'Auckland is# the capital of Newzealand.', 1, NULL, NULL, NULL),
(33, 1, 82, 'Dhaka is situated on# the bank of Buriganga.', 1, NULL, NULL, NULL),
(34, 1, 83, 'Dhaka is the capital# of Bangladesh.', 1, NULL, NULL, NULL),
(35, 1, 84, 'Washington DC is# the capital of USA.', 1, NULL, NULL, NULL),
(36, 3, 5, 'Cox\'s Bazar is the# largest sea beach in the world', 1, NULL, NULL, NULL),
(37, 3, 80, 'Canada is situated# in North America.', 1, NULL, NULL, NULL),
(38, 3, 81, 'Auckland is# the capital of Newzealand.', 1, NULL, NULL, NULL),
(39, 3, 82, 'Dhaka is situated on# the bank of Buriganga.', 1, NULL, NULL, NULL),
(40, 3, 83, 'Dhaka is the capital# of Bangladesh.', 1, NULL, NULL, NULL),
(41, 3, 84, 'Washington DC is# the capital of USA.', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sentence_task`
--

CREATE TABLE `sentence_task` (
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `experience_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sentence_task`
--

INSERT INTO `sentence_task` (`sentence_task_id`, `task_id`, `topic_id`, `level_id`, `experience_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 1, 1, NULL, NULL, NULL),
(2, 2, 1, 1, 1, NULL, NULL, NULL),
(3, 4, 2, 1, 1, NULL, NULL, NULL),
(4, 2, 2, 1, 1, NULL, NULL, NULL),
(5, 3, 1, 1, 1, NULL, NULL, NULL),
(6, 1, 1, 1, 1, NULL, NULL, NULL),
(7, 1, 1, 2, 1, NULL, NULL, NULL),
(8, 3, 1, 1, 1, NULL, NULL, NULL),
(9, 1, 2, 1, 1, NULL, NULL, NULL),
(10, 3, 1, 1, 1, NULL, NULL, NULL),
(11, 4, 1, 1, 1, NULL, NULL, NULL),
(15, 5, 1, 1, 1, NULL, NULL, NULL),
(16, 9, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `synonyms_antonyms`
--

CREATE TABLE `synonyms_antonyms` (
  `synonym_antonym_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `base_word_id` int(10) UNSIGNED NOT NULL,
  `synonym_word_id` int(10) UNSIGNED NOT NULL,
  `antonym_word_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(10) UNSIGNED NOT NULL,
  `task_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wordOrsentence` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_name`, `wordOrsentence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sentenceMatching', 0, NULL, NULL, NULL),
(2, 'fillintheblanks', 1, NULL, NULL, NULL),
(3, 'truefalse', 1, NULL, NULL, NULL),
(4, 'sentenceMatchingEngToEng', 1, NULL, NULL, NULL),
(5, 'mcq', 1, NULL, NULL, NULL),
(6, 'memorygame', 2, NULL, NULL, NULL),
(7, 'picture_to_word', 1, NULL, NULL, NULL),
(8, 'word_to_picture', 1, NULL, NULL, NULL),
(9, 'fixed_jumbled_sentence', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `topic_id` int(10) UNSIGNED NOT NULL,
  `topic_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topic_id`, `topic_name`, `topic_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'people', 'localhost.com\\oKwSOJFGaj', NULL, NULL, NULL),
(2, 'place', 'localhost.com\\hhwlTGbRmw', NULL, NULL, NULL),
(3, 'month', 'localhost.com\\JOIki1r22G', NULL, NULL, NULL),
(4, 'sun', 'localhost.com\\rp3ESehghi', NULL, NULL, NULL),
(5, 'fuVtxMDY9j', 'localhost.com\\8GiR6DKg0K', NULL, NULL, NULL),
(6, 'seHznonRT5', 'localhost.com\\IEAPjWYhv2', NULL, NULL, NULL),
(7, 'uoV0jHwx26', 'localhost.com\\1Q8ywOVfQL', NULL, NULL, NULL),
(8, 'JLy6uKzdEG', 'localhost.com\\2ExAd4Czuu', NULL, NULL, NULL),
(9, 'qSLUlxdK1S', 'localhost.com\\5K8r7PBNtR', NULL, NULL, NULL),
(10, 'kJPBrwqBl9', 'localhost.com\\1u8skil69F', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE `translation` (
  `translation_id` int(10) UNSIGNED NOT NULL,
  `another_sentence_id` int(10) UNSIGNED NOT NULL,
  `bangla_sent_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`translation_id`, `another_sentence_id`, `bangla_sent_id`, `user_id`, `language_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, 50, 1, 1, NULL, NULL, NULL),
(2, 11, 13, 1, 2, NULL, NULL, NULL),
(3, 31, 30, 1, 1, NULL, NULL, NULL),
(4, 2, 42, 2, 1, NULL, NULL, NULL),
(5, 7, 3, 2, 2, NULL, NULL, NULL),
(6, 9, 5, 2, 1, NULL, NULL, NULL),
(7, 1, 48, 2, 1, NULL, NULL, NULL),
(8, 33, 2, 1, 2, NULL, NULL, NULL),
(9, 34, 14, 2, 2, NULL, NULL, NULL),
(10, 38, 47, 2, 1, NULL, NULL, NULL),
(11, 50, 12, 2, 2, NULL, NULL, NULL),
(12, 17, 41, 1, 1, NULL, NULL, NULL),
(13, 38, 46, 1, 2, NULL, NULL, NULL),
(14, 48, 49, 2, 1, NULL, NULL, NULL),
(15, 31, 16, 2, 1, NULL, NULL, NULL),
(16, 46, 16, 2, 1, NULL, NULL, NULL),
(17, 9, 34, 2, 1, NULL, NULL, NULL),
(18, 46, 30, 2, 2, NULL, NULL, NULL),
(19, 48, 8, 2, 2, NULL, NULL, NULL),
(20, 46, 43, 1, 1, NULL, NULL, NULL),
(21, 46, 42, 2, 1, NULL, NULL, NULL),
(22, 31, 1, 1, 1, NULL, NULL, NULL),
(23, 38, 5, 2, 2, NULL, NULL, NULL),
(24, 23, 31, 1, 2, NULL, NULL, NULL),
(25, 26, 18, 1, 2, NULL, NULL, NULL),
(26, 6, 35, 2, 1, NULL, NULL, NULL),
(27, 29, 49, 1, 1, NULL, NULL, NULL),
(28, 12, 39, 2, 2, NULL, NULL, NULL),
(29, 2, 3, 2, 2, NULL, NULL, NULL),
(30, 14, 22, 1, 2, NULL, NULL, NULL),
(31, 33, 29, 2, 1, NULL, NULL, NULL),
(32, 26, 34, 1, 2, NULL, NULL, NULL),
(33, 22, 37, 1, 1, NULL, NULL, NULL),
(34, 47, 24, 2, 2, NULL, NULL, NULL),
(35, 4, 8, 1, 1, NULL, NULL, NULL),
(36, 22, 17, 1, 2, NULL, NULL, NULL),
(37, 42, 26, 1, 1, NULL, NULL, NULL),
(38, 30, 27, 1, 1, NULL, NULL, NULL),
(39, 41, 45, 2, 1, NULL, NULL, NULL),
(40, 2, 42, 1, 2, NULL, NULL, NULL),
(41, 30, 17, 1, 2, NULL, NULL, NULL),
(42, 9, 32, 2, 2, NULL, NULL, NULL),
(43, 37, 38, 2, 1, NULL, NULL, NULL),
(44, 34, 14, 1, 1, NULL, NULL, NULL),
(45, 37, 36, 2, 2, NULL, NULL, NULL),
(46, 22, 9, 2, 1, NULL, NULL, NULL),
(47, 21, 27, 2, 1, NULL, NULL, NULL),
(48, 35, 45, 1, 2, NULL, NULL, NULL),
(49, 21, 30, 1, 1, NULL, NULL, NULL),
(50, 30, 15, 2, 2, NULL, NULL, NULL),
(51, 27, 2, 2, 2, NULL, NULL, NULL),
(52, 13, 16, 1, 1, NULL, NULL, NULL),
(53, 50, 9, 2, 1, NULL, NULL, NULL),
(54, 33, 26, 2, 2, NULL, NULL, NULL),
(55, 16, 13, 2, 2, NULL, NULL, NULL),
(56, 35, 43, 1, 2, NULL, NULL, NULL),
(57, 13, 49, 1, 1, NULL, NULL, NULL),
(58, 3, 32, 1, 2, NULL, NULL, NULL),
(59, 5, 3, 1, 2, NULL, NULL, NULL),
(60, 26, 19, 1, 1, NULL, NULL, NULL),
(61, 50, 23, 2, 1, NULL, NULL, NULL),
(62, 33, 39, 1, 1, NULL, NULL, NULL),
(63, 9, 2, 1, 1, NULL, NULL, NULL),
(64, 2, 33, 2, 2, NULL, NULL, NULL),
(65, 48, 23, 1, 2, NULL, NULL, NULL),
(66, 47, 36, 2, 1, NULL, NULL, NULL),
(67, 15, 10, 1, 1, NULL, NULL, NULL),
(68, 17, 47, 2, 1, NULL, NULL, NULL),
(69, 8, 49, 1, 2, NULL, NULL, NULL),
(70, 49, 17, 1, 2, NULL, NULL, NULL),
(71, 27, 46, 2, 1, NULL, NULL, NULL),
(72, 32, 15, 1, 1, NULL, NULL, NULL),
(73, 2, 20, 1, 2, NULL, NULL, NULL),
(74, 39, 27, 2, 1, NULL, NULL, NULL),
(75, 21, 43, 2, 1, NULL, NULL, NULL),
(76, 19, 10, 2, 1, NULL, NULL, NULL),
(77, 37, 41, 2, 2, NULL, NULL, NULL),
(78, 1, 46, 1, 1, NULL, NULL, NULL),
(79, 7, 32, 1, 2, NULL, NULL, NULL),
(80, 3, 1, 2, 1, NULL, NULL, NULL),
(81, 4, 4, 1, 1, NULL, NULL, NULL),
(82, 5, 6, 1, 2, NULL, NULL, NULL),
(83, 6, 7, 2, 1, NULL, NULL, NULL),
(84, 8, 8, 2, 2, NULL, NULL, NULL),
(85, 44, 36, 2, 1, NULL, NULL, NULL),
(86, 18, 8, 1, 2, NULL, NULL, NULL),
(87, 10, 29, 1, 2, NULL, NULL, NULL),
(88, 18, 25, 2, 1, NULL, NULL, NULL),
(89, 14, 24, 1, 2, NULL, NULL, NULL),
(90, 22, 33, 1, 1, NULL, NULL, NULL),
(91, 26, 9, 2, 1, NULL, NULL, NULL),
(92, 38, 12, 1, 2, NULL, NULL, NULL),
(93, 21, 39, 2, 1, NULL, NULL, NULL),
(94, 5, 5, 1, 2, NULL, NULL, NULL),
(95, 9, 3, 1, 1, NULL, NULL, NULL),
(96, 5, 22, 1, 1, NULL, NULL, NULL),
(97, 24, 40, 1, 1, NULL, NULL, NULL),
(98, 43, 14, 2, 2, NULL, NULL, NULL),
(99, 3, 14, 2, 1, NULL, NULL, NULL),
(100, 30, 14, 2, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `true_false`
--

CREATE TABLE `true_false` (
  `true_false_id` int(10) UNSIGNED NOT NULL,
  `sentence_task_id` int(10) UNSIGNED NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `explanation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `true_false`
--

INSERT INTO `true_false` (`true_false_id`, `sentence_task_id`, `answer`, `explanation`, `user_id`, `translation_id`, `question`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'True', 'It\'s a constant truth.', 1, 23, 'The Sun rises in the east.', '2019-05-12 12:00:01', '2019-05-23 09:18:36', NULL),
(2, 5, 'False', 'Cows can\'t fly.', 1, 43, 'The cow is flying in the sky.', '2019-05-12 12:00:01', '2019-05-23 09:18:36', NULL),
(3, 5, 'False', 'Bill Gates is the founder of Microsoft.', 2, 57, 'Bill Gates is the founder of Google.', '2019-05-12 12:00:01', '2019-05-23 09:18:36', NULL),
(4, 5, 'True', 'Canada is in North America.', 1, 92, 'Canada is in North America.', NULL, NULL, NULL),
(5, 5, 'False', 'Sundarban is in Bangladesh and India.', 1, 32, 'Sundarban is in China.', NULL, NULL, NULL),
(6, 5, 'True', 'The fastest land animal in the world is the Cheetah.', 1, 32, 'The fastest land animal in the world is the Cheetah.', NULL, NULL, NULL),
(7, 5, 'True', 'Newton discovered gravity.', 2, 1, 'Newton discovered gravity.', NULL, NULL, NULL),
(8, 5, 'False', 'Dhaka is the capital city of Bangladesh.', 1, 3, 'Dhaka is the capital city of India.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privilege` tinyint(1) DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latest_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `age`, `password`, `privilege`, `mobile_number`, `institution`, `present_address`, `latest_image`, `experience_id`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'abc', 'abc@gamil.com', NULL, '12355678', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'aehkbd', 'asdjb@hdkc', 21, 'sfhdbjsn', 1, NULL, NULL, NULL, 'sfhbd', NULL, NULL, NULL, NULL, NULL),
(3, 'messi', 'bca32@gmil.com', NULL, '$2y$10$FLtqhOf..2WoD0Tw4.qW3eGEaJ4LaMT42Ja6ZK46d1Av3A.vhdnsG', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-24 18:55:03', '2019-06-24 18:55:03'),
(4, 'Navid', 'glitchbox29@gmail.com', NULL, '$2y$10$javjTCF4Lm8I/9WyAyi8cuFK8hiKggc0YAhtw8Q7muU3sLWDrTg0K', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-24 19:11:35', '2019-06-24 19:11:35'),
(5, 'Rumman', 'rumman@gmail.com', NULL, '$2y$10$Gv5P8EJl5WO8ADnLsK10Quyloma30/wz.M5HSgfyg3P49Rt.CMxie', 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-25 20:40:28', '2019-06-25 20:40:28'),
(6, 'rumman', 'mann@gmail.com', NULL, '$2y$10$Pf1X8J.WzzVsc4S5fA69Mu.b/mdx6rsdN0T.AnUPZcnNxXZVa8IDW', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-29 03:15:28', '2019-06-29 03:15:28'),
(7, 'montu', 'kokiko@gmail.com', NULL, '$2y$10$noIsCQ99P/e52BEIHdBoX.CcuT0uRwvYlYPH/p2x9FmfSCd8OLfQG', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-05 19:39:03', '2019-07-05 19:39:03'),
(8, 'aapon', 'aapon@gmail.com', NULL, '$2y$10$so2kz2XHNGZCTsU3ZOyOaeyY1dG5k9JsJq0PGs/J5OQ7lBlecRJmK', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 10:27:25', '2019-07-09 10:27:25'),
(9, 'protiki', 'protiki@gmail.com', NULL, '$2y$10$9zYpZBlYBfx//8fIexU6N.TlXGdbWPYQtmgMbC.eyMQ/y0H5U.d6C', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 22:56:41', '2019-07-09 22:56:41'),
(10, 'pranto', 'pranto@gmail.com', NULL, '$2y$10$f.bHh8vDRciMJrKGAhWuYuOpxkMEO8NyE3YQFD4v0tE.47izF0laa', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:04:06', '2019-07-09 23:04:06'),
(11, 'protik', 'protik@gmail.com', NULL, '$2y$10$xKR7O39a.Y/XeltW4lXPSOOy38ocl/RG89VbFWlOzFyCsvzzC4CVO', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:10:58', '2019-07-09 23:10:58'),
(12, 'gg', 'gg@gmail.com', NULL, '$2y$10$XibzeuOo/2UoQxTJoXxLv.eSuKlVJZcGYUMsytY05eauTl/SE9qum', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:15:53', '2019-07-09 23:15:53'),
(13, 'eimama', 'ei@gmail.com', NULL, '$2y$10$P4nvf7SjdO1gh0/kIGkXfeWFscSg0dvvcq0O8OMC2XWpNT81fxVNy', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:31:12', '2019-07-09 23:31:12'),
(14, 'bhog', 'b@gmail.com', NULL, '$2y$10$ZjtOGQwf07a96i3PIg2Sru0cd4qPWQk6eNvvb8gLGvQhDzjicEKPe', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:33:33', '2019-07-09 23:33:33'),
(15, 'navu', 'navu@gmail.com', NULL, '$2y$10$rKr2BRHckNWpG0s3fVsWuurnI82xMeyoBDH5ixchRHM/sJgIY.r4C', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-09 23:59:36', '2019-07-09 23:59:36'),
(16, 'jonsnow', 'jon2@gmail.com', NULL, '$2y$10$oXXmhtqQferOSK/n/89Aguey4URRrH/266/Pgxf0s.8yxWpBvpCU.', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-10 03:09:04', '2019-07-10 03:09:04'),
(17, 'mahmudhera', 'mahmudhera93@gmail.com', NULL, '$2y$10$zvfrScnBuV9LO1utR93kWe1ul1bg6H/qxVMu9RL0Phy31zkVPVPUK', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-10 03:44:26', '2019-07-10 03:44:26'),
(18, 'Aapon007', 'pritomboseaapon@gmail.com', NULL, '$2y$10$QtGAX7gKs6u5B3fc1UXVsOmIVbGnYEp.OyPXS/9KMRQazsrYeMQWa', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-10 10:14:27', '2019-07-10 10:14:27'),
(19, 'abg', 'abg@gmail.com', NULL, '$2y$10$ilMBLi3W.oOMkAERt9MfreW5NFA8S9HJOVd1RWZYG2IDWnOzkNN9y', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-07-11 00:18:15', '2019-07-11 00:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_history`
--

CREATE TABLE `user_history` (
  `userhistory_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `progress` double NOT NULL,
  `upload_time` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_history`
--

INSERT INTO `user_history` (`userhistory_id`, `user_id`, `topic_id`, `task_id`, `level_id`, `progress`, `upload_time`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 3, 2, 70, NULL, NULL, NULL, NULL, NULL),
(2, 1, 3, 1, 3, 23, NULL, NULL, NULL, NULL, NULL),
(3, 2, 3, 2, 3, 53, NULL, NULL, NULL, NULL, NULL),
(4, 2, 3, 1, 2, 15, NULL, NULL, NULL, NULL, NULL),
(5, 3, 3, 1, 1, 40, NULL, NULL, NULL, NULL, NULL),
(6, 1, 3, 2, 1, 73, NULL, NULL, NULL, NULL, NULL),
(7, 4, 1, 4, 1, 15, NULL, NULL, NULL, NULL, NULL),
(8, 2, 3, 2, 1, 31, NULL, NULL, NULL, NULL, NULL),
(9, 1, 2, 5, 3, 10, NULL, NULL, NULL, NULL, NULL),
(10, 2, 2, 2, 3, 72, NULL, NULL, NULL, NULL, NULL),
(11, 1, 1, 5, 1, 98, NULL, NULL, NULL, NULL, NULL),
(12, 1, 2, 3, 2, 30, NULL, NULL, NULL, NULL, NULL),
(13, 1, 2, 3, 1, 18, NULL, NULL, NULL, NULL, NULL),
(14, 1, 3, 2, 1, 99, NULL, NULL, NULL, NULL, NULL),
(15, 1, 1, 3, 1, 28, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_performance`
--

CREATE TABLE `user_performance` (
  `user_performance_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `solved` tinyint(1) NOT NULL,
  `specific_task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vocabulary_task`
--

CREATE TABLE `vocabulary_task` (
  `vocabulary_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `word_picture`
--

CREATE TABLE `word_picture` (
  `word_picture_id` int(10) UNSIGNED NOT NULL,
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `word_picture`
--

INSERT INTO `word_picture` (`word_picture_id`, `word_task_id`, `dictionary_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 20, 34, 1, NULL, NULL, NULL),
(2, 20, 35, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `word_picture_options`
--

CREATE TABLE `word_picture_options` (
  `options_id` int(10) UNSIGNED NOT NULL,
  `word_picture_id` int(10) UNSIGNED NOT NULL,
  `options` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `word_picture_options`
--

INSERT INTO `word_picture_options` (`options_id`, `word_picture_id`, `options`, `created_at`, `updated_at`) VALUES
(1, 1, 34, NULL, NULL),
(2, 1, 32, NULL, NULL),
(3, 2, 35, NULL, NULL),
(4, 2, 45, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `word_task`
--

CREATE TABLE `word_task` (
  `word_task_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `experience_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `word_task`
--

INSERT INTO `word_task` (`word_task_id`, `task_id`, `topic_id`, `level_id`, `experience_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 2, 1, 1, NULL, NULL, NULL),
(2, 3, 1, 2, 1, NULL, NULL, NULL),
(3, 3, 2, 2, 1, NULL, NULL, NULL),
(4, 3, 1, 1, 1, NULL, NULL, NULL),
(5, 2, 2, 1, 1, NULL, NULL, NULL),
(6, 3, 1, 1, 1, NULL, NULL, NULL),
(7, 3, 2, 1, 1, NULL, NULL, NULL),
(8, 1, 1, 1, 1, NULL, NULL, NULL),
(9, 5, 1, 1, 1, NULL, NULL, NULL),
(10, 1, 2, 2, 1, NULL, NULL, NULL),
(11, 6, 1, 1, 1, NULL, NULL, NULL),
(12, 7, 1, 1, 1, NULL, NULL, NULL),
(20, 8, 1, 1, 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bangla_resource`
--
ALTER TABLE `bangla_resource`
  ADD PRIMARY KEY (`bangla_word_id`);

--
-- Indexes for table `bangla_sentence`
--
ALTER TABLE `bangla_sentence`
  ADD PRIMARY KEY (`bangla_sentence_id`);

--
-- Indexes for table `cross_word_puzzle`
--
ALTER TABLE `cross_word_puzzle`
  ADD PRIMARY KEY (`cross_id`),
  ADD KEY `cross_word_puzzle_user_id_foreign` (`user_id`),
  ADD KEY `cross_word_puzzle_sentence_task_id_foreign` (`sentence_task_id`);

--
-- Indexes for table `cross_word_puzzle_info`
--
ALTER TABLE `cross_word_puzzle_info`
  ADD PRIMARY KEY (`info_id`),
  ADD KEY `cross_word_puzzle_info_cross_id_foreign` (`cross_id`),
  ADD KEY `cross_word_puzzle_info_dictionary_id_foreign` (`dictionary_id`);

--
-- Indexes for table `dictionary`
--
ALTER TABLE `dictionary`
  ADD PRIMARY KEY (`dictionary_id`),
  ADD KEY `dictionary_another_word_id_foreign` (`another_word_id`),
  ADD KEY `dictionary_bangla_word_id_foreign` (`bangla_word_id`),
  ADD KEY `dictionary_language_id_foreign` (`language_id`);

--
-- Indexes for table `english_resource`
--
ALTER TABLE `english_resource`
  ADD PRIMARY KEY (`english_word_id`);

--
-- Indexes for table `english_sentence`
--
ALTER TABLE `english_sentence`
  ADD PRIMARY KEY (`english_sentence_id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`experience_id`);

--
-- Indexes for table `fb_answer`
--
ALTER TABLE `fb_answer`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `fb_answer_fill_blanks_id_foreign` (`fill_blanks_id`),
  ADD KEY `fb_answer_answer_foreign` (`answer`);

--
-- Indexes for table `fb_options`
--
ALTER TABLE `fb_options`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `fb_options_fill_blanks_id_foreign` (`fill_blanks_id`),
  ADD KEY `fb_options_options_foreign` (`options`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`),
  ADD KEY `feedback_user_id_foreign` (`user_id`);

--
-- Indexes for table `fill_in_the_blanks`
--
ALTER TABLE `fill_in_the_blanks`
  ADD PRIMARY KEY (`fill_blanks_id`),
  ADD KEY `fill_in_the_blanks_sentence_task_id_foreign` (`sentence_task_id`),
  ADD KEY `fill_in_the_blanks_translation_id_foreign` (`translation_id`),
  ADD KEY `fill_in_the_blanks_user_id_foreign` (`user_id`);

--
-- Indexes for table `fill_in_the_blanks_word_ver`
--
ALTER TABLE `fill_in_the_blanks_word_ver`
  ADD PRIMARY KEY (`fill_blanks_word_id`),
  ADD KEY `fill_in_the_blanks_word_ver_word_task_id_foreign` (`word_task_id`),
  ADD KEY `fill_in_the_blanks_word_ver_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `fill_in_the_blanks_word_ver_user_id_foreign` (`user_id`);

--
-- Indexes for table `fix_jumbled_sentence`
--
ALTER TABLE `fix_jumbled_sentence`
  ADD PRIMARY KEY (`fix_jum_sen_id`),
  ADD KEY `fix_jumbled_sentence_sentence_task_id_foreign` (`sentence_task_id`),
  ADD KEY `fix_jumbled_sentence_translation_id_foreign` (`translation_id`),
  ADD KEY `fix_jumbled_sentence_user_id_foreign` (`user_id`);

--
-- Indexes for table `image_match`
--
ALTER TABLE `image_match`
  ADD PRIMARY KEY (`matchword_id`),
  ADD KEY `image_match_word_task_id_foreign` (`word_task_id`),
  ADD KEY `image_match_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `image_match_user_id_foreign` (`user_id`);

--
-- Indexes for table `image_match_option`
--
ALTER TABLE `image_match_option`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `image_match_option_matchword_id_foreign` (`matchword_id`),
  ADD KEY `image_match_option_option_foreign` (`option`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `loved_topic`
--
ALTER TABLE `loved_topic`
  ADD PRIMARY KEY (`favourite_topic_id`),
  ADD KEY `loved_topic_topic_id_foreign` (`topic_id`),
  ADD KEY `loved_topic_user_id_foreign` (`user_id`);

--
-- Indexes for table `mcq`
--
ALTER TABLE `mcq`
  ADD PRIMARY KEY (`mcq_id`),
  ADD KEY `mcq_word_task_id_foreign` (`word_task_id`),
  ADD KEY `mcq_user_id_foreign` (`user_id`),
  ADD KEY `mcq_answer_foreign` (`answer`);

--
-- Indexes for table `mcq_options`
--
ALTER TABLE `mcq_options`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `mcq_options_mcq_id_foreign` (`mcq_id`),
  ADD KEY `mcq_options_dictionary_id_foreign` (`dictionary_id`);

--
-- Indexes for table `memory_game`
--
ALTER TABLE `memory_game`
  ADD PRIMARY KEY (`memory_game_id`),
  ADD KEY `memory_game_word_task_id_foreign` (`word_task_id`),
  ADD KEY `memory_game_user_id_foreign` (`user_id`);

--
-- Indexes for table `memory_game_options`
--
ALTER TABLE `memory_game_options`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `memory_game_options_memory_game_id_foreign` (`memory_game_id`),
  ADD KEY `memory_game_options_options_foreign` (`options`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`profile_picture_id`),
  ADD KEY `profile_picture_user_id_foreign` (`user_id`);

--
-- Indexes for table `sentence_matching`
--
ALTER TABLE `sentence_matching`
  ADD PRIMARY KEY (`sen_match_id`),
  ADD KEY `sentence_matching_sentence_task_id_foreign` (`sentence_task_id`),
  ADD KEY `sentence_matching_translation_id_foreign` (`translation_id`),
  ADD KEY `sentence_matching_user_id_foreign` (`user_id`);

--
-- Indexes for table `sentence_task`
--
ALTER TABLE `sentence_task`
  ADD PRIMARY KEY (`sentence_task_id`),
  ADD KEY `sentence_task_task_id_foreign` (`task_id`),
  ADD KEY `sentence_task_topic_id_foreign` (`topic_id`),
  ADD KEY `sentence_task_level_id_foreign` (`level_id`),
  ADD KEY `sentence_task_experience_id_foreign` (`experience_id`);

--
-- Indexes for table `synonyms_antonyms`
--
ALTER TABLE `synonyms_antonyms`
  ADD PRIMARY KEY (`synonym_antonym_id`),
  ADD KEY `synonyms_antonyms_word_task_id_foreign` (`word_task_id`),
  ADD KEY `synonyms_antonyms_base_word_id_foreign` (`base_word_id`),
  ADD KEY `synonyms_antonyms_synonym_word_id_foreign` (`synonym_word_id`),
  ADD KEY `synonyms_antonyms_antonym_word_id_foreign` (`antonym_word_id`),
  ADD KEY `synonyms_antonyms_user_id_foreign` (`user_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`translation_id`),
  ADD KEY `translation_another_sentence_id_foreign` (`another_sentence_id`),
  ADD KEY `translation_bangla_sent_id_foreign` (`bangla_sent_id`),
  ADD KEY `translation_user_id_foreign` (`user_id`),
  ADD KEY `translation_language_id_foreign` (`language_id`);

--
-- Indexes for table `true_false`
--
ALTER TABLE `true_false`
  ADD PRIMARY KEY (`true_false_id`),
  ADD KEY `true_false_sentence_task_id_foreign` (`sentence_task_id`),
  ADD KEY `true_false_user_id_foreign` (`user_id`),
  ADD KEY `true_false_translation_id_foreign` (`translation_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_experience_id_foreign` (`experience_id`);

--
-- Indexes for table `user_history`
--
ALTER TABLE `user_history`
  ADD PRIMARY KEY (`userhistory_id`),
  ADD KEY `user_history_user_id_foreign` (`user_id`),
  ADD KEY `user_history_topic_id_foreign` (`topic_id`),
  ADD KEY `user_history_task_id_foreign` (`task_id`),
  ADD KEY `user_history_level_id_foreign` (`level_id`);

--
-- Indexes for table `user_performance`
--
ALTER TABLE `user_performance`
  ADD PRIMARY KEY (`user_performance_id`),
  ADD KEY `user_performance_user_id_foreign` (`user_id`),
  ADD KEY `user_performance_task_id_foreign` (`task_id`),
  ADD KEY `user_performance_topic_id_foreign` (`topic_id`),
  ADD KEY `user_performance_level_id_foreign` (`level_id`);

--
-- Indexes for table `vocabulary_task`
--
ALTER TABLE `vocabulary_task`
  ADD PRIMARY KEY (`vocabulary_id`),
  ADD KEY `vocabulary_task_word_task_id_foreign` (`word_task_id`),
  ADD KEY `vocabulary_task_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `vocabulary_task_user_id_foreign` (`user_id`);

--
-- Indexes for table `word_picture`
--
ALTER TABLE `word_picture`
  ADD PRIMARY KEY (`word_picture_id`),
  ADD KEY `word_picture_word_task_id_foreign` (`word_task_id`),
  ADD KEY `word_picture_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `word_picture_user_id_foreign` (`user_id`);

--
-- Indexes for table `word_picture_options`
--
ALTER TABLE `word_picture_options`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `word_picture_options_word_picture_id_foreign` (`word_picture_id`),
  ADD KEY `word_picture_options_options_foreign` (`options`);

--
-- Indexes for table `word_task`
--
ALTER TABLE `word_task`
  ADD PRIMARY KEY (`word_task_id`),
  ADD KEY `word_task_task_id_foreign` (`task_id`),
  ADD KEY `word_task_topic_id_foreign` (`topic_id`),
  ADD KEY `word_task_level_id_foreign` (`level_id`),
  ADD KEY `word_task_experience_id_foreign` (`experience_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bangla_resource`
--
ALTER TABLE `bangla_resource`
  MODIFY `bangla_word_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `bangla_sentence`
--
ALTER TABLE `bangla_sentence`
  MODIFY `bangla_sentence_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `cross_word_puzzle`
--
ALTER TABLE `cross_word_puzzle`
  MODIFY `cross_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cross_word_puzzle_info`
--
ALTER TABLE `cross_word_puzzle_info`
  MODIFY `info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dictionary`
--
ALTER TABLE `dictionary`
  MODIFY `dictionary_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `english_resource`
--
ALTER TABLE `english_resource`
  MODIFY `english_word_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `english_sentence`
--
ALTER TABLE `english_sentence`
  MODIFY `english_sentence_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `experience_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fb_answer`
--
ALTER TABLE `fb_answer`
  MODIFY `answer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `fb_options`
--
ALTER TABLE `fb_options`
  MODIFY `options_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fill_in_the_blanks`
--
ALTER TABLE `fill_in_the_blanks`
  MODIFY `fill_blanks_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fill_in_the_blanks_word_ver`
--
ALTER TABLE `fill_in_the_blanks_word_ver`
  MODIFY `fill_blanks_word_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fix_jumbled_sentence`
--
ALTER TABLE `fix_jumbled_sentence`
  MODIFY `fix_jum_sen_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `image_match`
--
ALTER TABLE `image_match`
  MODIFY `matchword_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `image_match_option`
--
ALTER TABLE `image_match_option`
  MODIFY `options_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `level_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loved_topic`
--
ALTER TABLE `loved_topic`
  MODIFY `favourite_topic_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mcq`
--
ALTER TABLE `mcq`
  MODIFY `mcq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `mcq_options`
--
ALTER TABLE `mcq_options`
  MODIFY `options_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `memory_game`
--
ALTER TABLE `memory_game`
  MODIFY `memory_game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `memory_game_options`
--
ALTER TABLE `memory_game_options`
  MODIFY `options_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `profile_picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sentence_matching`
--
ALTER TABLE `sentence_matching`
  MODIFY `sen_match_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `sentence_task`
--
ALTER TABLE `sentence_task`
  MODIFY `sentence_task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `synonyms_antonyms`
--
ALTER TABLE `synonyms_antonyms`
  MODIFY `synonym_antonym_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `topic_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
  MODIFY `translation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `true_false`
--
ALTER TABLE `true_false`
  MODIFY `true_false_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_history`
--
ALTER TABLE `user_history`
  MODIFY `userhistory_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_performance`
--
ALTER TABLE `user_performance`
  MODIFY `user_performance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vocabulary_task`
--
ALTER TABLE `vocabulary_task`
  MODIFY `vocabulary_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `word_picture`
--
ALTER TABLE `word_picture`
  MODIFY `word_picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `word_picture_options`
--
ALTER TABLE `word_picture_options`
  MODIFY `options_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `word_task`
--
ALTER TABLE `word_task`
  MODIFY `word_task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cross_word_puzzle`
--
ALTER TABLE `cross_word_puzzle`
  ADD CONSTRAINT `cross_word_puzzle_sentence_task_id_foreign` FOREIGN KEY (`sentence_task_id`) REFERENCES `sentence_task` (`sentence_task_id`),
  ADD CONSTRAINT `cross_word_puzzle_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `cross_word_puzzle_info`
--
ALTER TABLE `cross_word_puzzle_info`
  ADD CONSTRAINT `cross_word_puzzle_info_cross_id_foreign` FOREIGN KEY (`cross_id`) REFERENCES `cross_word_puzzle` (`cross_id`),
  ADD CONSTRAINT `cross_word_puzzle_info_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`);

--
-- Constraints for table `dictionary`
--
ALTER TABLE `dictionary`
  ADD CONSTRAINT `dictionary_another_word_id_foreign` FOREIGN KEY (`another_word_id`) REFERENCES `english_resource` (`english_word_id`),
  ADD CONSTRAINT `dictionary_bangla_word_id_foreign` FOREIGN KEY (`bangla_word_id`) REFERENCES `bangla_resource` (`bangla_word_id`),
  ADD CONSTRAINT `dictionary_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`language_id`);

--
-- Constraints for table `fb_answer`
--
ALTER TABLE `fb_answer`
  ADD CONSTRAINT `fb_answer_answer_foreign` FOREIGN KEY (`answer`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `fb_answer_fill_blanks_id_foreign` FOREIGN KEY (`fill_blanks_id`) REFERENCES `fill_in_the_blanks` (`fill_blanks_id`);

--
-- Constraints for table `fb_options`
--
ALTER TABLE `fb_options`
  ADD CONSTRAINT `fb_options_fill_blanks_id_foreign` FOREIGN KEY (`fill_blanks_id`) REFERENCES `fill_in_the_blanks` (`fill_blanks_id`),
  ADD CONSTRAINT `fb_options_options_foreign` FOREIGN KEY (`options`) REFERENCES `dictionary` (`dictionary_id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `fill_in_the_blanks`
--
ALTER TABLE `fill_in_the_blanks`
  ADD CONSTRAINT `fill_in_the_blanks_sentence_task_id_foreign` FOREIGN KEY (`sentence_task_id`) REFERENCES `sentence_task` (`sentence_task_id`),
  ADD CONSTRAINT `fill_in_the_blanks_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`translation_id`),
  ADD CONSTRAINT `fill_in_the_blanks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `fill_in_the_blanks_word_ver`
--
ALTER TABLE `fill_in_the_blanks_word_ver`
  ADD CONSTRAINT `fill_in_the_blanks_word_ver_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `fill_in_the_blanks_word_ver_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `fill_in_the_blanks_word_ver_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `fix_jumbled_sentence`
--
ALTER TABLE `fix_jumbled_sentence`
  ADD CONSTRAINT `fix_jumbled_sentence_sentence_task_id_foreign` FOREIGN KEY (`sentence_task_id`) REFERENCES `sentence_task` (`sentence_task_id`),
  ADD CONSTRAINT `fix_jumbled_sentence_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`translation_id`),
  ADD CONSTRAINT `fix_jumbled_sentence_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `image_match`
--
ALTER TABLE `image_match`
  ADD CONSTRAINT `image_match_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `image_match_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `image_match_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `image_match_option`
--
ALTER TABLE `image_match_option`
  ADD CONSTRAINT `image_match_option_matchword_id_foreign` FOREIGN KEY (`matchword_id`) REFERENCES `image_match` (`matchword_id`),
  ADD CONSTRAINT `image_match_option_option_foreign` FOREIGN KEY (`option`) REFERENCES `dictionary` (`dictionary_id`);

--
-- Constraints for table `loved_topic`
--
ALTER TABLE `loved_topic`
  ADD CONSTRAINT `loved_topic_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`),
  ADD CONSTRAINT `loved_topic_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `mcq`
--
ALTER TABLE `mcq`
  ADD CONSTRAINT `mcq_answer_foreign` FOREIGN KEY (`answer`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `mcq_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `mcq_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `mcq_options`
--
ALTER TABLE `mcq_options`
  ADD CONSTRAINT `mcq_options_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `mcq_options_mcq_id_foreign` FOREIGN KEY (`mcq_id`) REFERENCES `mcq` (`mcq_id`);

--
-- Constraints for table `memory_game`
--
ALTER TABLE `memory_game`
  ADD CONSTRAINT `memory_game_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `memory_game_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `memory_game_options`
--
ALTER TABLE `memory_game_options`
  ADD CONSTRAINT `memory_game_options_memory_game_id_foreign` FOREIGN KEY (`memory_game_id`) REFERENCES `memory_game` (`memory_game_id`),
  ADD CONSTRAINT `memory_game_options_options_foreign` FOREIGN KEY (`options`) REFERENCES `dictionary` (`dictionary_id`);

--
-- Constraints for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD CONSTRAINT `profile_picture_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `sentence_matching`
--
ALTER TABLE `sentence_matching`
  ADD CONSTRAINT `sentence_matching_sentence_task_id_foreign` FOREIGN KEY (`sentence_task_id`) REFERENCES `sentence_task` (`sentence_task_id`),
  ADD CONSTRAINT `sentence_matching_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`translation_id`),
  ADD CONSTRAINT `sentence_matching_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `sentence_task`
--
ALTER TABLE `sentence_task`
  ADD CONSTRAINT `sentence_task_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`experience_id`),
  ADD CONSTRAINT `sentence_task_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `level` (`level_id`),
  ADD CONSTRAINT `sentence_task_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`),
  ADD CONSTRAINT `sentence_task_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`);

--
-- Constraints for table `synonyms_antonyms`
--
ALTER TABLE `synonyms_antonyms`
  ADD CONSTRAINT `synonyms_antonyms_antonym_word_id_foreign` FOREIGN KEY (`antonym_word_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `synonyms_antonyms_base_word_id_foreign` FOREIGN KEY (`base_word_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `synonyms_antonyms_synonym_word_id_foreign` FOREIGN KEY (`synonym_word_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `synonyms_antonyms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `synonyms_antonyms_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `translation`
--
ALTER TABLE `translation`
  ADD CONSTRAINT `translation_another_sentence_id_foreign` FOREIGN KEY (`another_sentence_id`) REFERENCES `english_sentence` (`english_sentence_id`),
  ADD CONSTRAINT `translation_bangla_sent_id_foreign` FOREIGN KEY (`bangla_sent_id`) REFERENCES `bangla_sentence` (`bangla_sentence_id`),
  ADD CONSTRAINT `translation_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`language_id`),
  ADD CONSTRAINT `translation_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `true_false`
--
ALTER TABLE `true_false`
  ADD CONSTRAINT `true_false_sentence_task_id_foreign` FOREIGN KEY (`sentence_task_id`) REFERENCES `sentence_task` (`sentence_task_id`),
  ADD CONSTRAINT `true_false_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`translation_id`),
  ADD CONSTRAINT `true_false_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`experience_id`);

--
-- Constraints for table `user_history`
--
ALTER TABLE `user_history`
  ADD CONSTRAINT `user_history_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `level` (`level_id`),
  ADD CONSTRAINT `user_history_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`),
  ADD CONSTRAINT `user_history_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`),
  ADD CONSTRAINT `user_history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `user_performance`
--
ALTER TABLE `user_performance`
  ADD CONSTRAINT `user_performance_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `level` (`level_id`),
  ADD CONSTRAINT `user_performance_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`),
  ADD CONSTRAINT `user_performance_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`),
  ADD CONSTRAINT `user_performance_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `vocabulary_task`
--
ALTER TABLE `vocabulary_task`
  ADD CONSTRAINT `vocabulary_task_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `vocabulary_task_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `vocabulary_task_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `word_picture`
--
ALTER TABLE `word_picture`
  ADD CONSTRAINT `word_picture_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `word_picture_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `word_picture_word_task_id_foreign` FOREIGN KEY (`word_task_id`) REFERENCES `word_task` (`word_task_id`);

--
-- Constraints for table `word_picture_options`
--
ALTER TABLE `word_picture_options`
  ADD CONSTRAINT `word_picture_options_options_foreign` FOREIGN KEY (`options`) REFERENCES `dictionary` (`dictionary_id`),
  ADD CONSTRAINT `word_picture_options_word_picture_id_foreign` FOREIGN KEY (`word_picture_id`) REFERENCES `word_picture` (`word_picture_id`);

--
-- Constraints for table `word_task`
--
ALTER TABLE `word_task`
  ADD CONSTRAINT `word_task_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`experience_id`),
  ADD CONSTRAINT `word_task_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `level` (`level_id`),
  ADD CONSTRAINT `word_task_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`),
  ADD CONSTRAINT `word_task_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
